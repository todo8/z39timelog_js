#z39timelog_js 名称:'时间清单'
 时间管理系统
## 线上运行信息
   网站：https://7dtime.com
   小程序：7天时间清单
   ![](http://cdn.7dtime.com/time_tlog/2018-04-05_edda87_1280.jpg?imageView/2/w/200/h/200)

## 系统代码视频简要说明
      <video id="my-video" class="video-js" controls preload="auto" width="100%" poster="http://cdn.7dtime.com/time_tlog/2018-04-05_1598dmjdsm.jpg" data-setup='{"aspectRatio":"16:9"}'>
       <source src="http://cdn.7dtime.com/time_tlog/2018-04-05_1598dmjdsm.mp4" type="video/mp4" >
       <p class="vjs-no-js">
       To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
      </video>
      
## 思维导图简要待办任务
在线地址 https://www.processon.com/view/link/5995577ae4b0b83fa260c3a3 
![](http://cdn.7dtime.com/time_tlog/2018-04-03_z39sjqd0403.png?imageView/2/w/800/h/667)
项目待办任务 http://tlog.7dtime.com/pro52SJRZ/todo_dev.html

## 接口文档
https://7dtime.com/api/

## 数据结构
http://h5.7dtime.com/md/#/z39data

## 目录代码说明
app_z39time 手机版本代码,采用vue,vux技术开发
pc_vue 电脑网页端代码.采用vue,elementui开发
pc_exe 电脑客户端代码.把网页端转换成windows和mac的可安装桌面程序.

sublime 目录是 sublime插件版,主要适用于与开发人员使用文本编辑器使用系统.文本编辑器中利用快捷键创建编辑任务,基本是纯键盘操作.编辑的文件后缀名是tlog,tlog中的任务内容可以直接导入网站系统,系统会智能统计分析数据.

小程序和APP(苹果、安卓) 代码未公开.

## 启动运行&注意
    1.安装依赖包 windows中执行install.bat 或直接运行下面命令
     npm install --registry=https://registry.npm.taobao.org --verbose 
    2.运行启动  run.bat 或 npm run dev
    3.src/api/api.js 文件根据情况修改base变量.本地开发环境服务器可能没启动或不稳定(需要修改其他域名).详情看代码中的注释说明.

## pc_vue 电脑端启动注意
z39timelog_js_os\myPlugins\elementUI 目录里的文件覆盖 z39timelog_js_os\pc_vue\node_modules\element-ui 中的文件 ， 复制，粘贴、覆盖。
```video
<video id="my-video" class="video-js" controls preload="none" width="100%" poster="http://cdn.7dtime.com/time_tlog/2019-03-30_6pc_vuexmqd.jpg" data-setup='{"aspectRatio":"16:9"}'>
 <source src="http://cdn.7dtime.com/time_tlog/2019-03-30_6pc_vuexmqd.mp4" type="video/mp4" >
 <p class="vjs-no-js">
 To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
</video>
```

## app_z39time 启动运行
需要把z39timelog_js_os\vuxExt 文件夹下的文件复制到z39timelog_js_os\app_z39time\node_modules\vux 依赖包中.vuxExt是新开发的插件.详细情况请查看下面视频说明.
```video
<video id="my-video" class="video-js" controls preload="auto" width="100%" poster="http://cdn.7dtime.com/time_tlog/2018-08-04_1460sjdqdsm.jpg" data-setup='{"aspectRatio":"16:9"}'>
 <source src="http://cdn.7dtime.com/time_tlog/2018-08-04_1460sjdqdsm.mp4" type="video/mp4" >
 <p class="vjs-no-js">
 To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
</video>
```
