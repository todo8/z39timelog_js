import ClockComponent from '../../components/clock'
import { mergeOptions } from '../../libs/plugin_helper'

let $vm

const plugin = {
  install(vue, options = {}) {
    const Clock = vue.extend(ClockComponent)

    if (!$vm) {
      $vm = new Clock({
        el: document.createElement('div'),
        propsData: {
          title: ''
        }
      })
      document.body.appendChild($vm.$el)
    }

    const clock = {
      show(options) {
        if (typeof options === 'object') {
          mergeOptions($vm, options)
        }
        if (typeof options === 'object' && (options.onShow || options.onHide)) {
          options.onShow && options.onShow()
        }
        this.$watcher && this.$watcher()
        this.$watcher = $vm.$watch('showValue', (val) => {
          if (!val && options && options.onHide) {
            options.onHide()
          }
        })

        $vm.$off('on-cancel')
        $vm.$off('on-delay')

        $vm.$on('on-cancel', () => {
          options && options.onCancel && options.onCancel()
        })
        $vm.$on('on-delay', msg => {
          options && options.onDelay && options.onDelay(msg)
        })
        $vm.showValue = true
      },
      setInputValue(val) {
        vue.nextTick(() => {
          setTimeout(() => {
            $vm.setInputValue(val)
          }, 10)
        })
      },
      prompt(placeholder, options) {
        this.show(Object.assign({}, options, {
          placeholder,
          showInput: true
        }))
      },
      hide() {
        $vm.showValue = false
      }
    }

    // all Vux's plugins are included in this.$vux
    if (!vue.$vux) {
      vue.$vux = {
        clock
      }
    } else {
      vue.$vux.clock = clock
    }

    vue.mixin({
      created: function() {
        this.$vux = vue.$vux
      }
    })
  }
}

export default plugin
export const install = plugin.install
