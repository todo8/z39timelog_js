if(!/^http(s*):\/\//.test(location.href)){
  alert('请部署到localhost上查看该演示');
}

document.title = document.title + userConfig.username;

/**  Begin URL配置 **/
var apiHost = 'http://d7game.free.idcfengye.com';
window.apiHost = apiHost;
var ImURLConfig = {
  loginUrl: apiHost + '/center/public/loginmob',
  tokenUrl: apiHost + '/apix/z39time/tokenry',
  friendListUrl: apiHost + '/api/z39friend',
  groupListUrl: apiHost + '/api/z43group',
  groupMemberListUrl: apiHost + '/apix/z39time/ryfriend',
  uploadImgUrl: '/layim/server/upload.php', // 正式地址：apiHost + '/apix/z39time/uploadlayim'
  uploadFileUrl: '/layim/server/upload.php', // 正式地址：apiHost + '/apix/z39time/uploadlayim'

  findFriendUrl: apiHost + '/api/z39friend?name=',
  findGroupUrl: apiHost + '/api/z43group?name=',
  reqFriendUrl: apiHost + '/api/z39request',
  reqJoinGroupUrl: apiHost + '/api/z39request',

  msgBoxUrl: apiHost + '/apix/z39time/requestlayim',
  msgBoxRespUrl: apiHost + '/api/z39request' // method=PUT
};
window.ImURLConfig = ImURLConfig;
/** End URL配置 **/

/** Begin 融云代码 **/
//注册自定义消息
function registerMessage(type){
    var messageName = type; // 消息名称。
    var objectName = "s:" + type; // 消息内置名称，请按照此格式命名 *:* 。
    var mesasgeTag = new RongIMLib.MessageTag(true,true); //true true 保存且计数，false false 不保存不计数。
    var propertys = ["avatar","groupid","id", "name", "type", "username", "content"]; // 消息类中的属性名。
    RongIMClient.registerMessageType(messageName, objectName, mesasgeTag, propertys);
}
function startInit(layim, userConfig){
    var params = userConfig;
    params.appKey = 'lmxuhwagl0a8d';
    params.appSecret = 'jDxiZuKnf2lH8';
    //params.token = userConfig.token;
    var userId = "";
    var callbacks = {
        getInstance : function(instance){
            //RongIMLib.RongIMEmoji.init();
            //instance.sendMessage
            registerMessage("FriendMessage");
            registerMessage('GroupMessage');
        },
        getCurrentUser : function(userInfo){
            console.log(userInfo.userId);
            userId = userInfo.userId;
            console.log("链接成功；userid=" + userInfo.userId);
        },
        receiveNewMessage : function(message){
            //判断是否有 @ 自己的消息
            var mentionedInfo = message.content.mentionedInfo || {};
            var ids = mentionedInfo.userIdList || [];
            for(var i=0; i < ids.length; i++){
                if( ids[i] == userId){
                    alert("有人 @ 了你！");
                }
            }
            var msgdata = message.content;
            if (message.senderUserId != userConfig.userId) {
                layim.getMessage({
                  username: msgdata.username
                  ,avatar: msgdata.avatar
                  ,id: message.targetId
                  ,type: msgdata.type
                  ,content: msgdata.content
              });
            }
        }
    };
    init(params,callbacks);
}
/** End 融云代码 **/

layui.use(['jquery', 'layim'], function(){
  var $ = layui.jquery;
  var layim = layui.layim;
  window.layim = layim;
  var uinfo = {}; // 用户信息
  var friends = [];
  var groups = [];
  var cookie = 'thinkjs=cd4fbc1d-e74f-4da3-bacc-248019c08173';
  var host = apiHost; //http://d7game.free.ngrok.cc'; //'http://7dtime.tunnel.echomod.cn';
  window.apihost = host;
  var lastUnread = 0;
  $.ajaxSetup({
    xhrFields: {
      withCredentials: true
   }
  });
    // Step 1. 登录
  $.post(ImURLConfig.loginUrl, {username: userConfig.username, password: userConfig.password}, function(data, status, xhr){
    console.log('登录结果:', data);
    var udata = data.data;
    if (data.errno == 0) {
      // Step 2. token
      $.get(ImURLConfig.tokenUrl, function(data){
        var udata = data.data;
        console.log("token 结果：", data);
        if (!udata.tokenRy) {
          layer.alert('登录失败，请联系管理员');
          //return;
        }
        uinfo = {
          userId: udata.uid,
          username: udata.username,
          headimg: udata.headimgurl || udata.headimg,
          token: udata.tokenRy
        };
        console.log("uinfo", uinfo);
          
        // 消息盒子-未读消息数
        if (udata.msgnum) {
          setTimeout(function(){
            lastUnread = udata.msgnum;
            layim.msgbox(lastUnread);
            /**setInterval(function(){
              $.get(ImURLConfig.msgBoxUrl, {
                  page: page || 1
                }, function(res){
                  console.log(res);
                  if (typeof res == 'string') {
                    res = JSON.parse(res);
                  }
                  if(res.code == 0){
                    var unread = count(data.data);
                    if (unread > lastUnread) {
                      layim.msgbox(unread-lastUnread);
                      lastUnread = unread;
                    }
                  }
              });
            }, 3000);**/
          }, 3000);
        }

        window.uinfo = uinfo;
        $.ajaxSetup({
          headers: {
            Cookie: 'thinkjs=' + udata.thinkjs,
          }
        });
        window.cookiejs = 'thinkjs=' + udata.thinkjs;

        // Step 2: 初始化融云
        startInit(layim, uinfo);
        // Step 3: 获取好友列表
        $.get(ImURLConfig.friendListUrl, function(data, status){
          console.log('好友列表:', data);
          if (data.errno == 0) {
            // 获取好友列表成功
            // TODO: 添加好友
            var defaultFGroup = {
              groupname: '默认分组',
              id: 1,
              online: 0,
              list: []
            };
            var friendList = [];
            var fdata = data.data;
            for(var i=0;i<fdata.length;i++) {
              friendList.push({
                id: fdata[i].fuid,
                username: fdata[i].nname || "",
                avatar: fdata[i].headimgurl || fdata[i].headimg || "",
                sign: fdata[i].sign || ""
              });
            }
            defaultFGroup.list = friendList;
            friends.push(defaultFGroup);
            // End 添加好友
            // Step 4: 获取群组列表
            $.get(ImURLConfig.groupListUrl, function(data, status){
              console.log('群组列表:', data);
              if (data.errno == 0) {
                // 获取群组成功
                // TODO: 添加群组
                var gdata = data.data;
                for(var i=0; i<gdata.length; i++) {
                  groups.push({
                    id: gdata[i].gid,
                    groupname: gdata[i].title || '默认群组',
                    avatar: ''
                  });
                }
                // End 添加群组
                // Begin 初始化layim
                layim.config({
                  init: {
                    mine: {
                      username: uinfo.username,
                      id: uinfo.userId,
                      status: "online",
                      remark: "我是" + uinfo.username,
                      avatar: uinfo.headimg
                    },
                    friend: friends,
                    group: groups,
                  },
                  members: {
                    url: ImURLConfig.groupMemberListUrl, // 群组成员列表
                    data:{}
                  }
                  //上传图片接口
                  ,uploadImage: {
                    url: ImURLConfig.uploadImgUrl
                    ,type: '' //默认post
                  }
                  
                  //上传文件接口
                  ,uploadFile: {
                    url: ImURLConfig.uploadFileUrl
                    ,type: '' //默认post
                  }
                  
                  ,isAudio: true //开启聊天工具栏音频
                  ,isVideo: true //开启聊天工具栏视频
                  
                  //扩展工具栏
                  ,tool: [{
                    alias: 'code'
                    ,title: '代码'
                    ,icon: '&#xe64e;'
                  }]
                  //,brief: true //是否简约模式（若开启则不显示主面板）
                  //,title: 'WebIM' //自定义主面板最小化时的标题
                  //,right: '100px' //主面板相对浏览器右侧距离
                  //,minRight: '90px' //聊天面板最小化时相对浏览器右侧距离
                  ,initSkin: '5.jpg' //1-5 设置初始背景
                  //,skin: ['aaa.jpg'] //新增皮肤
                  //,isfriend: false //是否开启好友
                  //,isgroup: false //是否开启群组
                  //,min: true //是否始终最小化主面板，默认false
                  ,notice: true //是否开启桌面消息提醒，默认false
                  //,voice: false //声音提醒，默认开启，声音文件为：default.mp3
                  
                  ,msgbox: layui.cache.dir + 'css/modules/layim/html/msgbox.html' //消息盒子页面地址，若不开启，剔除该项即可
                  ,find: layui.cache.dir + 'css/modules/layim/html/find.html' //发现页面地址，若不开启，剔除该项即可
                  ,chatLog: layui.cache.dir + 'css/modules/layim/html/chatLog.html' //聊天记录页面地址，若不开启，剔除该项即可
                });
                // End 初始化layim

                //监听在线状态的切换事件
                layim.on('online', function(data){
                  //console.log(data);
                });

                //监听签名修改
                layim.on('sign', function(value){
                  //console.log(value);
                });

                //监听自定义工具栏点击，以添加代码为例
                layim.on('tool(code)', function(insert){
                  layer.prompt({
                    title: '插入代码'
                    ,formType: 2
                    ,shade: 0
                  }, function(text, index){
                    layer.close(index);
                    insert('[pre class=layui-code]' + text + '[/pre]'); //将内容插入到编辑器
                  });
                });

                //监听layim建立就绪
                layim.on('ready', function(res){
                  console.log('layim ready');
                });

                //监听发送消息
                layim.on('sendMessage', function(data){
                  var To = data.to;
                  console.log(data);
                  if(To.type === 'friend'){
                    //layim.setChatStatus('<span style="color:#FF5722;">对方正在输入。。。</span>');
                    var msg = new RongIMLib.RongIMClient.RegisterMessage.FriendMessage({
                      avatar: data.mine.avatar,
                      groupid: To.groupid || '101',
                      id: uinfo.userId,
                      username: uinfo.username,
                      name: To.name,
                      type: 'friend',
                      content: data.mine.content
                    });
                    console.log("发送消息:", msg);
                    RongIMLib.RongIMClient.getInstance().sendMessage(RongIMLib.ConversationType.PRIVATE,To.id.toString(), msg, {
                        onSuccess: function (message) {
                          console.log(data.mine.content, " 发送成功");
                        },
                        onError: function (errorCode) {
                          console.log(data.mine.content, " 发送失败");
                        }
                    });
                  } else if (To.type === 'group') {
                    var msg = new RongIMLib.RongIMClient.RegisterMessage.GroupMessage({
                      avatar: data.mine.avatar,
                      groupid: To.id,
                      id: uinfo.userId,
                      username: uinfo.username,
                      name: To.name,
                      type: To.type,
                      content: data.mine.content
                    });
                    RongIMLib.RongIMClient.getInstance().sendMessage(RongIMLib.ConversationType.GROUP,To.id.toString(), msg, {
                        onSuccess: function (message) {
                          console.log(data.mine.content, " 群消息发送成功");
                        },
                        onError: function (errorCode) {
                          console.log(data.mine.content, " 群消息发送失败");
                          console.log(errorCode);
                        }
                    });
                  } else {
                    console.log("other message");
                  }
                });

                //监听查看群员
                layim.on('members', function(data){
                  console.log('view group members:', data);
                });
                // End
                
              } else {
                layer.alert('获取群组列表失败:' + data.errmsg);
                console.log('获取群组列表失败:errmsg=' + data.errmsg);
              }
            });
          } else {
            layer.alert('获取好友列表失败：' + data.errmsg);
            console.log('获取好友列表失败：errmsg=' + data.errmsg);
          }
        });
      });
    } else {
      layer.alert('登录失败: ' + data.errmsg);
      console.log('登录失败: errmsg=' + data.errmsg);
    }
  });
});