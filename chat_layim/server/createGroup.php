<?php

header('Content-Type: application/json;charset=utf8');

/**
    userId=101
    name = user101

    userId=102
    name=user102

    userId=103
    name=user103
**/

$host = 'http://api.cn.ronghub.com';
$apiUri = '/group/create.json';
$portraitUri = 'http://wx.qlogo.cn/mmopen/9M8Q7cBIckENspibT7SRMdmVgMoytEI0XgF8YiaUrqvWbnZ0rWkQ6a7zkep33chxibzJPH2LRFTL7icVUEltbEDpKoWM7S7Z6ib47/0';

$userId = isset($_GET['userId']) ? $_GET['userId'] : '';
$groupId = isset($_GET['groupId']) ? $_GET['groupId'] : '';
$groupName = isset($_GET['groupName']) ? $_GET['groupName'] : '';
if (empty($userId) || empty($groupName) || empty($groupId)) {
    echo json_encode(array(
        'code' => -1,
        'userId' => '',
        'token' => ''
    ));
    exit();
}

$appKey = 'lmxuhwagl0a8d'; //'mgb7ka1nmwprg';
$appSecret = 'jDxiZuKnf2lH8'; //'TEWsJnRmgFtOB';

srand((double)microtime()*1000000);
$nonce = rand();
$timestamp = time();
$signature = sha1($appSecret . $nonce . $timestamp);

$headers = array(
    'App-Key:' . $appKey,
    'Nonce:' . $nonce,
    'Timestamp:'. $timestamp,
    'Signature:' . $signature,
);

//print_r($headers);


$postData = http_build_query(
    array(
        'userId' => $userId,
        'groupId' => $groupId,
        'groupName' => $groupName
    )
);


$url = $host . $apiUri;
$ch = curl_init(); 
curl_setopt($ch, CURLOPT_URL, $url);  
curl_setopt($ch, CURLOPT_HEADER, false);  
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  
curl_setopt($ch, CURLOPT_TIMEOUT, 30); //30秒超时  
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);  
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

curl_setopt($ch, CURLOPT_POST, 1);  
curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);  

$data = curl_exec($ch);

curl_close($ch);

echo $data;
