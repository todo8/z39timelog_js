<?php

$img = $_FILES['file'];

if ($img) {
    $path = 'upload/' . date('YmdHis') . $img['name'];
    move_uploaded_file($img['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . '/layim/upload/' . date('YmdHis') . $img['name']);
    echo json_encode([
        "code" => 0,
        'msg' => "上传成功",
        "data" => [
            "src" => "http://localhost/layim/" . $path,
            "name" => date('YmdHis') . $img['name']
        ]
    ]);
} else {
    echo json_encode([
        "code" => 1,
        'msg' => "上传失败",
        "data" => [
            "src" => "",
            "name" => ""
        ]
    ]);
}