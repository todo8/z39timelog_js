<?php

header('Content-Type: application/json;charset=utf8');

$returnData = array(
    'errno' => 0,
    'errmsg' => "",
    'data' => []
);

$friends = [];
$friend1 = array(
    'uid' => 101,
    'gid' => 1,
    'gname' => '好友分组1',
    'fuid' => 1,
    'nname' => 'user101',
    'headimgurl' => "static/images/headimg/1.jpg"
);
$friend2 = array(
    'uid' => 102,
    'gid' => 2,
    'gname' => '好友分组2',
    'fuid' => 2,
    'nname' => 'user102',
    'headimgurl' => "static/images/headimg/2.jpg"
);
$friend3 = array(
    'uid' => 103,
    'gid' => 3,
    'gname' => '好友分组3',
    'fuid' => 3,
    'nname' => 'user103',
    'headimgurl' => "static/images/headimg/3.jpg"
);

$userId = $_GET['userId'];
if ($userId == '101') {
    $friends[] = $friend2;
    $friends[] = $friend3;
} else if ($userId == '102') {
    $friends[] = $friend1;
    $friends[] = $friend3;
} else if ($userId == '103') {
    $friends[] = $friend1;
    $friends[] = $friend2;
}

$returnData['data'] = $friends;

echo json_encode($returnData);