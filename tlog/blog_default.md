# 默认说明文档

#### 任务1(已完成)
任务描述，视频教程.

#### 任务2
图片和动态图，

#### 思维导图
思维导字和思维导图，

#### markdown格式
- 支持“标准”Markdown和Github风格的语法
- 支持实时预览、图片（跨域）上传、预格式文本/代码/表格插入、代码折叠、搜索替换.
![](https://img.shields.io/github/tag/pandao/editor.md.svg) ![](https://img.shields.io/github/release/pandao/editor.md.svg) 
缩进:四个空格
    <?php
        echo "Hello 时间清单!";
    ?>对
表格：

|   服务      |   价值   |
| ------------- | ------------- |
| 时间管理  | 提高工作效率  |
| 项目管理  | 团队协同工作  |


- [x] 已完成的任务
- [ ] 文档分享