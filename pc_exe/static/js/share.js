var wxjson = { appId: "", timestamp: "", nonceStr: "", signature: "" };

$(function() {
    getjssdk();
})

function isWeixin() {
    var ua = navigator.userAgent.toLowerCase();
    if (ua.match(/MicroMessenger/i) == "micromessenger") {
        return true;
    } else {
        return false;
    }
}

function getjssdk() {
    if (location.href.indexOf("192.168") > -1 || location.href.indexOf("127.0.0") > -1 || !isWeixin()) { //判断是本地或非微信,不需要微信分享.
        return;
    }
    $.post('//thinkido.tunnel.qydev.com/api/z43phone/jssdk', { url: location.href }, function(data) {
        // $.post('//h.d7game.com/api/z43phone/jssdk', { url: location.href }, function(data) {
        wxjson = data.data;
        wxjson.debug = false;
        wxjson.jsApiList = ['onMenuShareTimeline', 'onMenuShareAppMessage', 'chooseImage', 'uploadImage'];
        wx.config(wxjson);
        // console.info(wxjson);
        // doWxShare();
        wx.ready(function() {
            doWxShare();
            // 在这里调用 API
        });
    });
}

var sharesuccessfunc;
var sharefuncobj;
var shareData = {
    title: "知达电话名片-录音电话",
    content: "一秒种添加电话号码",
    sharepng: "http://www.yqhdw.com/logo.jpg",
    link: 'http://d7game.tunnel.qydev.com/z43phone'
};

function doWxShare() {
    try {
        setTimeLine(null);
        setAppMessage(null);
    } catch (e) {}
}
//分享的具体信息，请自行添加修改。
function setTimeLine(data) {

    data = data || shareData;
    wx.onMenuShareTimeline({
        title: data.title, // 分享标题
        desc: data.content, // 分享描述
        link: data.link, // 分享链接
        imgUrl: data.sharepng, // 分享图标
        success: function() {
            // 用户确认分享后执行的回调函数
            if (sharesuccessfunc != null)
                sharesuccessfunc.apply(sharefuncobj, []);
        },
        cancel: function() {
            // 用户取消分享后执行的回调函数

        }
    });
}

function setAppMessage(data) {
    data = data || shareData;

    wx.onMenuShareAppMessage({
        title: data.title, // 分享标题
        desc: data.content, // 分享描述
        link: data.link, // 分享链接
        imgUrl: data.sharepng, // 分享图标
        type: '', // 分享类型,music、video或link，不填默认为link
        dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
        success: function() {
            if (sharesuccessfunc != null)
                sharesuccessfunc.apply(sharefuncobj, []);
        },
        cancel: function() {}
    });
}
