// http://efe.baidu.com/blog/position-sticky/

// 检测iOS版本大于等于6
function gtIOS6() {
  var userAgent = window.navigator.userAgent
  var ios = userAgent.match(/(iPad|iPhone|iPod)\s+OS\s([\d_.]+)/)
  return ios && ios[2] && (parseInt(ios[2].replace(/_/g, '.'), 10) >= 6)
}

// 判断是否支持sticky属性
function isSupportSticky() {
  var prefixTestList = ['', '-webkit-', '-ms-', '-moz-', '-o-']
  var stickyText = ''
  for (var i = 0; i < prefixTestList.length; i++) {
    stickyText += 'position:' + prefixTestList[i] + 'sticky'
  }
  // 创建一个dom来检查
  var div = document.createElement('div')
  var body = document.body
  div.style.cssText = 'display:none' + stickyText
  body.appendChild(div)
  var isSupport = /sticky/i.test(window.getComputedStyle(div).position)
  body.removeChild(div)
  div = null
  return isSupport
}
let sortlist;
export default function(nav, options = {}, _this) {
  // console.log("nav ", nav);
  sortlist = _this;
  let scrollBox = options.scrollBox || window
  let offset = options.offset || 0
  const checkStickySupport = options.checkStickySupport === true || false
  if (typeof scrollBox === 'string') {
    scrollBox = document.getElementById(scrollBox)
  }

  let navOffsetY = nav.offsetTop - offset
  scrollBox.removeEventListener('scroll', scrollBox.e)

  const getTop = function() {
    if (scrollBox === window) {
      return document.documentElement.scrollTop
    } else {
      return scrollBox.scrollTop
    }
  }
  let lastDis = 0;
  const scrollHandler = function() {
    const distance = getTop()
    if (distance >= navOffsetY && distance != 0) {
      nav.style.top = offset + 'px'
      sortlist.listshow = true;
      if (lastDis < distance && !sortlist.stateA) {
        sortlist.stateA = true;
      }
      // nav.classList.add('vux-fixed')
    } else {
      sortlist.listshow = false;
      sortlist.stateA = true;
      // nav.classList.remove('vux-fixed')
    }
    if (distance == 0) {
      sortlist.listshow = false;
      sortlist.stateA = true;
    }
    lastDis = distance;
    // console.log("distance", distance, navOffsetY, distance >= navOffsetY && distance != 0);
  }
  // console.log("check ", checkStickySupport, gtIOS6(), isSupportSticky());
  if (checkStickySupport && (gtIOS6() || isSupportSticky())) {
    // 大于等于iOS6版本使用sticky
    nav.classList.add('vux-sticky')
  } else {
    navOffsetY = nav.offsetTop - offset
    scrollBox.e = scrollHandler
    scrollBox.addEventListener('scroll', scrollHandler)
    // console.log("addEventListener ", scrollBox, getTop());
  }
}
