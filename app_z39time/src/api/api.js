import axios from 'axios';
import wrapper from 'axios-cache-plugin'

var isMobile = true; // h5 app 小程序 都使用token
export var token = localStorage.getItem('token') || '';
var uid = 0;
console.log("api token:", token);
import store from '../store/store.js';

var base;
if (process.env.NODE_ENV == "development") { //开发环境
  // base = 'http://192.168.0.104:8361';
  // base = 'http://d7game.free.idcfengye.com'; //备选1
  // base = 'http://thinkido.tunnel.qydev.com'
  // base = 'http://7dtime.tunnel.echomod.cn'
  var href = window.location.href;
  if (href.indexOf("192.") != -1) {
    base = href.substr(0, href.indexOf(":80")) + ":8361";
    console.log("api:", base);
  }
  // base = 'http://thinkido.tunnel.qydev.com'
  // base = 'https://7dtime.com'
} else {
  base = 'https://7dtime.com' //发布环境
  //  base = 'http://d7game.free.ngrok.cc'
}
axios.defaults.withCredentials = true;
axios.defaults.timeout = 6000; //http默认超时时间是60秒
//在main.js设置全局的请求次数，请求的间隙
axios.defaults.retry = 3;
axios.defaults.retryDelay = 500;
// Add a request interceptor
axios.interceptors.request.use(function(config) {
  // Do something before request is sent
  if ((config.params && !config.params.ignore) || (config.data && !config.data.ignore)) store.commit('updateLoadingStatus', { isLoading: true, text: "请求中" });
  return config;
}, function(error) {
  // Do something with request error
  return Promise.reject(error);
});
axios.interceptors.response.use(function(response) {
  store.commit('updateLoadingStatus', { isLoading: false });
  return response;
}, function axiosRetryInterceptor(err) {
  var config = err.config;
  // If config does not exist or the retry option is not set, reject
  if (!config || !config.retry) {
    store.commit('updateLoadingStatus', { isLoading: false });
    return Promise.resolve({ errno: 1002, errmsg: "网络超时" }); //Promise.reject(err);
  }
  // Set the variable for keeping track of the retry count
  config.__retryCount = config.__retryCount || 0;
  // Check if we've maxed out the total number of retries
  if (config.__retryCount >= config.retry) {
    // Reject with the error
    store.commit('updateLoadingStatus', { isLoading: false });
    return Promise.resolve({ errno: 1002, errmsg: "网络超时" }); //Promise.reject(err);
  }
  // Increase the retry count
  config.__retryCount += 1;
  // Create new promise to handle exponential backoff
  var backoff = new Promise(function(resolve) {
    setTimeout(function() {
      resolve();
    }, config.retryDelay || 1);
  });
  // Return the promise in which recalls axios to retry the request
  return backoff.then(function() {
    return axios(config);
  });
});

let http = wrapper(axios, {
  maxCacheSize: 15
})
http.__addFilter(/api\/z39/)
http.__addFilter(/apix\/z39/)
export const globalUrl = base;
//---------------系统通用接口---------------
// token登录
export const loginToken = params => {
  return axios.post(`${ base }/center/public/logintoken`, params).then(res => {
    let data = res.data;
    if (data.data && data.data.uid) {
      token = params.token;
      uid = data.data.uid;
      if (params.token != data.data.token && data.data.token) {
        token = data.data.token;
      }
      localStorage.token = token;
      // console.log("token:", token);
    }
    if (!data.errno) this.checkAppRegist();
    return Promise.resolve(data);
  });
};
// 登录
export const login = params => {
  return axios.post(`${ base }/center/public/loginmob`, params).then(res => {
    let data = res.data;
    if (data.data && data.data.token) {
      token = data.data.token;
      uid = data.data.uid;
      localStorage.token = token;
      // console.log("token:", token);
    }
    if (!data.errno) this.checkAppRegist();
    return Promise.resolve(data);
  });
};
// app登录,调用微信登录,QQ登录.
export const loginApp = params => {
  return axios.post(`${ base }/apix/7dtime/loginapp`, params).then(res => {
    let data = res.data;
    if (data.data && data.data.token) {
      token = data.data.token;
      uid = data.data.uid;
      localStorage.token = token;
      // console.log("token:", token);
    }
    if (!data.errno) this.checkAppRegist();
    return Promise.resolve(data);
  });
};

// 登录-手机验证码登录,可能未注册
export const loginCode = params => {
  return axios.post(`${ base }/center/public/logincode`, params).then(res => {
    let data = res.data;
    if (data.data && data.data.token) {
      token = data.data.token;
      uid = data.data.uid;
      localStorage.token = token;
      // console.log("token:", token);
    }
    if (!data.errno) this.checkAppRegist();
    return Promise.resolve(data);
  });
};
// 退出登录
export const logout = params => {
  return axios.post(`${ base }/center/public/logoutmob`, params).then(res => {
    token = "";
    uid = 0;
    return Promise.resolve(res.data);
  });
};

// 检查手机号是否注册, 没注册直接显示注册码
export const checkUser = params => {
  return axios.post(`${ base }/center/public/checkuser`, params).then(res => res.data);
};

// 游客注册接口,方便不用注册就可以进行浏览使用.
export const guestRegist = params => {
  return axios.post(`${ base }/apix/7dtime/guestregist`, params).then(res => {
    let data = res.data;
    if (data.data && data.data.token) {
      token = data.data.token;
      uid = data.data.uid;
      localStorage.setItem('guestToken', token);
    }
    if (!data.errno) this.checkAppRegist();
    return Promise.resolve(data);
  });
};

// 注册
export const reg = params => {
  return axios.post(`${ base }/center/weixin/organizing`, params).then(res => {
    let data = res.data;
    if (data.data && data.data.token) {
      token = data.data.token;
      uid = data.data.uid;
      localStorage.token = token;
    }
    if (!data.errno) this.checkAppRegist();
    return Promise.resolve(data);
  });
  // return axios.post(`${ base }/center/weixin/organizing`, params).then(res => res.data);
};
// 获取手机注册验证码
export const mobCode = params => {
  return axios.post(`${ base }/ext/dayu/index/verifycodesend`, params).then(res => res.data);
};

export const smsBind = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/ext/dayu/index/smsbind`, params).then(res => res.data);
};
// 修改密码
export const updatepwd = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/center/seting/updatepassword`, params).then(res => res.data);
};
// 通过手机验证码修改密码
export const updatePwdMob = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/ext/dayu/index/smsupdatepwd`, params).then(res => res.data);
};


export const jssdk = params => {
  return axios.post(`${ base }/apix/7dtime/jssdk`, params).then(res => res.data);
};
export const authurl = params => {
  return axios.post(`${ base }/apix/7dtime/authurl`, params).then(res => res.data);
};
//  ------------------------- 建议 rest相关处理 -------------------------
export const suggestGet = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/api/d7suggest`, { "params": params }).then(res => res.data);
};
export const suggestAdd = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/api/d7suggest`, params).then(res => res.data);
};
export const suggestSet = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.put(`${ base }/api/d7suggest`, params).then(res => res.data);
};
export const suggestDel = params => {
  return axios.delete(`${ base }/api/d7suggest`, { "params": params }).then(res => res.data);
};
//  ------------------------- 合作 rest相关处理 -------------------------
export const cooperateGet = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/api/d7cooperate`, { "params": params }).then(res => res.data);
};
export const cooperateAdd = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/api/d7cooperate`, params).then(res => res.data);
};
export const cooperateSet = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.put(`${ base }/api/d7cooperate`, params).then(res => res.data);
};
export const cooperateDel = params => {
  return axios.delete(`${ base }/api/d7cooperate`, { "params": params }).then(res => res.data);
};
//---------------系统通用接口 结束---------------

export const info = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/apix/z43phone/info`, params).then(res => res.data);
};
export const phones = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/apix/z43phone/phones`, params).then(res => res.data);
};
export const call = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/apix/z43phone/call`, params).then(res => res.data);
};
export const callme = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/apix/z43phone/callme`, params).then(res => res.data);
};
export const getmp3 = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/apix/z43phone/getmp3`, params).then(res => res.data);
};
export const delinfo = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/apix/z43phone/delinfo`, params).then(res => res.data);
};
export const order = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/apix/z43phone/order`, params).then(res => res.data);
};
export const getuser = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/apix/7dtime/getuser`, params).then(res => res.data);
};

// 群查询
export const groups = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/api/z43group`, { "params": params }).then(res => res.data);
};
// 群创建
export const groupAdd = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/api/z43group`, params).then(res => res.data);
};
// 群修改
export const groupSet = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.put(`${ base }/api/z43group`, params).then(res => res.data);
};
// 群删除
export const groupDel = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.delete(`${ base }/api/z43group`, { "params": params }).then(res => res.data);
};

// 群功能-短信
export const mobinfo = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/api/z43mobinfo`, params).then(res => res.data);
};


// 成员查询
export const friends = params => { //和z43phone/phones 一样
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/apix/z39time/friends`, params).then(res => res.data);
};
// 成员查询
export const getProjectFriends = params => { //和z43phone/phones 一样
  return axios.get(`${ base }/api/z39friend`, { "params": params }).then(res => res.data);
};
// 成员创建
export const friendAdd = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/api/z43friend`, params).then(res => res.data);
};
// 成员修改
export const friendSet = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.put(`${ base }/api/z43friend`, params).then(res => res.data);
};
// 成员删除
export const friendDel = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.delete(`${ base }/api/z43friend`, { "params": params }).then(res => res.data);
};

// 群成员导入
export const friendAddGrp = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/api/z43friends/addgrp`, params).then(res => res.data);
};
// 通讯录导入
export const friendsAdd = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/apix/z43friends/add`, params).then(res => res.data);
};
// 群成员邀请
export const invitefriends = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.get(`${ base }/api/z39invite`, { "params": params }).then(res => res.data);
};
export const getInviteTitle = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.get(`${ base }/apix/z39time/getinvitetitle`, { "params": params }).then(res => res.data);
};
// 获取  app自动记录的任务
export const taskautoGet = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/api/z39taskauto`, { "params": params }).then(res => res.data);
};
export const taskautoAdd = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/api/z39taskauto`, params).then(res => res.data);
};
export const taskautoPut = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.put(`${ base }/api/z39taskauto`, params).then(res => res.data);
};
export const taskautoDel = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.delete(`${ base }/api/z39taskauto`, { "params": params }).then(res => res.data);
};


// 获取登录初始数据.
export const usertask = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/apix/z39time/usertask`, { "params": params }).then(res => {
    token = token || localStorage.getItem('token');
    // console.log("token:", token);
    return Promise.resolve(res.data);
  });
};
//检查是否登陆
export const checkLogin = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.get(`${ base }/apix/z39base/userinfo`, { "params": params }).then(res => res.data);
};
export const days = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/apix/z39time/days`, { "params": params }).then(res => res.data);
};
export const sortdayGet = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.get(`${ base }/apix/z39time/sortday`, { "params": params }).then(res => res.data);
};
export const sortdayAdd = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.get(`${ base }/apix/z39time/sortdayadd`, { "params": params }).then(res => res.data);
};
// 获取最近常用标签
export const tagsrec = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/apix/z39time/tagsrec`, { "params": params }).then(res => res.data);
};


//  ------------------------- 任务 rest相关处理 -------------------------
export const tasksGet = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/api/z39tasks`, { "params": params }).then(res => res.data);
};
export const tasksAdd = params => {
  var item;
  for (var i = params.tasks.length - 1; i >= 0; i--) {
    item = params.tasks[i];
    if (item.title && item.title.length > 100) return Promise.resolve({ errno: 1001, errmsg: "标题最长100字" });
    if (item.desc && item.desc.length > 9000) return Promise.resolve({ errno: 1001, errmsg: "描述最长9000字" });
  };
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/api/z39tasks`, params).then(res => res.data);
};
export const taskGet = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.get(`${ base }/api/z39task`, { "params": params }).then(res => res.data);
};
export const taskAdd = params => {
  if (!params.title) return Promise.resolve({ errno: 1001, errmsg: "请输入标题" });
  if (params.title && params.title.length > 100) return Promise.resolve({ errno: 1001, errmsg: "标题最长100字" });
  if (params.desc && params.desc.length > 9000) return Promise.resolve({ errno: 1001, errmsg: "描述最长9000字" });
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/api/z39task`, params).then(res => res.data);
};
export const taskSet = params => {
  if (params.title != undefined && params.title.length == 0) return Promise.resolve({ errno: 1001, errmsg: "请输入标题" });
  if (params.title && params.title.length > 100) return Promise.resolve({ errno: 1001, errmsg: "标题最长100字" });
  if (params.desc && params.desc.length > 9000) return Promise.resolve({ errno: 1001, errmsg: "描述最长9000字" });
  token && (params = params || {}) && (params.token = token);
  return axios.put(`${ base }/api/z39task`, params).then(res => res.data);
};
export const taskDel = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.delete(`${ base }/api/z39task`, { "params": params }).then(res => res.data);
};
export const taskMend = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.get(`${ base }/apix/z39time/taskmend`, { "params": params }).then(res => res.data);
};
export const taskOne = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.get(`${ base }/apix/z39time/taskone`, { "params": params }).then(res => res.data);
};

export const Likeset = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.get(`${ base }/apix/z39base/suggestvote`, { "params": params }).then(res => res.data);
};
//  ------------------------- 项目 rest相关处理 -------------------------
export const projects = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/api/z39projects`, { "params": params }).then(res => res.data);
};
export const projectGet = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.get(`${ base }/api/z39project`, { "params": params }).then(res => res.data);
};
export const projectAdd = params => {
  if (params.title && params.title.length > 15) return Promise.resolve({ errno: 1001, errmsg: "标题最长15字" });
  if (params.desc && params.desc.length > 100) return Promise.resolve({ errno: 1001, errmsg: "描述最长100字" });
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/api/z39project`, params).then(res => res.data);
};
export const projectSet = params => {
  if (params.title && params.title.length > 15) return Promise.resolve({ errno: 1001, errmsg: "标题最长15字" });
  if (params.desc && params.desc.length > 100) return Promise.resolve({ errno: 1001, errmsg: "描述最长100字" });
  token && (params = params || {}) && (params.token = token);
  return axios.put(`${ base }/api/z39project`, params).then(res => res.data);
};
export const projectDel = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.delete(`${ base }/api/z39project`, { "params": params }).then(res => res.data);
};

//  ------------------------- 项目成员邀请 处理 -------------------------
export const inviteGet = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/api/z39invite`, { "params": params }).then(res => res.data);
};
export const inviteTitleGet = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/apix/z39time/getinvitetitle`, { "params": params }).then(res => res.data);
};

//  ------------------------- 标签 rest相关处理 -------------------------
export const tagGet = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/api/z39tag`, { "params": params }).then(res => res.data);
};
export const tagAdd = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/api/z39tag`, params).then(res => res.data);
};
export const tagSet = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.put(`${ base }/api/z39tag`, params).then(res => res.data);
};
export const tagDel = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.delete(`${ base }/api/z39tag`, { "params": params }).then(res => res.data);
};

export const tagsAdd = params => {
  return axios.post(`${ base }/api/z39tags`, params).then(res => res.data);
};

//  ------------------------- 标签模板 rest相关处理 -------------------------
export const tagTmpAdd = params => {
  return axios.post(`${ base }/api/z39tagtmp`, params).then(res => res.data);
};

export const tagTmpPut = params => {
  return axios.put(`${ base }/api/z39tagtmp`, params).then(res => res.data);
};
export const tagTmpGet = params => {
  return axios.get(`${ base }/api/z39tagtmp`, { "params": params }).then(res => res.data);
};
export const tagTmpDel = params => {
  return axios.delete(`${ base }/api/z39tagtmp`, { "params": params }).then(res => res.data);
};
//  ------------------------- 日报文章 rest相关处理 -------------------------
export const blogs = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.get(`${ base }/api/z39blogs`, { "params": params }).then(res => res.data);
};
export const blogGet = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.get(`${ base }/api/z39blog`, { "params": params }).then(res => res.data);
};
export const blogAdd = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/api/z39blog`, params).then(res => res.data);
};
export const blogSet = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.put(`${ base }/api/z39blog`, params).then(res => res.data);
};
export const blogContentUpdate = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.put(`${ base }/api/z39blog`, params).then(res => res.data);
}
export const blogDel = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.delete(`${ base }/api/z39blog`, { "params": params }).then(res => res.data);
};

//  ------------------------- 项目成员 rest相关处理 -------------------------
// export const members = params => {
//   token && (params = params || {}) && (params.token = token);
//   return http.get(`${ base }/api/z39friend`, { "params": params }).then(res => res.data);
// };
export const memberGet = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.get(`${ base }/api/z39friend`, { "params": params }).then(res => res.data);
};
export const memberAdd = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/api/z39friend`, params).then(res => res.data);
};
export const memberSet = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.put(`${ base }/api/z39friend`, params).then(res => res.data);
};
export const memberDel = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.delete(`${ base }/api/z39friend`, { "params": params }).then(res => res.data);
};

//  ------------------------- 重复任务 rest相关处理 -------------------------
export const repeatGet = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/api/z39repeat`, { "params": params }).then(res => res.data);
};
export const repeatAdd = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/api/z39repeat`, params).then(res => res.data);
};
export const repeatSet = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.put(`${ base }/api/z39repeat`, params).then(res => res.data);
};
export const repeatDel = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.delete(`${ base }/api/z39repeat`, { "params": params }).then(res => res.data);
};

//  ------------------------- 习惯养成 rest相关处理 -------------------------
export const habitGet = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/api/z39habit`, { "params": params }).then(res => res.data);
};
export const habitAdd = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/api/z39habit`, params).then(res => res.data);
};
export const habitSet = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.put(`${ base }/api/z39habit`, params).then(res => res.data);
};
export const habitDel = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.delete(`${ base }/api/z39habit`, { "params": params }).then(res => res.data);
};

//  ------------------------- 标签 rest相关处理 -------------------------
export const tagitemsGet = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/api/z39tagitem`, { "params": params }).then(res => res.data);
};
export const tagitemsAdd = params => {
  return axios.post(`${ base }/api/z39tagitem`, params).then(res => res.data);
};

export const tagitemGet = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/api/z39tagitem`, { "params": params }).then(res => res.data);
};
export const tagitemAdd = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/api/z39tagitem`, params).then(res => res.data);
};
export const tagitemSet = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.put(`${ base }/api/z39tagitem`, params).then(res => res.data);
};
export const tagitemDel = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.delete(`${ base }/api/z39tagitem`, { "params": params }).then(res => res.data);
};
//  ------------------------- 任务音频录音 rest相关处理 -------------------------
export const audioGet = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/api/z39audio`, { "params": params }).then(res => res.data);
};
export const audioAdd = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/api/z39audio`, params).then(res => res.data);
};
export const audioSet = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.put(`${ base }/api/z39audio`, params).then(res => res.data);
};
export const audioDel = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.delete(`${ base }/api/z39audio`, { "params": params }).then(res => res.data);
};
export const audioTTS = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/apix/z39sound/tts`, { "params": params }).then(res => res.data);
};
//  ------------------------- 任务提醒的闹钟 rest相关处理 -------------------------
export const clockGet = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/api/z39clock`, { "params": params }).then(res => res.data);
};
export const clockAdd = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.post(`${ base }/api/z39clock`, params).then(res => res.data);
};
export const clockSet = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.put(`${ base }/api/z39clock`, params).then(res => res.data);
};
export const clockDel = params => {
  token && (params = params || {}) && (params.token = token);
  return axios.delete(`${ base }/api/z39clock`, { "params": params }).then(res => res.data);
};
// 任务提醒方式的开通状态获取
export const noticeStateGet = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/apix/z39notice/noticestate`, { "params": params }).then(res => res.data);
};

// 导入任务

export const importTask = params => {
  return axios.post(`${ base }/apix/z39time/importtask`, params).then(res => res.data);
};
// 导入excel

export const importExcel = params => {
  return axios.post(`${ base }/apix/z39time/importexcel`, params).then(res => res.data);
};


// app消息推送用的, 保存用户设备client id
export const appRegistAdd = params => {
  token && (params = params || {}) && (params.token = token);
  return http.get(`${ base }/apix/z39time/appregist`, { "params": params }).then(res => res.data);
};

// 检查是否需要调用appRegistAdd
export const checkAppRegist = params => {
  if (!isApp || !uid) return;
  try { //部分参数只有在app中才有,h5和pc版本没有这变量.会报错.
    if (!clientid) return;
    let data = { uid: uid, cid: clientid, device: deviceToken, type: appType };
    this.appRegistAdd(data);
  } catch (e) {

  }
};
