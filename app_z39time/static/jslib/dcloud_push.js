var Push = function() {}
//推送格式
//{"e":"admin_edit_driver","eid":"1111","params":{"barcode":"IIG370554","status":"100"}}

/**
 *
 * 推送的监听
 */
var pushListener = function() { //Push.prototype.
    // var _this = this;
    plus.push.addEventListener("click", function(msg) {
        plus.push.clear();
        return;
        switch (msg.payload) {
            case "LocalMSG":
                // plus.nativeUI.alert("点击本地创建消息启动：", null, "测试");
                break;
            default:
                // plus.nativeUI.alert("点击离线推送消息启动：", null, "测试");
                break;
        }
        if (msg.payload) {
            plus.nativeUI.alert("click:" + msg, null, "测试");
            // _this.handle(msg);
        }
    }, false);
    plus.push.addEventListener("receive", function(msg) {
        var data = JSON.parse(msg.content);
        plus.nativeUI.alert(data.body, null, data.title);
        // plus.nativeUI.alert(msg.content, null, msg.title);
        // return app.$store.commit({ type: "z39/taskClock", "clock": msg });
        plus.device.vibrate(200);
        setTimeout(function() { plus.device.vibrate(200) }, 700);
        return;
        if (msg.aps) { // Apple APNS message
            plus.nativeUI.alert("接收到在线APNS消息：", null, "测试");
        } else {
            plus.nativeUI.alert("接收到在线透传消息：", null, "测试");
        }
        if (plus.os.name == 'iOS') {
            if (msg.payload) {
                // _this.notificationMessage(msg);
            }
        } else {
            // _this.notificationMessage(msg);
        }
    }, false);
}

/**
 * 解析透传并分别处理
 * @param {Object} msg
 */
Push.prototype.notificationMessage = function(msg) {
    plus.nativeUI.toast(msg);
    // ApiConfig.staticIsDebug("notificationMessage", msg, 1);
    var content = '';
    var _this = this;
    var jsonData = '';
    switch (plus.os.name) {
        case "Android":
            jsonData = eval("(" + msg.payload + ")");
            break;
        case "iOS":
            jsonData = msg.payload;
            break;
    }
    switch (jsonData.e) {
        case 'admin_version_update':
            content = '版本更新';
            break;
        default:
            break;
    }
    if (content) {
        _this.createLocalPushMsg(msg, content);
    }
}

/**
 * 根据透传信息创建一条本地推送消息内容
 * @param {Object} msg
 * @param {Object} content
 * main页面    2中情况
 *            1.订单状态改变 刷新页面
 *            2.订单完成创建本地推送消息点击跳到订单详情界面
 * orderDetail页面
 *            1.相同的order_id加载成订单改变的界面
 *            2.不相同的order_id通知一下
 *            3.支付成功相同的order_id跳到成功页面
 *            4.支付成功不相同order_id通知一下
 * updateProduct页面
 *            1.所有的都只通知一下
 *
 */
Push.prototype.createLocalPushMsg = function(msg, content) {
    // ApiConfig.staticIsDebug("createLocalPushMsg", msg, 1);
    plus.nativeUI.toast("createLocalPushMsg" + msg);
    var _this = this;
    var options = {
        cover: false
    };
    var jsonData = '';
    switch (plus.os.name) {
        case "Android":
            jsonData = eval("(" + msg.payload + ")");
            break;
        case "iOS":
            jsonData = msg.payload;
            break;
    }
    var str = content ? content : "";
    _this.createMessage(str, jsonData, options);
}

/**
 * mui创建本地方法的API
 * @param str
 * @param jsonData
 * @param options
 */
Push.prototype.createMessage = function(str, jsonData, options) {
    switch (plus.os.name) {
        case "Android":
            break;
        case "iOS":
            jsonData = jsonData.eid;
            break;
    }
    plus.push.createMessage(str, jsonData, options);
}

/**
 * 支付成功后掉头跳转
 * @param orderDetail
 */
Push.prototype.openPaySuccess = function(orderDetail) {
    var openUrl = 'view/driver/paySuccess.html';
    var openId = 'paySuccess';
    var orderParams = {
        order: orderDetail
    };
    utilsJs.openWebView(openUrl, openId, orderParams);
}

/**
 * 点击通知的处理方法
 * @param {Object} msg
 */
Push.prototype.handle = function(msg) {
    var _this = this;
    var isPushOrder;
    // var sell_main = plus.webview.getWebviewById('sell_main');
    // ApiConfig.staticIsDebug("msg", msg, 1);
    var order_id = '';
    var jsonData = '';
    switch (plus.os.name) {
        case "Android":
            jsonData = eval("(" + msg.payload + ")");
            order_id = jsonData.eid;
            break;
        case "iOS":
            if (msg.aps) {
                try {
                    jsonData = msg.payload;
                    order_id = jsonData.eid;
                } catch (e) {}
            } else {
                jsonData = msg.payload;
                order_id = jsonData;
            }
            break;
    }
    // var userInfo = DbUtils.getStorage('userInfo', 1);
    var openUrl = '';
    var openId = '';
    var order = null;
    var task = null;
    switch (jsonData.e) {
        case 'admin_order_status':
            isPushOrder = 1;
            break;
        case 'admin_order_pay':
            isPushOrder = 1;
            break;
        case 'admin_driver_new_task':
            isPushOrder = 0;
            break;
        case 'admin_prospect_verification':
            isPushOrder = 0;
            break;
        default:
            break;
    }

}

/**
 * 跳转的页面方法
 * @param {Object} openUrl
 * @param {Object} openId
 * @param {Object} params
 */
Push.prototype.openUrlParams = function(openUrl, openId, params) {
    utilsJs.openWebView(openUrl, openId, params);
}

/**
 *清除ios小红点
 */
Push.prototype.cancelPushClear = function() {
    plus.push.clear();
}