var wxjson = null; // { appId: "", timestamp: "", nonceStr: "", signature: "" };

$(function() {
  getjssdk();
})

function isWeixin() {
  var ua = navigator.userAgent.toLowerCase();
  if (ua.match(/MicroMessenger/i) == "micromessenger") {
    return true;
  } else {
    return false;
  }
}

/* 小程序中的录音需要调用jssdk时不跳出. */
function getjssdk(get) {
  var local = false;
  var href = location.href;
  if (href.indexOf("192.168") != -1 || href.indexOf("127.0.0") != -1 || href.indexOf("d7game.tunnel.") != -1) { //判断是本地或非微信,不需要微信分享.
    local = true;
    // return;
  }
  if (!isWeixin() || wxjson != null) return;
  // if (!get && window.__wxjs_environment === 'miniprogram') return; //微信小程序中.

  var jssdkUrl = '//7dtime.com/apix/7dtime/jssdk';
  if (local) jssdkUrl = 'http://192.168.11.187:8361/apix/7dtime/jssdk';
  // $.get('//thinkido.tunnel.qydev.com/apix/7dtime/jssdk', { url: location.href }, function(data) {
  $.get(jssdkUrl, { url: location.href }, function(data) {
    wxjson = data.data;
    wxjson.debug = false; // local; // 
    wxjson.jsApiList = ["onMenuShareTimeline", "onMenuShareAppMessage",
      "startRecord", "stopRecord",
      "onVoiceRecordEnd", "playVoice", "pauseVoice", "stopVoice",
      "uploadVoice", "downloadVoice",
      "translateVoice"
    ];
    wx.config(wxjson);
    // console.info(wxjson);
    // doWxShare();
    wx.ready(function() {
      doWxShare();
      // 在这里调用 API
    });
  });
}

var sharesuccessfunc;
var sharefuncobj;
var shareData = {
  title: "时间清单,7天时间看一生",
  content: "行为影响习惯,习惯影响性格,性格影响人生",
  sharepng: "https://cdns.7dtime.com/logo.png",
  link: 'https://7dtime.com'
};

function doWxShare() {
  try {
    setTimeLine(null);
    setAppMessage(null);
  } catch (e) {}
}
//分享到朋友圈
function setTimeLine(data) {

  data = data || shareData;
  wx.onMenuShareTimeline({
    title: data.title, // 分享标题
    desc: data.content, // 分享描述
    link: data.link, // 分享链接
    imgUrl: data.sharepng, // 分享图标
    success: function() {
      // 用户确认分享后执行的回调函数
      if (sharesuccessfunc != null)
        sharesuccessfunc.apply(sharefuncobj, []);
    },
    cancel: function() {
      // 用户取消分享后执行的回调函数

    }
  });
}

function setAppMessage(data) { //分享给好友
  data = data || shareData;

  wx.onMenuShareAppMessage({
    title: data.title, // 分享标题
    desc: data.content, // 分享描述
    link: data.link, // 分享链接
    imgUrl: data.sharepng, // 分享图标
    type: '', // 分享类型,music、video或link，不填默认为link
    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
    success: function() {
      if (sharesuccessfunc != null)
        sharesuccessfunc.apply(sharefuncobj, []);
    },
    cancel: function() {}
  });
}
