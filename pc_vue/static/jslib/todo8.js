(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(factory());
}(this, (function () { 'use strict';

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var componentQuery = createCommonjsModule(function (module, exports) {
function one(selector, el) {
  return el.querySelector(selector);
}

exports = module.exports = function (selector, el) {
  el = el || document;
  return one(selector, el);
};

exports.all = function (selector, el) {
  el = el || document;
  return el.querySelectorAll(selector);
};

exports.engine = function (obj) {
  if (!obj.one) throw new Error('.one callback required');
  if (!obj.all) throw new Error('.all callback required');
  one = obj.one;
  exports.all = obj.all;
  return exports;
};
});

var componentQuery_1 = componentQuery.all;
var componentQuery_2 = componentQuery.engine;

/**
 * Module dependencies.
 */

try {
  var query$1 = componentQuery;
} catch (err) {
  var query$1 = componentQuery;
}

/**
 * Element prototype.
 */

var proto = Element.prototype;

/**
 * Vendor function.
 */

var vendor = proto.matches || proto.webkitMatchesSelector || proto.mozMatchesSelector || proto.msMatchesSelector || proto.oMatchesSelector;

/**
 * Expose `match()`.
 */

var componentMatchesSelector = match;

/**
 * Match `el` to `selector`.
 *
 * @param {Element} el
 * @param {String} selector
 * @return {Boolean}
 * @api public
 */

function match(el, selector) {
  if (!el || el.nodeType !== 1) return false;
  if (vendor) return vendor.call(el, selector);
  var nodes = query$1.all(selector, el.parentNode);
  for (var i = 0; i < nodes.length; ++i) {
    if (nodes[i] == el) return true;
  }
  return false;
}

/**
 * Module Dependencies
 */

try {
  var matches = componentMatchesSelector;
} catch (err) {
  var matches = componentMatchesSelector;
}

/**
 * Export `closest`
 */

var componentClosest = closest;

/**
 * Closest
 *
 * @param {Element} el
 * @param {String} selector
 * @param {Element} scope (optional)
 */

function closest(el, selector, scope) {
  scope = scope || document.documentElement;

  // walk up the dom
  while (el && el !== scope) {
    if (matches(el, selector)) return el;
    el = el.parentNode;
  }

  // check scope for match
  return matches(el, selector) ? el : null;
}

function styleInject(css, ref) {
  if (ref === void 0) ref = {};
  var insertAt = ref.insertAt;

  if (!css || typeof document === 'undefined') {
    return;
  }

  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';

  if (insertAt === 'top') {
    if (head.firstChild) {
      head.insertBefore(style, head.firstChild);
    } else {
      head.appendChild(style);
    }
  } else {
    head.appendChild(style);
  }

  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
}


var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();


var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};


//组合成碎片图数组并返回
var fragment = function(data) {
    var fragmentData = [];
    for (var i = 0; i < 24 * 6; i++) {
        var title = parseInt(i / 6) + ':' + (i % 6 * 10);
        fragmentData.push({ time: 0, item: [], title: title, tags: {}, count: 0 });
    }

    //获取清单标签
    this.getTag = function(tags) {
        var tag = '';
        var tagArr = ['critical', 'high', 'low', 'minor', 'life'];
        for (var i = 0; i < tagArr.length; i++) {
            if (tags[tagArr[i]] !== undefined)
                return tagArr[i];
        }

    }
    //一个任务会有多次暂停情况，将一个任务分解为全部相连的小任务
    this.cutItems = function(tags) {
        var started = tags['started'],
            done = tags['done']?tags['done']:tags['cancelled'];
        var toggle = tags['toggle'];
        let toggleArr = [];
        if (toggle) {
            //替换、切割
            toggle = toggle.replace(/\ (\d{2}-\d{2}-\d{2})/g, ' 20$1').trim();
            toggleArr = toggle.replace(/\ (\d{4}-\d{2}-\d{2})/g, '|$1').split('|');
            //排序

            for (let i = 0; i < toggleArr.length; i++) {
                toggleArr[i] = moment(toggleArr[i]).format('X');
            }
            toggleArr.sort();
            if (toggleArr.length % 2 != 0 && toggleArr.length > 1) {
                toggleArr.splice(toggleArr.length - 1, 1);
            }
            for (let i = 0; i < toggleArr.length; i++) {
                toggleArr[i] = moment.unix(toggleArr[i]).format('YYYY-MM-DD HH:mm');
            }
        }
        var timeGroup = [started, ...toggleArr];
        if(done) timeGroup.push(done);
        for (let i = 0; i < timeGroup.length; i += 2) {
            tags['started'] = timeGroup[i];
            tags['done'] = timeGroup[i + 1];
            this.fillItems(tags);
        }


    }

    //开始或结束时间点
    this.getTimePoint = function(time){
        
        if(moment(time).isBefore(moment(time).format("YYYY-MM-DD"))){
            return 0;
        }
        if(moment(time).isAfter(moment(time).format("YYYY-MM-DD 23:59:59"))){
            return 9;
        }
        return moment(time).minute()%10;
    }

    //填充多个小碎片
    this.fillItems = function(tags){
        if(!(tags['started']&&tags['done'])) return ;
        var started = tags['started'], done = tags['done'];
        var startItemKey = this.getIndex(started);
        var endItemKey = this.getIndex(done);
        var start=this.getTimePoint(started), end;
        
        for(var ItemKey=startItemKey; ItemKey<=endItemKey; ItemKey++){
            end = 9;
        
            if(ItemKey==endItemKey) end=this.getTimePoint(done);
            fragmentData[ItemKey] = this.fillItem(fragmentData[ItemKey], start, end, this.getTag(tags));

            start = 0;
        }
    }

    //空心圆
    this.circleItem = function(tags){
        var index = this.getIndex(tags['started']||tags['done']||tags['cancelled']);
        fragmentData[index]['circle'] = true;
    }

    //填充每个小碎片
    this.fillItem = function(item, start, end, tag) {
        var begin = Math.max(0, start);
        end = Math.min(9, end);
        if (!item['tags'][tag]) item['tags'][tag] = 0;
        item['tags'][tag] += (end - begin);


        //if(item['item'].length >= 10) return item;
        for (var i = begin; i <= end; i++) {
            if (item['item'].indexOf(i) >= 0) {
                item['count'] += 1; //当这分钟已有任务时，累加标记
                continue;
            }
            item['item'].push(i);
        }
        item['time'] = item['item'].length * 10;
        return item;
    };

    //根据时间返回对应数组下标
    this.getIndex = function(time) {
        var h = moment(time).hour();
        var m = moment(time).minute();
        var index = h * 6 + parseInt(m / 10);
        return index;
    }

    for (let tskey in data) {
        var tasks = data[tskey];
        for (let tkey in tasks) {
            var tags = tasks[tkey];
            if (!(tags['started'] && (tags['done'] || tags['toggle'] || tags['cancelled']))) {
                //标注空心园
                if(tags['started'] || tags['done'] || tags['cancelled']){
                    this.circleItem(tags);
                }
                continue;
            }
            this.cutItems(tags);
        }
    }
    //整理成按小时的数组
    var fragmentData1 = [];
    var group = [];
    var i = 0;
    do {
        if (i % 6 == 0) {
            group = [];
        }
        if (i % 6 == 5) {
            fragmentData1.push(group);
        }
        group.push(fragmentData[i]);
        i++;
    } while (i < fragmentData.length);

    return fragmentData1;

};


//画出碎片图html
var drawFragment = function(data) {

    var FLAG_SYSTEM = ["critical", "high", "low", "minor", "life"];
    var FLAG_COLOR = ["#1ba4ff", "#72c7ff", "#f7c3c3", "#fb6969", "orange"]; // 浅蓝#72c7ff 天蓝#1ba4ff green yellowgreen orange red
    var TAG_LIST = ['重要紧急', '重要不紧急', '紧急不重要', '不紧急不重要', '生活'];

    //获取同一碎片内重叠标签数
    this.isMulti = function(item) {
        return item['count'] > 1;
    }
    //取时间占比最多的标签颜色
    this.getTagColor = function(item) {
        var tag = {};
        for (var i in item['tags']) {
            if (!tag['key']) tag = { key: i, val: item['tags'][i] };
            if (item['tags'][i] > tag['val']) tag = { key: i, val: item['tags'][i] };
        }
        if (!tag['key']) return '';

        return FLAG_COLOR[FLAG_SYSTEM.indexOf(tag['key'])];
    }



    var html = $('<div class="fragment"></div>');
    for(var i in data){
        var item = data[i];
        var _item = $('<dl class="fragment-item"></dl>');
        for(var k in item ){
            var multi = this.isMulti(item[k])?' multi':'';
            var circle = item[k]['circle']?' circle':'';
            var inner = $('<dt class="fragment-item-inner" title="'+item[k]['title']+'"><div class="fragment-item-inner-progress'+multi+circle+'"><div class="fragment-item-inner-progress-item" style="height:'+item[k]['time']+'%;background-color:'+this.getTagColor(item[k])+';"></div></div></dt>');
            _item.append(inner);
        }
        html.append(_item);
    }

    var tagHtml = '';
    for(var i=0; i<TAG_LIST.length; i++) {
        tagHtml += ('<span class="tag-color" style="background-color:' + FLAG_COLOR[i] + '"></span><span class="tag-name">' + TAG_LIST[i] + '</span>');
    }
    tagHtml = '<div class="tag-group">' + tagHtml + '</div>';
    html.append(tagHtml);
    //html.append('<div class="tag-tip">任务四象限：重要紧急 重要不紧急 紧急不重要 不紧急不重要 生活</div>');

    return html;

}

// var css = ".docsify-pagination-container{display:flex;}";
// styleInject(css);

/**
 * installation
 */
function install(hook, vm) {

  hook.doneEach(function () {
    let parsed , element , text ;
      try {
          element = $('pre[data-lang=todo8] code')[0] ;
          text = $('pre[data-lang=todo8] code').html() ;
          if(text) parsed = JSON.parse(text);
      } catch (err) {
          console.log("Woops! Error parsing", err);
          return;
      }
      if( parsed){
        var html = new drawFragment(new fragment(parsed));
        var replacement = document.createElement('div');
        replacement.innerHTML = $(html).html() ;
        // $('pre[data-lang=todo8] code').parent()[0].replaceChild(replacement, element);
        $('pre[data-lang=todo8]').replaceWith(`<div class='_todo8'><div class="fragment">${$(html).html()}</div></div>`);
      }
    // return render();
  });
}

window.$docsify = window.$docsify || {};

window.$docsify.plugins = [install].concat(window.$docsify.plugins || []);

})));