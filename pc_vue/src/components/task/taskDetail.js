import Swiper from 'swiper'
import api from '../../api'
import moment from 'moment'
import _ from 'underscore'
import { mapState } from 'vuex'
import { taskAndTag } from '../../common/mixin'
import { logic } from '../../common/mixinTaskLogic'
import { mixinPc } from '../../common/mixinPc'
import popoverTask from './popoverTask.vue'
var lastSelectTaskId
var remindBtnsArr = [
  { label: '提前时间<br/><span style="color:#666;font-size:12px;">任务结束前提前几分钟提醒</span>', type: 'info' },
  { label: '提前05分钟', value: '5' },
  { label: '提前10分钟', value: '10' },
  { label: '提前15分钟', value: '15' },
  { label: '提前20分钟', value: '20' },
  { label: '提前30分钟', value: '30' }
]
var remindTypeBtnsArr = [
  { label: '闹钟提醒方式<br/><span style="color:#666;font-size:12px;">任务结束前和超时共2次提醒</span>', type: 'info' },
  { label: '系统闹钟', value: '1' },
  { label: '公众号提醒', value: '2' },
  { label: 'APP消息', value: '3' },
  { label: '短信提醒', value: '4' },
  { label: '电话呼叫', value: '5' }
]
var tagSlider = function() {
  var swiper = new Swiper('.detail-tags-swiper', {
    slidesPerView: 'auto',
    spaceBetween: 0,
    freeMode: true,
    freeModeMomentumRatio: 0.5,
    freeModeMomentumBounce: false,
    freeModeMomentum: true,
    scrollbarHide: true
  })
  return swiper
}
export default {
  name: 'taskDetail',
  props: ['taskEditor'],
  data() {
    return {
      batchTasks: {
        buMen: null,
        curBuMen: null
      },
      fourQuadrantList: [
        { key: "critical", "value": "1.重要紧急(立刻办)", active: false },
        { key: "high", "value": "2.重要不紧急(持续做)", active: false },
        { key: "low", "value": "3.紧急不重要(交给他人)", active: false },
        { key: "minor", "value": "4.不紧急不重要(尽量不要做)", active: false }
      ],
      curFourQuadrant: null,
      remindBtnsDef: remindBtnsArr,
      remindTypeBtnsDef: remindTypeBtnsArr,
      blurFocus: true,
      remind: 5,
      remindTime: '',
      doneDt: '',
      remindType: '', // '',
      remindTypeValue: 1,
      lastRemainTime: '', // 解决完成任务后,显示评估时间
      taskEditTxt: '编辑',
      isSwitch: this.taskEditor.status,
      taskId: 0,
      tagSwiper: '',
      taskDetailHeight: '',
      taskDetail: '',
      dueValue: '',
      dueValueDialog: false,
      btnLoading: false,
      loading: false,
      taskBigTitle: '',
      taskDesc: '',
      taskSmallTitle: '',
      parentTask: '',
      childrenList: '',
      showClock: false,
      taskState: 'done',
      popoverTime: {
        hour: '',
        minute: ''
      },
      clockParams: {
        dueDone: '',
        finishTime: '',
        remainTime: '00:00:00',
        remindType: ''
      },
      clockTimer: '',
      lastTaskIdState: '',
      selectedTask: '', // 被选中的任务
      timeNeedDialogVisible: false,
      selectTimeNeed: '', // 选中的need时间
      initPickerTime: '',
      selectTimeDue: '', // 选中的due时间
      selectTimeAdd: '',
      timeAddDialogVisible: false, // 选择加时时间
      memberDialogVisible: false, // 成员dialog
      memberList: [],
      selectedMember: '',
      tagDialogVisible: false, // 标签dialog
      allTags: [],
      memberListDic: {},
      notShowTagsArr: [], // 任务不显示的标签
      popoverTimeList: ['00:05', '00:10', '00:15', '00:20', '00:25', '00:30', '00:35', '00:40', '00:45', '00:50',
        '00:55', '01:00', '01:05', '01:10', '01:15', '01:20', '01:25', '01:30', '01:35', '01:40', '01:45', '01:50',
        '01:55', '02:00', '02:05', '02:10', '02:15', '02:20', '02:25', '02:30', '02:35', '02:40', '02:45', '02:50',
        '02:55', '03:00'
      ],
      tagsRepeatList: [{
        name: '每天',
        key: 'repeat',
        active: false,
        value: 'D'
      }, {
        name: '每周1',
        key: 'repeat',
        active: false,
        value: 'W1'
      }, {
        name: '每周2',
        key: 'repeat',
        active: false,
        value: 'W2'
      }, {
        name: '每周3',
        key: 'repeat',
        active: false,
        value: 'W3'
      }, {
        name: '每周4',
        key: 'repeat',
        active: false,
        value: 'W4'
      }, {
        name: '每周5',
        key: 'repeat',
        active: false,
        value: 'W5'
      }, {
        name: '每周6',
        key: 'repeat',
        active: false,
        value: 'W6'
      }, {
        name: '每周日',
        key: 'repeat',
        active: false,
        value: 'W7'
      }, {
        name: '每月',
        key: 'repeat',
        active: false,
        value: 'M'
      }, {
        name: '每年',
        key: 'repeat',
        active: false,
        value: 'Y'
      }],
      tagsRepeatDic: {},
      commonTagsList: [],
      detailObj: {
        taskId: '',
        leader: '',
        due: '',
        need: '',
        clock: '',
        remind: 5,
        tagsArr: [],
        tagsObj: {}
      },
      systemTags: ['need', 'used', 'usedCalc', 'created', 'started', 'done', 'toggle', 'cancelled', 'due', 'to',
        'remind', 'repeat'
      ],
      contentAutoSaveTimer:''//内容自动保存定时器
    }
  },
  components: {
    popoverTask
  },
  mixins: [taskAndTag, mixinPc, logic],
  computed: {
    ...mapState({
      uid: state => state.vux.uid,
      username: state => state.vux.username,
      taskAll: state => state.z39.taskAll,
      tagDic: state => state.z39.tagDic,
      taskDic: state => state.z39.taskDic,
      tagitems: state => state.z39.tagitems,
      tagitemDic: state => state.z39.tagitemDic,
      members: state => state.z39.members,
      memberDic: state => state.z39.memberDic,
      projects: state => state.z39.projects,
      filtersFlag: state => state.z39.filtersFlag,
    }),
    popoverTimeSure() {
      function isNumber(obj) {
        return typeof obj === 'number' && !isNaN(obj)
      }
      if (isNumber(+this.popoverTime.hour) && isNumber(+this.popoverTime.minute) && (this.popoverTime.hour || this.popoverTime
          .minute) && +this.popoverTime.minute < 60) return true
      return false
    },
    // 时间显示
    showDateOrTime() {
      if (this.detailObj.due !== '') {
        var datestr = moment(this.detailObj.due).format('MM-DD')
        var timestr = moment(this.detailObj.due).format('HH:mm')
        if (timestr == '' || timestr == '00:00') return datestr
        else return timestr
      } else {
        return '开始时间'
      }
    }
  },
  watch: {
    'selectedTask': {
      handler() {
//      if (lastSelectTaskId === this.selectedTask.id) return
        lastSelectTaskId = this.selectedTask.id
        this.detailObj.leader = (this.selectedTask.tags && this.selectedTask.tags.to && this.memberListDic['fuid_' + this.selectedTask.tags.to]) || ''
        this.detailObj.due = (this.selectedTask.tags && this.selectedTask.tags.due) || ''
        this.detailObj.need = (this.selectedTask.tags && this.selectedTask.tags.need) || ''
        this.detailObj.remind = (this.selectedTask.remind && this.selectedTask.tags.remind) || 5
        this.remind = (this.selectedTask.remind && this.selectedTask.tags.remind) || 5
        this.detailObj.tagsObj = Object.assign({}, this.selectedTask.tags);
        this.detailObj.pid=this.selectedTask.pid;
        this.detailObj.projectName='';
        this.selectTimeDue = this.detailObj.due || ''
        if (this.selectTimeDue.length == 10) {
          this.selectTimeDue = this.detailObj.due + ' ' + '00:00'
        }
        if (this.detailObj.tagsObj.remind) {
          var tempR = this.detailObj.tagsObj.remind+'';
          var tempA = tempR.split('_')
          this.detailObj.remind = tempR
          this.remind = tempA[0]
          this.remindTypeValue = tempA[1]
          var tempt = parseInt(tempA[1] || 1)
          tempt = this.remindTypeBtnsDef[tempt].label
          this.remindType = tempt.substr(0, tempt.length - 2)
          this.changeRemindSelect()
        } else {
          this.remind = 5
        }
        
        if (this.selectedTask.pid) {
          this.obMembers(this.selectedTask.pid)
          this.detailObj.tagsObj.pid = this.selectedTask.pid
        } else this.memberList = [{ nname: this.username, fuid: this.uid }];
        
		this.projects.forEach(item=>{
			if(item.id==this.detailObj.pid){
				this.detailObj.projectName=item.title;
			}
		})
        this.stopClock()
        this.setClockParams()
      },
      deep: true
    },
    'selectedTask.id' () {
      var _this = this
      this.lastRemainTime = ''
      this.detailObj.tagsArr = []
      if (!this.selectedTask.id) return
      api.taskGet({
        id: this.selectedTask.id
      }).then(data => {
        _this.detailObj.tagsArr = data.data.tags
      })
    },
    'taskEditor.taskDesc' () {
      this.taskDesc = this.taskEditor.taskDesc
    },
    'taskEditor.status' () {
      this.isSwitch = this.taskEditor.status;
      if(!this.isSwitch){
      	clearInterval(this.contentAutoSaveTimer)
      }else{
      	this.contentAutoSaveTimer=setInterval(this.saveTaskContent,60000)
      }
    },
    taskDic: {
      handler() {
        if (this.selectedTask && this.taskDic[this.selectedTask.id]) {
          this.selectedTask.state = this.taskDic[this.selectedTask.id].state
          this.selectedTask.finished = this.taskDic[this.selectedTask.id].finished
          this.selectedTask.tags = this.taskDic[this.selectedTask.id].tags
          api.taskGet({
            id: this.selectedTask.id
          }).then(data => {
            this.detailObj.tagsArr = data.data.tags
          })
        }
      },
      deep: true
    },
    'members' () {
      var _this = this
      this.memberList = this.members
      this.memberList.forEach(function(item) {
        _this.memberListDic['fuid_' + item.fuid] = item
      })
    },
    projects() {
      this.projects.forEach(item => {
        this.$set(item, 'active', false)
      })
    },
    // selectTimeDue (val) {
    //   if (!val || this.initPickerTime === val) return  // 初始化数值
    //   this.detailObj.due = moment(this.selectTimeDue).format('YYYY-MM-DD HH:mm').replace(' 00:00', '')
    //   this.saveTask()
    // },
    isSwitch(val) {
      this.$emit('vTaskEditorStatus', val)
      if (val === true) {
        this.taskEditor.taskDesc = this.taskDesc || '';
        this.taskEditor.taskID = this.taskId
        // $('img').lightbox_me();
      } else {
        this.taskDesc = this.taskEditor.taskDesc
      }
    }
  },
  methods: {
    async obBumen(proId) { //    获取部门
      var params = { id: proId, tag: 1 };
      let data = await api.projectGet(params); //.then(data => {})
      if (!data || data.errno) return this.$message.error(data.errmsg);
      let tmp = data.data.tagtmp.tmp.split(',');
      this.batchTasks.buMen = tmp.map(a => { return { key: a, value: this.tagitemDic[a] } })
    },
    async obMembers(proId) { //    获取成员
      this.obBumen(proId)
      var params = { id: proId, member: 1 };
      var data = await api.projectGet(params); //.then(data => { })
      if (!data || data.errno) return this.$message.error(data.errmsg);
      this.memberList = data.data.members;
    },
    childSelectedMember(data) {
      const vm = this
      vm.selectedMember = data
    },
    selectTimeChange(e) {
      const vm = this
      if (e.length > 10 && e.slice(11, e.length) == '00:00') {

        vm.detailObj.due = e.slice(0, 10)



      } else {
        vm.detailObj.due = e
      }
		
      vm.saveTask()
    },
    // initPickerDate () {
    //   this.selectTimeDue = this.initPickerTime
    // },
    formatTime(secend) {
      return formatUsed(secend)
    },
    chooseRemind() {
      var taskitem = this.selectedTask // this.taskDic[this.selectedTask.id]
      if (taskitem === undefined) return // 点击取消.
      this.changeRemind(taskitem, this.remind, this.remindTypeValue)
    },
    changeRemindSelect() { // 修改弹出闹钟显示的文字
      var index = this.remind / 5
      var temp = index < 4 ? (index === 0 ? '不提醒' : this.remindBtnsDef[index].label) : `提前${this.remind}分钟`
      let type = this.remindTypeValue || 0;
      var temp1 = this.remindTypeBtnsDef[type].label
      this.remindValue = [temp, temp1]
    },
    async changeRemind(task, remind, remindValue) {
      if (!task) return // 刚今日页面
      var value = `${remind}_${remindValue}`
      this.remind = remind
      this.detailObj.remind = value
      if (task['tags'] === undefined || value === task.tags.remind) return
      let res = await this.doTagSet(task, 'remind', value);
      if (res) this.checkSetClock(task)
    },
    setDueValue() {
      if (!this.dueValue) return this.showNotice('请选择时间', 'warning')
      this.dueValueDialog = false
      this.detailObj.clock = moment(this.dueValue).format('MM-DD')
      this.saveTask()
    },
    setClockParams: function() {
      var { dueDone } = this.getDone2Due(this.selectedTask)
      let time = dueDone.substr(11)
      this.clockParams.dueDone = time
      this.clockParams.remainTime = '00:00:00'
      this.clockParams.finishTime = ''
      if (this.clockParams.dueDone && this.detailObj.tagsObj.need) {
        var temp, h, m, need
        need = this.detailObj.tagsObj.need
        if (_.isString(need) && need.indexOf(':') === -1) need = parseInt(need)
        if (typeof need === 'number' && isFinite(need)) {
          h = Math.ceil(need / 3600)
          m = Math.ceil(need % 3600 / 60)
        } else { // 旧代码,字符形式的兼容.
          temp = need.split(':')
          h = temp[0]
          m = temp[1]
        }
        var finishMom = moment(this.detailObj.due).add(h, 'h').add(m, 'm')
        this.clockParams.finishTime = finishMom.format('YYYY-MM-DD HH:mm')
        var usedTime = this.detailObj.tagsObj.used
        var remainTime = need
        if (usedTime) remainTime = need - getSecond(usedTime)

        if (_.isString(remainTime) && remainTime.split(':').length === 2) {
          remainTime += ':00'
        } // 旧代码可删除
        if (this.lastRemainTime) this.clockParams.remainTime = this.lastRemainTime
        else this.showClockTime(remainTime)
        // this.clockParams.remainTime = formatTimeHM(remainTime,true);
        if (this.selectedTask.state === 'started') this.timerInterval(remainTime, this.selectedTask)
      }
      this.clockParams.remindType = this.remindType
    },
    timerInterval: function(time, task) {
      var st, tg, ud, nd, now, used
      var tags = task.tags
      used = 0
      now = moment().valueOf()
      st = tags.started
      tg = tags.toggle
      ud = tags.used
      nd = parseInt(tags.need || 0)
      console.warn('task state', task.state, st, tg, ud)
      if (task.state === 'toggle') { // 如果继续执行已暂停任务，
        used = getSecond(ud) // + (now - moment(tg).valueOf())/1000;
      } else if (tg && task.state === 'started') used = getSecond(ud) + (now - moment(tg).valueOf()) / 1000
      else if (st) used = (now - moment(st).valueOf()) / 1000
      used = Math.ceil(used)
      var remain = nd - used
      var rTime = nd // time;
      this.stopClock()
      var temp1, h1, m1, s1, allSeconds
      if (_.isNumber(rTime)) {
        allSeconds = rTime
      } else { // 旧数据格式，需要删除.
        temp1 = rTime.split(':')
        h1 = Number(temp1[0])
        m1 = Number(temp1[1])
        s1 = Number(s1 = temp1[2])
        allSeconds = h1 * 3600 + m1 * 60 + s1
      }
      allSeconds -= used // 减去消耗时间
      this.showClockTime(allSeconds)
      if (task.state === 'done' || task.state === 'cancelled') return
      this.clockTimer = setInterval(() => {
        allSeconds--
        this.showClockTime(allSeconds)
      }, 1000)
    },
    stopClock() {
      if (!this.clockTimer) return
      clearInterval(this.clockTimer)
      this.clockTimer = 0
    },
    showClockTime(time) {
      var num = Math.abs(time)
      var h2 = parseInt(num / 3600)
      var m2 = parseInt((num - h2 * 3600) / 60)
      var s2 = num - h2 * 3600 - m2 * 60
      h2 = (String(h2).length < 2 ? ('0' + h2) : h2)
      m2 = (String(m2).length < 2 ? ('0' + m2) : m2)
      s2 = (String(s2).length < 2 ? ('0' + s2) : s2)
      this.lastRemainTime = (time < 0 ? '-' : '') + h2 + ':' + m2 + ':' + s2
      this.clockParams.remainTime = this.lastRemainTime
    },
    async changeTaskState(state,dontShowSwith) {
      var realState = state
      let st = this.selectedTask;
      var startedVal = moment().format('YYYY-MM-DD HH:mm')
      if (st.state === 'toggle' && state === 'toggle') realState = 'started'
      if (realState == "started"){
//    	if(!dontShowSwith){
      		this.isSwitch = true;
//    	}
      	
      } else if (realState == "done") this.isSwitch = false;
      this.$nextTick(() => { //如果保存失败会有问题
        if (realState == "done") st.finished = true, this.doneDt = startedVal;
      });
      let opts = { title: this.taskBigTitle , desc : this.taskDesc } ;
      let data = await this.doTaskEdit(st, state, startedVal , opts ); //.then((data) => {})
      st.state = realState
      if (realState === 'started') {
        this.stopClock() //
        this.timerInterval(this.clockParams.remainTime, st)
      } else this.stopClock()
      this.checkSetClock(st, startedVal)
    },
    /**
     * 显示任务详情浮窗
     */
     showTaskDetail(taskId) {
      this.showClock = false
      this.loading = true
      this.resetForm()
      var _this = this
      this.taskDetail = this.taskDic[taskId]
      return api.taskGet({
        id: taskId
      }).then(data => {
        this.loading = false
        if (data.errno !== 0) {
          this.showNotice(data.errmsg, 'warning')
          return Promise.reject(data.errmsg)
        }
        data = data.data
        // 添加到stroe
        let childtasks = data.tasks || []
        let allTasks = [data].concat(childtasks)
        allTasks.forEach(item => { item.tagsArr = item.tags })
        this.$store.commit({ type: 'z39/taskToDB', tasks: allTasks, act: 'add' })
        this.$store.commit({ type: 'z39/taskCurr', task: data })
        this.parentTask = data
        this.taskBigTitle = data.title
        this.taskDesc = data.desc
        this.doneDt = ''
        allTasks.forEach((item, index) => {
          if (index === 0) this.selectedTask = this.taskDic[taskId]; //item          
          if (item.state === 'done') {
            item.finished = true
            this.doneDt = item.tags.done
          } else {
            item.finished = false
          }
        })
        this.childrenList = allTasks
        this.tagSwiper.setWrapperTranslate(0)
        setTimeout(() => {
          this.tagSwiper.updateSlidesSize()
        }, 500)
      })
    },
    resetForm: function() {
      this.taskBigTitle = ''
      this.taskDesc = ''
      this.childrenList = ''
      this.selectedTask = ''
      this.detailObj = {
        taskId: '',
        leader: '',
        due: '',
        need: '',
        remind: '',
        tagsArr: [],
        tagsObj: {}
      }
      this.allTags = this.tagitems.filter((item, index, arr) => {
        this.$set(item,'active',false)
        if (this.filtersFlag.indexOf(item.key) == -1) return item
      })
    },
    changeTask: function(item) {
      this.stopClock()
      this.taskBigTitle = item.title
      this.taskDesc = item.desc
      this.selectedTask = item
    },
    addTask: function() {
      var _this = this
      if (!this.taskSmallTitle) {
        this.$message({
          message: '请输入任务标题',
          type: 'warning'
        })
        return
      }
      this.btnLoading = true
      api.taskAdd({
        parid: this.parentTask.id,
        title: this.taskSmallTitle,
        desc: ''
      }).then(data => {
        _this.btnLoading = false
        if (data.errno !== 0) {
          _this.showNotice(data.errmsg, 'warning')
          return Promise.reject(data.errmsg)
        }
        _this.taskSmallTitle = ''
        data = data.data
        data.finished = false
        _this.childrenList.push(data)
        _this.$store.commit({
          type: 'z39/taskToDB',
          tasks: [data],
          act: 'add'
        })
      })
    },
    delTask: function() {
      var _this = this
      var isParent = false
      if (this.selectedTask.id === this.parentTask.id) {
        isParent = true
      }
      _this.btnLoading = true
      api.taskDel({
        id: this.selectedTask.id
      }).then(data => {
        _this.btnLoading = false
        if (data.errno !== 0) {
          _this.showNotice(data.errmsg, 'warning')
          return Promise.reject(data.errmsg)
        }
        if (isParent) {
          _this.$store.commit({
            type: 'z39/taskToDB',
            tasks: [{
              id: this.selectedTask.id
            }],
            act: 'del'
          })
        } else {
          _this.childrenList = _this.childrenList.filter(function(item) {
            return item.id !== _this.selectedTask.id
          })
          _this.childrenList[0].checked = true
        }
      })
    },
    saveTask: function() {
      this.btnLoading = true
      let st = this.selectedTask;
      // 设置任务
      var taskParams = { title: this.taskBigTitle, desc: this.taskDesc, id: st.id }
      if (st.pid && !this.detailObj.tagsObj['pid']) taskParams.pid = null // 如果原先有项目id现在没有
      else taskParams.pid = this.detailObj.tagsObj['pid']
      this.taskSet(taskParams, st)
      // 先循环自带的tag,如果没有就删除
      _.isArray(st.tagsArr) && st.tagsArr.forEach(item => {
        // key为due need to时需要判断
        if (item.key === 'due' || item.key === 'need' || item.key === 'to' || item.key === 'remind') {
          return false
        } else if (this.detailObj.tagsObj[item.key] === undefined) {
          if (this.tagDic[st.id + '_' + item.key]) {
            this.tagDel(this.tagDic[st.id + '_' + item.key], true, st)
          }
        }
      })
      // 再循环新增的tag,如果没有就添加
      this.detailObj.tagsArr.forEach(item => {
        // key为due need to时需要判断
        if (item.key === 'due' || item.key === 'need' || item.key === 'to' || item.key === 'remind') {
          return
        } else if (st.tags[item.key] === undefined) {
          // 如果没有就新增
          this.tagAdd({
            taskid: st.id,
            key: item.key,
            value: item.value
          }, true, st)
        } else if (item.value !== st.tags[item.key]) {
          // 如果有但不一样，就要做修改
          if (item.key === 'repeat') {
            this.doTagSet(st, item.key, this.detailObj.tagsObj[item.key], false)
          } else {
            this.doTagSet(st, item.key, item.value, false)
          }
        }
      })
      // key为due need to时需要判断
      if (this.detailObj.due &&
        !(st.tags && st.tags.due && this.detailObj.due === st.tags.due)) {
        this.doTagSet(st, 'due', this.detailObj.due, false)
      }
      if (this.detailObj.remind &&
        !(st.tags && st.tags.remind && this.detailObj.remind === st.tags.remind)) {
        this.doTagSet(st, 'remind', this.detailObj.remind, false).then((data) => {
          this.checkSetClock(st)
        })
      }
      if (this.detailObj.need &&
        !(st.tags && st.tags.need && this.detailObj.need === st.tags.need)) {
        var value = this.detailObj.need
        if (value.indexOf(':') !== -1) value = value.split(':')[0] * 3600 + value.split(':')[1] * 60
        this.doTagSet(st, 'need', value, false).then((data) => {
          this.checkSetClock(st)
        })
      }
      if (this.detailObj.leader &&
        !(st.tags && st.tags.to && this.detailObj.leader.fuid === st.tags.to)) {
        this.doTagSet(st, 'to', this.detailObj.leader.fuid, false)
      }
      var btnTimer = setTimeout(() => {
        clearTimeout(btnTimer)
        this.btnLoading = false
        this.tagSwiper.setWrapperTranslate(0)
        this.tagSwiper.updateSlidesSize()
      }, 1000)
    },
    setTimeNeed: function() {
      if (!this.selectTimeNeed) {
        this.showNotice('请选择时间', 'warning')
        return
      }

      this.timeNeedDialogVisible = false
      this.detailObj.need = this.selectTimeNeed
      this.saveTask()
    },
    chooseEvlTime: function(item) {
      this.detailObj.need = item
      this.saveTask()
      this.clearPopoverTime()
    },
    confirmEvlTime: function() {
      var hour = this.popoverTime.hour.length === 1 ? '0' + this.popoverTime.hour : this.popoverTime.hour
      this.detailObj.need = (hour || '00') + ':' + (this.popoverTime.minute || '00')
      this.clearPopoverTime()
      this.saveTask()
    },
    clearPopoverTime: function() {
      this.popoverTime.hour = ''
      this.popoverTime.minute = ''
    },
    formatTimeNeed: function(begin, end, type) {
      var temp1 = begin.split(':')
      var temp2 = end.split(':')
      if (type === 'add') {
        var h = Number(temp1[0]) + Number(temp2[0])
        var m = Number(temp1[1]) + Number(temp2[1])
        var s = Number(temp1[2] || 0) + Number(temp2[2] || 0)
        if (s >= 60) {
          m = m + 1
          s = s - 60
        }
        if (m >= 60) {
          h = h + 1
          m = m - 60
        };
        h = (String(h).length < 2 ? ('0' + h) : h)
        m = (String(m).length < 2 ? ('0' + m) : m)
        s = (String(s).length < 2 ? ('0' + s) : s)
        return (h + ':' + m)
      } else if (type === 'minus') {
        var h1 = Number(temp1[0])
        var h2 = Number(temp2[0])
        var m1 = Number(temp1[1])
        var m2 = Number(temp2[1])
        var s1 = Number(temp1[2] || 0)
        var s2 = Number(temp2[2] || 0)
        var allS = h2 * 3600 + m2 * 60 + s2 - h1 * 3600 - m1 * 60 - s1
        var h3 = parseInt(allS / 3600)
        var m3 = parseInt((allS - h3 * 3600) / 60)
        var s3 = allS % 60
        if (m3 < 0 || h3 < 0 || s3 < 0) {
          return '00:00:00'
        }
        h3 = (String(h3).length < 2 ? ('0' + h3) : h3)
        m3 = (String(m3).length < 2 ? ('0' + m3) : m3)
        s3 = (String(s3).length < 2 ? ('0' + s3) : s3)

        return (h3 + ':' + m3 + ':' + s3)
      }
    },
    timeHM: function(time) {
      var str
      if (time.length > 10) {
        str = time.substring(11)
      } else if (time.length > 5) {
        str = time.substring(5)
      } else {
        str = time
      }
      return str
    },
    setTimeAdd: function() {
      if (!this.selectTimeAdd) {
        this.showNotice('请选择时间', 'warning')
        return
      }
      this.timeAddDialogVisible = false
      var timeNeed = this.selectTimeAdd
      if (this.selectedTask.tags && this.selectedTask.tags.need) {
        timeNeed = this.formatTimeNeed(timeNeed, this.selectedTask.tags.need, 'add')
      }
      this.doTagSet(this.selectedTask, 'need', timeNeed, false)
      this.saveTask()
    },

    setTaskMember: function() {
      this.memberDialogVisible = false
      this.detailObj.leader = this.memberListDic['fuid_' + this.selectedMember]
      this.saveTask()
    },
    showSetTagDialog: function() {
      this.notShowTagsArr = []
      var _this = this
      this.tagDialogVisible = true
      this.detailObj.tagsArr.forEach(item => {
        if (this.systemTags.indexOf(item.key) > 0 || this.tagitemDic[item.key] === undefined) {
          this.notShowTagsArr.push(item)
        }
      })
      this.commonTagsList.forEach(item => {
        item.active = false
      })
      this.tagsRepeatList.forEach(item => {
        if (this.detailObj.tagsObj[item.key] === item.value) {
          item.active = true
        } else {
          item.active = false
        }
      })
      this.allTags.forEach(function(item, index, arr) {
        if (_this.detailObj.tagsObj[item.key] !== undefined && _this.systemTags.indexOf(item.key) < 0) {
          item['active'] = true
        } else {
          item['active'] = false
        }
      })

      this.fourQuadrantList.forEach(function(item, index, arr) {
        if (_this.detailObj.tagsObj[item.key] !== undefined && _this.systemTags.indexOf(item.key) < 0) {
          item['active'] = true
        } else {
          item['active'] = false
        }
      })

      this.projects.forEach(item => {
        if (item.id === this.selectedTask.pid) {
          item.active = true
        } else {
          item.active = false
        }
      })
    },
    setTaskTag: function() {
      var _this = this
      this.detailObj.tagsObj = {}
      this.detailObj.tagsArr = []
      this.allTags.forEach(function(item) {
        if (item['active']) {
          _this.detailObj.tagsArr.push(item)
          _this.detailObj.tagsObj[item.key] = item.name
        }
      })
      this.commonTagsList.forEach(function(item) {
        if (item['active']) {
          _this.detailObj.tagsArr.push(item)
          _this.detailObj.tagsObj[item.key] = item.name
        }
      })
      this.tagsRepeatList.forEach(function(item) {
        if (item['active']) {
          _this.detailObj.tagsArr.push(item)
          // 重复和重复任务标签冲突
          _this.detailObj.tagsObj[item.key] = item.value
        }
      })
      this.projects.forEach(function(item) {
        if (item['active']) {
          _this.detailObj.tagsObj['pid'] = item.id
        }
      })
      if(_this.detailObj.tagsObj['repeat']){
      	var notShowArr=this.notShowTagsArr.filter(item=>item.key!='repeat')
      }
      notShowArr&&(this.detailObj.tagsArr = this.detailObj.tagsArr.concat(notShowArr))
      this.detailObj.tagsArr.forEach(item => {
        this.detailObj.tagsObj[item.key] = item.name
        if (item.key === 'repeat' && item.value) {
          this.detailObj.tagsObj[item.key] = item.value
        }
      })
       
      _this.tagDialogVisible = false
      this.saveTask()
    },
    // 所有标签点击
    tagsClick: function(item, type) {
      item.active = !item.active
      if (type === 'all') {
        this.commonTagsList.forEach(item1 => {
          if (item.key === item1.key) {
            item1.active = false
          }
        })
      }else if (type === 'fourTags') {
        this.allTags.slice(0,4).forEach(item1 => {
          if (item.key != item1.key) {
            item1.active = false
          }
        })
      } else if (type === 'fourAll') {
        this.fourQuadrantList.forEach(item1 => {
          if (item.key === item1.key) {
            item1.active = false
          }
        })
      } else if (type === 'common') {
        this.allTags.forEach(item1 => {
          if (item.key === item1.key) {
            item1.active = false
          }
        })
      } else if (type === 'repeat') {
        this.tagsRepeatList.forEach(item1 => {
          if (item.value !== item1.value) {
            item1.active = false
          }
        })
      } else if (type === 'project') {
        this.batchTasks.curBuMen = null
        this.obBumen(item.id)
        this.projects.forEach(item1 => {
          if (item.id !== item1.id) {
            item1.active = false
          }
        })
      }
    },
    /**
     * 关闭任务编辑器
     */
    closeTaskDetail() {
      if (this.isSwitch) {
        this.$alert('请先保存任务，然后关闭编辑器')
        return false
      }
      if (this.$route.name === '今日任务') {
        $('.kanban-main-warp .kanban-main-container').animate({
          right: '45px'
        }, 300)
      } else {
        $('.kanban-main-warp .kanban-main-container').animate({
          right: '0px'
        }, 300)
      }
      $('.task-detail-container').animate({
        right: '-400px'
      }, 300)
    },
    getCommonTags: function() {
      api.tagsrec({}).then(data => {
        if (data.errno !== 0) {
          this.showNotice(data.errmsg, 'warning')
          return Promise.reject(data.errmsg)
        }
        data.data.forEach(item => {
          item.active = false
        })
        this.commonTagsList = data.data
        return Promise.resolve(data)
      })
    },
    show() {
      this.$refs.dataPicker.pickerVisible = true
    },
    hide() {
      this.$refs.dataPicker.pickerVisible = false
    },
    //mardown保存任务内容
    saveTaskContent(){
    		let content=this.$parent.$refs.vEditor.getContent();
    		if(this.taskDesc == content) return ;
  			this.taskDesc = content;
  			this.saveTask();
    },
    //将时间转为冒号格式
    formatTimeToColon(time){
    		if(time&&time>0){
    			time=parseInt(time/60);
    			var h=parseInt(time/60);
    			var m=time%60;
    			h<10&&(h='0'+h)
    			m<10&&(m='0'+m)
    			return h+':'+m
    		}else{
    			return time
    		}
    }
  },
  mounted() {
    var _this = this
    this.tagSwiper = tagSlider()
    this.taskDetailHeight = $('.task-detail-container').height()
    this.memberList = this.members
    this.memberList.forEach(function(item) {
      _this.memberListDic['fuid_' + item.fuid] = item
    })
    this.allTags = this.tagitems.map(function(item, index, arr) {
      var item1 = Object.assign({}, item)
      item1['active'] = false
      return item1
    })
    this.getCommonTags()
    this.tagsRepeatList.forEach(item => { this.tagsRepeatDic[item.value] = item.name })
  }
}
