import {mapState} from 'vuex'
import api from '@/api'
import UploadImg from '@/libs/image-uploader.js'
export default {
  props:{
    'editorContent':{ type:String,default: '' },
    'pageType':{ type:String,default: '' },
    'outlinePosition':{ type:String,default: 'left' }
  },
  data () {
    return {
      editorMD: null
    }
  },
  computed: {
    ...mapState({})
  },
  watch: {
    'editorContent' () {
      this.init()
      if( this.editorMD ) this.resizeEditor()
    }
  },
  methods: {
    getContent () {
      return this.editorMD.getValue()
    },
    getHTML () {
      return this.editorMD.getHTML()
    },
    resizeEditor () {
      $('.vditor-ir').scrollTop(0)
      console.log('resizeEditor:', $('#editormd').width() )
    },
    init () {
      if (this.editorMD) return  this.editorMD.setValue(this.editorContent || '')
      var self = this ;
      let doResize = () => {
        let showOutline = $(window).width() > 1600 ;
        // let showOutline = $('.vditor-content').width() > 900
        self.editorMD.vditor.options.outline.enable = showOutline ;
        if (showOutline) $('.vditor-outline').show()
        else $('.vditor-outline').hide()
      }
      let showOutline = $(window).width() > 1600 ;
      $(window).resize( doResize )
      // $('.vditor-content').resize(doResize);
      // eslint-disable-next-line no-undef
      this.editorMD = new Vditor('vditor', {
          // width: 'auto',
          width: "100%",
          height: "100%",
          mode:'ir' , //默认
          outline: { enable: showOutline , position: this.outlinePosition } ,
          counter:{  enable:true ,type:'md'},
          placeholder: '使用AI麦克风用语音代替键盘输入，效率提升6倍，每分钟输入带排版的300多字。http://ai.todo8.cn' ,
          "cache": {"enable": false},
          typewriterMode: true,
          // esc:  ,
          toolbarConfig: {
            pin: true,
          },
          toolbar: [
            {name:'emoji',tipPosition: 's'},
            {name:'headings',tipPosition: 's'},
            {name:'bold',tipPosition: 's'},
            {name:'italic',tipPosition: 's'},
            {name:'strike',tipPosition: 's'},
            {name:'link',tipPosition: 's'},
            {name:'|',tipPosition: 's'},
            {name:'list',tipPosition: 's'},
            {name:'ordered-list',tipPosition: 's'},
            {name:'check',tipPosition: 's'},
            {name:'outdent',tipPosition: 's'},
            {name:'indent',tipPosition: 's'},
            "|",
            {name:'quote',tipPosition: 's'},
            {name:'line',tipPosition: 's'},
            {name:'code',tipPosition: 's'},
            {name:'inline-code',tipPosition: 's'},
            "|",
            {name:'upload',tipPosition: 's'},
            {name:'record',tipPosition: 's'},
            {name:'table',tipPosition: 's'},
            "|",
            {name:'undo',tipPosition: 's'},
            {name:'redo',tipPosition: 's'},
            "|",
            {name:'fullscreen',tipPosition: 's'},
            {name:'edit-mode',tipPosition: 's'},
            {
                name: "more",tipPosition: 's',
                toolbar: [
                    "both",
                    "code-theme",
                    "content-theme",
                    "export",
                    "outline",
                    "preview",
                    "devtools",
                    "info",
                    "help",
                ],
            }],
          cache: {
            enable: false,
          },
          upload:{
            accept: 'image/*,.mp3, .wav, mp4, avi, rmvb, wmv, webm',
            url:`${api.globalUrl}/apix/z39base/upload?from=vditor`,
            headers:{ token: localStorage.token || api.token },
            success(editor,msg){
              if( !msg ) return console.error({"msg": "网络异常", code: 1000 })  ,self.editorMD.tip("网络异常");
              let { errno , errmsg , data } = JSON.parse(msg) ;
              if( errno ) return console.error('upload error:' , errmsg) , self.editorMD.tip( errmsg ) ;
              let md = self.editorMD.getValue() ;
              // self.editorMD.setValue( `${md}\n![](${ data })\n` ) ;
              self.editorMD.insertValue( `\n![](${ data })\t` ) ;
              self.editorMD.tip('上传成功')
              console.log('上传成功');
              // self.editorMD.focus()
            },
            format(files , responseText ){ //使用success后，format这里就无效了。
              console.log( "format:" ,files , responseText , typeof responseText )
              if( !responseText ) return JSON.stringify({"msg": "网络异常", code: 1000 }) 
              let { errno , errmsg , data } = JSON.parse(responseText) ;
              let succMap = {  } ;
              succMap[`${ files[0].name }`] = data ;
              return JSON.stringify({ msg : errmsg , code : 0 , data :{ succMap , "errFiles": [] } })  ;
            },
            filename (name) {
              return name.replace(/[^(a-zA-Z0-9\u4e00-\u9fa5\.)]/g, '').
                replace(/[\?\\/:|<>\*\[\]\(\)\$%\{\}@~]/g, '').
                replace('/\\s/g', '')
            },
          },
          after: () => {
            this.editorMD.setValue(this.editorContent || '');
            // if( this.outlinePosition == 'right') $('.vditor-outline').appendTo('.vditor-content'); 
            // if( $(window).width() < 1600  ) $('.vditor-outline').hide();
          },
      })

    },
    //ctrl+s保存内容
    saveContent(){
    		try{
          // 耦合性太高，需要修改为派发事件。
    			this.$parent.$refs.taskDetailComponent.saveTaskContent()
    		}catch(err){
    			console.log(err)
    		}
    }
  },
  mounted () {
    this.init()
    // let that = this
    // let m = document.createElement('script')
    // m.src = "/static/jslib/vditor.js"
    // document.body.appendChild(m)
    // m.onload = function () {
    //   that.init()
    //   console.log($('vditor onload'))
    // }
    
  }
}
