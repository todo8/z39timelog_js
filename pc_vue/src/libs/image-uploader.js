/*
  pase & upload image or video for editor.md

 * @file        upload-image.js
 * @version     v1.0.0
 * @description pase & upload image or video for editor.md.
 * @author      yoyo-git
 * @updateTime  2018-11-05
*/
// default config
import { Loading } from 'element-ui'
var vConfig = {
  imageUpload: false,
  imageFormats: ['jpg', 'jpeg', 'gif', 'png', 'bmp'],
  imageUploadURL: '',
  uploadImgMaxSize: 3 * 1024 * 1024,
  uploadImgMaxLength: 5,
  uploadImgTimeout: 12 * 1000,

  videoUpload: false,
  videoFormats: ['mp4', 'avi', 'rmvb', 'wmv', "webm"],
  videoUploadURL: '',
  uploadVideoMaxSize: 50 * 1024 * 1024,
  uploadVideoMaxLength: 1,
  uploadVideoTimeout: 12 * 1000
}
// 构造函数
function UploadImg (editor) {
  this.editor = editor
  this.config = $.extend({}, vConfig, editor.settings)
  if ((this.config.imageUpload || this.config.videoUpload)) {
    this._bindEvent()
  }
  // 上传状态
  this.loaderStauts = {
    loading: null, // loading 对象
    image: false, // 是否有图片处于上传中， true: 存在， false：不存在
    video: false // 是否有视频处于上传中， true: 存在， false：不存在
  }
}
// 原型
UploadImg.prototype = {
  constructor: UploadImg,
  // 上传图片
  _bindEvent: function () {
    var that = this
    var $this = this.editor.editor
    $this.on('paste', function (e) {
      // 获取粘贴的图片或者视频
      const pasteFiles = that.getPasteFiles(e)
      if ((pasteFiles.images && pasteFiles.images.length) || (pasteFiles.videos && pasteFiles.videos.length)) {
        e.preventDefault()
        // 上传图片
        if (that.config.imageUpload && pasteFiles.images.length) {
          that.handleImage(pasteFiles.images)
        }
        // 上传视频
        if (that.config.videoUpload && pasteFiles.videos) {
          that.handleVideo(pasteFiles.videos)
        }
      }
    })
    // 禁用 document 拖拽事件
    // 禁用 document 拖拽事件
    const $document = $(document)
    $document.on('dragleave drop dragenter dragover', function (e) {
      e.preventDefault()
    })
    // 添加编辑区域拖拽事件
    $this.on('drop', function (e) {
      var pasteEvent = e.originalEvent
      var dragFiles = pasteEvent.dataTransfer && pasteEvent.dataTransfer.files
      if (dragFiles && dragFiles.length) {
        e.preventDefault()
        // 上传图片
        var images = []
        var videos = []
        var errInfo = []
        for (var i = 0; i < dragFiles.length; i++) {
          var file = dragFiles[i]
          if (/(image)|(video)/i.test(file.type)) {
            if (/image/i.test(file.type)) {
              images.push(file)
              continue
            }
            if (/video/i.test(file.type)) {
              videos.push(file)
              continue
            }
          } else {
            errInfo.push(file.name)
          }
        }
        if (errInfo.length) {
          alert('文件格式有误: \n' + errInfo.join('\n'))
          return
        }
        if (that.config.imageUpload && images && images.length) {
          that.handleImage(images)
        }
        if (that.config.videoUpload && videos && videos.length) {
          that.handleVideo(videos)
        }
      }
    })
    $this.on('dragover', function (e) {
      e.preventDefault()
    })
    $this.on('dragenter', function (e) {
      e.preventDefault()
    })
    $this.on('dragleave', function (e) {
      e.preventDefault()
    })
  },
  getPasteFiles: function (e) {
    const result = {
      images: [],
      videos: []
    }
    const clipboardData = e.clipboardData || (e.originalEvent && e.originalEvent.clipboardData) || {}
    const items = clipboardData.items
    if (!items) {
      return result
    }
    for (var i = 0; i < items.length; i++) {
      var value = items[i]
      if (/image/i.test(value.type)) {
        result.images.push(value.getAsFile())
        continue
      }
      if (/video/i.test(value.type)) {
        result.videos.push(value.getAsFile())
        continue
      }
    }

    return result
  },
/**
 * 处理拷贝图片
 * [Blob File] files
 */
  handleImage: function (files) {
    if (!(files && files.length)) return
    var that = this
    var resultFiles = []
    var errInfo = []
    var config = that.config
    var uploadMaxSize = config.uploadImgMaxSize ? config.uploadImgMaxSize : 2 * 1024 * 1024
    const maxSizeM = uploadMaxSize / 1024 / 1024
    var uploadImgMaxLength = config.uploadImgMaxLength ? config.uploadImgMaxLength : 5
    for (var i = 0; i < files.length; i++) {
      var file = files[i]
      var name = file.name
      var size = file.size
      // chrome 低版本 name === undefined
      if (!name || !size) {
        return
      }
      var regex = new RegExp(`\\.(` + config.imageFormats.join('|') + `)$`, 'i')
      // 校验图片后缀名
      if (regex.test(name) === false) {
        // 后缀名不合法，不是图片
        errInfo.push(`【${name}】不是图片`)
        continue
      }
      if (uploadMaxSize < size) {
        // 上传图片过大
        errInfo.push(`【${name}】大于 ${maxSizeM}M`)
        continue
      }
      // 验证通过的加入结果列表
      resultFiles.push(file)
    }
    // 抛出验证信息
    if (errInfo.length) {
      alert('图片验证未通过: \n' + errInfo.join('\n'))
      return
    }
    if (resultFiles.length > uploadImgMaxLength) {
      alert('一次最多上传' + uploadImgMaxLength + '张图片')
      return
    }
    if (config.imageUploadURL && typeof config.imageUploadURL === 'string') {
      let resultTask = resultFiles.map(file => {
        return that.uploadImage(file)
      })
      if (that.loaderStauts.loading == null) {
        that.loaderStauts.loading = Loading.service({
          lock: true,
          text: '正在上传...',
          spinner: 'el-icon-loading',
          background: 'rgba(0, 0, 0, 0.3)'
        })
        that.loaderStauts.image = true // 正在上传
      }
      Promise.all(resultTask).then((values) => {
        let errInfo = values.filter(item => {
          return !!item
        })
        if (errInfo && errInfo.length) {
          alert('失败信息: \n' + errInfo.join('\n'))
        }
        that.loaderStauts.image = false // 上传结束
        if (!that.loaderStauts.video && !that.loaderStauts.image) {
          that.loaderStauts.loading.close()
          that.loaderStauts.loading = null
        }
      })
    } else {
      alert('图片上传地址配置有误')
    }
  },
  /**
   * 处理拷贝视频
   * [Blob File] files
   */
  handleVideo: function (files) {
    if (!(files && files.length)) return
    var that = this
    var resultFiles = []
    var errInfo = []
    var config = that.config
    var uploadMaxSize = config.uploadVideoMaxSize ? config.uploadVideoMaxSize : 50 * 1024 * 1024
    var maxSizeM = uploadMaxSize / 1024 / 1024
    var uploadVideoMaxLength = config.uploadVideoMaxLength ? config.uploadVideoMaxLength : 1
    for (var i = 0; i < files.length; i++) {
      var file = files[i]
      var name = file.name
      var size = file.size
      // chrome 低版本 name === undefined
      if (!name || !size) {
        return
      }
      // 校验视频后缀名
      var regex = new RegExp(`\\.(` + config.videoFormats.join('|') + `)$`, 'i')
      if (regex.test(name) === false) {
        // 后缀名不合法，不是视频
        errInfo.push(`【${name}】不是视频`)
        continue
      }
      if (uploadMaxSize < size) {
        // 上传图片过大
        errInfo.push(`【${name}】大于 ${maxSizeM}M`)
        continue
      }
      // 验证通过的加入结果列表
      resultFiles.push(file)
    }
    // 抛出验证信息
    if (errInfo.length) {
      alert('视频验证未通过: \n' + errInfo.join('\n'))
      return
    }
    if (resultFiles.length > uploadVideoMaxLength) {
      alert('一次最多上传' + uploadVideoMaxLength + '个视频')
      return
    }
    if (config.videoUploadURL && typeof config.videoUploadURL === 'string') {
      let resultTask = resultFiles.map(file => {
        return that.uploadVideo(file)
      })
      if (that.loaderStauts.loading == null) {
        that.loaderStauts.loading = Loading.service({
          lock: true,
          text: '正在上传...',
          spinner: 'el-icon-loading',
          background: 'rgba(0, 0, 0, 0.3)'
        })
        that.loaderStauts.video = true // 正在上传
      }
      Promise.all(resultTask).then((values) => {
        let errInfo = values.filter(item => {
          return !!item
        })
        if (errInfo && errInfo.length) {
          alert('失败信息: \n' + errInfo.join('\n'))
        }
        that.loaderStauts.video = false // 上传结束
        if (!that.loaderStauts.video && !that.loaderStauts.image) {
          that.loaderStauts.loading.close()
          that.loaderStauts.loading = null
        }
      })
    } else {
      alert('视频上传地址配置有误')
    }
  },
  /**
   * 上传图片
   */
  uploadImage: function (file) {
    let that = this
    return new Promise((resolve, reject) => {
      var fileReader = new FileReader()
      var config = this.config
      fileReader.readAsDataURL(file)
      fileReader.onload = function (e) {
        var base64 = e.target.result
        var form = document.createElement('form').setAttribute('enctype', 'multipart/form-data')
        var formData = new FormData(form)
        formData.append('imgData', base64)
        var xhr = new XMLHttpRequest()
        xhr.open('POST', config.imageUploadURL, true)
        xhr.timeout = config.uploadImgTimeout ? config.uploadImgTimeout : 120 * 1000
        xhr.ontimeout = function (e) {
          resolve('系统响应过慢， 图片' + file.name + ' 上传失败!')
        }
        xhr.onload = function (event) {
          var responseText = event.currentTarget.responseText
          var json = JSON.parse(responseText)
          if (json.errno === 0) {
            that.insertImageToTextArea(json.data)
            resolve()
          } else {
            resolve(json.errmsg)
          }
        }
        xhr.send(formData)
      }
    })
  },
  uploadVideo: function (file) {
    var that = this
    return new Promise((resolve, reject) => {
      var config = that.config
      var form = document.createElement('form').setAttribute('enctype', 'multipart/form-data')
      var formData = new FormData(form)
      formData.append('testsf', file)
      var xhr = new XMLHttpRequest()
      xhr.open('POST', config.videoUploadURL, true)
      xhr.timeout = config.uploadVideoTimeout ? config.uploadVideoTimeout : 120 * 1000
      xhr.withCredentials = true
      xhr.ontimeout = function (e) {
        resolve('系统响应过慢， 视频' + file.name + ' 上传失败!')
      }
      xhr.onload = function (event) {
        var responseText = event.currentTarget.responseText
        var json = JSON.parse(responseText)
        if (json.errno === 0) {
          that.insertVideoToTextArea(json.data)
          resolve()
        } else {
          resolve(json.errmsg)
        }
      }
      xhr.send(formData)
    })
  },
  insertImageToTextArea: function (url) {
    this.editor.insertValue('![](' + url + ')' + '\n')
    // testEditor.insertValue("![" + filename + "](" + url + ")" + "\n")
  },
  insertVideoToTextArea: function (url) {
    var text = `\`\`\`video\n<video id="my-video" class="video-js" controls preload="auto" width="100%" data-setup='{"aspectRatio":"16:9"}'>
                 <source src="${url}" type="video/mp4" >
                 <p class="vjs-no-js">
                 To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
                </video>\n\`\`\``
    this.editor.insertValue(text + '\n')
  }
}

export default UploadImg
