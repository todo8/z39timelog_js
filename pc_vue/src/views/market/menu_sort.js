var menuSys = [{ name: "状态:"}, { key: "", name: "全部" }, { key: "created", name: "创建" }, { key: "due", name: "评估" }, { key: "started", name: "开始" }, { key: "toggle", name: "暂停" }, { key: "done", name: "完成" }, { key: "cancelled", name: "取消" }];
var menuPart4 = [{ name: "重要:"}, { key: "", name: "全部" }, { key: "critical", name: "紧重" }, { key: "high", name: "重要" }, { key: "low", name: "紧急" }, { key: "minor", name: "杂事" }, { key: "help", name: "求助" }];
var menuTimeDiff = [{ name: "异动:" }, { key: "", name: "全部" }, { key: "a", name: "提前" }, { key: "b", name: "准时" }, { key: "c", name: "超时" }, { key: "d", name: "无数据" }];
var menuNeed = [{ name: "评估:" }, { key: "", name: "全部" }, { key: "0-1800", name: "0.5h" }, { key: "1800-3600", name: "1h" }, { key: "3600-5400", name: "1.5h" }, { key: "5400-7200", name: "2h" }, { key: "7200-10800", name: "3h" }, { key: "10800-18000", name: "5h" }, { key: "18000-28800", name: "8h" }, { key: "28800-360000", name: "10h" }, { key: "360000", name: "10h+" }];
var menuPrice = [{ name: "价格:" }, { key: "", name: "全部" }, { key: "50", name: "¥50" }, { key: "100", name: "¥100" }, { key: "150", name: "¥150" }, { key: "200", name: "¥200" }, { key: "250", name: "¥250" }, { key: ">250", name: "¥250+" }];
var menuTime = [{ name: "时间:" }, { key: "", name: "全部" }, { key: "01", name: "1月" }, { key: "02", name: "2月" }, { key: "03", name: "3月" }, { key: "04", name: "4月" }, { key: "05", name: "5月" }, { key: "06", name: "6月" }, { key: "07", name: "7月" }, { key: "08", name: "8月" }, { key: "09", name: "9月" }, { key: "10", name: "10月" }, { key: "11", name: "11月" }, { key: "12", name: "12月" }];
var basemenu = [menuSys, menuPart4, menuTimeDiff, menuNeed, menuTime];
var pricemenu = [menuSys, menuPart4, menuTimeDiff, menuNeed, menuPrice];

exports.basemenu = basemenu;
exports.pricemenu = pricemenu;
