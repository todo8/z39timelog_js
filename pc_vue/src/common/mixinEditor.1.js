import _ from 'underscore'
import '../../static/z39pc/js/jquery-2.1.0.js'
import UploadImg from '@/libs/image-uploader.js'

export const mixinEditor = {
  data () {
    return {
      testEditor: null
    }
  },
  methods: {
    resizeEditor () {
      var w = $('#taskeditor').width() - 300
      console.log(w)
      $('#taskeditor').resize(w + 'px')
      $('.editormd-toolbar').resize('100%')
      $('.CodeMirror-wrap').css({ 'width': '50%', 'margin-top': '80px' })
      $('.editormd-preview').css({ 'width': '50%', 'top': '80px' })
    },
    showPreview () {
      let _this = this
      try {
        $('.markdown-body').appendTo($('.editormd-preview'))
      } catch (e) {
        setTimeout(function () {
          _this.showPreview()
        }, 5)
      };
    },
    addMask () {
      let _this = this
      var $document = $(document)
      var h = $document.height() + 'px'
      var w = $document.width() + 'px'
      var size = 'height:' + h + ';width:' + w + ';'
      var maskHtml = '<div id="editormd-mask" style="' + size + 'position:absolute;z-index:999;background-color:rgba(0,0,0,0.7);"></div>'
      $('body').append(maskHtml)
      $('#editormd-mask').click(function () {
        for (let i = 0; i < _this.$children.length; i++) {
          if (_this.$children[i].changeSwitch) {
            _this.$children[i].changeSwitch()
            $('.task-detail-container').focus()
            return
          }
        }
        $(this).remove()
      })
    },
    removeMask () {
      $('#editormd-mask').remove()
    },
    initEditor () {
      var _this = this
      if (typeof editormd === 'undefined') {
        var m = document.createElement('script')
        m.src = "/static/jslib/editormd.js";
        document.body.appendChild(m)
        m.onload = function () {
          _this.createEditor()
        }
      } else {
        _this.createEditor()
      }
    },
    createEditor () {
      var md = ''
      var that = this
      that.testEditor = editormd('taskeditor', {
        width: '100%',
        height: '900',
        path: '/static/lib/',
        previewTheme: 'dark',
        editorTheme: "pastel-on-dark",
        markdown: md,
        codeFold: true,
        // syncScrolling : false,
        saveHTMLToTextarea: true, // 保存 HTML 到 Textarea
        searchReplace: true,
        // watch : false,                // 关闭实时预览
        htmlDecode: 'style,script,iframe|on*',
        emoji: true,
        taskList: true,
        tocm: true, // Using [TOCM]
        tex: true, // 开启科学公式TeX语言支持，默认关闭
        flowChart: true, // 开启流程图支持，默认关闭
        sequenceDiagram: true, // 开启时序/序列图支持，默认关闭,

        imageUpload: true,
        imageFormats: ['jpg', 'jpeg', 'gif', 'png', 'bmp'],
        imageUploadURL: 'http://d7game.free.idcfengye.com/apix/z39base/uploadbase64',
        uploadImgMaxSize: 3 * 1024 * 1024,
        uploadImgMaxLength: 5,
        uploadImgTimeout: 12 * 1000,

        videoUpload: true,
        videoFormats: ['mp4', 'avi', 'rmvb', 'wmv', "webm"],
        videoUploadURL: 'http://d7game.free.idcfengye.com/apix/z39base/upload',
        uploadVideoMaxSize: 50 * 1024 * 1024,
        uploadVideoMaxLength: 1,
        uploadVideoTimeout: 10 * 60 * 1000, // 10 min

        onlyTask: true,
        uploadCallbackURL: 'eval',
        mind: true,
        onload () {
          if (document.body.clientWidth < 1220) {
            that.testEditor.previewing()
          }
        },
        toolbarIconsClass: {
          saveBlog: 'fa-save'  // 指定一个FontAawsome的图标类
        },
        lang: {
          toolbar: {
            saveBlog: '保存任务'
          }
        }
      })
      that.testEditor.uploadImg = new UploadImg(that.testEditor)
      that.showPreview()
    },
    openTaskEditor: function (taskId, taskDesc) {
      $('#taskeditor').css({'right': 340, 'top': '-6%', 'left': '-3%'})
      $('#taskeditor').css({'z-index': 1100}).show()
      this.testEditor.setMarkdown(taskDesc)
      this.resizeEditor()
      window.editor = this.testEditor
      this.addMask()
    },
    closeTaskEditor: function () {
      $('#taskeditor').css({'z-index': 0}).hide()
      this.removeMask()
    },
    getEditorContent: function () {
      return this.testEditor.getMarkdown()
    }
  },
  mounted () {
    this.initEditor()
  }
}
