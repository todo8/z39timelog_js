import hotkeys from 'hotkeys-js';
import moment from "moment";
export let quickKey = function(obj,showAddHideBox) {
	var me = obj;
	var meta = 'ctrl'
	if(/macintosh|mac os x/i.test(navigator.userAgent)) {
		meta = 'command'
	}
	hotkeys.unbind('ctrl+n,esc,ctrl+h,t')
	hotkeys.unbind('esc')
	//关闭任务详情，关闭新建任务窗口
	hotkeys('esc', function(event, handler) {
		event.preventDefault()
		if(me.$refs.navBar4) {
			me.$refs.navBar4.creatTaskContainerType = 1
		}
		//原先每个页面还写了双击关闭的快捷键，没修改
		if(me.$refs.taskChildPage.taskEditor.status) {
			me.$refs.taskChildPage.handleEditorStatus(false)
			return
		}
		$('.task-detail-container').animate({
			right: '-400px',
			zIndex: '2000'
		}, 300)
		$('.kanban-main-warp .kanban-main-container').animate({
			right: '0px'
		}, 300)

	});
	//打开新建任务窗口
	hotkeys('ctrl+n', function(e) {
		e.preventDefault()
		me.$refs.navBar4.creatTaskContainerType = 2
		setTimeout(() => {
			$('#secondCreateTaskContainerInput').focus()
		}, 300)

	});
	//	隐藏任务箱
	hotkeys('ctrl+h', function(event) {
		event.preventDefault()
		var isActive=$('.task-main-container .container-left-content.task-box').hasClass('active')
		if(isActive){
			localStorage.setItem('task-box-status', 0)
			showAddHideBox('hide')
		}else{
			localStorage.setItem('task-box-status', 1)
			showAddHideBox('show')
		}
		

	});
	//	左下角重置
	hotkeys('t', function(event) {
		event.preventDefault()
		me.$refs.navBar3.navReset()
		return false;
	});
	//修改任务状态：开始,完成,暂停,取消. ctrl+s,d,w,c,
	hotkeys.unbind('shift+s,shift+d,shift+w,shift+c')
	let changeTaskState=function(state){
		var selectedId = me.$refs.taskChildPage.selectedTaskId;
		if(selectedId) {
			(async function(){
				await me.$refs.taskChildPage.$refs.taskDetailComponent.showTaskDetail(selectedId)
				me.$refs.taskChildPage.$refs.taskDetailComponent.changeTaskState(state,true)
				if(state='started'){
					$($(':focus').parents('li')[0]).trigger('dblclick')
					
				}
			})()
		}else{
			me.$message('请选择任务')
		}
	}
	hotkeys('shift+s', function(event) {
		event.preventDefault()
		changeTaskState('started')
	})
	hotkeys('shift+d', function(event) {
		event.preventDefault()
		changeTaskState('done')
	})
	hotkeys('shift+w', function(event) {
		event.preventDefault()
		changeTaskState('toggle')
	})
	hotkeys('shift+c', function(event) {
		event.preventDefault()
		changeTaskState('cancelled')
	})
	//日期切换的 ctrl+上下左右.
	hotkeys.unbind(`${meta}+up,w,${meta}+down,s,'${meta}+left,a,${meta}+right,d`)
//	切换路由
	var hotKeyChangeDateRoute = function(type) {
		let routesArr = [{
			path: '/todayTask',
			type: 'today'
		}, {
			path: '/weekTask',
			type: 'week'
		}, {
			path: '/monthTask',
			type: 'month'
		}, {
			path: '/yearTask',
			type: 'year'
		}]
		let routeIndex = 0;
		routesArr.forEach((item, index) => {
			if(me.$route.path == item.path) {
				routeIndex = index
			}
		})
		if(type == 'up') {
			if(routeIndex > 0) {
				me.$router.push({
					path: routesArr[routeIndex - 1].path
				})
				me.taskType = routesArr[routeIndex - 1].type
			}
		} else if(type == 'down') {
			if(routeIndex <3) {
				me.$router.push({
					path: routesArr[routeIndex + 1].path
				})
				me.taskType = routesArr[routeIndex + 1].type
			}
		}
	}
//	切换时间
	var hotKeyChangeRouteDate = function(type) {
		let routesArr = [{
			path: '/todayTask',
			type: 'today',
			unit:'days'
		}, {
			path: '/weekTask',
			type: 'week',
			unit:'weeks'
		}, {
			path: '/monthTask',
			type: 'month',
			unit:'months'
		}, {
			path: '/yearTask',
			type: 'year',
			unit:'years'
		}]
		let routeIndex = 0;
		routesArr.forEach((item, index) => {
			if(me.$route.path == item.path) {
				routeIndex = index
			}
		})
		let newDate;
		if(type == 'left') {
			newDate=moment(me.selectedDayDate).subtract(1,routesArr[routeIndex].unit).format('YYYY-MM-DD');
		} else if(type == 'right') {
			newDate=moment(me.selectedDayDate).add(1, routesArr[routeIndex].unit).format('YYYY-MM-DD');
		}
		if(newDate){
			me.$store.commit({ type: "z39/changeDate", value:newDate,dateType:'day' });
			me.getTasksList({day:newDate})
		}
	}
	hotkeys( `${meta}+up,w`, function(event) {
		event.preventDefault()
		hotKeyChangeDateRoute('down')
	})
	hotkeys( `${meta}+down,s`, function(event) {
		event.preventDefault()
		hotKeyChangeDateRoute('up')
	})
	hotkeys( `${meta}+left,a`, function(event) {
		event.preventDefault()
		hotKeyChangeRouteDate('left')
	})
	hotkeys( `${meta}+right,d`, function(event) {
		event.preventDefault()
		hotKeyChangeRouteDate('right')
	})
	//快速把任务延迟到下一天，数字小键盘的 + 和 1
	let changeTaskDate=function(event,type){
		event.preventDefault()
		var selectedId = me.$refs.taskChildPage.selectedTaskId;
		if(selectedId) {
			(async function(){
				await me.$refs.taskChildPage.$refs.taskDetailComponent.showTaskDetail(selectedId)
				let taskDate=me.$refs.taskChildPage.$refs.taskDetailComponent.detailObj.due;
				if(type=='add'){
					taskDate=moment(taskDate).add(1, 'days').format('YYYY-MM-DD');
				}else{
					taskDate=moment(taskDate).subtract(1, 'days').format('YYYY-MM-DD');
				}
				me.$refs.taskChildPage.$refs.taskDetailComponent.detailObj.due=taskDate;
				me.$refs.taskChildPage.$refs.taskDetailComponent.saveTask()
			})()
		}else{
			me.$message('请选择任务')
		}
	}
	hotkeys.unbind('*')
	//+-号不能监听
	hotkeys('*', function(event) {
		if(event.code=='NumpadAdd'&&event.keyCode=='107'){
			changeTaskDate(event,'add')
		}else if(event.code=='NumpadSubtract'&&event.keyCode=='109'){
			changeTaskDate(event,'subtract')
		}
	})
	//数字 123 切换任务状态：全部 未完成 已完成。
	hotkeys.unbind('1,2,3')
	let filter=function(type){
		var options=me.filterOptions;
		me.filterTaskStateChange(options[type-1].value,options[type-1].label)
	}
	hotkeys('1', function(event) {
		event.preventDefault()
		filter(1)
	})
	hotkeys('2', function(event) {
		event.preventDefault()
		filter(2)
	})
	hotkeys('3', function(event) {
		event.preventDefault()
		filter(3)
	})
	
	//项目
	hotkeys.unbind('r')
	hotkeys('r', function(event) {
		event.preventDefault()
		let projectHistoryObj=localStorage.getItem('projectClickHistory')?JSON.parse(localStorage.getItem('projectClickHistory')):{};
		if(projectHistoryObj.uid!=me.uid){
			return;
		}
		let historyArr=me.projectClickHistory;
		if(historyArr.length>1){
			if(me.$route.path.indexOf('/projectTask')>-1){
				me.projectClickHistory.pop();
			}
		}
		let pid=me.projectClickHistory[me.projectClickHistory.length-1];
		me.projectId = pid;
		me.$router.push({
			path: '/projectTask/' + pid,
		})
		localStorage.setItem('projectClickHistory',JSON.stringify(
			{
				uid:me.uid,
				list:me.projectClickHistory
			}
		))
	})
}