// import moment from "moment";
// import _ from "../../static/jslib/underscore-min.js";
import _ from "underscore";
import moment from "moment";

import Vue from 'vue';

import api from '../api';
import {
	mapState,
	mapActions
} from 'vuex'
// doTaskEdit,tagAdd,tagDel,tagSet,taskGet,taskAdd,taskSet,taskDel

export const mixinPc = {
	computed: {
		...mapState({
			projectDue: state => state.z39pc.projectDue,
		})
	},
	data() {
		return {
			addCardItemLoading: false,
			addCardItemTitle: '',
			dbTimer: null,
			escCountE: 0, //编辑器退出按键技术
			escCountT: 0, //任务详情退出按键计数 
		}
	},
	methods: {
		daysGet(sdata) {
			let _this = this;
			api.days(sdata).then(data => {
				if(data && data.errno == 0) {
					data = data.data;
					_this.$store.commit({
						type: "z39/daysDic",
						days: data,
						month: _this.showMonth
					});
				} else { //发生错误.
					_this.$message({
						message: data.errmsg,
						type: 'error'
					});
				}
			})
		},
		//拖拽添加
		dragAdd: function(event) {
			var _this = this;
			var tagKeyAdd = $(event.to).attr('tagKey');
			var tagVal = $(event.to).attr('tagVal');
			var id = $(event.clone).attr("taskId");
			var item = this.taskDic[id];
			var sdata = {};
			if(tagKeyAdd == "to" || tagKeyAdd == "noMember") sdata = {
				uid: tagVal
			};
			else if(tagKeyAdd != "unClassified") this.doTagSet(item, tagKeyAdd, tagVal);
			// this.doTaskEdit(item, tagKeyAdd, tagVal); //tagKeyAdd == "due"
			if(this.$route.path.indexOf("projectTask") != -1 && this.pid) {
				var fromTagKey = $(event.from).attr('tagKey');
				var fromTagId = this.tagDic[id + '_' + fromTagKey] ? this.tagDic[id + '_' + fromTagKey].id : '';
				if(fromTagKey && fromTagId) {
					this.tagDel({
						id: fromTagId,
						key: fromTagKey
					}, true, item);
				}
				console.error("projectDue", this.projectDue);
				if(this.projectDue && !item.pid) this.doTagSet(item, "due", this.projectDue);
				if(item.pid == this.pid && !sdata.uid && tagKeyAdd != "noMember"){}else{
					this.taskSet(Object.assign(sdata, {
						pid: this.pid,
						id: item.id
					}), item);
				};
				//this.doTaskEdit(item, 'pid',this.pid); //添加pid
				
			}
			//拖动到今日任务，标签被选中，则添加标签
			if(this.$route.name == "今日任务" || this.$route.name == "单任务") {
				
				var fromTagKey = $(event.from).attr('tagKey');
				if(fromTagKey == 'none') {

					var tagArr = [];
					$('#tagTmpList li.kanban-Item-container.active').each(function() {
						tagArr.push({
							taskid: id,
							key: $(this).attr('tagKey'),
							value: $(this).attr('value') || ''
						})
					})

					tagArr.forEach(function(obj) {
						_this.doTagSet(item, obj.key, obj.value)
					})
					//单列表拖动要添加pid
					if(this.$route.params.projectId) {
						_this.taskSet({
							pid: _this.$route.params.projectId,
							id: item.id
						}, item);
					}
				}
			}
			//四象限拖动
			if(this.$route.name == "今日任务") {
				// let fourArea = ['critical', 'high', 'low', 'minor'];
				// let {critical, high, low, minor} = item.tags ;
				var anotherKey1 = $(event.to).attr('anotherKey1');
				var anotherValue1 = $(event.to).attr('anotherValue1');
				//debugger
				var fromTagKey = $(event.from).attr('anotherKey1');
				var fromTagId = this.tagDic[id + '_' + fromTagKey] ? this.tagDic[id + '_' + fromTagKey].id : '';
				if(fromTagKey && fromTagId) {
					this.tagDel({
						id: fromTagId,
						key: fromTagKey
					}, true, item);
				}
				if(anotherKey1) {
					console.log('item', item, 'anotherKey1', anotherKey1, anotherValue1);
					this.doTagSet(item, anotherKey1, anotherValue1);
				}
			}
		},
		//跳到单
		jumpToOneTask: function(params) {
			//            this.$emit('transformCircle','单任务')
			this.$router.push({
				name: '单任务',
				params: params
			})
		},
		addTaskWithDueKV: function(key, value, taskdata, isProject) {
			if(isProject && (this.pid == '' || this.pid == null)) return this.$message({
				message: '请选择项目新建任务',
				type: 'warning'
			});
			var tags = {};
			if(key == "to") taskdata.uid = value; //以前是to=uid,现在修改为task.uid = uid ;
			else tags[key] = value;
			this.addTaskWithDue(tags, taskdata, isProject);
		},
		//    日月周年添加任务
		addTaskWithDue: function(tags, taskdata, isProject) {
			if(this.addCardItemLoading) {
				return
			};
			if(!this.addCardItemTitle) {
				this.$message({
					message: '请输入标题',
					type: 'warning'
				});
				return;
			}
			if(isProject && !taskdata.pid) {
				this.$message({
					message: '请选择项目',
					type: 'warning'
				});
				return;
			}
			this.addCardItemLoading = true;
			var params = {
				//uid: 1,
				title: this.addCardItemTitle
			}
			taskdata = taskdata || {};
			params = Object.assign(params, taskdata);
			tags.created= moment().format('YYYY-MM-DD HH:mm') ;
			this.taskAdd(params, tags).then(data => {
				this.addCardItemLoading = false;
				this.addCardItemTitle = ''
			}, data => {
				this.addCardItemLoading = false;
			});

		},
		showTmpDialog: function(tagItem) {
			if((this.$route.name == '今日任务' || this.$route.name == '单任务') && (tagItem.key == 'other' || tagItem.key == 'dev')) {
				this.$refs.tmpDialog.showDialog(tagItem);
			} else if(this.$route.name == '项目任务') {
				if(!this.pid) {
					this.showNotice('请先选中项目或创建项目', 'warning')
				} else {
					this.$refs.tmpDialog.showDialog(tagItem, this.pid);
				}

			}
		},
		//判断任务状态显示不同的颜色
		taskStatusClass: function(status) {
			switch(status) {
				case 'started':
					return 'doing';
				case 'toggle':
					return 'pause';
				case 'cancelled':
					return 'cancel';
				case 'done':
					return 'finish';
				default:
					return ''
					break;
			}
		},
		colorStatus: function(item) {
			var arr = []
			if(item['tags'] && item['tags']['timeDiff'] && item['tags']['timeDiff'].indexOf('_c') > 0) {
				arr.push('has-over');
			}
			if(item['tags'] && item['tags']['timeDiff'] && item['tags']['timeDiff'].indexOf('_a') > 0) {
				arr.push('has-before');
			}
			if(item.tags && item.tags.due) {
				switch(item.tags.due.length) {
					case 4:
						arr.push('has-year');
						break;
					case 7:
						arr.push('has-mon');
						break;
					case 10:
						arr.push('has-day');
						break;
					case 16:
						arr.push('has-clock');
						break;
					default:
						break;
				}
			}
			return arr;
		},
		checkLevel: function(obj) {
			let arr = [];
			for(let key in obj) {
				if(key == 'life') {
					arr.push('life');
				} else {
					switch(key) {
						case 'critical':
							arr.push('critical');
							break;
						case 'high':
							arr.push('high');
							break;
						case 'low':
							arr.push('low');
							break;
						default:
							break;
					}
				}
			}
			return arr;
		},
		changeTaskStatusBox: function(item, status) {

			item = this.taskDic[item.id];
			var state = 'done';
			if(status != undefined) {
				if(!status) {
					state = 'due';
				}
			} else if(!item.finished) {
				state = 'due';
			}
			this.doTaskEdit(item, state, moment().format("YYYY-MM-DD HH:mm"));
			//this.taskSet({state: state,id: item.id}, item);
		},
		//任务头部点击
		taskHeaderClick(index, list) {
			console.log(index)
			list.forEach((item, i) => {
				if(index == i) {
					item.active = true
				} else {
					item.active = false;
				}
			})
		},
		//alt + up 移动任务
		up: function(index, lists, tag) {
			if(index === 0) {
				this.$message('任务已经到达顶部了')
				return false
			}
			let newList = [...lists]
			let temp = newList[index - 1]
			let id = temp.id
			document.getElementById(tag + id).focus()
			
			if(this.changeSelectedTask) {
				this.changeSelectedTask(temp)
			}
		},

		//alt + down 移动任务
		down: function(index, lists, tag) {
			let newList = [...lists]
			if(index + 1 === newList.length) {
				this.$message('任务已经在最后了')
				return false
			}
			let temp = newList[index + 1]
			let id = temp.id
			document.getElementById(tag + id).focus()
			
			if(this.changeSelectedTask) {
				this.changeSelectedTask(temp)
			}
		},
		//left 任务列向左
		left: function(index, lists, tag1) {
			event.preventDefault()
			if(index === 0) {
				return false
			}
			
			let newList = [...lists]
			newList[index].active = false;
			newList[index - 1].active = true;
			let tag=tag1||newList[index - 1].tag||newList[index - 1].key;
			if(newList[index - 1].list.length > 0) {
				
				let temp = newList[index - 1].list[0]
				let id = temp.id
				document.getElementById(tag + id).focus()
				if(this.changeSelectedTask) {
					this.changeSelectedTask(temp)
				}
				
			} else {
				document.getElementById(tag + (index - 1)).focus()
				
			}

		},
		right: function(index, lists, tag1) {
			event.preventDefault()
			if(index === lists.length - 1) {
				return false
			}
			let newList = [...lists]
			newList[index].active = false;
			newList[index + 1].active = true;
			let tag=tag1||newList[index + 1].tag||newList[index + 1].key;
			if(newList[index + 1].list.length > 0) {
				let temp = newList[index + 1].list[0]
				let id = temp.id;
				document.getElementById(tag + id).focus()
				if(this.changeSelectedTask) {
					this.changeSelectedTask(temp)
				}
			} else {
				document.getElementById(tag + (index + 1)).focus()
			}
		},
		setTaskToToday(item){
			var date=moment().format('YYYY-MM-DD')
			this.doTagSet(item, 'due', date)
		},
		//双击ESC退出编辑框
		escEditor: function() {
			return
			//debugger
//			$("#taskeditor").css("left", "0px")
//			if(this.escCountE == 0) {
//				this.escCountE++;
//			} else {
				this.dbTimer = setTimeout(() => {
					for(let i = 0; i < this.$children.length; i++) {
						if(this.$children[i].changeSwitch) {
							this.$children[i].changeSwitch()
							$('.task-detail-container').focus()
							return;
						}
					}
				}, 300);
				this.escCountE = 0;
//			}
//			var ett = this;
//			setTimeout(function() {
//				ett.escCountE = 0;
//			}, 2000) //2s内如果不双击则返回0
		},

		//双击ESC退出任务详情
		escTaskDetail() {
			return;
			if(this.escCountT == 0) {
				this.escCountT++;
			} else {
				clearTimeout(this.dbTimer);
				if(this.dbTimer > 300 || this.dbTimer == null) {
					$('.task-detail-container').animate({
						right: '-400px'
					}, 300)
				}
				this.escCountT = 0;
			}
			var ett = this;
			setTimeout(function() {
				ett.escCountE = 0;
			}, 2000) //2s内如果不双击则返回0   
		}

	}

}