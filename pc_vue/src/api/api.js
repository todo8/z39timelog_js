import axios from 'axios';
import wrapper from 'axios-cache-plugin'
import Vue from 'vue';

var isMobile = true; // h5 app 小程序 都使用token
export var token = localStorage.getItem('token') || '';
var uid = 0;
console.log("api token:", token);
import store from '../common/store.js';

var base;
if (process.env.NODE_ENV == "development") { //开发环境
  // base = 'http://192.168.0.104:8361';
  base = 'http://d7game.free.idcfengye.com'; //备选1
  //base = 'http://7dtime.tunnel.echomod.cn'
  var href = window.location.href;
  if (href.includes("192.") || href.includes("127.")) { //|| href.includes("localhost")
    base = href.substr(0, href.indexOf(":80")) + ":8361";
    console.log("api:", base);
  }
  // base = 'https://todo8.cn'
} else {
  base = 'https://todo8.cn' //发布环境
  if( window.location.host.includes('todo8.cn') ) base = "https://todo8.cn"
  // base = `${window.location.protocol}//${window.location.host}`;
}
export const globalUrl = base;

axios.defaults.withCredentials = true;
axios.defaults.timeout = 20000; //http默认超时时间是60秒
//在main.js设置全局的请求次数，请求的间隙
axios.defaults.retry = 3;
axios.defaults.retryDelay = 20000;
// Add a request interceptor
axios.interceptors.request.use(function(config) {
  // Do something before request is sent
  if (token) config.headers.token = token ;
  if ((config.params && !config.params.ignore) || (config.data && !config.data.ignore)) store.commit('updateLoadingStatus', {
    isLoading: true,
    text: "请求中"
  });
  return config;
}, function(error) {
  // Do something with request error
  return Promise.reject(error);
});
axios.interceptors.response.use(function(response) {
  store.commit('updateLoadingStatus', {
    isLoading: false
  });
  return response;
}, function axiosRetryInterceptor(err) {
  var config = err.config;
  // If config does not exist or the retry option is not set, reject
  if (!config || !config.retry) {
    store.commit('updateLoadingStatus', {
      isLoading: false
    });
    return Promise.resolve({
      errno: 1002,
      errmsg: "网络超时"
    }); //Promise.reject(err);
  }
  // Set the variable for keeping track of the retry count
  config.__retryCount = config.__retryCount || 0;
  // Check if we've maxed out the total number of retries
  if (config.__retryCount >= config.retry) {
    // Reject with the error
    store.commit('updateLoadingStatus', {
      isLoading: false
    });
    return Promise.resolve({
      errno: 1002,
      errmsg: "网络超时"
    }); //Promise.reject(err);
  }
  // Increase the retry count
  config.__retryCount += 1;
  // Create new promise to handle exponential backoff
  var backoff = new Promise(function(resolve) {
    setTimeout(function() {
      resolve();
    }, config.retryDelay || 1);
  });
  // Return the promise in which recalls axios to retry the request
  return backoff.then(function() {
    return axios(config);
  });
});

let http = wrapper(axios, {
  maxCacheSize: 15
})
http.__addFilter(/api\/z39/)
http.__addFilter(/apix\/z39/)
//---------------系统通用接口---------------
// token登录
export const loginToken = params => {
  return axios.post(`${ base }/center/public/logintoken`, params).then(res => {
    let data = res.data;
    if (data.data && data.data.uid) {
      token = params.token;
      uid = data.data.uid;
      if (params.token != data.data.token && data.data.token) {
        token = data.data.token;
      }
      localStorage.token = token;
      Vue.$cookies.set('token', token );
      initChat(base, data.data);
      // console.log("token:", token);
    }
    if (!data.errno) this.checkAppRegist();
    return Promise.resolve(data);
  });
};
// 登录
export const login = params => {
  return axios.post(`${ base }/center/public/loginmob`, params).then(res => {
    let data = res.data;
    if (data.data && data.data.token) {
      token = data.data.token;
      uid = data.data.uid;
      localStorage.token = token;
      Vue.$cookies.set('token', token );
      initChat(base, data.data);
      // console.log("token:", token);
    }
    if (!data.errno) this.checkAppRegist();
    return Promise.resolve(data);
  });
};
// app登录,调用微信登录,QQ登录.
export const loginApp = params => {
  return axios.post(`${ base }/apix/7dtime/loginapp`, params).then(res => {
    let data = res.data;
    if (data.data && data.data.token) {
      token = data.data.token;
      uid = data.data.uid;
      localStorage.token = token;
      Vue.$cookies.set('token', token );
      // console.log("token:", token);
    }
    if (!data.errno) this.checkAppRegist();
    return Promise.resolve(data);
  });
};

// 登录-手机验证码登录,可能未注册
export const loginCode = params => {
  return axios.post(`${ base }/center/public/logincode`, params).then(res => {
    let data = res.data;
    if (data.data && data.data.token) {
      token = data.data.token;
      uid = data.data.uid;
      localStorage.token = token;
      Vue.$cookies.set('token', token );
      initChat(base, data.data);
      // console.log("token:", token);
    }
    if (!data.errno) this.checkAppRegist();
    return Promise.resolve(data);
  });
};
// 退出登录
export const logout = params => {
  return axios.post(`${ base }/center/public/logoutmob`, params).then(res => {
    token = "";
    uid = 0;
    return Promise.resolve(res.data);
  });
};

// 检查手机号是否注册, 没注册直接显示注册码
export const checkUser = params => {
  return axios.post(`${ base }/center/public/checkuser`, params).then(res => res.data);
};

// 游客注册接口,方便不用注册就可以进行浏览使用.
export const guestRegist = params => {
  return axios.post(`${ base }/apix/7dtime/guestregist`, params).then(res => {
    let data = res.data;
    if (data.data && data.data.token) {
      token = data.data.token;
      uid = data.data.uid;
      localStorage.setItem('guestToken', token);
    }
    // 初始化聊天模块
    initChat(base, data.data);
    return data;
  });
};

// 注册
export const reg = params => {
  return axios.post(`${ base }/center/weixin/organizing`, params).then(res => {
    let data = res.data;
    if (data.data && data.data.token) {
      token = data.data.token;
      uid = data.data.uid;
      localStorage.token = token;
      Vue.$cookies.set('token', token );
    }
    if (!data.errno) this.checkAppRegist();
    return Promise.resolve(data);
  });
  // return axios.post(`${ base }/center/weixin/organizing`, params).then(res => res.data);
};
// 获取手机注册验证码
export const mobCode = params => {
  return axios.post(`${ base }/ext/dayu/index/verifycodesend`, params).then(res => res.data);
};

export const smsBind = params => {
  return axios.post(`${ base }/ext/dayu/index/smsbind`, params).then(res => res.data);
};
// 修改密码
export const updatepwd = params => {
  return axios.post(`${ base }/center/seting/updatepassword`, params).then(res => res.data);
};
// 通过手机验证码修改密码
export const updatePwdMob = params => {
  return axios.post(`${ base }/ext/dayu/index/smsupdatepwd`, params).then(res => res.data);
};


export const jssdk = params => {
  return axios.post(`${ base }/apix/7dtime/jssdk`, params).then(res => res.data);
};
export const authurl = params => {
  return axios.post(`${ base }/apix/7dtime/authurl`, params).then(res => res.data);
};
//  ------------------------- 建议 rest相关处理 -------------------------
export const suggestGet = params => {
  return axios.get(`${ base }/api/d7suggest`, {"params": params}).then(res => res.data);
};
export const suggestAdd = params => {
  return axios.post(`${ base }/api/d7suggest`, params).then(res => res.data);
};
export const suggestSet = params => {
  return axios.put(`${ base }/api/d7suggest`, params).then(res => res.data);
};
export const suggestDel = params => {
  return axios.delete(`${ base }/api/d7suggest`, {"params": params}).then(res => res.data);
};
//  ------------------------- 合作 rest相关处理 -------------------------
export const cooperateGet = params => {
  return axios.get(`${ base }/api/d7cooperate`, {"params": params}).then(res => res.data);
};
export const cooperateAdd = params => {
  return axios.post(`${ base }/api/d7cooperate`, params).then(res => res.data);
};
export const cooperateSet = params => {
  return axios.put(`${ base }/api/d7cooperate`, params).then(res => res.data);
};
export const cooperateDel = params => {
  return axios.delete(`${ base }/api/d7cooperate`, {"params": params}).then(res => res.data);
};
//---------------系统通用接口 结束---------------

export const info = params => {
  return axios.post(`${ base }/apix/z43phone/info`, params).then(res => res.data);
};
export const phones = params => {
  return axios.post(`${ base }/apix/z43phone/phones`, params).then(res => res.data);
};
export const call = params => {
  return axios.post(`${ base }/apix/z43phone/call`, params).then(res => res.data);
};
export const callme = params => {
  return axios.post(`${ base }/apix/z43phone/callme`, params).then(res => res.data);
};
export const getmp3 = params => {
  return axios.post(`${ base }/apix/z43phone/getmp3`, params).then(res => res.data);
};
export const delinfo = params => {
  return axios.post(`${ base }/apix/z43phone/delinfo`, params).then(res => res.data);
};
export const order = params => {
  return axios.post(`${ base }/apix/z43phone/order`, params).then(res => res.data);
};
export const getuser = params => {
  return axios.post(`${ base }/apix/7dtime/getuser`, params).then(res => res.data);
};

// 群查询
export const groups = params => {
  return axios.get(`${ base }/api/z43group`, {"params": params}).then(res => res.data);
};
// 群创建
export const groupAdd = params => {
  return axios.post(`${ base }/api/z43group`, params).then(res => res.data);
};
// 群修改
export const groupSet = params => {
  return axios.put(`${ base }/api/z43group`, params).then(res => res.data);
};
// 群删除
export const groupDel = params => {
  return axios.delete(`${ base }/api/z43group`, {"params": params}).then(res => res.data);
};

// 群功能-短信
export const mobinfo = params => {
  return axios.post(`${ base }/api/z43mobinfo`, params).then(res => res.data);
};


// 成员查询
export const friends = params => { //和z43phone/phones 一样
  return axios.get(`${ base }/apix/z39time/friends`, params).then(res => res.data);
};
// 成员查询
export const getProjectFriends = params => { //和z43phone/phones 一样
  return axios.get(`${ base }/api/z39friend`, {"params": params}).then(res => res.data);
};
// 成员创建
export const friendAdd = params => {
  return axios.post(`${ base }/api/z43friend`, params).then(res => res.data);
};
// 成员修改
export const friendSet = params => {
  return axios.put(`${ base }/api/z43friend`, params).then(res => res.data);
};
// 成员删除
export const friendDel = params => {
  return axios.delete(`${ base }/api/z43friend`, {"params": params}).then(res => res.data);
};

// 群成员导入
export const friendAddGrp = params => {
  return axios.post(`${ base }/api/z43friends/addgrp`, params).then(res => res.data);
};
// 通讯录导入
export const friendsAdd = params => {
  return axios.post(`${ base }/apix/z43friends/add`, params).then(res => res.data);
};
// 群成员邀请
export const invitefriends = params => {
  return axios.get(`${ base }/api/z39invite`, {"params": params}).then(res => res.data);
};
export const getProjectMember = params => {
  return axios.get(`${ base }/api/z39time/project/get`, {"params": params}).then(res => res.data);
};
export const getInviteTitle = params => {
  return axios.get(`${ base }/apix/z39time/getinvitetitle`, {"params": params}).then(res => res.data);
};

// 获取  app自动记录的任务
export const taskautoGet = params => {
  return axios.get(`${ base }/api/z39taskauto`, {"params": params}).then(res => res.data);
};
export const taskautoAdd = params => {
  return axios.post(`${ base }/api/z39taskauto`, params).then(res => res.data);
};
export const taskautoPut = params => {
  return axios.put(`${ base }/api/z39taskauto`, params).then(res => res.data);
};
export const taskautoDel = params => {
  return axios.delete(`${ base }/api/z39taskauto`, {"params": params}).then(res => res.data);
};


// 获取登录初始数据.
export const usertask = params => {
  return axios.get(`${ base }/apix/z39time/usertask`, {"params": params}).then(res => {
    token = token || localStorage.getItem('token');
    // console.log("token:", token);
    return Promise.resolve(res.data);
  });
};
//检查是否登陆
var timer = null;
export const checkLogin = params => {
  return axios.get(`${ base }/apix/z39base/userinfo`, {"params": params}).then(res => {
    if (timer) {
      clearTimeout(timer)
    }
    timer = setTimeout(() => {
      initChat(base, res.data.data)
    }, 500);
    return res.data
  });
};
function initChat( base ,data ){
  try{ initChatModule(base, data)} catch(e){ console.warn('initChatModule，聊天模块可能关闭或没有加载') }
}
export const days = params => {
  return axios.get(`${ base }/apix/z39time/days`, {"params": params}).then(res => res.data);
};
export const sortdayGet = params => {
  return axios.get(`${ base }/apix/z39time/sortday`, {"params": params}).then(res => res.data);
};
export const sortdayAdd = params => {
  return axios.get(`${ base }/apix/z39time/sortdayadd`, {"params": params}).then(res => res.data);
};
// 获取最近常用标签
export const tagsrec = params => {
  return axios.get(`${ base }/apix/z39time/tagsrec`, {"params": params}).then(res => res.data);
};


//  ------------------------- 任务 rest相关处理 -------------------------
export const tasksGet = params => {
  return axios.get(`${ base }/api/z39tasks`, {"params": params}).then(res => res.data);
};
export const tasksAdd = params => {
  var item;
  for (var i = params.tasks.length - 1; i >= 0; i--) {
    item = params.tasks[i];
    if (item.title && item.title.length > 100) return Promise.resolve({
      errno: 1001,
      errmsg: "标题最长100字"
    });
    if (item.desc && item.desc.length > 9000) return Promise.resolve({
      errno: 1001,
      errmsg: "描述最长9000字"
    });
  };
  return axios.post(`${ base }/api/z39tasks`, params).then(res => res.data);
};
export const taskGet = params => {
  return axios.get(`${ base }/api/z39task`, {"params": params}).then(res => res.data);
};
export const taskAdd = params => {
  if (!params.title) return Promise.resolve({
    errno: 1001,
    errmsg: "请输入标题"
  });
  if (params.title && params.title.length > 100) return Promise.resolve({
    errno: 1001,
    errmsg: "标题最长100字"
  });
  if (params.desc && params.desc.length > 9000) return Promise.resolve({
    errno: 1001,
    errmsg: "描述最长9000字"
  });
  return axios.post(`${ base }/api/z39task`, params).then(res => res.data);
};
export const taskSet = params => {
  if (params.title != undefined && params.title.length == 0) return Promise.resolve({
    errno: 1001,
    errmsg: "请输入标题"
  });
  if (params.title && params.title.length > 100) return Promise.resolve({
    errno: 1001,
    errmsg: "标题最长100字"
  });
  if (params.desc && params.desc.length > 9000) return Promise.resolve({
    errno: 1001,
    errmsg: "描述最长9000字"
  });
  return axios.put(`${ base }/api/z39task`, params).then(res => res.data);
};
export const taskDel = params => {
  return axios.delete(`${ base }/api/z39task`, {"params": params}).then(res => res.data);
};
export const taskMend = params => {
  return axios.get(`${ base }/apix/z39time/taskmend`, {"params": params}).then(res => res.data);
};
export const taskOne = params => {
  return axios.get(`${ base }/apix/z39time/taskone`, {"params": params}).then(res => res.data);
};

export const Likeset = params => {
  return axios.get(`${ base }/apix/z39base/suggestvote`, {"params": params}).then(res => res.data);
};
export const setTaskPlan = params => {
  return axios.put(`${ base }/apix/z39time/plan`, params).then(res => res.data);
};
//  ------------------------- 项目 rest相关处理 -------------------------
export const projects = params => {
  return axios.get(`${ base }/api/z39projects`, {"params": params}).then(res => res.data);
};
export const projectGet = params => {
  return axios.get(`${ base }/api/z39project`, {"params": params}).then(res => res.data);
};
export const projectAdd = params => {
  if (params.title && params.title.length > 15) return Promise.resolve({
    errno: 1001,
    errmsg: "标题最长15字"
  });
  if (params.desc && params.desc.length > 100) return Promise.resolve({
    errno: 1001,
    errmsg: "描述最长100字"
  });
  return axios.post(`${ base }/api/z39project`, params).then(res => res.data);
};
export const projectSet = params => {
  if (params.title && params.title.length > 15) return Promise.resolve({
    errno: 1001,
    errmsg: "标题最长15字"
  });
  if (params.desc && params.desc.length > 100) return Promise.resolve({
    errno: 1001,
    errmsg: "描述最长100字"
  });
  return axios.put(`${ base }/api/z39project`, params).then(res => res.data);
};
export const projectDel = params => {
  return axios.delete(`${ base }/api/z39project`, {"params": params}).then(res => res.data);
};

//  ------------------------- 项目成员邀请 处理 -------------------------
export const inviteGet = params => {
  return axios.get(`${ base }/api/z39invite`, {"params": params}).then(res => res.data);
};
export const inviteTitleGet = params => {
  return axios.get(`${ base }/apix/z39time/getinvitetitle`, {"params": params}).then(res => res.data);
};

//  ------------------------- 标签 rest相关处理 -------------------------
export const tagGet = params => {
  return axios.get(`${ base }/api/z39tag`, {"params": params}).then(res => res.data);
};
export const tagAdd = params => {
  return axios.post(`${ base }/api/z39tag`, params).then(res => res.data);
};
export const tagSet = params => {
  return axios.put(`${ base }/api/z39tag`, params).then(res => res.data);
};
export const tagDel = params => {
  return axios.delete(`${ base }/api/z39tag`, {"params": params}).then(res => res.data);
};

export const tagsAdd = params => {
  return axios.post(`${ base }/api/z39tags`, params).then(res => res.data);
};

//  ------------------------- 标签模板 rest相关处理 -------------------------
export const tagTmpAdd = params => {
  return axios.post(`${ base }/api/z39tagtmp`, params).then(res => res.data);
};

export const tagTmpPut = params => {
  return axios.put(`${ base }/api/z39tagtmp`, params).then(res => res.data);
};
export const tagTmpGet = params => {
  return axios.get(`${ base }/api/z39tagtmp`, {"params": params}).then(res => res.data);
};
export const tagTmpDel = params => {
  return axios.delete(`${ base }/api/z39tagtmp`, {"params": params}).then(res => res.data);
};
//  ------------------------- 日报文章 rest相关处理 -------------------------
export const blogs = params => {
  return axios.get(`${ base }/api/z39blogs`, {"params": params}).then(res => res.data);
};
export const blogGet = params => {
  return axios.get(`${ base }/api/z39blog`, {"params": params}).then(res => res.data);
};
export const blogAdd = params => {
  return axios.post(`${ base }/api/z39blog`, params).then(res => res.data);
};
export const blogSet = params => {
  return axios.put(`${ base }/api/z39blog`, params).then(res => res.data);
};
export const blogContentUpdate = params => {
  return axios.put(`${ base }/api/z39blog`, params).then(res => res.data);
}
export const updateDayMd = params => {
  return axios.get(`${ base }/apix/z39md/daymd`, {"params": params}).then(res => res.data);
}

export const blogDel = params => {
  return axios.delete(`${ base }/api/z39blog`, {"params": params}).then(res => res.data);
};
// 获取周报
export const weekGet = params => {
  return axios.get(`${ base }/apix/z39report/week`, { params }).then(res => res.data);
};

//  ------------------------- 项目成员 rest相关处理 -------------------------
// export const members = params => {
//   token && (params = params || {}) && (params.token = token);
//   return axios.get(`${ base }/api/z39friend`, { "params": params }).then(res => res.data);
// };
export const memberGet = params => {
  return axios.get(`${ base }/api/z39friend`, {"params": params}).then(res => res.data);
};
export const memberAdd = params => {
  return axios.post(`${ base }/api/z39friend`, params).then(res => res.data);
};
export const memberSet = params => {
  return axios.put(`${ base }/api/z39friend`, params).then(res => res.data);
};
export const memberDel = params => {
  return axios.delete(`${ base }/api/z39friend`, {"params": params}).then(res => res.data);
};

//  ------------------------- 重复任务 rest相关处理 -------------------------
export const repeatGet = params => {
  return axios.get(`${ base }/api/z39repeat`, {"params": params}).then(res => res.data);
};
export const repeatAdd = params => {
  return axios.post(`${ base }/api/z39repeat`, params).then(res => res.data);
};
export const repeatSet = params => {
  return axios.put(`${ base }/api/z39repeat`, params).then(res => res.data);
};
export const repeatDel = params => {
  return axios.delete(`${ base }/api/z39repeat`, {"params": params}).then(res => res.data);
};

//  ------------------------- 习惯养成 rest相关处理 -------------------------
export const habitGet = params => {
  return axios.get(`${ base }/api/z39habit`, {"params": params}).then(res => res.data);
};
export const habitAdd = params => {
  return axios.post(`${ base }/api/z39habit`, params).then(res => res.data);
};
export const habitSet = params => {
  return axios.put(`${ base }/api/z39habit`, params).then(res => res.data);
};
export const habitDel = params => {
  return axios.delete(`${ base }/api/z39habit`, {"params": params}).then(res => res.data);
};

//  ------------------------- 标签 rest相关处理 -------------------------
export const tagitemsGet = params => {
  return axios.get(`${ base }/api/z39tagitem`, {"params": params}).then(res => res.data);
};
export const tagitemsAdd = params => {
  return axios.post(`${ base }/api/z39tagitem`, params).then(res => res.data);
};

export const tagitemGet = params => {
  return axios.get(`${ base }/api/z39tagitem`, {"params": params}).then(res => res.data);
};
export const tagitemAdd = params => {
  return axios.post(`${ base }/api/z39tagitem`, params).then(res => res.data);
};
export const tagitemSet = params => {
  return axios.put(`${ base }/api/z39tagitem`, params).then(res => res.data);
};
export const tagitemDel = params => {
  return axios.delete(`${ base }/api/z39tagitem`, {"params": params}).then(res => res.data);
};
//  ------------------------- 任务音频录音 rest相关处理 -------------------------
export const audioGet = params => {
  return axios.get(`${ base }/api/z39audio`, {"params": params}).then(res => res.data);
};
export const audioAdd = params => {
  return axios.post(`${ base }/api/z39audio`, params).then(res => res.data);
};
export const audioSet = params => {
  return axios.put(`${ base }/api/z39audio`, params).then(res => res.data);
};
export const audioDel = params => {
  return axios.delete(`${ base }/api/z39audio`, {"params": params}).then(res => res.data);
};
export const audioTTS = params => {
  return axios.get(`${ base }/apix/z39sound/tts`, {"params": params}).then(res => res.data);
};
//  ------------------------- 任务提醒的闹钟 rest相关处理 -------------------------
export const clockGet = params => {
  return axios.get(`${ base }/api/z39clock`, {"params": params}).then(res => res.data);
};
export const clockAdd = params => {
  return axios.post(`${ base }/api/z39clock`, params).then(res => res.data);
};
export const clockSet = params => {
  return axios.put(`${ base }/api/z39clock`, params).then(res => res.data);
};
export const clockDel = params => {
  return axios.delete(`${ base }/api/z39clock`, {"params": params}).then(res => res.data);
};
// 任务提醒方式的开通状态获取
export const noticeStateGet = params => {
  return axios.get(`${ base }/apix/z39notice/noticestate`, {"params": params}).then(res => res.data);
};

// 导入任务
export const importTask = params => {
  return axios.post(`${ base }/apix/z39time/importtask`, params).then(res => res.data);
};
// 导入excel

export const importExcel = params => {
  return axios.post(`${ base }/apix/z39time/importexcel`, params).then(res => res.data);
};


// app消息推送用的, 保存用户设备client id
export const appRegistAdd = params => {
  return axios.get(`${ base }/apix/z39time/appregist`, {"params": params}).then(res => res.data);
};

// 上传图片文件，现在粘贴base64上传成功
export const upload = file => {
  var formData = new FormData();
  formData.append("image", file);
  return axios.post(`${ base }/apix/z39base/upload`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  }).then(res => res.data);
};

// 检查是否需要调用appRegistAdd
export const checkAppRegist = params => {
  if (!isApp || !uid) return;
  try { //部分参数只有在app中才有,h5和pc版本没有这变量.会报错.
    if (!clientid) return;
    let data = {
      uid: uid,
      cid: clientid,
      device: deviceToken,
      type: appType
    };
    this.appRegistAdd(data);
  } catch (e) {

  }
};


export const ruanwenList = params => {
  return axios.get(`${ base }/api/z39ruanwen`, {"params": params}).then(res => res.data);
};

export const putRuanwen = params => {
  return axios.put(`${ base }/api/z39ruanwen`, params).then(res => res.data);
};
