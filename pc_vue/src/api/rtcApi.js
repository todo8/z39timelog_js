import api from '@/api'
export default {
  blob: '',
  recorder: null, //录制对象
  recording: false, //避免录制出错的标志
  video: {
    width: 3840,
    height: 2160
  },
  init(cb, errcb) {
    if (!navigator.getDisplayMedia && !navigator.mediaDevices.getDisplayMedia) {
      var error = '该浏览器不支持录屏功能。';
      alert(error);
      return;
    }
    if (!this.recorder) {
      this.captureScreen((screen) => {
        this.recorder = RecordRTC(screen, {
          type: 'video'
        });
        // this.recorder.screen = screen;
        this.start(cb);
      }, errcb);
    } else {
      this.start(cb);
    }
  },
  start(cb) {
    if (!this.recorder) {
      this.init(cb)
      return
    }
    if (this.recording) {
      alert('视频录制中，请先结束录制！')
      return;
    }
    this.recording = true;
    this.recorder.startRecording();
    cb && cb();
  },
  stop(cb) {
    this.recorder && this.recorder.stopRecording(() => {
      this.blob = this.recorder.getBlob();
      // this.recorder.screen.stop();
      cb && cb(this.blob);
      this.recording = false;
      this.reset();
    });
  },
  pause(cb) {
    this.recorder && this.recorder.pauseRecording();
  },
  resume() {
    this.recorder && this.recorder.resumeRecording();
  },
  captureScreen(callback, errcallback) {
    this.invokeGetDisplayMedia((screen) => {
      navigator.mediaDevices.getUserMedia({audio:true}).then((mic) => {
        screen.addTrack(mic.getTracks()[0]);
        this.addStreamStopListener(screen, () => {
          this.stop();
        });
        callback(screen);
      });
    }, function (error) {
      console.log('视频录制失败。\n' + error);
      errcallback && errcallback()
    });
  },
  invokeGetDisplayMedia(success, error) {
    let displaymediastreamconstraints = {
      video: this.video
    };
    if (navigator.mediaDevices.getDisplayMedia) {
      navigator.mediaDevices.getDisplayMedia(displaymediastreamconstraints).then(success).catch(error);
    } else {
      navigator.getDisplayMedia(displaymediastreamconstraints).then(success).catch(error);
    }
  },
  addStreamStopListener(stream, callback) {
    stream.addEventListener('ended', () => {
      callback();
      callback = function () {};
    }, false);
    stream.addEventListener('inactive', () => {
      callback();
      callback = function () {};
    }, false);
    stream.getTracks().forEach((track) => {
      track.addEventListener('ended', () => {
        callback();
        callback = function () {};
      }, false);
      track.addEventListener('inactive', () => {
        callback();
        callback = function () {};
      }, false);
    });
  },
  save() {
    this.recorder && this.recorder.save('file' + (new Date()).getTime());
    this.recording = false;
  },
  reset() {
    this.recorder && this.recorder.reset();
    this.recording = false;
  },
  destroy() {
    this.recorder && this.recorder.destroy();
    this.recorder = null;
    this.recording = false;
  },
  uploadVideo(file) {
    return new Promise((resolve, reject) => {
      var form = document.createElement('form').setAttribute('enctype', 'multipart/form-data')
      var formData = new FormData(form)
      formData.append('testsf', file)
      var xhr = new XMLHttpRequest()
      xhr.open('POST', `${api.globalUrl}/apix/z39base/upload`, true)
      xhr.timeout = 10 * 60 * 1000
      xhr.withCredentials = true
      xhr.ontimeout = function (e) {
        resolve('系统响应过慢， 视频' + file.name + ' 上传失败!')
      }
      xhr.onload = function (event) {
        var responseText = event.currentTarget.responseText
        var json = JSON.parse(responseText)
        if (json.errno === 0) {
          resolve(json.data)
        } else {
          resolve(json.errmsg)
        }
      }
      xhr.send(formData)
    })
  }
}
