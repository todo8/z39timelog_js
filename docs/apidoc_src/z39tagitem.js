/**
 * @apiDefine z39tagitem
 * @apiVersion 1.0.0
 *
 */

/**
 * @api {get} /api/z39tagitem?id=:id 获取标签
 * @apiVersion 1.0.0
 * @apiName Getz39tagitem
 * @apiGroup z39tagitem
 * @apiPermission code_system
 *
 * @apiDescription 获取单个标签的详细信息.
 * /api/z39tagitem?id=xx 获取单个标签数据
 * /api/z39tagitem 获取用户所有标签+系统默认标签
 * 
 * 标签表 z39tagitem: id key value name type position count uid
 * 部分字段暂未使用 type position count 
 *
 * @apiParam {Number} [id] 标签id
 *
 * @apiExample 示例用法:
 * curl -i https://www.7dtime.com/api/z39tagitem?id=4711
 *
 * @apiSuccess {Number}   id            标签id .
 * @apiSuccess {String}   key           标签key,英文或拼音 .
 * @apiSuccess {String}   value    标签值
 * @apiSuccess {String}   name          标签中文名
 * @apiSuccess {String}   type     分类
 * @apiSuccess {String}   position   行业 职位
 * @apiSuccess {Number}   count   使用次数, 每次跟新一次,减少数据库压力. tag_tagid=1 的个数可判断.
 * @apiSuccess {Number}   uid           用户uid
 * 
 * @apiError NoAccessRight Only authenticated Admins can access the data.
 * @apiError z39tagitemNotFound   The <code>id</code> of the z39tagitem was not found.
 *
 * @apiErrorExample Response (example):
 *     HTTP/1.1 401 Not Authenticated
 *     {
 *       "error": "NoAccessRight"
 *     }
 */
function getz39tagitem() { return; }

/**
 * @api {post} /api/z39tagitem 添加标签
 * @apiVersion 1.0.0
 * @apiName Postz39tagitem
 * @apiGroup z39tagitem
 * @apiPermission none
 *
 * @apiDescription In this case "apiErrorStructure" is defined and used.
 * Define blocks with params that will be used in several functions, so you dont have to rewrite them.
 *
 * @apiParam {String}   key           唯一key,英文或拼音 .
 * @apiParam {String}   [value]    标签值
 * @apiParam {String}   name          标签中文名
 * @apiParam {String}   [type]     分类
 * @apiParam {String}   [position]   行业 职位
 *
 * @apiSuccess {Number} id     新的标签id z39tagitems-ID.
 *
 * @apiUse z39tagitem
 */
function postz39tagitem() { return; }

/**
 * @api {put} /api/z39tagitem?id=:id 修改标签
 * @apiVersion 1.0.0
 * @apiName Putz39tagitem
 * @apiGroup z39tagitem
 * @apiPermission none
 *
 * @apiDescription 创建标签后基本不会修改.现在pc和手机版系统都没有提供修改标签.
 *
 * @apiParam {Number}   id    标签id
 * @apiParam {String}   value    标签值
 * @apiParam {String}   name          标签中文名
 * @apiParam {String}   [type]     分类
 * @apiParam {String}   [position]   行业 职位
 *
 * @apiUse z39tagitem
 */
function putz39tagitem() { return; }

/**
 * @api {delete} /api/z39tagitem?id=:id 删除标签
 * @apiVersion 1.0.0
 * @apiName Deletez39tagitem
 * @apiGroup z39tagitem
 * @apiPermission none
 *
 * @apiDescription 删除标签,有接口但没提供给用户使用.
 *
 * @apiParam {Number}     id    标签id
 *
 * @apiUse z39tagitem
 */
function deletez39tagitem() { return; }