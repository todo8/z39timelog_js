# 时间清单-开放接口文档

![](http://cdn.7dtime.com/time_tlog/2017-11-29_2017111.gif?imageView/2/w/620/h/667)

接口可以在www.7dtime.com系统登录后,填入对应参数可直接调用调试接口.
![时间清单接口使用方法](http://cdn.7dtime.com/time_tlog/2017-11-29_1695openapi.gif?imageView/2/w/620/h/667)

可以在官网的开放接口页面看到主要接口说明,少部分新增的接口没有及时更新到文档中,详情请看app_z39time/src/api/api.js文件.这个api.js文件中包含所有接口.我主要有两种方式要用测试接口.1.项目直接本地开发运行 2.使用postman模拟参数调用接口. 方式2中我们演示1条登录协议. 需要注意后端接口地址不稳定,需要根据情况调整网址.这部分在项目的readme有说明. 
     ```video
      <video id="my-video" class="video-js" controls preload="auto" width="100%" poster="http://cdn.7dtime.com/time_tlog/2018-04-12_1578kfjk1.jpg" data-setup='{"aspectRatio":"16:9"}'>
       <source src="http://cdn.7dtime.com/time_tlog/2018-04-12_1578kfjk1.mp4" type="video/mp4" >
       <p class="vjs-no-js">
       To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
      </video>
      ```