/**
 * @apiDefine z39projects
 * @apiVersion 1.0.0
 *
 */

/**
 * @api {get} /api/z39projects?id=:id 获取项目
 * @apiVersion 1.0.0
 * @apiName GetProjects
 * @apiGroup z39projects
 * @apiPermission admin | user
 *
 * @apiDescription 获取项目详细信息.包含任务、标签、成员数组
 *
 * @apiParam {Number} id 项目id
 *
 * @apiExample 示例用法:
 * curl -i https://www.7dtime.com/api/projects/4711
 *
 * @apiSuccess {Number}   id            任务id .
 * @apiSuccess {String}     key    项目唯一标识,高级功能导入导出数据使用. 
 * @apiSuccess {String}     title    任务标题
 * @apiSuccess {String}     desc          任务描述
 * @apiSuccess {String}   state     项目状态 "" "done" | "cancelled"
 * @apiSuccess {Object[]} members     成员数组
 * @apiSuccess {Number}   members.fuid     群成员id
 * @apiSuccess {Number}   members.admin    null或9
 * @apiSuccess {String}   members.headimgurl      用户成员头像member
 * @apiSuccess {String}   members.username      用户成员昵称
 * @apiSuccess {Object[]} tags     标签数组
 * @apiSuccess {String}   tags.id  标签 id.
 * @apiSuccess {String}   tags.key  标签 别名唯一
 * @apiSuccess {String}   tags.name  标签 中文名
 * @apiSuccess {String}   tags.value 标签 值 
 * @apiSuccess {Object[]} tasks     收纳箱任务数组
 * @apiSuccess {Number}   tasks.id            任务id tasks-ID.
 * @apiSuccess {String}     tasks.title    任务标题
 * @apiSuccess {String}     tasks.desc          任务描述
 * @apiSuccess {String}   tasks.state     任务状态 "" | due | started | toggle | check | "done" | "cancelled"
 * @apiSuccess {Number}   tasks.parid       父任务id
 * @apiSuccess {Number}   tasks.uid      用户成员id ,没有则不返回.
 * @apiSuccess {Object[]} tasks.tags     标签数组
 * @apiSuccess {String}   tasks.tags.id  标签 id.
 * @apiSuccess {String}   tasks.tags.key  标签 别名唯一.
 * @apiSuccess {String}   tasks.tags.name  标签 中文名.
 * @apiSuccess {String}   tasks.tags.value 标签 值 .
 * 
 * 
 * @apiError NoAccessRight Only authenticated Admins can access the data.
 * @apiError ProjectsNotFound   The <code>id</code> of the z39projects was not found.
 *
 * @apiErrorExample Response (example):
 *     HTTP/1.1 401 Not Authenticated
 *     {
 *       "error": "NoAccessRight"
 *     }
 */
function getProjects() { return; }

/**
 * @api {post} /api/z39projects 添加项目
 * @apiVersion 1.0.0
 * @apiName PostProjects
 * @apiGroup z39projects
 * @apiPermission admin
 *
 * @apiParam {Object[]}   projlist    项目列表
 * @apiParam {String}     projlist.key    项目唯一标识,高级功能导入导出数据使用. 
 * @apiParam {String}     projlist.title    任务标题
 * @apiParam {String}     projlist.desc          任务描述
 * @apiParam {String}   projlist.state     项目状态 "" "done" | "cancelled"
 * @apiParam {Object[]} projlist.tags     标签数组
 * @apiParam {String}   projlist.tags.key  标签 别名唯一
 * @apiParam {String}   projlist.tags.value 标签 值 
 * @apiParam {Object[]} projlist.tasks     收纳箱任务数组
 * @apiParam {String}     projlist.tasks.title    任务标题
 * @apiParam {String}     projlist.tasks.desc          任务描述
 * @apiParam {String}   projlist.tasks.state     任务状态 "" | due | started | toggle | check | "done" | "cancelled"
 * @apiParam {Object[]} projlist.tasks.tags     标签数组
 * @apiParam {String}   projlist.tasks.tags.key  标签 别名唯一.
 * @apiParam {String}   projlist.tasks.tags.value 标签 值 .
 * 
 * @apiDescription 从tlog导入文件,添加多个项目使用. 一般用户不会用到.
 *
 *
 * @apiSuccess {Number[]} ids     新的项目id z39projects-ID.
 *
 * @apiUse z39projects
 */
function postProjects() { return; }