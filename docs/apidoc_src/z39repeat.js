/**
 * @apiDefine z39repeat
 * @apiVersion 1.0.0
 *
 */

/**
 * @api {get} /api/z39repeat?id=:id 获取重复任务
 * @apiVersion 1.0.0
 * @apiName Getz39repeat
 * @apiGroup z39repeat
 * @apiPermission admin | user
 *
 * @apiDescription 获取重复任务. 重复任务数据主要是服务端使用,每天0点定时创建克隆任务数据.
 * 用户获取重复任务数据无作用.
 * 
 * 表结构 z39repeat: id taskid currid rule time_rule 	uid
 * 				数据 25	3657	null	W1	2017-11-02	461
 * {name:"每天",key:"repeat=D"},{name:"每周1",key:"repeat=W1"},{name:"每周2",key:"repeat=W2"},{name:"每周3",key:"repeat=W3"},{name:"每周4",key:"repeat=W4"},{name:"每周5",key:"repeat=W5"},{name:"每周6",key:"repeat=W6"},{name:"每周日",key:"repeat=W7"},{name:"每月",key:"repeat=M"},{name:"每年",key:"repeat=Y"}
 * 如果参数包含id,则返回的是指定id 单个数据对象.
 * 
 * @apiParam {Number} id 对象id
 *
 * @apiExample 示例用法:
 * curl -i https://www.7dtime.com/api/z39repeat?id=4711
 *
 * @apiSuccess {Number}   id            重复id .
 * @apiSuccess {String}   taskid    重复任务源taskid
 * @apiSuccess {String}   currid    当前克隆taskid
 * @apiSuccess {Number}   uid      用户uid
 * @apiSuccess {String}   rule    重复任务规则. 
 * @apiSuccess {String}   time_rule    重复任务的初始使用
 * 
 * @apiError NoAccessRight Only authenticated Admins can access the data.
 * @apiError z39repeatNotFound   The <code>id</code> of the z39repeat was not found.
 *
 * @apiErrorExample Response (example):
 *     HTTP/1.1 401 Not Authenticated
 *     {
 *       "error": "NoAccessRight"
 *     }
 */
function getz39repeat() { return; }

/**
 * @api {post} /api/z39repeat 添加重复任务
 * @apiVersion 1.0.0
 * @apiName Postz39repeat
 * @apiGroup z39repeat
 * @apiPermission none
 *
 * @apiDescription 对任务添加重复任务标签时调用
 *
 * @apiSuccess {String}   taskid    重复任务源taskid
 * @apiSuccess {String}   currid    当前克隆taskid
 * @apiSuccess {String}   rule    重复任务规则. 
 * @apiSuccess {String}   time_rule    重复任务的初始使用
 *
 * @apiSuccess {Number} id     新插入数据id
 *
 * @apiUse z39repeat
 */
function postz39repeat() { return; }

/**
 * @api {put} /api/z39repeat?id=:id 修改重复任务
 * @apiVersion 1.0.0
 * @apiName Putz39repeat
 * @apiGroup z39repeat
 * @apiPermission none
 *
 * @apiDescription 修改任务的重复规则时调用,如果每日重复改为,每周一重复.
 *
 * @apiParam {Number} id     重复任务id
 * @apiSuccess {String}   [taskid]    重复任务源taskid
 * @apiSuccess {String}   [currid]    当前克隆taskid
 * @apiSuccess {String}   rule    重复任务规则. 
 * @apiSuccess {String}   time_rule    重复任务的初始使用
 * 
 * @apiSuccess {Number} data     更新数据数量
 * 
 *
 * @apiUse z39repeat
 */
function putz39repeat() { return; }

/**
 * @api {delete} /api/z39repeat?id=:id 删除重复任务
 * @apiVersion 1.0.0
 * @apiName Deletez39repeat
 * @apiGroup z39repeat
 * @apiPermission none
 *
 * @apiDescription 删除重复任务, 取消任务的重复标签数据时调用.
 *
 * @apiParam {Number}     [id]    重复任务id
 * @apiParam {Number}     [taskid]    任务id
 *
 * @apiUse z39repeat
 */
function deletez39repeat() { return; }