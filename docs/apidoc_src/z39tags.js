/**
 * @apiDefine z39tags
 * @apiVersion 1.0.0
 *
 */

/**
 * @api {get} /api/z39tags?taskid=:taskid 获取标签
 * @apiVersion 1.0.0
 * @apiName Getz39tags
 * @apiGroup z39tags
 * @apiPermission user
 *
 * @apiDescription 获取单个任务的所有标签
 * 标签表 z39tag: id taskid pid bid uid value key  
 * 
 *
 * @apiExample 示例用法:
 * curl -i https://www.7dtime.com/api/z39tags?taskid=88
 *
 * @apiParam {Number}   taskid         任务id .
 * 
 * @apiSuccess {Object[]}   tags           唯一key,英文或拼音 .
 * @apiSuccess {Number}   tags.id            标签id .
 * @apiSuccess {String}   tags.key           唯一key,英文或拼音 .
 * @apiSuccess {String}   tags.value    标签值
 * @apiSuccess {String}   tags.type     分类,暂未使用
 * @apiSuccess {String}   tags.position   行业 职位,暂未使用
 * 
 * @apiError NoAccessRight Only authenticated Admins can access the data.
 * @apiError z39tagsNotFound   The <code>id</code> of the z39tags was not found.
 *
 * @apiErrorExample Response (example):
 *     HTTP/1.1 401 Not Authenticated
 *     {
 *       "error": "NoAccessRight"
 *     }
 */
function getz39tags() { return; }

/**
 * @api {post} /api/z39tags 添加标签
 * @apiVersion 1.0.0
 * @apiName Postz39tags
 * @apiGroup z39tags
 * @apiPermission none
 *
 * @apiDescription 批量添加标签
 * 
 * @apiParam {Object[]}   tags           唯一key,英文或拼音 .
 * @apiParam {Number}   tags.key         标签key
 * @apiParam {String}   [tags.value]         标签值
 * @apiParam {Number}   [tags.taskid]         任务id . 3选其一, 现在基本使用taskid
 * @apiParam {Number}   [tags.pid]            项目id .
 * @apiParam {Number}   [tags.bid]            blog表id .
 * *
 * @apiSuccess {Number[]} ids     新的标签id数组
 *
 * @apiUse z39tags
 */
function postz39tags() { return; }

/**
 * @api {put} /api/z39tags?taskid=:taskid 修改标签
 * @apiVersion 1.0.0
 * @apiName Putz39tags
 * @apiGroup z39tags
 * @apiPermission none
 *
 * @apiDescription 批量修改标签
 * 
 * @apiParam {Number}   taskid           任务id
 * @apiParam {Object[]}   tags           唯一key,英文或拼音 .
 * @apiParam {Number}   tags.key         标签key
 * @apiParam {String}   [tags.value]         标签值
 * @apiParam {Number}   [tags.taskid]         任务id . 3选其一
 * @apiParam {Number}   [tags.pid]            项目id .
 * @apiParam {Number}   [tags.bid]            blog表id .
 * *
 * @apiSuccess {Number} data     标签更新数量
 *
 * @apiUse z39tags
 */
function putz39tags() { return; }


/**
 * @api {delete} /api/z39tags?id=:id 删除标签
 * @apiVersion 1.0.0
 * @apiName Deletez39tags
 * @apiGroup z39tags
 * @apiPermission none
 *
 * @apiDescription 批量删除标签 
 *
 * @apiParam {Number[]}     ids    标签id 数据
 *
 * @apiUse z39tags
 */
function deletez39tags() { return; }