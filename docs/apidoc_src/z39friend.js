/**
 * @apiDefine z39friend
 * @apiVersion 1.0.0
 *
 */

/**
 * @api {get} /api/z39friend?id=:id 获取好友或群成员
 * @apiVersion 1.0.0
 * @apiName Getz39friend
 * @apiGroup z39friend
 * @apiPermission admin | user
 *
 * @apiDescription 获取单个好友或群成员的详细信息. 包含但不限于以下字段.
 * 表结构 z39friend: id nname pid desc flag uid gid admin fuid
 *
 * 4种调用方式, 返回的数据格式稍有不同.
 * 如果参数包含id,则返回的是指定id 单个数据对象. 包含改好友的 name,head
 * 如果不含参数id,则返回用户所有"好友"数据的数组.
 * /api/z39friend?gid=xx 返回(项目)群的所有成员.
 * /api/z39friend?name=xx 模糊查询搜索成员,添加好友时使用, xx 可以是username,email,phone
 * 
 * @apiParam {Number} id 对象id
 *
 * @apiExample 示例用法:
 * curl -i https://www.7dtime.com/api/z39friend/4711
 *
 * @apiSuccess {Number}   id            好友或群成员表 id .
 * @apiSuccess {String}   nname   昵称
 * @apiSuccess {String}   pid     电话id, z43phone项目中的z43phone id
 * @apiSuccess {String}   desc    备注
 * @apiSuccess {String}   flag    分类标签
 * @apiSuccess {Number}   uid      用户uid
 * @apiSuccess {Number}   gid      (项目)群group id
 * @apiSuccess {Number}   admin    9是群创建者,默认null
 * @apiSuccess {Number}   fuid     好友或者群成员uid
 * 
 * @apiError NoAccessRight Only authenticated Admins can access the data.
 * @apiError z39friendNotFound   The <code>id</code> of the z39friend was not found.
 *
 * @apiErrorExample Response (example):
 *     HTTP/1.1 401 Not Authenticated
 *     {
 *       "error": "NoAccessRight"
 *     }
 */
function getz39friend() { return; }

/**
 * @api {post} /api/z39friend 添加好友或群成员
 * @apiVersion 1.0.0
 * @apiName Postz39friend
 * @apiGroup z39friend
 * @apiPermission none
 *
 * @apiDescription In this case "apiErrorStructure" is defined and used.
 * Define blocks with params that will be used in several functions, so you dont have to rewrite them.
 *
 * @apiSuccess {String}   [nname]   昵称
 * @apiSuccess {String}   [pid]     电话id, z43phone项目中的z43phone id
 * @apiSuccess {String}   [desc]    备注
 * @apiSuccess {String}   [flag]    分类标签
 * @apiSuccess {Number}   uid      用户uid, 和gid 二选一
 * @apiSuccess {Number}   [gid]      (项目)群group id, 和uid 二选一
 * @apiSuccess {Number}   [admin]    9是群创建者,默认null
 * @apiSuccess {Number}   fuid     好友或者群成员uid
 *
 * @apiSuccess {Number} id     新插入数据id
 *
 * @apiUse z39friend
 */
function postz39friend() { return; }

/**
 * @api {put} /api/z39friend?id=:id 修改好友或群成员
 * @apiVersion 1.0.0
 * @apiName Putz39friend
 * @apiGroup z39friend
 * @apiPermission none
 *
 * @apiDescription 可能会修改标题和描述.
 *
 * @apiParam {Number} id     好友或群成员id
 * @apiSuccess {String}   [nname]   昵称
 * @apiSuccess {String}   [pid]     电话id, z43phone项目中的z43phone id
 * @apiSuccess {String}   [desc]    备注
 * @apiSuccess {String}   [flag]    分类标签
 * @apiSuccess {Number}   uid      用户uid, 和gid 二选一
 * @apiSuccess {Number}   [gid]      (项目)群group id, 和uid 二选一
 * @apiSuccess {Number}   [admin]    9是群创建者,默认null
 * @apiSuccess {Number}   fuid     好友或者群成员uid
 * 
 *
 * @apiUse z39friend
 */
function putz39friend() { return; }

/**
 * @api {delete} /api/z39friend?id=:id 删除好友或群成员
 * @apiVersion 1.0.0
 * @apiName Deletez39friend
 * @apiGroup z39friend
 * @apiPermission none
 *
 * @apiDescription 删除好友或群成员
 *
 * @apiParam {Number}     id    好友或群成员id
 *
 * @apiUse z39friend
 */
function deletez39friend() { return; }