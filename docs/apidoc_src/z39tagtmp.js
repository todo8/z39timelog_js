/**
 * @apiDefine z39tagtmp
 * @apiVersion 0.0.2
 *
 */

/**
 * @api {get} /api/z39tagtmp?id=:id 获取标签模板
 * @apiVersion 1.0.0
 * @apiName Getz39tagtmp
 * @apiGroup z39tagtmp
 * @apiPermission user
 *
 * @apiDescription 获取标签模板的详细信息. 
 * z39tagtmp表字段： id pid uid tmp count type init key name
 * 
 * 横列使用"key,key", 那纵列呢？"key:value,key:value" ,如果没有值就不传值.
 * 横列和纵列一样.
 * 3种获取方式：获取单条模板;获取项目模板;用户所有模板;
 * /api/z39tagtmp?id=xx 获取指定模板数据
 * /api/z39tagtmp?pid=xx 获取项目id对应的模板
 * /api/z39tagtmp 获取用户所有的模板,包含系统默认模板. 
 * 
 * @apiParam {Number} id 模板id
 * @apiParam {Number}   [uid]         用户id
 * @apiParam {Number}   [pid]         项目id
 *
 * @apiExample 示例用法:
 * curl -i https://www.7dtime.com/api/z39tagtmp?id=4711
 *
 * @apiSuccess {Number}   id          模板id
 * @apiSuccess {Number}   pid       项目id,和用户uid 2选一
 * @apiSuccess {Number}   uid       用户uid,和项目pid 2选一
 * @apiSuccess {String}   tmp         模板值,格式“key,key” 参考数据为"1305,1201"
 * 
 * @apiSuccess {Number}   init       是否初始化, 如果为1则是初始化. 如果init==1,修改模板则应该调用新建模板.
 * @apiSuccess {Number}   count       使用次数
 * @apiSuccess {Number}   type       类型 0 1   0:横列 1:纵列
 * @apiSuccess {Number}   key       系统默认标签使用,用户修改标签后通过此key覆盖默认标签.
 * @apiSuccess {String}   name      标签中文名
 *  
 * @apiError NoAccessRight Only authenticated Admins can access the data.
 * @apiError z39tagtmpNotFound   The <code>id</code> of the z39tagtmp was not found.
 *
 * @apiErrorExample Response (example):
 *     HTTP/1.1 401 Not Authenticated
 *     {
 *       "error": "NoAccessRight"
 *     }
 */
function getz39tagtmp() { return; }

/**
 * @api {post} /api/z39tagtmp 添加模板
 * @apiVersion 1.0.0
 * @apiName Postz39tagtmp
 * @apiGroup z39tagtmp
 * @apiPermission none
 *
 * @apiDescription 创建模板. 修改tagtmp.init=1的系统默认数据,(不能修改系统默认模板)必须新创建一个模板
 * 
 * @apiParam {String}   key      tags_key唯一key,英文或拼音 .
 * @apiParam {Number}   [pid]         项目id,和用户uid 2选一
 * @apiParam {Number}   [uid]         用户uid,和项目pid 2选一
 * @apiParam {String}   [tmp]     模板内容字符串,格式“key,key” 参考数据为"1305,1201"
 * @apiParam {Number}   type     模板内容字符串
 * @apiParam {Number}   key     模板内容字符串
 * 
 * @apiSuccess {Number}   id        模板id *
 *
 * @apiUse z39tagtmp
 */
function postz39tagtmp() { return; }

/**
 * @api {put} /api/z39tagtmp?id=:id 修改模板
 * @apiVersion 1.0.0
 * @apiName Putz39tagtmp
 * @apiGroup z39tagtmp
 * @apiPermission none
 *
 * @apiDescription 修改模板后.如果修改tagtmp.init=1的系统默认数据,(不能修改系统默认模板)必须新创建一个模板,才能修改.
 *
 * @apiParam {Number}   id    模板id
 * 
 * @apiParam {String}   key      tags_key唯一key,英文或拼音 .
 * @apiParam {Number}   [uid]         用户id
 * @apiParam {Number}   [pid]         项目id
 * @apiParam {String}   [tmp]     模板内容字符串,格式“key,key” 参考数据为"1305,1201"
 * 
 * @apiSuccess {Number}   id    模板id
 *
 * @apiUse z39tagtmp
 */
function putz39tagtmp() { return; }

/**
 * @api {delete} /api/z39tagtmp?id=:id 删除模板
 * @apiVersion 1.0.0
 * @apiName Deletez39tagtmp
 * @apiGroup z39tagtmp
 * @apiPermission none
 *
 * @apiDescription 删除模板 
 *
 * @apiParam {Number}     id    模板id
 *
 * @apiUse z39tagtmp
 */
function deletez39tagtmp() { return; }