/**
 * @apiDefine z39request
 * @apiVersion 1.0.0
 *
 */

/**
 * @api {get} /api/z39request 获取'申请添加'好友或群的信息
 * @apiVersion 1.0.0
 * @apiName Getz39request
 * @apiGroup z39request
 * @apiPermission admin | user
 *
 * @apiDescription 获取所有'申请添加'的数据
 * 表结构 z39request: id suid tuid gid state time read remark content
 * 
 *
 * @apiExample 示例用法:
 * curl -i https://www.7dtime.com/api/z39request
 *
 * @apiSuccess {Number}   id      '申请添加'数据id .
 * @apiSuccess {String}   suid    申请者用户uid
 * @apiSuccess {String}   tuid     被添加者目标用户uid
 * @apiSuccess {Number}   gid      群groupid 
 * @apiSuccess {String}   state    申请状态,0 待处理. 1通过 2邀请 -1拒绝
 * @apiSuccess {String}   time     申请时间
 * @apiSuccess {Number}   read     是否已读 1已读
 * @apiSuccess {String}   remark   申请好友附加语言
 * @apiSuccess {String}   content  系统显示内容,如姜禄建 已经同意你的好友申请
 * 
 * @apiError NoAccessRight Only authenticated Admins can access the data.
 * @apiError z39requestNotFound   The <code>id</code> of the z39request was not found.
 *
 * @apiErrorExample Response (example):
 *     HTTP/1.1 401 Not Authenticated
 *     {
 *       "error": "NoAccessRight"
 *     }
 */
function getz39request() { return; }

/**
 * @api {post} /api/z39request 添加'申请添加'
 * @apiVersion 1.0.0
 * @apiName Postz39request
 * @apiGroup z39request
 * @apiPermission none
 *
 * @apiDescription In this case "apiErrorStructure" is defined and used.
 * Define blocks with params that will be used in several functions, so you dont have to rewrite them.
 *
 * 
 * @apiParam {String}   [tuid]     被添加者目标用户uid ,tuid和gid二选其一
 * @apiParam {Number}   [gid]      群groupid ,tuid和gid二选其一
 * @apiParam {String}   [remark]   申请好友附加语言
 *
 * @apiSuccess {Number} id     新插入数据id
 *
 * @apiUse z39request
 */
function postz39request() { return; }

/**
 * @api {put} /api/z39request?id=:id 修改'申请添加'
 * @apiVersion 1.0.0
 * @apiName Putz39request
 * @apiGroup z39request
 * @apiPermission none
 *
 * @apiDescription 同意申请,拒绝申请
 *
 * @apiParam {Number}   id         '申请添加'id
 * @apiParam {Number}   state      申请状态,0 待处理. 1通过 2邀请 -1拒绝
 * @apiParam {String}   [remark]   申请好友附加语言
 * 
 *
 * @apiUse z39request
 */
function putz39request() { return; }