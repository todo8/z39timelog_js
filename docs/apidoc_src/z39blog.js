/**
 * @apiDefine z39blog
 * @apiVersion 1.0.0
 *
 */

/**
 * @api {get} /api/z39blog?id=:id 获取文章
 * @apiVersion 1.0.0
 * @apiName GetBlog
 * @apiGroup z39blog
 * @apiPermission admin | user
 *
 * @apiDescription 获取单个文章的详细信息.
 * 表结构 z39blog: id title desc filename img status uid time type open count_view count_support
 * 部分字段暂未使用, status type open tount_view count_support
 * 如果参数包含id,则返回的是指定id文章详情.
 * 如果不含参数id,则返回所有文章的数组.
 * 
 * @apiParam {Number} id 文章id
 *
 * @apiExample 示例用法:
 * curl -i https://www.7dtime.com/api/z39blog/4711
 *
 * @apiSuccess {Number}   id            文章id .
 * @apiSuccess {String}   title    文章标题
 * @apiSuccess {String}   desc          文章描述
 * @apiSuccess {String}   img      文章封面图片url
 * @apiSuccess {String}   filename      文章路径名
 * @apiSuccess {String}   status      文章状态,自己申请精品. "" send success fail
 * @apiSuccess {Number}   uid      作者
 * @apiSuccess {Number}   uhead      作者头像
 * @apiSuccess {Number}   uname      作者名称
 * @apiSuccess {Number}   count_view      查看次数
 * @apiSuccess {Number}   count_support   赞的次数
 * @apiSuccess {String}   time     时间
 * @apiSuccess {Number}   type     类型 type 1 2 3 4 日 周 月 年
 * @apiSuccess {Number}   open     开放等级 open 0 1 2 0所有人可见  1朋友 2自己.
 * 
 * @apiError NoAccessRight Only authenticated Admins can access the data.
 * @apiError BlogNotFound   The <code>id</code> of the z39blog was not found.
 *
 * @apiErrorExample Response (example):
 *     HTTP/1.1 401 Not Authenticated
 *     {
 *       "error": "NoAccessRight"
 *     }
 */
function getBlog() { return; }

/**
 * @api {post} /api/z39blog 添加文章
 * @apiVersion 1.0.0
 * @apiName PostBlog
 * @apiGroup z39blog
 * @apiPermission none
 *
 * @apiDescription In this case "apiErrorStructure" is defined and used.
 * Define blocks with params that will be used in several functions, so you dont have to rewrite them.
 *
 * @apiParam {String}   title    文章标题, 长度20字符
 * @apiParam {String}   desc          文章描述, 长度150字符
 * @apiParam {String}   filename      文章md路径名,相对路径,长度100字符.
 * @apiParam {Number}   uid      作者
 * @apiParam {String}   [img]      文章封面图片url,长度100字符.
 * @apiParam {Number}   [type]     类型 type 1 2 3 4 日 周 月 年
 * @apiParam {Number}   [open]     开放等级 open 0 1 2 0所有人可见  1朋友 2自己.
 *
 * @apiSuccess {Number} id     新的文章id Blogs-ID.
 *
 * @apiUse z39blog
 */
function postBlog() { return; }

/**
 * @api {put} /api/z39blog?id=:id 修改文章
 * @apiVersion 1.0.0
 * @apiName PutBlog
 * @apiGroup z39blog
 * @apiPermission none
 *
 * @apiDescription 文章主要是自动自动生成,基本不会修改. 且主要内容在md文件中.
 *
 * @apiParam {Number} id     文章id
 * @apiParam {String}   [title]    文章标题, 长度20字符
 * @apiParam {String}   [desc]     文章描述, 长度150字符
 * @apiParam {String}   [img]      文章封面图片url,长度100字符.
 * @apiParam {Number}   [open]     开放等级 open 0 1 2 0所有人可见  1朋友 2自己.
 * @apiSuccess {String}   [status]      文章状态,自己申请精品. "" send success fail
 *
 * @apiUse z39blog
 */
function putBlog() { return; }

/**
 * @api {delete} /api/z39blog?id=:id 删除文章
 * @apiVersion 1.0.0
 * @apiName DeleteBlog
 * @apiGroup z39blog
 * @apiPermission none
 *
 * @apiDescription 删除文章,从数据库及硬盘中删除.
 *
 * @apiParam {Number}     id    文章id
 *
 * @apiUse z39blog
 */
function deleteBlog() { return; }