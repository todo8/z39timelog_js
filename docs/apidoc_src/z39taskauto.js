/**
 * @apiDefine z39taskauto
 * @apiVersion 1.0.0
 *
 */

/**
 * @api {get} /api/z39taskauto?id=:id 获取APP自动收集数据
 * @apiVersion 1.0.0
 * @apiName Getz39taskauto
 * @apiGroup z39taskauto
 * @apiPermission user
 *
 * @apiDescription 获取单个APP自动收集数据, APP使用接口.
 * 表结构 z39taskauto: id uid start end typea title speed stepcount img px py addr desc timeday duration dis key new
 * 
 * 如果参数包含id,则返回的是指定id 单个数据对象.
 * 如果不含参数id,则返回用户所有数据的数组.
 * 
 * @apiParam {Number} id 对象id
 * @apiParam {String} [timeday] 指定日期,如果没传则返回当日数据.
 *
 * @apiExample 示例用法:
 * curl -i https://www.7dtime.com/api/z39taskauto?id=4711
 *
 * @apiSuccess {Number}   id            APP自动收集数据id .
 * @apiSuccess {String}   uid     用户uid
 * @apiSuccess {String}   start    动作开始时间
 * @apiSuccess {String}   end      动作结束时间
 * @apiSuccess {Number}   typea    1静态 2动态
 * @apiSuccess {String}   title    动作标题
 * @apiSuccess {Number}   speed    动作移动速度
 * @apiSuccess {Number}   stepcount    移动步数,运动计步器
 * @apiSuccess {String}   img      动作图片, 现在系统默认. 还为提供用户上传
 * @apiSuccess {String}   px      GPS经纬度
 * @apiSuccess {String}   py     GPS经纬度
 * @apiSuccess {String}   addr    GPS 地址
 * @apiSuccess {String}   desc     动作描述
 * @apiSuccess {String}   timeday   日期
 * @apiSuccess {String}   duration   动作持续时间
 * @apiSuccess {Number}   dis      移动距离
 * @apiSuccess {String}   key     动作名称key,通常对应生活类tagitem
 * @apiSuccess {Number}   new     是否用户主动新建
 * 
 * @apiError NoAccessRight Only authenticated Admins can access the data.
 * @apiError z39taskautoNotFound   The <code>id</code> of the z39taskauto was not found.
 *
 * @apiErrorExample Response (example):
 *     HTTP/1.1 401 Not Authenticated
 *     {
 *       "error": "NoAccessRight"
 *     }
 */
function getz39taskauto() { return; }

/**
 * @api {post} /api/z39taskauto 添加APP自动收集数据
 * @apiVersion 1.0.0
 * @apiName Postz39taskauto
 * @apiGroup z39taskauto
 * @apiPermission none
 *
 * @apiDescription APP自动通过GPS和手机震动数据自己记录用户行为.
 *
 * @apiParam {String}   uid     用户uid
 * @apiParam {String}   start    动作开始时间
 * @apiParam {String}   end      动作结束时间
 * @apiParam {Number}   typea    1静态 2动态
 * @apiParam {String}   title    动作标题
 * @apiParam {Number}   speed    动作移动速度
 * @apiParam {Number}   stepcount    移动步数,运动计步器
 * @apiParam {String}   img      动作图片, 现在系统默认. 还为提供用户上传
 * @apiParam {String}   px      GPS经纬度
 * @apiParam {String}   py     GPS经纬度
 * @apiParam {String}   addr    GPS 地址
 * @apiParam {String}   desc     动作描述
 * @apiParam {String}   timeday   日期
 * @apiParam {String}   duration   动作持续时间
 * @apiParam {Number}   dis      移动距离
 * @apiParam {String}   key     动作名称key,通常对应生活类tagitem
 * @apiParam {Number}   new     是否用户主动新建
 *
 * @apiSuccess {Number} id     新插入数据id
 *
 * @apiUse z39taskauto
 */
function postz39taskauto() { return; }

/**
 * @api {put} /api/z39taskauto?id=:id 修改APP自动收集数据
 * @apiVersion 1.0.0
 * @apiName Putz39taskauto
 * @apiGroup z39taskauto
 * @apiPermission none
 *
 * @apiDescription 可能会修改标题和描述.
 *
 * @apiParam {Number} id     APP自动收集数据id
 * @apiParam {String}   uid     用户uid
 * @apiParam {String}   start    动作开始时间
 * @apiParam {String}   end      动作结束时间
 * @apiParam {Number}   typea    1静态 2动态
 * @apiParam {String}   title    动作标题
 * @apiParam {Number}   speed    动作移动速度
 * @apiParam {Number}   stepcount    移动步数,运动计步器
 * @apiParam {String}   img      动作图片, 现在系统默认. 还为提供用户上传
 * @apiParam {String}   px      GPS经纬度
 * @apiParam {String}   py     GPS经纬度
 * @apiParam {String}   addr    GPS 地址
 * @apiParam {String}   desc     动作描述
 * @apiParam {String}   timeday   日期
 * @apiParam {String}   duration   动作持续时间
 * @apiParam {Number}   dis      移动距离
 * @apiParam {String}   key     动作名称key,通常对应生活类tagitem
 * @apiParam {Number}   new     是否用户主动新建
 * 
 *
 * @apiUse z39taskauto
 */
function putz39taskauto() { return; }

/**
 * @api {delete} /api/z39taskauto?id=:id 删除APP自动收集数据
 * @apiVersion 1.0.0
 * @apiName Deletez39taskauto
 * @apiGroup z39taskauto
 * @apiPermission none
 *
 * @apiDescription 删除APP自动收集数据,从数据库及硬盘中删除.
 *
 * @apiParam {Number}     id    APP自动收集数据id
 *
 * @apiUse z39taskauto
 */
function deletez39taskauto() { return; }