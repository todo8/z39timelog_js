# 时间日志

### 时间日志
- [x] **17-04-02 13:36** 任务界面 rishiqing/index.html 整理. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-02 14:23** app logo设计. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      17-04-02 14:23logo带草稿的详细需求
      17-04-02 17:02设计 找人&沟通
- [x] **17-04-02 15:42** 项目创建&基本代码. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-02 17:56** 位置,陀螺仪数据采集外包沟通. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-02 20:55** 位置文件接收查看&需求沟通. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-02 23:00** 时间日志md展现内容形式. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-08 17:03** 时间日志app行为采集沟通&视频查看. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-10 12:59** 动作采集构思. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-11 13:00** 手机设备测试-陀螺仪、加速度. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-11 13:24** 程序构思. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-11 14:12** 后台定时采集数据. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-11 14:19** 创建git项目,转移项目目录后提交代码. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-11 14:33** 界面显示数据. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-11 16:50** 变化数据. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-11 18:25** bug position数据为打印出来.进入了 watchPosition out. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-11 19:45** 关键点数据. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-11 20:05** 手机抖动容错率. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-11 20:27** 敏感度参数. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-11 20:30** 10秒内清空抖动数据. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-11 20:50** 手机静止动作显示. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-11 20:56** bug 启动app后第一个1秒的静止动作. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-11 21:11** 记录陀螺仪抖动数值变化大小.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-11 22:15** 24点结束今日动作. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      17-04-11 22:15静止-直接结束.
      17-04-11 22:15移动-正在用手机. 结束今日动作,动作添加字段表明正在进行.
- [x] **17-04-11 22:21** 时间转化为24小时制. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-11 22:34** bug 24点交替. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-11 22:58** app启动即动作开始. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-12 14:10** list布局构思. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-12 14:33** 所有动作罗列. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-12 14:48** 操作界面构思. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-12 17:13** list展示方式,图文. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      17-04-12 16:00列表布局
      17-04-12 17:13数据整合显示
- [x] **17-04-12 18:56** 当前进行中的动作置顶. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-12 19:20** 静止时首个li正在进行的动作状态变化.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
- [x] **17-04-12 19:28** 首个li的大小及顶部显示不正常.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
- [x] **17-04-12 19:42** 数据标签的显示. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-12 19:48** 刷新当前动作的时间. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-12 20:24** 图标临时素材查找下载. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-12 21:04** 图标处理成png并添加圆角. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-12 21:50** item.start.format is not a function. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
- [x] **17-04-13 00:02** 手机数据的保存js. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-13 00:27** 只记录关键(变化)数据. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-13 00:30** 经纬度的小数点精确度. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-13 14:28** 白天黑夜图标下载抠图. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-13 14:38** 所有动作栏罗列. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-13 15:21** 根据时间段自动显示图标. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-13 16:06** bug 多显示了2个0秒的动作. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-13 16:09** 获取到的地址显示到动作列表中;. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-13 16:34** bug gps定位经常会出现人5秒钟出现几个不同地址.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
- [x] **17-04-13 16:59** speed 速度标签显示. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-13 17:56** bug 触发动作时,没有打印静止动作.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
- [x] **17-04-13 19:53** 静态动作单独一栏. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-13 20:42** 打坐图标&隔日标签. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-13 23:22** 动作重命名. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      17-04-13 23:06编辑好弹框内容. 名字&值的键值对.
      17-04-13 23:22需要把修改后的数据显示到界面
- [x] **17-04-14 14:44** 零碎的时间如何处理. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-14 15:00** 上班这个过程包含很多小动作如何处理?. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
- [x] **17-04-16 15:54** 摇一摇触发新动作. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      17-04-16 14:05摇一摇功能代码&测试
      17-04-16 15:54摇一摇触发新动作
- [x] **17-04-16 17:56** 摇一摇逻辑判断优化. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-16 18:20** 通过加速度值简单实现计步.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-16 19:16** 摇一摇“新建”标签. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-16 19:57** 步数显示. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-16 20:04** 修改进行中的标签颜色. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-16 20:13** 地址显示优化. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-16 22:01** 公司附近的经纬度距离计算. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![first ](https://img.shields.io/badge/-first-72c7ff.svg) 
- [x] **17-04-16 22:31** 合并操作. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      17-04-16 22:31界面布局
      17-04-16 22:44阻止选择框事件冒泡.
- [x] **17-04-16 22:56** 用户修改标题后的颜色.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-16 23:03** 自定义标题. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-17 16:02** 现在怀疑app后台,逻辑不能正常运行.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-17 21:19** 相关词语. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-17 21:20** 经测试app后台摇一摇不一样. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
- [x] **17-04-19 13:14** logo外包沟通付款2th. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-19 19:18** 聊天发现相关时间记录“乐动”查看. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-20 11:13** logo需求沟通,提供产考效果 3th. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![first ](https://img.shields.io/badge/-first-72c7ff.svg) 
- [x] **17-04-20 11:47** logo需求整理成markdown,初级美术说做不了.重新找人 4th. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-20 12:31** logo需求美化md格式. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-04-21 11:02** logo优化淘宝找人沟通,消耗1小时无果. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-05-04 21:31** 思维文字. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-05-04 22:48** 思考方式. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-23 13:45** 时间管理记事本查看. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-23 14:11** 时间管理记事本销量查看. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-23 15:05** 如何节省别人的时间 https://www.zhihu.com/question/34611072. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![save url](https://img.shields.io/badge/save-url-72c7ff.svg) 
- [x] **17-06-23 15:17** 查看知乎GTD文章. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-24 15:09** 如果有什么东西是我的使命,那就是时间管理. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![mission life](https://img.shields.io/badge/mission-life-72c7ff.svg) 
- [x] **17-06-24 15:27** 不知道如何思考新的知识体系,如时间管理创业. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![unknown wait](https://img.shields.io/badge/unknown-wait-72c7ff.svg) 
- [x] **17-06-24 20:39** 时间是每人拥有的高频发生,如果收费模式适当可以成为全球第一大公司. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![vision ](https://img.shields.io/badge/-vision-72c7ff.svg) 
- [x] **17-06-24 21:17** 系统行为罗列. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-24 21:26** 收费模式. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-24 21:36** 罗列出常见干扰行为. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-24 22:04** 搜索内容重复:任务延迟到下一天会重复出现到多个文本中.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
- [x] **17-06-26 10:13** 他人的日报周报格式. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-26 15:38** 网站系统功能构思. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-26 19:27** 网站页面手稿图理清思路. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-27 11:07** 手机版布局手稿图. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-27 11:16** 思维文字-首页. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![add ](https://img.shields.io/badge/-add-72c7ff.svg) 
- [x] **17-06-27 11:49** 大目标需要框架性划分目标,现在完成了目标也看不出当前进度. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question vision](https://img.shields.io/badge/question-vision-72c7ff.svg) 
- [x] **17-06-27 11:54** 假如罗列的功能都完成,然后呢？. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-27 12:03** 添堵-黄牛、项目负责人. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bad ](https://img.shields.io/badge/-bad-red.svg) 
- [x] **17-06-27 13:52** element组件再次查看. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-27 14:25** 首页构思优化. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-27 14:56** 标签有哪些？用户自添加的如何处理. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-27 17:20** 正式取名时间日志,appstore和国内都能使用.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-27 17:23** md显示方式构思. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-27 18:49** tlog如何导入. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-27 20:00** 构思第一版可用功能Pc. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-27 20:42** 首页. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      17-06-27 20:42评估
      17-06-27 21:50数据库设计
      17-06-28 12:25数据库创建
      17-06-28 12:28边创建db边犹豫的修改,时间可以分为：执行力和创新修正.
      17-06-28 22:19在坐不住的之后,先勇敢的start任务.然后状态就好了.
      17-06-28 22:21任务都按时完成,但让自己感到泄气的是感觉和日事清没多大区别,实际有.担心做的又没人用;
      17-06-29 17:38前端逻辑
      17-06-29 14:32之前任务引起的相关问题,不应该在当前阶段处理.
      17-06-29 14:34任务暂停片段太多,难以看出总耗时.不知该快该慢.
      17-06-29 17:59成功的体验了失败
      17-06-29 18:36遗漏任务当做新增任务处理,重新评估时间
      17-06-30 09:07难受的感觉掩盖了目标吗
- [x] **17-06-28 09:47** 搜索“时间管理”. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-30 11:31** 日周月任务安排方式. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-30 13:38** 布局需求视频录制. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-30 14:12** 自己修改布局. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![error ](https://img.shields.io/badge/-error-red.svg) 
- [x] **17-06-30 14:41** 求助操作及显示方式. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![better ](https://img.shields.io/badge/-better-1ba4ff.svg) 
- [x] **17-06-30 15:26** Tlog、MD文件构思. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-30 16:00** 模板创建编辑的功能设计构思. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-30 17:37** 联系的前端都没时间.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-30 19:46** 自己调整水平滚动条失败. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-30 21:22** 上次想法的目标是周末展示自己的想法. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-06-30 21:59** 已发送文件给前端并支付部分订金. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-03 18:07** 重新构思系统思维文字&手稿图. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![sleepy ](https://img.shields.io/badge/-sleepy-72c7ff.svg) 
- [x] **17-07-03 18:09** weixin.sougou 搜索时间管理. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-03 20:23** sublime和npm是如何做的. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 09:24** 下载张萌app的思维导图. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 09:41** 番茄工作法适合创造性类无法切割时间的工作.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![exp ](https://img.shields.io/badge/-exp-72c7ff.svg) 
- [x] **17-07-04 10:00** 纪元-时间管理音频. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 10:13** 计划看未来. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 10:47** 叶武滨:时间管理一日看人生视频. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 11:30** 统筹安排的方式说明. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 11:56** 叶武滨:日理万机的秘密. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 13:16** 易效能音频-问答. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![change ](https://img.shields.io/badge/-change-72c7ff.svg) 
- [x] **17-07-04 14:24** 易效能10堂课1/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 14:46** 易效能10堂课2/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 15:10** 易效能10堂课3/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 15:29** 易效能10堂课4/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 15:56** 易效能10堂课5/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 16:21** 易效能10堂课6/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 16:39** 议论文百科查看. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 16:45** 易效能10堂课7/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 17:06** 易效能10堂课8/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 17:19** Omnifocus软件了解. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 17:47** 叶武滨：时间管理干货100讲泛读. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 19:26** 思维导图书籍理论学习. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 19:47** 华罗庚《统筹方法》查看. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 19:51** 易效能10堂课9/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 19:52** 时间的名言. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 20:29** 易效能10堂课10/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-04 20:45** 演讲的框架. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-05 08:36** 上班前早起的这段时间使用率低. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) ![error waste](https://img.shields.io/badge/error-waste-red.svg) 
- [x] **17-07-05 09:39** 区域标示批量添加. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![flag ](https://img.shields.io/badge/-flag-72c7ff.svg) 
- [x] **17-07-05 10:13** 纠结开发功能或找人参与. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![noway ](https://img.shields.io/badge/-noway-72c7ff.svg) ![change ](https://img.shields.io/badge/-change-72c7ff.svg) 
- [x] **17-07-05 10:33** 合作常见问题. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-05 11:32** 两个方向需要做的工作. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-05 12:02** 系统日报格式. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-05 14:21** 新的分类模板tlog. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-05 15:58** 下一步、上一版和已完成待发布的显示方式. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-05 16:30** 统计分析显示内容构思. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-05 18:25** 时间任务生态圈. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 09:20** 系统日报格式改良. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 11:18** 任务时间计算方式理清. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
- [x] **17-07-06 11:57** 系统功能版本划分方式理清. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 13:15** 康熙起居注. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 13:25** 误区：起床一定要设置闹钟,不要靠梦想和意志力. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
- [x] **17-07-06 13:32** 高效能工作密码1/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 15:17** 高效能工作密码3/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 15:42** 高效能工作密码4/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 15:52** 高效能工作密码2/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 16:18** 高效能工作密码5/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 16:42** 高效能工作密码6/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 17:06** 高效能工作密码7/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 17:27** 高效能工作密码8/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 17:47** 高效能工作密码9/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 17:53** 高效能工作密码10/10. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 19:46** 高效时间管理——职场1/14. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 19:56** 高效时间管理——职场2/14. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 20:05** 高效时间管理——职场3/14. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 20:18** 高效时间管理——职场4/14. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 20:31** 高效时间管理——职场5/14. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 20:40** 高效时间管理——职场6/14. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 20:57** 高效时间管理——职场7/14. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 21:05** 高效时间管理——职场8/14. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 21:11** 高效时间管理——职场9/14. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 21:17** 高效时间管理——职场10/14. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 21:22** 高效时间管理——职场11/14. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 21:26** 高效时间管理——职场12/14. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 21:35** 高效时间管理——职场13/14. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-06 21:40** 高效时间管理——职场14/14. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-07 09:09** 到公司和上班是两种情况. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
- [x] **17-07-07 09:14** 干扰排除方法. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-07 09:16** 拒绝的艺术. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-07 09:25** 时间六大杀手. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-07 09:35** 生活类重复任务导入管理问题思考. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-07 10:04** 时间管理视频博士查看1/2. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-07 10:21** 时间管理视频博士查看2/2. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-07 10:43** 时间管理视频博士查看3/2. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-07 22:09** 陈赫搞笑视频-说纠结的主题似乎和时间也有关系. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-07 22:15** 超时或明显不够时,重新预估时间.构思. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-08 15:55** 统计报表思路. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-08 16:06** md文件查看及微调. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-08 16:40** 构思系统如何生成到服务器上的文件. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-08 17:10** flowchart学习使用. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![study ](https://img.shields.io/badge/-study-72c7ff.svg) 
- [x] **17-07-08 17:23** 任务描述在解析后换行丢失. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
- [x] **17-07-08 19:22** 序列图学习使用. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![study ](https://img.shields.io/badge/-study-72c7ff.svg) 
- [x] **17-07-09 21:14** markdown 的思维导图构思. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-10 14:12** 疑问：amd格式js如何运行使用？. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![new ](https://img.shields.io/badge/-new-red.svg) 
- [x] **17-07-10 15:18** markdown 标签图片制作方式.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-11 10:28** markdown思维导图修改整合. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![title ](https://img.shields.io/badge/-title-72c7ff.svg) 
      17-07-10 08:25任务分解理清思路
      17-07-10 11:04解析思维导字
      17-07-10 10:34导入思维文字
      17-07-10 20:30自定义mind插件,自己没有动脑想过
      17-07-11 09:58渲染导图
      17-07-11 10:21多思维导图扩展
- [x] **17-07-11 11:39** 解析描述中思维导图. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-11 11:47** parsetlog.js 解析运行报错unproject. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
- [x] **17-07-11 14:10** md中标签替换成图片. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-11 14:34** 描述中的时间替换成标签图片. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-11 15:53** 标签图片标签显示中文. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-11 16:24** 时间日志不显示任务状态信息. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-11 16:26** life summary view 等标签不显示done. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-11 17:50** 任务时间计算&结果分析. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![better ](https://img.shields.io/badge/-better-1ba4ff.svg) 
      17-07-11 17:50不含暂停的单任务计时.
      17-07-11 17:53注意2次想点外卖后及时停止
- [x] **17-07-11 19:42** 标签按顺序显示. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-11 20:28** md标签图片分类颜色. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-11 21:57** 如果是当天就不显示日期. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-11 22:05** 任务详情不显示time. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
- [x] **17-07-12 09:18** 解析7-11.tlog文件并查看. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-12 11:47** z1flag标签整理. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-12 13:17** 笔记5w2h思维导图查看. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-12 13:36** 任务列表缺少生活,生活类任务用斜线标识. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
- [x] **17-07-12 14:04** 部分任务没有按时间顺序显示. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
- [x] **17-07-12 14:34** 甘特图展示多任务(重叠执行时文字难以展示). ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      17-07-12 14:34甘特图展示构思
      17-07-12 15:00mermaid/gantt 官网查看
- [x] **17-07-12 15:51** tlog文件一键插入模板. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-12 18:45** 练习用思维导图方式思考任务. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![exercise ](https://img.shields.io/badge/-exercise-72c7ff.svg) 
- [x] **17-07-12 18:53** "取消的任务"横线划掉的方式标识. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-12 19:37** 观点和总结一起时不显示观点.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-12 20:27** editor整合到docute. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      17-07-12 15:58构思
      17-07-12 16:07查看editor关键代码
      17-07-12 16:11查看docute关键代码
      17-07-12 20:27editor单页面
- [x] **17-07-13 09:13** it&me 群中时间日志信息查看与回复. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-13 09:36** 标签图片seo优化,添加实际关键词.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-13 10:05** 每日日志的标题标签. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-13 11:56** 服务器创建git项目,上传代码. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-13 13:55** 项目太庞大,眼前创意不急,历史任务多而不清.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
- [x] **17-07-13 14:18** 时间日志朋友反馈信息. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![record ](https://img.shields.io/badge/-record-72c7ff.svg) 
- [x] **17-07-14 10:13** 计划自己的一天. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![change ](https://img.shields.io/badge/-change-72c7ff.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [x] **17-07-14 11:32** 时间碎片展示构思. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [x] **17-07-14 12:01** 使命目的. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [x] **17-07-14 13:31** pendo 搜索了解. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [x] **17-07-14 14:48** git项目目录权限. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [x] **17-07-14 17:23** ```mind 字符输入太难,创建快捷标签. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [x] **17-07-15 08:41** 百度“时间”没有广告. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [x] **17-07-15 17:05** 时间系统框架. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![better ](https://img.shields.io/badge/-better-1ba4ff.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [x] **17-07-15 18:24** 建议页面构思. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [x] **17-07-15 21:03** 手机收纳箱和计划手稿图. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [x] **17-07-17 08:58** 兼职沉默后说不做了. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bad ](https://img.shields.io/badge/-bad-red.svg) ![waste ](https://img.shields.io/badge/-waste-red.svg) 
- [x] **17-07-17 10:01** tmp模板文件中添加新的行为. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-17 11:20** 日事清员工新搜索功能通知. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-17 14:13** 电影纪录片 一日人生 / 同一天的生活/ 浮生一日. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-17 15:12** “一日一生”目录&资料查看. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-17 17:25** 当前只有我1个人,首要日计划功能,需求展示.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-17 19:30** sublime插件安装说明&打包发xc. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-17 20:29** sublime插入时间快捷键. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-17 21:55** 做明日计划. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![change ](https://img.shields.io/badge/-change-72c7ff.svg) 
- [x] **17-07-18 08:13** https升级2域名中的1个. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-18 08:43** 标签添加等级. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      17-07-18 08:43理清思路和功能
- [x] **17-07-18 09:18** 升级https需要的证书资料. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-18 10:06** 所有tlog任务整理. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      17-07-18 10:06框架代码
      17-07-18 10:42多tlog加载
      17-07-18 11:58node版parsetlog
      17-07-18 11:47node版js 直接在底部添加export.function 最快
      17-07-18 13:49测试
      17-07-18 16:55多文件切换-保持数据
      17-07-18 17:40多文件切换-清空数据
      17-07-18 18:33任务按项目归纳-跨日任务不同状态
      17-07-18 18:37任务保存方式-项目md
      17-07-18 21:34导出tlog字符
      17-07-18 21:34多文件切换因为调用接口错误导致延迟.
- [x] **17-07-19 09:57** 熊超电话沟通进展. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-19 10:30** 提交parseTlog代码. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-19 10:48** 代码整合到parseTlog文件中. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-19 12:12** 生成项目的tlog&md. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![first ](https://img.shields.io/badge/-first-72c7ff.svg) ![tech nodejs](https://img.shields.io/badge/tech-nodejs-72c7ff.svg) 
- [x] **17-07-19 14:13** 事情&工作的分类. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-19 14:15** 常用行为动作. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-19 14:30** z39timelog整合. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-19 14:58** 生成项目md中需要显示日期. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-19 15:14** 项目md需要以项目名为 title. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-19 15:22** 参考资料不分项目,如何整合到项目md中?. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [x] **17-07-19 15:38** 项目md任务标签中的时间需要显示日期. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
- [x] **17-07-19 16:04** node版解析tlog缺少加载中文标签yml. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 项目md中标签时间未显示完成.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
- [ ] tlog中的项目描述会被覆盖. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] app自动记录位置. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 修改以前tlog文件参考链接为任务. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 修改之前文件格式,给任务添加项目.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] logo美化需求. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![help ](https://img.shields.io/badge/-help-72c7ff.svg) 
- [ ] ~~解析项目时,不计算当日日期~~. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] js和node版本对代码的引入和使用. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] tlog文件日期优化,done标签的时间权值为5. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 模块化js中如何引入外部文件,如fs. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 评估时间分包含毛时间和纯时间. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 标签添加等级. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] ~~描述中的md解析时间,时间前面天-符号.~~. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 所有tlog任务整理. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      任务保存方式-数据库
- [ ] 任务搜索. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 软件系统收费,提供用劳动换免费.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 项目展示设置面板. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 电脑版本任务swipeout显示按钮接任务. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 项目分解部门工作类型. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 自己需求目标整理罗列. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 需求需要系统,又进入了之前的恶循环.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 罗列需求,找人一起参与完成.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 程序可以关注任务状态,更改通知. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] app主页显示当然完成任务总数,实时显示. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 熊超18:30电话聊15分钟,说大概需要做的工作.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] app录音记录创意想法,空闲时在定期整理.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 自己价值观如何融入家庭和朋友中. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 兼职需要解决快速分发需求达成合作,减少小事情和单个人沟通失败造成的浪费.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
- [ ] 通过讲师培训内容了解“时间管理”关键能力. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 思考&决定. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 项目页面+分组列表显示&标签分组. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 加入20个时间管理群. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 加群"时间管理"批量发送推广信息. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 收集可能节约时间的建议和创意. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 手机如何展示查看分类代办任务. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [ ] 时间除了外包还有哪些市场应用. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [ ] 构思：如何与桌面操作系统整合. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [ ] 每天3个电话和他人沟通项目. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [ ] 如果我有100w我会如何展开工作. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [ ] 找到有相同志向的人. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [ ] 同一事情多种分类难以查找和管理.如work,excel. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [ ] 文件分开管理又不知今日做了什么.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [ ] 梦想目标是想赚钱,而当下忙的这些事情不赚钱,那你愿意花多少钱解放你的时间,让人替代完成这些.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [ ] 任务地图. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [ ] 各部分细节梳理. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [ ] 眼前工作顺序梳理. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [ ] 任务上传服务器. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [ ] tlog解析后上传保存md. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [ ] 生活类数据上传服务器. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [ ] 新版app实现生活类行为. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [ ] 和电话系统结合. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [ ] 网页系统添加图片使用拖动上传或粘贴上传. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [ ] 时间碎片展示. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
- [ ] 资料生成可打印信息,装订成书. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 整理出历史tlog文件中项目任务. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] ~~恢复数据库~~. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 时间日志页面优化. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 时间日志生成页面的seo优化. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 付费开通隐私功能,可关闭日志空开. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 设置为主页的相关构思. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 手机页面布局构思. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 甘特图展示多任务(重叠执行时文字难以展示). ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 如果以电话录音的方式问卷调查,让更多人参与完成. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 和他人沟通,20个人大概会有项目全貌. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 每一次沟通都是一次进步,通话录音也记录到项目信息中. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 以上市公司产品为参考,罗列需要的领域和工作. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 未完成的任务自动添加创建日期. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] tlog任务单独评论？又如何显示及更新.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] tlog文章可以评论回复.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 每日视频发给他人截取出特别部分. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 现有内容整理出思维导图. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 文章关键信息显示. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 标签配色自定义. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 自动获取任务开始时间. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 任务时间计算&结果分析. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![better ](https://img.shields.io/badge/-better-1ba4ff.svg) 
      暂停型任务计算
      含子任务计算
      跨天一级任务计算,部分子任务已完成.不在列表中
      need(20m+30m) 时间计算.
- [ ] editor.md的显示不一样. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] ~~任务中的标签用md标签显示.~~. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] md显示pc版完善构思. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] md添加多功能,如editor.md. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 项目报表每天更新直到完结.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 根据地点显示时间百分比,活动半径.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 服务器定时执行tlog2md转换与压力优化. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 任务管理进一步构思. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] app中可以直接购买书籍. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] "1h+1:30"字符与时间的转换. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 学习类任务. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 尝试每天提前一点起床时间. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![try ](https://img.shields.io/badge/-try-72c7ff.svg) ![change ](https://img.shields.io/badge/-change-72c7ff.svg) 
- [ ] 在不同环境运用时间管理. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 每个任务的解决方法. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 每天或每个任务的打分. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 任务评价开放等级. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 问题与解决方法列出. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 到时间但暂未完成的任务. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
- [ ] 查看他人刻意练习的过程. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 刻意练习类任务. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 评估总结行为,在评估回顾一天. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 显示特别标签数或内容. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 可选择日期和主题生成html. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 积累性工作如何处理. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 单个任务添加项目标签. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 系统自动生成第二天的tlog文件. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] ~~智能手表系统~~. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 独立与时间系统的找人单页面. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 超出10分没有开始任务就发起警报. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 时间管理音频张萌11问. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![record ](https://img.shields.io/badge/-record-72c7ff.svg) 
- [ ] 习惯列表. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 专注度统计. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] app快速卡片. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 意愿匹配的行动. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
- [ ] 软件的使用录制自己操作录像作为教程. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 方法论的演变过程. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 贡献权限设置. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 没有理清项目整体工作量多少,不知道多少功能.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 点滴提升时间价值. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 刻意练习. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 学习、练习、评卷、教授. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 练习题. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 没有安排好事情,焦虑的心情无法活在当下(无法做事). ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
- [ ] 专注的方法-我是用任务评估的倒计时. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 时间管理系统演讲框架. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 说名常见问题. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 用大家都易懂的比喻,等比说明分析时间管理. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 第0集部分框架说明视频框架. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 让他人体验快速改变的方法. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 从老师分享的角度考虑. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 每个人基础不一样. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
- [ ] 使用黑色UI皮肤,就像之前ps软件或flex皮肤一样. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 显示已管理总时长,天,时.纯分钟. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 显示已经坚持的行为习惯次数. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 突发任务统计. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 已完成的任务可以添加分类标签. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 任务列表页面,右边小窗口显示任务详情.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 收纳箱展开与关闭&快捷键. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 标签中放入各种特性,如周末,总结,生活,工作. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 生活采购类事务周任务收纳箱. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 统计暂停次数. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 其他人看了任务都能知道我现在和接下来做什么. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 用自己的时间管理日志为分析案例做讲解. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 计划分解 任务评估 真实记录 结果比对 分析原因 经验总结. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 首页. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      布局
      api接口
      前端逻辑+1
      后端
      对接
      测试
- [ ] 罗列出3种等级需要总结的任务. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 任务完成状态,变成添加总结输入区. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 一个项目有成百上前个功能和问题,如何安排处理. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 专业领域标准化思考方式&不同难度表格. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 这个项目需要多少钱？假如100万做这项目？. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 相同任务完成时间的比较,如1个页面布局. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 手机版左右翻页. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 任务设计多个方案. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 如何把他人往项目里带. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 编辑模式&查看模式. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 产品优化-时间监听(按键、屏幕、时间). ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 工作和生活、学习重要性4象限的划分. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 任务执行专注度.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 键盘快捷键动画ctrl&shift+数字.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 批量操作&任务属性带选中状态.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 选择任务,右边就显示任务详情.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 通过查看用户的任务和描述,主管的猜测判断进行优化.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 素材拖动区,就像FLASH一样.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 鸡蛋挑骨头-否定日事清来寻求优化. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 实际教学展示视频录制. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 教练教学自我视频录制. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 教练有效教学价值观点. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 相同问题. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 朋友问题克隆. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 共享生活. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 付费问题列表,其他人电话解答.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 相关应用app,如早起App. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 可接受和希望的合作方向,还有暂不考虑列表. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 项目日报,周报. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 如果有效积累,减少泄露. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 需要分类罗列出自己的想法,别人才能有效提出意见. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 用手机布局的方式思考pc端. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 所到之处就分享演讲时间管理,并获取反馈. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 他人用过什么任务、时间管理工具,感觉怎样？. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 和他人沟通请教时间管理,如何沟通. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 容易忘记就用软件记录,提前一天做准备. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 录制自己的视频语音,在电梯里面听. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 选择任务时,出现同类历史情况和总结. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 新版界面构思. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 起床闹铃DIY-学习音频. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 从游戏角度看时间管理. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 建立新手区. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 经常未完成任务. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 今日打卡排名&总共排名. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 任务子任务模块化如何使用. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 周计划、年计划的任务单元. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 开始模式-系统全屏.便于专注当前任务. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 人物标签技术分类. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 模块市场. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 模块开发,界面拖动DIY. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 键盘操作. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 快捷键显示与测试区域. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 任务序号. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] ui与纯文字版的任务编译页面相互切换. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 了解客户需求,如何提问. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 兼职人员,电话了解未完成任务用户需求. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 网站、app、h5、小程序. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 思考方式:功能性任务、公司性任务. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 网站系统把各类待完任务分类.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 一开始确认意愿,支付10元可获得他人关心和建议. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 从现象中找解决方法. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 关注他人未完成任务. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 任务. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 信息收集、点赞排序、整理、官方收纳、制作视频、. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 好处收集. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 一万小时定律. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 番茄始终&25m时长修改. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 私有数据库. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 整理我现有资料成视频,并寻求他人意见. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 新增资料如何处理. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 现有资料整理. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 域名选择. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 根据重复次数筛选行为习惯. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 开发者token,可以用于开发者获取个人或他人信息. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![admire ](https://img.shields.io/badge/-admire-72c7ff.svg) 
- [ ] 展示所有工作,他人可以查看或参与完成. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 分享. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 打印. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 数据导入导出,. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 企业用户:使用企业短信或者公众号发送通知信息;. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 查看等级:付费查看,作者同意查看,按次查看,通讯录高付费查看.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 等待任务,分工作和生活.自己的计划他人可以查看,如通讯录;. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 按分类显示个人习惯,如所有早起的备注描述.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 工作中勿打扰-通知. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![tool think](https://img.shields.io/badge/tool-think-72c7ff.svg) 
- [ ] 私人秘书-旅行规划、会议安排. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 语音记录-需要标准普通话.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 共享时间. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 奇特的一生读后感文章查看. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 闭环体系形成相互流动. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 定时收费方式,如2020年之后免费,现在按使用次数收费.每次1毛;. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 一个人不够就集合大家的力量. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 按时间分组排名、进阶、. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 分解、重组、四象限、统筹习题. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 目标重组. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 目标分解. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 日周月年计划. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 四象限任务分时间段-上班工作下班生活. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 事情安排总时间. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 粉丝. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 姜禄建我的一生. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] md文件附加思维导图. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 构思项目开发各环节配合常见问题. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 他人模块开发配合. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 长期可输入的文章. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 查看时间列表,统筹安排. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 计划、评估、执行、总结. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 语音临时记录. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 需求区. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 误区、有效经验、错误经验、方法、. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 精品区. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 问答区. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 线下活动. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 徽章. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 任务-奇特任务(随机他人任务). ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 多种投资、代理方式. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 给他人参与的接口. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 平台思考. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 学习资料、打卡、定目标、改习惯、给建议、分享、教学、. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 时间管理工作分类规划. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 时间管理网站、QQ群、贴吧、微信群、论坛. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 项目推送到多职位的人. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 任务分类展示. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 经验分享图文+分类数据库设置. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 重复坚持的习惯次数. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 需要帮忙的任务. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 想完成的任务. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 录制视频. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 每天和2个人聊时间管理. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 玩不成的任务就成了需求. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 互助服务,付费帮忙划分目标,经验传授. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 成长等级. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 社区共修的人,如keep. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] sublime自动上传当日任务. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 任务模板. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 自动导入工作日志. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 系统生成日报. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 每天重复做有改变的事,缩短改变周期.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 操作区域按钮组(新增 停止 合并 删除 取消). ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 新动作的语音提示. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 自动添加名称~~添加标签~~. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 显示每个人的位置和方向. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] app自动采集的参数设置界面. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] ~~甘特图构思~~. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 移动轨迹和不同时段路线颜色.. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 手动添加标签. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 系统推荐标签. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 自动添加标签. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 系统推荐分类. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 合并操作. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] ~~动作激活的灵敏度~~. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 短信提醒用户app开机. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 过滤时间小于5分钟的动作. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] 分析后数据. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] ~~数据记录到内存(js对象),定时发送服务器~~. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] bug_录音mp3没录音但一直都在新增(重复)的文件. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] ~~时间日志md网页设计~~. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] ~~后台运行app获取位置测试~~. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
- [ ] app logo设计. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      美术设计&沟通

### 观点及其他
1.工作优先级![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![view ](https://img.shields.io/badge/-view-72c7ff.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
2.康熙起居录![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![view ](https://img.shields.io/badge/-view-72c7ff.svg) ![record ](https://img.shields.io/badge/-record-72c7ff.svg) 
3.找出时间块减少碎片时间![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![way ](https://img.shields.io/badge/-way-1ba4ff.svg) 
4.键盘的敲击电子速度可以作为一个工作投入度和工作效率的参考![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![view ](https://img.shields.io/badge/-view-72c7ff.svg) 
5.列出工作清单并公开,同事新增事情排队添加.![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![view ](https://img.shields.io/badge/-view-72c7ff.svg) 
6.提前安排减少突发事件![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![rule ](https://img.shields.io/badge/-rule-72c7ff.svg) 
7.视频漏听主体就只有回放![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![view ](https://img.shields.io/badge/-view-72c7ff.svg) ![video ](https://img.shields.io/badge/-video-72c7ff.svg) 
8.功能都用思维导图做评估![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![rule ](https://img.shields.io/badge/-rule-72c7ff.svg) ![better ](https://img.shields.io/badge/-better-1ba4ff.svg) 
9.心怀梦想就会一直和别人聊时间话题![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![view ](https://img.shields.io/badge/-view-72c7ff.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
10.明己惜言-表明自己,珍惜他人言行.![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![view ](https://img.shields.io/badge/-view-72c7ff.svg) ![classic ](https://img.shields.io/badge/-classic-72c7ff.svg) 

### 心中疑惑
- [ ] 如何向他人展现我比上班更努力. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![think ](https://img.shields.io/badge/-思考-72c7ff.svg) 
- [ ] 如何与更多人合作和寻求他人帮助. ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![think ](https://img.shields.io/badge/-思考-72c7ff.svg) 

### 任务详情
#### [ ]项目md中标签时间未显示完成.
![need 18m](https://img.shields.io/badge/预计-18m-orange.svg) ![started 17-07-19 16:13](https://img.shields.io/badge/开始-17/07/19 16:13-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
     查看生成的tlog发现,生成tlog中@cancelled=17-04-02 ![](https://img.shields.io/badge/时间-17:32-ff69b4.svg) 没被识别. 需要转换为括号(), 

#### [x]node版解析tlog缺少加载中文标签yml
![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 25m](https://img.shields.io/badge/耗时-25m-orange.svg) ![started 17-07-19 15:39](https://img.shields.io/badge/开始-17/07/19 15:39-orange.svg) ![done 17-07-19 16:04](https://img.shields.io/badge/完成-17/07/19 16:04-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     tagDic = { 'done': "完成" }
如果在node版中加载yml会导致多版本js难以管理.
项目md中很少用到其他标签. 2中解决方法。1、任务标签写入parsetlog.js中. 2、浏览器在解析tlog生成md. 
暂时采用第一种.
~~(.*): (.*)\n --> "$1":"$2"~~,  
从z1flag.md中复制出主要标签, 删除 #后面内容. 正则替换达到内容格式化.
(.*): (.*)(# .*){0,1}\n --> "$1":"$2"  直接删除并替换. 以下为测试内容.
 critical: 紧重# 重要紧急
 test: 紧重
 high: 重要# 重要不紧急
 low: 紧急# 紧急不重要
 minor: 杂事# 不重要不紧急
 summary: 总结#   =none 没有条件;  see 现象 unknow 出现答案也不认识; grow 成长
 think: 思考# 疑问思考,待解决.

#### [x]项目md任务标签中的时间需要显示日期
![need 15m](https://img.shields.io/badge/预计-15m-orange.svg) ![used 08m](https://img.shields.io/badge/耗时-08m-orange.svg) ![started 17-07-19 15:30](https://img.shields.io/badge/开始-17/07/19 15:30-orange.svg) ![done 17-07-19 15:38](https://img.shields.io/badge/完成-17/07/19 15:38-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
     有一处getTag 调用中忘记添加showDay参数.  ![](https://img.shields.io/badge/时间-15:35-ff69b4.svg)
修改showday后,部分标签没有替换成中文.

#### [ ]tlog中的项目描述会被覆盖
![created 17-07-19 15:16](https://img.shields.io/badge/创建-17/07/19 15:16-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     不覆盖就需要每个tlog文件写入描述,那样文件中信息太过冗余.
如果每个都写就不纯在问题.

#### [ ]app自动记录位置
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     大数据-当多人到公司,如果有1人触发上班.则默认"上班"事件;如果有其他标签,则出现在备选标签中;

#### [ ]修改以前tlog文件参考链接为任务
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     reference 已添加到z1flag.md中。 ![](https://img.shields.io/badge/时间-15:25-ff69b4.svg)

#### [x]参考资料不分项目,如何整合到项目md中?
![done 17-07-19 15:22](https://img.shields.io/badge/完成-17/07/19 15:22-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     近期的参考资料没有独立写到底部, md文件规范,论文、数据都是独立参考资料.
方案：参考资料放到任务描述中, 格式已md规范. 此任务添加特殊 标签@reference. 从描述内容中解析参考链接,可以生成md 文件.

#### [ ]修改之前文件格式,给任务添加项目.
![need 1h](https://img.shields.io/badge/预计-1h-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     之前的文件记录任务没有记录到项目下面。如“taskpaper 配置文件恢复了默认配置”

#### [x]常用行为动作
![done 17-07-19 14:15](https://img.shields.io/badge/完成-17/07/19 14:15-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     工作：...
生活: 醒 静思 发呆 报时 录音 起床 洗漱 出发 早餐 上班 午餐 wc10 晚饭 下班 wc3 喝水 洗澡 电话 动画 电影 旅游 浪费 购物
健康：(运动)
学习：技术 听书 视频 搜索 查看资料
交际：聚餐 活动 培训 
家庭：找女友 父母电话 

#### [x]事情&工作的分类
![done 17-07-19 14:13](https://img.shields.io/badge/完成-17/07/19 14:13-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     重要：重要紧急 不重要紧急 重要不紧急 不重要不紧急
类型：工作 生活 健康 家庭 学习 交际
属性：项目 任务 描述 
阶段: 安排 准备 开始 暂停 完成 取消 进行中
归属：个人 团体 公司
执行者：甲 乙 丙 姜禄建 团体 公司 
性质：任务 优化 问题 沟通 新增 修改 突发 计划
标签：***
项目：A B C D E
重复：无 日 周 月
熟悉：不会 有方向 了解 熟悉  擅长
设备：无 未购 快递 已有 测试
地点：无 5km 10km >10km
利益：亏 无 轻 小赚 大赚
积累：无 重复积累
时长：1天 多日 起始
提醒：无 定时 多次
成功率：10% 50% 80% 100% 
时间：9-18 加班 通宵
人数：1人 多人(沟通 配合)
意愿：讨厌 被动 一般 主动 喜爱  
条件：天气 钱 账号密码 素材 
特殊日：无 周末 节假日
身体：经常大病 小病 疲劳 正常 良好 充沛
分析、总结

#### [ ]logo美化需求
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![help ](https://img.shields.io/badge/-help-72c7ff.svg) 
     - 需要描述：分前景和背景, 前景“T”. 背景是logo
现在效果 ![](http://cdn.d7game.com/markdown/2017-04-20_9b3916c5.jpg?imageView/2/h/150)
效果参考图 ![](http://cdn.d7game.com/markdown/2017-04-22_B25.png?imageView/2/h/150)
前景参考图![](http://cdn.d7game.com/markdown/2017-04-20_9ee9ff5fd8e35b.png?imageView/2/h/150)
背景参考图中的点，需要类似效果 ![](http://cdn.d7game.com/markdown/2017-04-20_eef5f535d2ef7.jpg?imageView/2/h/150)
大概1个小时解决，背景透明+图层填充
前景：球形渐变+蒙版 + 浮雕  17-04-20 ![](https://img.shields.io/badge/时间-11:21-ff69b4.svg)
 - 失败图![](http://cdn.d7game.com/markdown/2017-04-20_sheji1.jpg?imageView/2/h/150)
![](http://cdn.d7game.com/markdown/2017-04-20_sheji2.jpg?imageView/2/h/150)
![](http://cdn.d7game.com/markdown/2017-04-20_sheji3.jpg?imageView/2/h/150)
 - 自己制作需求图
![](http://cdn.d7game.com/markdown/2017-04-20_logo.png?imageView/2/h/150)
![](http://cdn.d7game.com/markdown/2017-04-20_xuqiu1.jpg?imageView/2/h/150)
![](http://cdn.d7game.com/markdown/2017-04-20_xuqiu3.png?imageView/2/h/150)
![](http://cdn.d7game.com/markdown/2017-04-20_xuqiu2.jpg?imageView/2/h/150)
 - logo源文件 http://cdn.d7game.com/z39timelog/logo.psd

#### [x]z39timelog整合
![need 10m](https://img.shields.io/badge/预计-10m-orange.svg) ![used 20m](https://img.shields.io/badge/耗时-20m-orange.svg) ![started 17-07-19 14:10](https://img.shields.io/badge/开始-17/07/19 14:10-orange.svg) ![done 17-07-19 14:30](https://img.shields.io/badge/完成-17/07/19 14:30-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     计划是想把z39timelog.md中的文件整合,查看整个文件发现. 90%内容都是历史日志中拷贝出来的. 而历史文件中不包含

#### [ ]解析项目时,不计算当日日期
![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![cancelled 17-07-19 14:58](https://img.shields.io/badge/取消-17/07/19 14:58-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     上面已经解决,处于同一问题不同表现.

#### [x]项目md需要以项目名为 title
![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 08m](https://img.shields.io/badge/耗时-08m-orange.svg) ![started 17-07-19 15:06](https://img.shields.io/badge/开始-17/07/19 15:06-orange.svg) ![done 17-07-19 15:14](https://img.shields.io/badge/完成-17/07/19 15:14-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]生成项目md中需要显示日期
![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 22m](https://img.shields.io/badge/耗时-22m-orange.svg) ![started 17-07-19 14:36](https://img.shields.io/badge/开始-17/07/19 14:36-orange.svg) ![done 17-07-19 14:58](https://img.shields.io/badge/完成-17/07/19 14:58-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     计算日志都是再 getdaymd 和 tolog中, 可以通过添加参数控制. ![](https://img.shields.io/badge/时间-14:43-ff69b4.svg)

#### [ ]js和node版本对代码的引入和使用
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     pinyin , 把需要模块化的js和模板需要js文件发给他人.

#### [x]提交parseTlog代码
![done 17-07-19 10:30](https://img.shields.io/badge/完成-17/07/19 10:30-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]代码整合到parseTlog文件中
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 18m](https://img.shields.io/badge/耗时-18m-orange.svg) ![started 17-07-19 10:30](https://img.shields.io/badge/开始-17/07/19 10:30-orange.svg) ![done 17-07-19 10:48](https://img.shields.io/badge/完成-17/07/19 10:48-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     整合完成. ![](https://img.shields.io/badge/时间-10:40-ff69b4.svg)
上传代码, 现在只能上传本地git,服务器还没弄好.

#### [x]生成项目的tlog&md
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![started 17-07-19 10:51](https://img.shields.io/badge/开始-17/07/19 10:51-orange.svg) ![done 17-07-19 12:12](https://img.shields.io/badge/完成-17/07/19 12:12-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/19 12:08-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/19 11:42-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![first ](https://img.shields.io/badge/-first-72c7ff.svg) ![tech nodejs](https://img.shields.io/badge/tech-nodejs-72c7ff.svg) 
     所有文件整合生成; 新增文件添加修改;
查找资料 fs.write(fd, string[, position
await fs.readFile('data.csv', 'utf8');
https://www.npmjs.com/package/async-file
https://github.com/davetemplin/async-file  ![](https://img.shields.io/badge/时间-11:12-ff69b4.svg)
代码使用测试成功.  ![](https://img.shields.io/badge/时间-11:32-ff69b4.svg)
await fs.appendFile( think.ROOT_PATH + "/" + 'd7game.txt', 'data to append');
await fs.writeFile( think.ROOT_PATH + "/" + 'd7game.txt', 'hello d7game');
tlog生成成功 ![](https://img.shields.io/badge/时间-11:41-ff69b4.svg)
md生成 ![](https://img.shields.io/badge/时间-12:12-ff69b4.svg)
```code
for (var i = prolist.length - 1; i >= 0; i--) {
       var item = prolist[i] ;
       if( item.type =="project" && item.project != "unproject" ){
           var content = parse.task2log([item]) ;
           var name = think.ROOT_PATH + "/" + item.project + ".tlog" ;
           await fs.writeFile( name , content );
           var namemd = think.ROOT_PATH + "/" + item.project + ".md" ;
           content = parse.getDayMD([item]) ;
           await fs.writeFile( namemd , content );
       }
   };
```

#### [x]熊超电话沟通进展
![used 8m](https://img.shields.io/badge/耗时-8m-orange.svg) ![done 17-07-19 09:57](https://img.shields.io/badge/完成-17/07/19 09:57-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]tlog文件日期优化,done标签的时间权值为5
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]模块化js中如何引入外部文件,如fs
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]评估时间分包含毛时间和纯时间
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]升级https需要的证书资料
![done 17-07-18 09:18](https://img.shields.io/badge/完成-17/07/18 09:18-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     创建https站点 https://bbs.aliyun.com/read/303413.html?spm=5176.100241.0.0.7Wpbi1
七牛免费https证书使用  https://segmentfault.com/a/1190000007669889
![](http://cdn.d7game.com/markdown/2017-07-18_PNZF.png)

#### [ ]标签添加等级
![created 17-07-18 08:27](https://img.shields.io/badge/创建-17/07/18 08:27-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     点击等级可以查看自己和其他人尝试的努力和结果.
需要等级说明. 等级形式为 @better=lv8。 其中标签和等级数字会变化.
难点：每个人等级描述不一样, 每一个行为可以定义为一个等级.
自己做到一个就升一级. 好处在于可以直接借鉴使用他人的方法.
当等级过多时就需要优化. 对等级采用数、完成数、欢迎度进行统计、排序和推荐。 ![](https://img.shields.io/badge/时间-08:40-ff69b4.svg)
```mind
-标签加等级
  -等级描述
  -如何升降
  -等级查看
  -采用他人
  -等级推荐
```

##### [ ]理清思路和功能
![done 17-07-18 08:43](https://img.shields.io/badge/完成-17/07/18 08:43-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![better lv1](https://img.shields.io/badge/better-lv1-1ba4ff.svg) 
      思路和导图写在一级任务中.

#### [x]https升级2域名中的1个
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 05m](https://img.shields.io/badge/耗时-05m-orange.svg) ![started 17-07-18 08:08](https://img.shields.io/badge/开始-17/07/18 08:08-orange.svg) ![done 17-07-18 08:13](https://img.shields.io/badge/完成-17/07/18 08:13-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     阿里云的 cdns.d7game.com
https://cdns.d7game.com/logo1.png 从oss中找到服务器上文件路径测试,可以访问.
阿里云cdns可以使用.  ![](https://img.shields.io/badge/时间-08:14-ff69b4.svg)
另外一个xc在升级.

#### [ ]描述中的md解析时间,时间前面天-符号.
![need 15m](https://img.shields.io/badge/预计-15m-orange.svg) ![started 17-07-17 21:47](https://img.shields.io/badge/开始-17/07/17 21:47-orange.svg) ![cancelled 17-07-17 21:50](https://img.shields.io/badge/取消-17/07/17 21:50-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     评估中同样会出现时间, 容易分不清楚是时间记录还评估时间.  看的人可以自己分辨.
并且现在评估基本已经在mind当中.

#### [x]sublime插入时间快捷键
![done 17-07-17 20:29](https://img.shields.io/badge/完成-17/07/17 20:29-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     Default (Windows).sublime-keymap  insert_date
想不起快捷键实现的方式.之前文件无记录.  写上安装说明.

#### [x]sublime插件安装说明&打包发xc
![done 17-07-17 19:30](https://img.shields.io/badge/完成-17/07/17 19:30-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]所有tlog任务整理
![started 17-07-18 09:38](https://img.shields.io/badge/开始-17/07/18 09:38-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/18 21:36-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     ```mind
-tlog任务
   -多tlog加载
     -目录内遍历 40m
   -多文件切换
     -保持数据(数据积累,现在还没数据库) 50m
     -清空数据(同时上传多问题件,生成不同的md文件) 30m
   -任务按项目归纳
     -跨日任务标题和内容修改 45m
     -跨日任务不同状态 30m
   -任务保存方式
     -项目md 1:30
     -临时数据(还无数据库)
     -数据库 2h
   -导出tlog字符
     -复制到tlog文件 1:30
   -基本开发工作
     -框架代码 30m
     -测试 30m
     -bug 50m
     -遗漏部分 50m
```

##### [ ]框架代码
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![started 17-07-18 09:43](https://img.shields.io/badge/开始-17/07/18 09:43-orange.svg) ![done 17-07-18 10:06](https://img.shields.io/badge/完成-17/07/18 10:06-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      先暂时用现有nodejs项目,在此技术上开发。G:\workspace\z43phone\z43nodejs ![](https://img.shields.io/badge/时间-09:48-ff69b4.svg)
疑问：经常都是这样,是否需要公共nodejs开发项目？
z39tlog 命名 ![](https://img.shields.io/badge/时间-09:53-ff69b4.svg)
创建基本模块代码 修改删除不需要代码. ![](https://img.shields.io/badge/时间-09:56-ff69b4.svg)
放测试index_index.html 和 路由修改,  ![](https://img.shields.io/badge/时间-09:59-ff69b4.svg)
d7game.tunnel.qydev.com/z39tlog
http://192.168.1.2![](https://img.shields.io/badge/时间-01:83-ff69b4.svg)60/z39tlog  可运行,正常进入模块逻辑. ![](https://img.shields.io/badge/时间-10:03-ff69b4.svg)
逻辑js打印 hello d7game ![](https://img.shields.io/badge/时间-10:06-ff69b4.svg)

##### [ ]多tlog加载
![need 40m](https://img.shields.io/badge/预计-40m-orange.svg) ![started 17-07-18 10:07](https://img.shields.io/badge/开始-17/07/18 10:07-orange.svg) ![done 17-07-18 10:42](https://img.shields.io/badge/完成-17/07/18 10:42-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      读取本地文件不会,先查资料. 
thinkjs搜索资料&从历史记录中找到目录相关 this.display(think.ROOT_PATH + "/view/z43phone/index_test.html");
把tlog目录整个拷贝到项目根目录下面做好文件准备工作.包含多余文件,只读取tlog类型. ![](https://img.shields.io/badge/时间-10:11-ff69b4.svg)
think.isFile("/home/welefen/a.txt"); //true
var readFilePromise = think.promisify(fs.readFile, fs);   ![](https://img.shields.io/badge/时间-10:16-ff69b4.svg)
fs.readdir http://nodejs.cn/api/fs#fs_fs_readdir_path_options_callback 
以上资料查找&查看 ![](https://img.shields.io/badge/时间-10:26-ff69b4.svg)
读取目录所有文件并找到.tlog类型. ![](https://img.shields.io/badge/时间-10:41-ff69b4.svg)
还需要读取文件,这部分工作已经新增遗漏任务. ![](https://img.shields.io/badge/时间-10:42-ff69b4.svg)

##### [ ]node版parsetlog
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![started 17-07-18 10:52](https://img.shields.io/badge/开始-17/07/18 10:52-orange.svg) ![done 17-07-18 11:58](https://img.shields.io/badge/完成-17/07/18 11:58-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      疑问如何导入parsetlog.js? 通常情况,是模块开发的方式import 引入.
editor 中开发模块类似, 先把js 做成commonjs格式. 
parsetlog中已有读取目录文件。 ![](https://img.shields.io/badge/时间-11:09-ff69b4.svg)
正则替换 function (.*)\( --> exports.$1 = function(   ![](https://img.shields.io/badge/时间-11:17-ff69b4.svg)
~~js模块化引入~~ 可以不用,
理解Nodejs 的模块化  http://blog.csdn.net/loudyten/article/details/51255088
直接引入文件即可 var Hello = require ('./hello');  
成功引入外部js文件. ![](https://img.shields.io/badge/时间-11:24-ff69b4.svg)
疑问：模块化js中如何引入外部文件, 先删除多余不会的模块化代码. ![](https://img.shields.io/badge/时间-11:29-ff69b4.svg)
parseTlogFile is not defined  报错. ![](https://img.shields.io/badge/时间-11:35-ff69b4.svg)
parseTlogFile --> this.parseTlogFile 后当前报错解决,所有其他的引用都需要加this. ![](https://img.shields.io/badge/时间-11:42-ff69b4.svg)
所有添加this. 太麻烦, 直接删除之前的模块化修改. 添加代码exports.parseTlogFileDir = parseTlogFileDir ; 后代码正常运行,运行后保文件解析错误.  ![](https://img.shields.io/badge/时间-11:44-ff69b4.svg)
根据错误提示,找到并修改后解析成功. ![](https://img.shields.io/badge/时间-11:46-ff69b4.svg)
export. 需要的function 大概20个. ![](https://img.shields.io/badge/时间-11:59-ff69b4.svg)

##### [ ]node版js 直接在底部添加export.function 最快
![done 17-07-18 11:47](https://img.shields.io/badge/完成-17/07/18 11:47-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      第一次做,考虑复杂了. 想连带其他问题一起解决.

##### [ ]测试
![need 15m](https://img.shields.io/badge/预计-15m-orange.svg) ![started 17-07-18 13:37](https://img.shields.io/badge/开始-17/07/18 13:37-orange.svg) ![done 17-07-18 13:49](https://img.shields.io/badge/完成-17/07/18 13:49-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      多文件解析感觉可能不正常. ![](https://img.shields.io/badge/时间-13:37-ff69b4.svg)
Cannot read property 'tasks' of null ,报错表面看起来没问题,不能断点. ![](https://img.shields.io/badge/时间-13:45-ff69b4.svg)
因为控制台打印错误,注释后正常.

##### [ ]多文件切换-保持数据
![need 50m](https://img.shields.io/badge/预计-50m-orange.svg) ![started 17-07-18 13:52](https://img.shields.io/badge/开始-17/07/18 13:52-orange.svg) ![done 17-07-18 16:55](https://img.shields.io/badge/完成-17/07/18 16:55-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      parse.parseTlogFileDir 解析得到的所有文件的项目数组, ![](https://img.shields.io/badge/时间-14:00-ff69b4.svg)
数据去重整理, node 中还需要引入 underscore , 应该是自己新添代码中才用到,所以现在没有报错. ![](https://img.shields.io/badge/时间-14:03-ff69b4.svg) 
npm -i underscore -s
难点整合任务,比对标题相似度. 直接比对遍历次数过多.可以先完成功能在优化. ![](https://img.shields.io/badge/时间-14:16-ff69b4.svg)
如果完成的任务,后面不会出现,因此不需比对,麻烦是二级任务. ![](https://img.shields.io/badge/时间-14:30-ff69b4.svg)
如果文件名不同就添加为新任务,   ![](https://img.shields.io/badge/时间-15:28-ff69b4.svg)
+过滤每天重复性的生活.  生活如何处理. 按每天为单位分别存储. ![](https://img.shields.io/badge/时间-15:29-ff69b4.svg)
任务太多,需要删除文件剩余2个便于测试. 尽量减少无关内容. ![](https://img.shields.io/badge/时间-16:07-ff69b4.svg)
+前期版本tlog中生活任务不包含life标签.
删除大部分类容后测试,发现测试失败. ![](https://img.shields.io/badge/时间-16:24-ff69b4.svg)
2级任务合并成功, 一级的状态和标签失败. ![](https://img.shields.io/badge/时间-16:43-ff69b4.svg)
修改测试功能成功.

##### [ ]多文件切换-清空数据
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![started 17-07-18 16:58](https://img.shields.io/badge/开始-17/07/18 16:58-orange.svg) ![done 17-07-18 17:40](https://img.shields.io/badge/完成-17/07/18 17:40-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      同时上传多问题件,生成不同的md文件. 和上面任何有功能部分. for单独处理数据即可. 
通过parse.parseTlogFileDir 获取某天数据失败,看不出时间. ![](https://img.shields.io/badge/时间-17:15-ff69b4.svg)
用单个文件的内容转换.
之前正常的逻辑,现在各种报错. getTag 获取tag失败, Cannot read property 'tasks' of undefin ![](https://img.shields.io/badge/时间-17:37-ff69b4.svg)

##### [ ]任务按项目归纳-跨日任务不同状态
![done 17-07-18 18:33](https://img.shields.io/badge/完成-17/07/18 18:33-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![error ](https://img.shields.io/badge/-error-red.svg) 
      在上面任务一起完成了, 制作的时候没有明确划分.

##### [ ]任务保存方式-项目md
![need 1:30](https://img.shields.io/badge/预计-1:30-orange.svg) ![started 17-07-18 18:31](https://img.shields.io/badge/开始-17/07/18 18:31-orange.svg) ![done 17-07-18 18:37](https://img.shields.io/badge/完成-17/07/18 18:37-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      console.log( parse.getDayMD([prolist[1]]) );

##### [ ]任务保存方式-数据库
![need 2h](https://img.shields.io/badge/预计-2h-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      暂时不需要. 过几天需要如何处理

##### [ ]导出tlog字符
![need 1:30](https://img.shields.io/badge/预计-1:30-orange.svg) ![started 17-07-18 18:37](https://img.shields.io/badge/开始-17/07/18 18:37-orange.svg) ![done 17-07-18 21:34](https://img.shields.io/badge/完成-17/07/18 21:34-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      领取任务,在tlog编辑器操作.后期系统完善后可能不在使用. 主要在系统从数据库读取,已有任务导出tlog. 难点区域标签~~无法恢复~~可以恢复.
life critical high low minor view 也可以恢复
这是根据我自己的模板恢复, 其他人也有自己的模块标签. ![](https://img.shields.io/badge/时间-18:42-ff69b4.svg)
解析昨天tlog文件方便恢复. 解析7-17.tlog 直接生成 test.tlog  
找到6种分类的任务; 生成区域字符; 去除区域标签; task2str ; ![](https://img.shields.io/badge/时间-19:13-ff69b4.svg)
```mind
-导出tlog
  -获取今日任务 0m
  -任务转换成projectlist
  -找出6种分类 10m
  -生成区域字符 10m
  -去除区域标签 15m
  -task2str 20m
  -+没在分区的
```
思维导图 ![](https://img.shields.io/badge/时间-19:19-ff69b4.svg)
找出6种分类, 项目中都是同一类. unproject 中包含5中分类. ![](https://img.shields.io/badge/时间-19:58-ff69b4.svg)
生成区域字符  ![](https://img.shields.io/badge/时间-20:07-ff69b4.svg)
去除区域标签 ![](https://img.shields.io/badge/时间-20:40-ff69b4.svg)
task2str , 标签字符和缩进不正常. 
 标签正常显示,缩进还不正常. ![](https://img.shields.io/badge/时间-21:06-ff69b4.svg)
 缩进字符替换  ![](https://img.shields.io/badge/时间-21:33-ff69b4.svg)

##### [ ]多文件切换因为调用接口错误导致延迟.
![done 17-07-18 21:34](https://img.shields.io/badge/完成-17/07/18 21:34-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![summary ](https://img.shields.io/badge/-总结-1ba4ff.svg) 
#### [ ]任务搜索
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]软件系统收费,提供用劳动换免费.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     罗列出自己的需求,每个需求对应积分和价格.完成获得.

#### [ ]项目展示设置面板
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     设置显示隐藏已完成任务,显示顺序. 创建时间,自定义顺序,

#### [x]当前只有我1个人,首要日计划功能,需求展示.
![done 17-07-17 17:25](https://img.shields.io/badge/完成-17/07/17 17:25-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     需求是做日计划自己用; 需求展示可以方便和更多的人合作.
尽量简化需求,尽早展开工作.其中需求展示只要是网页既可.现在的markdown展示即可. 理想系统展示是整个项目更多功能, 考虑一步步展开工作,markdown更为合适. 
当日计划功能现在也用不上,现在的首要工具用sublime就可以了.然后把tlog文件导入系统即可. ![](https://img.shields.io/badge/时间-17:03-ff69b4.svg)
那当下需要整理出所有未完成任务, 把任务进行分类计划. ![](https://img.shields.io/badge/时间-17:04-ff69b4.svg)
整理历史tlog中项目所有任务, 分类显示并做今日计划,导出tlog字符文件. ![](https://img.shields.io/badge/时间-17:24-ff69b4.svg)

#### [x]做明日计划
![done 17-07-17 21:55](https://img.shields.io/badge/完成-17/07/17 21:55-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![change ](https://img.shields.io/badge/-change-72c7ff.svg) 
     总是内心不满足,缺少工具还没有做计划.

#### [ ]电脑版本任务swipeout显示按钮接任务
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]项目分解部门工作类型
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]自己需求目标整理罗列
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]需求需要系统,又进入了之前的恶循环.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]罗列需求,找人一起参与完成.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]程序可以关注任务状态,更改通知
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     当任务或子任务有状态更新时,即时通知.

#### [ ]app主页显示当然完成任务总数,实时显示
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     关注或群中人员的任务状态共享.

#### [ ]熊超18:30电话聊15分钟,说大概需要做的工作.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]app录音记录创意想法,空闲时在定期整理.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]“一日一生”目录&资料查看
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 33m](https://img.shields.io/badge/耗时-33m-orange.svg) ![started 17-07-17 14:39](https://img.shields.io/badge/开始-17/07/17 14:39-orange.svg) ![done 17-07-17 15:12](https://img.shields.io/badge/完成-17/07/17 15:12-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     “一日一生”的信念 今天的自己今天就已死去，明天又将诞生一个崭新的自己。
一日一生比"假如今天是生命最后一天"更容易理解. ![](https://img.shields.io/badge/时间-15:50-ff69b4.svg)

#### [x]电影纪录片 一日人生 / 同一天的生活/ 浮生一日
![need 94m](https://img.shields.io/badge/预计-94m-orange.svg) ![used 57m](https://img.shields.io/badge/耗时-57m-orange.svg) ![started 17-07-17 13:16](https://img.shields.io/badge/开始-17/07/17 13:16-orange.svg) ![done 17-07-17 14:13](https://img.shields.io/badge/完成-17/07/17 14:13-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]日事清员工新搜索功能通知
![done 17-07-17 11:20](https://img.shields.io/badge/完成-17/07/17 11:20-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     自然的就提出了建议,对方不懂. 然后产生了思绪和说明. 
减少干扰回复对方"我已经没使用日事清，虽然付费了，但不用通知我。" 通知我大概是源于我前几天的建议.

#### [x]熊超电话沟通进度&观点
![used 13m](https://img.shields.io/badge/耗时-13m-orange.svg) ![done 17-07-17 10:39](https://img.shields.io/badge/完成-17/07/17 10:39-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bad ](https://img.shields.io/badge/-bad-red.svg) 
     现在mysql 和nginx 安装好了, 剩下还需要6个小时.
10分钟电话,

##### [x]自己需求不明确,对方接受信息不吻合
![done 17-07-17 10:40](https://img.shields.io/badge/完成-17/07/17 10:40-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![summary ](https://img.shields.io/badge/-总结-1ba4ff.svg) 
      对方以为我不急.

##### [x]对方说话信息不全,总是问一句说一句.第3次提醒.
![done 17-07-17 10:42](https://img.shields.io/badge/完成-17/07/17 10:42-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![summary ](https://img.shields.io/badge/-总结-1ba4ff.svg) 
      有6个关键点,沟通中只说2个,其他4个就不知道什么情况.
和其他朋友在合作时,出现过下一阶段问题. 这些问题总是重复出现和遇见.需要罗杰解决.

#### [ ]自己价值观如何融入家庭和朋友中
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     把时间用在能实际帮助对方的方式上,而不是去人力物力去看对方一眼,400元的花只是接到手后因为麻烦就仍在旁边.

#### [x]tmp模板文件中添加新的行为
![need 5m](https://img.shields.io/badge/预计-5m-orange.svg) ![used 17m](https://img.shields.io/badge/耗时-17m-orange.svg) ![started 17-07-17 09:44](https://img.shields.io/badge/开始-17/07/17 09:44-orange.svg) ![done 17-07-17 10:01](https://img.shields.io/badge/完成-17/07/17 10:01-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     tmp添加完毕 ![](https://img.shields.io/badge/时间-09:47-ff69b4.svg) 
+所有生活标签内容添加对应标签词 ![](https://img.shields.io/badge/时间-10:04-ff69b4.svg)
```mind
-添加标签
    -标签英文
    -模板标签
    -标签格式化
```

#### [ ]兼职需要解决快速分发需求达成合作,减少小事情和单个人沟通失败造成的浪费.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
     难度在于目标不一致,主要是赚钱. 而大部分人不缺钱,好些人换工作半个月押金不要.

#### [x]兼职沉默后说不做了
![done 17-07-17 08:58](https://img.shields.io/badge/完成-17/07/17 08:58-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bad ](https://img.shields.io/badge/-bad-red.svg) ![waste ](https://img.shields.io/badge/-waste-red.svg) 
     兼职QQ沟通30分钟后,拿到我的需求20小时没回复,主动找上她“不好意思，昨天写了一点忽然公司有事，可能没有时间写了”.

#### [ ]通过讲师培训内容了解“时间管理”关键能力
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     企业内训 https://www.rishiqing.com/courses.php

#### [ ]思考&决定
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     喜欢并确定在"时间管理"领域,自己擅长技术. 其中践行&自己工具大概国内前10.
|上班|上班+储备|创业|
| ------------ | ------------ | ------------ |
|2w+| 7k+ | 0+ |
|   |   |   |

#### [ ]项目页面+分组列表显示&标签分组
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     创建项目页面.
项目页面分类列表显示, 每列可以显示多种标签,标签不能重叠.
难点：分类列表之间相互切换,标签如何改变？

#### [ ]加入20个时间管理群
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     了解群中主要话题,大家关注和需求点.

#### [ ]加群"时间管理"批量发送推广信息
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     有谁知道这是什么系统软件吗？ 会让小号大量时间查找,节约时间需要直接给出产品入口.
qq群搜索"时间 时间管理 "

#### [ ]收集可能节约时间的建议和创意
![created 17-07-16 09:43](https://img.shields.io/badge/创建-17/07/16 09:43-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     不限行业领域,类似头脑风暴.

#### [x]手机收纳箱和计划手稿图
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 29m](https://img.shields.io/badge/耗时-29m-orange.svg) ![started 17-07-15 20:34](https://img.shields.io/badge/开始-17/07/15 20:34-orange.svg) ![done 17-07-15 21:03](https://img.shields.io/badge/完成-17/07/15 21:03-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
     ![](http://cdn.d7game.com/markdown/2017-07-15_O63YM7.png?imageView/2/w/375/h/667)

#### [ ]手机如何展示查看分类代办任务
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
     碎片时间思考, 需要从待办是想中查看特定分类任务. 构思像腾讯视频一样,顶部弹出分类选择. ![](https://img.shields.io/badge/时间-20:04-ff69b4.svg)

#### [ ]时间除了外包还有哪些市场应用
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
#### [x]建议页面构思
![done 17-07-15 18:24](https://img.shields.io/badge/完成-17/07/15 18:24-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
     手稿图, ![](http://cdn.d7game.com/markdown/2017-07-15_748229773.jpg?imageView/2/w/375/h/667)

#### [ ]构思：如何与桌面操作系统整合
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
     ![](http://cdn.d7game.com/markdown/2017-07-15_RM2FJ.png?imageView/2/w/375/h/667)

#### [ ]每天3个电话和他人沟通项目
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
#### [ ]如果我有100w我会如何展开工作
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
     各社区发文章,

#### [ ]找到有相同志向的人
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
     其中会出现各种原因难以合作，如经济，理解，地域，性格，分工.

#### [ ]同一事情多种分类难以查找和管理.如work,excel
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
#### [ ]文件分开管理又不知今日做了什么.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
#### [ ]梦想目标是想赚钱,而当下忙的这些事情不赚钱,那你愿意花多少钱解放你的时间,让人替代完成这些.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
#### [ ]任务地图
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
     把技能知识点作为图谱,没完成掌握一点就点亮标签.

#### [ ]各部分细节梳理
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
#### [x]时间系统框架
![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 56m](https://img.shields.io/badge/耗时-56m-orange.svg) ![started 17-07-15 16:09](https://img.shields.io/badge/开始-17/07/15 16:09-orange.svg) ![done 17-07-15 17:05](https://img.shields.io/badge/完成-17/07/15 17:05-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![better ](https://img.shields.io/badge/-better-1ba4ff.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
     网站系统有哪些分类模块
整个环节行为：设目标 分解 重组 评估 排期 [归纳|标签] 准备 咨询 求助 执行 记录 分析 总结 归档  
思维文字: 日程 计划 市场 [任务] 日报 总结 [功过] 群组 
        [排行榜 挑战任务 称号 经典 名人]  
        建议 教程 社区 公司 合作 版本 功能 价格             
```mind
-时间系统
  -时间管理
    -日程 
      -年月日
      -项目
      -人员
    -计划 
    -日报 
    -总结
    -版本规划 
    -价格
    -群组v2
    -市场v2
    -教程v2
      -建议
      -需求
      -排行
      -付费优先
    -建议v2
    -咨询v2
    -评论v3
    -论坛v3
    -开放接口?
  -游戏化
    -签到
    -任务
    -勋章
    -经验
    -积分
    -排名
    -好友
    -在线奖励
  -公司
    -合作 
    -功能
    -地址
    -招聘
    -使命
```     
问题:v1 v2 v3多版本如何展示. 模块前直接添加版本. ![](https://img.shields.io/badge/时间-16:31-ff69b4.svg)
分产品和公司,放到一起就容易理不清了. 最好分成2个, 而分2个的工作量就更大. ![](https://img.shields.io/badge/时间-16:44-ff69b4.svg)

#### [ ]眼前工作顺序梳理
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
     上传 保存文件 全部上传 保存数据库 展示 ~~分类展示 单人 多人~~

#### [x]百度“时间”没有广告
![done 17-07-15 08:41](https://img.shields.io/badge/完成-17/07/15 08:41-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
#### [ ]任务上传服务器
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
#### [ ]tlog解析后上传保存md
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
#### [ ]生活类数据上传服务器
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
#### [ ]新版app实现生活类行为
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
#### [x]```mind 字符输入太难,创建快捷标签
![done 17-07-14 17:23](https://img.shields.io/badge/完成-17/07/14 17:23-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
#### [x]git项目目录权限
![done 17-07-14 14:48](https://img.shields.io/badge/完成-17/07/14 14:48-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
     Git 文件夹或者子目录的权限控制 http://blog.csdn.net/u010781856/article/details/47704801
git仓库添加用户权限 https://zhidao.baidu.com/question/922909824707143699.html

#### [ ]和电话系统结合
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
     群组和任务需求沟通,

#### [x]pendo 搜索了解
![done 17-07-14 13:31](https://img.shields.io/badge/完成-17/07/14 13:31-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
#### [x]使命目的
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 11m](https://img.shields.io/badge/耗时-11m-orange.svg) ![started 17-07-14 11:50](https://img.shields.io/badge/开始-17/07/14 11:50-orange.svg) ![done 17-07-14 12:01](https://img.shields.io/badge/完成-17/07/14 12:01-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
     提升时间使用率,用更少时间做更多事. 提升空闲时间价值.
有效求助帮助,活的明白,充实.  解决未来广场舞命运.
 风景看的太多,剩下的只是在美景下发呆,发愁.
让生活更有趣,现在静下来后也是发呆犯傻.
让所有时间有效化,包括学习和练手.

#### [ ]网页系统添加图片使用拖动上传或粘贴上传
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
     普通用户无法使用插件或者7牛账号.

#### [ ]时间碎片展示
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
#### [x]时间碎片展示构思
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 28m](https://img.shields.io/badge/耗时-28m-orange.svg) ![started 17-07-14 11:04](https://img.shields.io/badge/开始-17/07/14 11:04-orange.svg) ![done 17-07-14 11:32](https://img.shields.io/badge/完成-17/07/14 11:32-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
     不是时间块,看不出碎片时间间隔.
关键显示碎片时间. 其他任务简写.  显示碎片时间前后关键任务. 任务最多显示一行,不显示标签.
3、5分钟用固定高度颜色显示, 浅蓝颜色任务块和背景,绿色碎片时间块. 
其他任务固定高度, 碎片时间设置最低和最高，按照时间长度等比+时间显示.
纯文本系统难以每次手动记录开始时间,容易忘记. 图形化界面很容易看出(有提醒功能作用)
问题：所有任务都显示会显得太臃肿,太长(多余)而不想要. 
碎片时间的作用是,提醒主人合理安排时间.
以时间块为主题,任务缩短成4个子像标签一样显示的区域. 主要显示时间,任务半透明,
空白时间：显示推荐任务(人为或系统)  问题：不知道可以做什么？这和历史积累有关,如果前期没安排或记录,初级用户是不知道如何运用解决.这时可以推荐他人的任务(处理方式).  ![](https://img.shields.io/badge/时间-11:31-ff69b4.svg)
![](http://cdn.d7game.com/markdown/2017-07-14_U9L31.png)

#### [x]计划自己的一天
![used 13m](https://img.shields.io/badge/耗时-13m-orange.svg) ![started 17-07-14 10:00](https://img.shields.io/badge/开始-17/07/14 10:00-orange.svg) ![done 17-07-14 10:13](https://img.shields.io/badge/完成-17/07/14 10:13-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![change ](https://img.shields.io/badge/-change-72c7ff.svg) ![high ](https://img.shields.io/badge/-重要-yellowgreen.svg) 
     无法做好长期计划,就计划好一天.让当天100%的动起来. 不贪心(期望一次计划好5年的未来计划).
醒、起床、洗漱|洗澡、早饭、talk录音|录像、wc10、出发、上班、订餐、锻炼、午饭、晚饭、总结、整理文件、talk准备、计划(排期)、下班、回家、洗漱2、睡觉、
每日工作也许可以提前一天或者周末结束做好.

#### [x]时间日志朋友反馈信息
![done 17-07-13 14:18](https://img.shields.io/badge/完成-17/07/13 14:18-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![record ](https://img.shields.io/badge/-record-72c7ff.svg) 
     不明白;看起来好看;

#### [x]项目太庞大,眼前创意不急,历史任务多而不清.
![done 17-07-13 13:55](https://img.shields.io/badge/完成-17/07/13 13:55-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
#### [ ]资料生成可打印信息,装订成书
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]整理出历史tlog文件中项目任务
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]恢复数据库
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![started 17-07-13 13:56](https://img.shields.io/badge/开始-17/07/13 13:56-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/13 18:10-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/13 17:44-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/13 17:24-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/13 17:00-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/13 14:51-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/13 16:40-orange.svg) ![cancelled 17-07-13 20:28](https://img.shields.io/badge/取消-17/07/13 20:28-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     阿里云快照收费 ![](https://img.shields.io/badge/时间-14:05-ff69b4.svg)
数据库密码错误, 找不到数据存储目录
经过历史记录命名查看到目录不存在,数据已删除. 
原来修改密码的方式不能运行, 现在担心数据库安装失败. ![](https://img.shields.io/badge/时间-14:30-ff69b4.svg)
update mysql.user set authentication_string=password('zhida') where user='root' and host='localhost';  
 flush privileges;  
执行成功,修改失败. ![](https://img.shields.io/badge/时间-14:42-ff69b4.svg)
UPDATE user SET Password = password ( 'zhida' ) WHERE User = 'root' ; 
# mysql -u root -ppassword    //进入mysql控制台
# mysql>use mysql;
# mysql>update user set host = '%' where user = 'root';    //这个命令执行错误时可略过
# mysql>flush privileges;
# mysql>select host, user from user; //检查‘%’ 是否插入到数据库中
#mysql>quit
CentOS6.5下MySQL远程连接 http://blog.csdn.net/freedom_wbs/article/details/53043543
![](http://cdn.d7game.com/markdown/2017-07-13_14.png)
远程连接成功,但本地连接失败.经过navicat远程查看后本地连接数据库密码为空.
添加账号d7game成功,远程登录失败,本地登录成功.
需要查看mysql默认编码,担心有问题. 创建数据库需要选择编码,问题不存在 ![](https://img.shields.io/badge/时间-15:19-ff69b4.svg)
d7game可以远程和本地登录,但是测试创建的数据库没有权限,且链接上后弹框报错. ![](https://img.shields.io/badge/时间-15:33-ff69b4.svg)
![](http://cdn.d7game.com/markdown/2017-07-13_L1I.png)
创建数据库也报错,完全异常没权限. 
下载安装新版navicat软件 ![](https://img.shields.io/badge/时间-15:44-ff69b4.svg)
root本地登录和远程登录密码不一样,本地为空. ![](https://img.shields.io/badge/时间-15:57-ff69b4.svg)
root登录后使用数据库提示错误,
mysql> use mysql; 
ERROR 1044 (42000): Access denied for user ''@'localhost' to database 'mysql'
导入重新安装数据库之前出错的cmswing数据库,测试成功. 新版数据库是可以的.主要是基本配置运行 ![](https://img.shields.io/badge/时间-16:14-ff69b4.svg)
root导入和创建的数据库,d7game账号登录的都看不见. ![](https://img.shields.io/badge/时间-16:18-ff69b4.svg)
mysql root@% 创建的数据库 root@localhost 和其他账号都看不见,是什么情况？ ![](https://img.shields.io/badge/时间-16:33-ff69b4.svg)
求助,沟通失败,对不上号.直接发密码求助. ![](https://img.shields.io/badge/时间-16:39-ff69b4.svg)
问题解决,还是找专业有经验更好. 
新建test账号还是和刚才情况一样, 所以如何设置必须知道。 ![](https://img.shields.io/badge/时间-16:55-ff69b4.svg)
mysql 用户管理和权限设置  新建test账号还是和刚才情况一样, 所以如何设置必须知道。 
GRANT ALL PRIVILEGES ON *.* TO 'test'@'%'IDENTIFIED BY 'test' WITH GRANT OPTION; 
备份系统快照, mysql_setting.需要构思mysql恢复如何操作？ ![](https://img.shields.io/badge/时间-17:25-ff69b4.svg)
1、恢复快照进入数据目录,保存下载. 2、回滚现在数据, 上传解压. 
尝试aliyun搜索同时,想起独立数据库恢复很方便,查看价格最低配置1.4k/年. ![](https://img.shields.io/badge/时间-17:33-ff69b4.svg) 
压缩整个db,等待压缩完毕后下载  ![](https://img.shields.io/badge/时间-18:13-ff69b4.svg)
压缩完毕, \cp -rf /usr/local/mysql/var/alldb.zip /home/wwwroot/
du -sh 数据库1gb,文件有点大. ![](https://img.shields.io/badge/时间-18:22-ff69b4.svg)
删除部分后依然很大,查看 mysql/var/ 目录下的bin文件是什么？
rz xshell中直接上传文件. ftp现在无法运行. ![](https://img.shields.io/badge/时间-18:38-ff69b4.svg)
现在数据库目录 /usr/local/mysql/data/ ![](https://img.shields.io/badge/时间-18:44-ff69b4.svg)
CentOS中zip压缩和unzip解压缩  http://blog.csdn.net/carechere/article/details/50844846
ibdata1\ib_logfile0 是什么, 事务回滚文件.
cp /home/wwwroot/alldb.zip /home/data/ 
unzip /home/wwwroot/alldb.zip -d /home/data/ 
cp /usr/local/mysql/data /home/data  复制失败
cp -f /usr/local/mysql/data/* /home/data 复制失败, 子文件夹没复制 ![](https://img.shields.io/badge/时间-19:05-ff69b4.svg)
cp -rf /usr/local/mysql/data/* /home/data  添加r即可.
复制完毕,还需要重新指定数据库目录. ![](https://img.shields.io/badge/时间-19:08-ff69b4.svg)
service mysql restart 报错缺少iZ23w4kfbpoZ.pid , cp -rf /usr/local/mysql/data/iZ23w4kfbpoZ.pid /home/data
文件复制后重启mysql,iZ23w4kfbpoZ.pid 就消失报错了. ![](https://img.shields.io/badge/时间-19:18-ff69b4.svg)
unzip /home/wwwroot/alldb.zip -d /usr/local/mysql/data
启动后报错,感觉是权限问题. 修改所有目录为777,后单个数据库能进入,数据表打开错误. ![](https://img.shields.io/badge/时间-19:29-ff69b4.svg)
zentao 打开成功. 大部分失败,数据库无法打开,表没权限. ftp也不能运行,
![](http://cdn.d7game.com/markdown/2017-07-13_VX2.png)
![](http://cdn.d7game.com/markdown/2017-07-13_WQ7.png)
![](http://cdn.d7game.com/markdown/2017-07-13_YLL[Y.png)
无法打开也无法直接删除.  再次回滚备份,导出用到的2个数据库sql.
上传整个文件夹, 解压再次测试. 文件太大上传慢,放弃这种方式. ![](https://img.shields.io/badge/时间-19:48-ff69b4.svg)
sql导出成功, ![](https://img.shields.io/badge/时间-20:08-ff69b4.svg)
启动系统后,navicat连接报错吓死人,启动mysql后正常连接.
zentao数据库漏掉, 因为上次测试中只有它成功就感觉不需要备份了. 用单独考出文件无法上传. ![](https://img.shields.io/badge/时间-20:21-ff69b4.svg)
www.d7game.com 数据库导入时提示183条错误,网站依然无法运行. 网站和其他数据库都已经无人使用,直接放弃. ![](https://img.shields.io/badge/时间-20:25-ff69b4.svg)

#### [ ]时间日志页面优化
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     文件加载太大,需要删除多余部分.
图片懒加载.

#### [ ]时间日志生成页面的seo优化
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     生成meta,页面超链接.
img alt 添加关键词.

#### [ ]付费开通隐私功能,可关闭日志空开
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]设置为主页的相关构思
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]标签图片seo优化,添加实际关键词.
![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 10m](https://img.shields.io/badge/耗时-10m-orange.svg) ![started 17-07-13 09:26](https://img.shields.io/badge/开始-17/07/13 09:26-orange.svg) ![done 17-07-13 09:36](https://img.shields.io/badge/完成-17/07/13 09:36-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     img标签alt与title的seo作用 http://www.cnblogs.com/xiaoleidiv/p/3147978.html

#### [x]服务器创建git项目,上传代码
![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 51m](https://img.shields.io/badge/耗时-51m-orange.svg) ![started 17-07-13 11:05](https://img.shields.io/badge/开始-17/07/13 11:05-orange.svg) ![done 17-07-13 11:56](https://img.shields.io/badge/完成-17/07/13 11:56-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     z39timelog 已有项目,需要把新添加的代码分类管理. ![](https://img.shields.io/badge/时间-11:07-ff69b4.svg)
app pc h5
h5 从editor.md文件中把mind抽离成 timelog.html ![](https://img.shields.io/badge/时间-11:34-ff69b4.svg)
pc 从rishing中整理出需要的文件 ![](https://img.shields.io/badge/时间-11:40-ff69b4.svg)
+sublime插件

#### [x]it&me 群中时间日志信息查看与回复
![done 17-07-13 09:13](https://img.shields.io/badge/完成-17/07/13 09:13-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]手机页面布局构思
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     顶部：菜单、日期、年月、
底部：评论、关注、赞、踩

#### [x]观点和总结一起时不显示观点.
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 19m](https://img.shields.io/badge/耗时-19m-orange.svg) ![started 17-07-12 19:18](https://img.shields.io/badge/开始-17/07/12 19:18-orange.svg) ![done 17-07-12 19:37](https://img.shields.io/badge/完成-17/07/12 19:37-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     进入了删除逻辑,但是没效果. ![](https://img.shields.io/badge/时间-19:33-ff69b4.svg)

#### [ ]甘特图展示多任务(重叠执行时文字难以展示)
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
##### [ ]甘特图展示构思
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![started 17-07-12 14:11](https://img.shields.io/badge/开始-17/07/12 14:11-orange.svg) ![done 17-07-12 14:34](https://img.shields.io/badge/完成-17/07/12 14:34-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      展示什么信息;如何展示;插件使用方式;
展示什么：需要表示时间重叠.
如何展示：分早晨上班前,上午(9-12)中午(12-1:30)下午(1:30-18)晚上(![](https://img.shields.io/badge/时间-18:24-ff69b4.svg))
以24小时即时？通宵如何处理.    ![](https://img.shields.io/badge/时间-14:15-ff69b4.svg)
frappe/gantt js对象查看  ![](https://img.shields.io/badge/时间-14:17-ff69b4.svg)
mermaid/gantt 数据查看,纯文本. ![](https://img.shields.io/badge/时间-14:19-ff69b4.svg)
2种甘特图都需要通过现有task数据转换,因此一样. 2个都是跨天跨月. 我需要单日展示.  frappe/gantt 优点可以拖动,而我是手机展示,此优点用不上. 数据可以导出为json后在md中解析使用. ![](https://img.shields.io/badge/时间-14:28-ff69b4.svg)
mermaid/gantt 使用了moment功能更强大; 纯文本可支持md格式. 相比更适合. 问题: 无法即时修改查看,特别是支持的最小时间单位. ![](https://img.shields.io/badge/时间-14:29-ff69b4.svg)
分为5段时间,2类(工作和非工作). ![](https://img.shields.io/badge/时间-14:31-ff69b4.svg)

##### [ ]mermaid/gantt 官网查看
![started 17-07-12 14:35](https://img.shields.io/badge/开始-17/07/12 14:35-orange.svg) ![done 17-07-12 15:00](https://img.shields.io/badge/完成-17/07/12 15:00-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      发现有实时编辑器. 无法运行,翻墙后浏览正常. ![](https://img.shields.io/badge/时间-14:51-ff69b4.svg)
经过测试发现支持单日展示,内容根据内容自动调整. ![](https://img.shields.io/badge/时间-14:54-ff69b4.svg)
![](http://cdn.d7game.com/markdown/2017-07-12_RW5.png)

#### [x]部分任务没有按时间顺序显示
![need 15m](https://img.shields.io/badge/预计-15m-orange.svg) ![used 27m](https://img.shields.io/badge/耗时-27m-orange.svg) ![started 17-07-12 13:37](https://img.shields.io/badge/开始-17/07/12 13:37-orange.svg) ![done 17-07-12 14:04](https://img.shields.io/badge/完成-17/07/12 14:04-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
     1级任务;多个2级任务且分离.
1级任务排序. ![](https://img.shields.io/badge/时间-14:01-ff69b4.svg)
+生活类未完成任务不显示.  ![](https://img.shields.io/badge/时间-14:04-ff69b4.svg)
![](http://cdn.d7game.com/markdown/2017-07-12_(0WL.png)

#### [ ]如果以电话录音的方式问卷调查,让更多人参与完成
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]和他人沟通,20个人大概会有项目全貌
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     如何与他人交流,主要是询问的问题

#### [ ]每一次沟通都是一次进步,通话录音也记录到项目信息中
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]以上市公司产品为参考,罗列需要的领域和工作
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]z1flag标签整理
![done 17-07-12 11:47](https://img.shields.io/badge/完成-17/07/12 11:47-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     exercise 标签有遗漏,发现底部有5个未整理标签. 然后修改分类,添加注释等格式化数据.

#### [x]笔记5w2h思维导图查看
![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 27m](https://img.shields.io/badge/耗时-27m-orange.svg) ![started 17-07-12 12:50](https://img.shields.io/badge/开始-17/07/12 12:50-orange.svg) ![done 17-07-12 13:17](https://img.shields.io/badge/完成-17/07/12 13:17-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     你真的会做笔记吗 http://www.sohu.com/a/114504955_189657
5W2H分析法  http://baike.baidu.com/link?url=ZVLVRtNmQVFUPSBn9tTp4ztzDSGIksHf9U8hGbywWlmf5Z5VWey92Z-poS5DNoqoi5CgkBetuWl1xVqaItLNrhGidblWgmNaJfjCjC5Fn0iWz4WJZCWWuCSszKNlfQ_K
如何制作出漂亮的思维导图  https://www.zhihu.com/question/35829677

#### [ ]未完成的任务自动添加创建日期
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]任务列表缺少生活,生活类任务用斜线标识
![created 17-07-12 10:04](https://img.shields.io/badge/创建-17/07/12 10:04-orange.svg) ![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 15m](https://img.shields.io/badge/耗时-15m-orange.svg) ![started 17-07-12 13:21](https://img.shields.io/badge/开始-17/07/12 13:21-orange.svg) ![done 17-07-12 13:36](https://img.shields.io/badge/完成-17/07/12 13:36-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
     生活和任务详情冲突导致, 任务列表在过滤一次即可.   ![](https://img.shields.io/badge/时间-13:26-ff69b4.svg)
斜线标识, 斜线难以识别.   ![](https://img.shields.io/badge/时间-13:34-ff69b4.svg)
取消生活标签.  ![](https://img.shields.io/badge/时间-13:37-ff69b4.svg)

#### [x]"取消的任务"横线划掉的方式标识
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 06m](https://img.shields.io/badge/耗时-06m-orange.svg) ![started 17-07-12 18:47](https://img.shields.io/badge/开始-17/07/12 18:47-orange.svg) ![done 17-07-12 18:53](https://img.shields.io/badge/完成-17/07/12 18:53-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]tlog任务单独评论？又如何显示及更新.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]tlog文章可以评论回复.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]每日视频发给他人截取出特别部分
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]现有内容整理出思维导图
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     时间日志项目的所有功能.

#### [ ]文章关键信息显示
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     标题、作者、时间、标签、底部说明(产品\版权)

#### [x]思考接下来的事情
![need 45m](https://img.shields.io/badge/预计-45m-orange.svg) ![started 17-07-12 10:13](https://img.shields.io/badge/开始-17/07/12 10:13-orange.svg) ![done 17-07-12 11:27](https://img.shields.io/badge/完成-17/07/12 11:27-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![error unknow](https://img.shields.io/badge/error-unknow-red.svg) 
     ```mind
-思考
  -tlog下一步
    -tlog写书
    -任务众书
  -需要做什么
    -pc版功能
    -手机功能
  -为什么做 ![](https://img.shields.io/badge/时间-11:16-ff69b4.svg)
    -自用系统
      -节约时间
        -行为简化
        -自动软件
        -快捷模板
      -理清思路
        -时间去向
          -干扰原因
          -空白时间
          -高效成果
          -无理安排
        -思维盲区
          -已有能力
          -缺失方向
          -待思领域
        -努力进度
          -明确目标
          -有效肯定
          -待办事项
        -流程优化
          -关键任务
          -任务简化
          -避免错误
        -自我支持
          -跳出挫折
          -坚定初心
          -扩舒适区
        -碎片时间
          -当机立断
          -立刻行动
          -珍惜时间
          -合理安排
        -认清自己
          -评估核算
          -经验记录
          -明确缺点
      -成长历程
      -肯定成长
      -任务聚焦
        -专注一事
        -屏蔽干扰
        -提醒自我
      -有序分工
        -任务明确
        -快速分发
        -合理分工
        -减少依赖
      -经验分享
        -经验沉淀
        -快速查找
        -信息分类
      -透明自己
        -观点
        -习惯
        -性格
        -规则
        -为人
        -缺点
        -思维方式
        -过去未来
      -快速求助
        -需求清晰
        -情况明了
        -无需交接
        -便于传播
      -自我数据
        -健康
        -工作
        -生活
        -交际
      -便于合作
        -需求展示
        -自我价值
        -合作注意
    -工具赚钱
      -时间记录
      -任务管理
      -相互求助
      -专家咨询
      -社群互助
      -有效用时
```
对自己有帮助的作用,要完成这些任务需要怎样的功能？

##### [x]不会思考,思考的时候跑遍变成整理思绪而不自知.
![done 17-07-12 11:34](https://img.shields.io/badge/完成-17/07/12 11:34-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![summary ](https://img.shields.io/badge/-总结-1ba4ff.svg) 
#### [x]练习用思维导图方式思考任务
![need 40m](https://img.shields.io/badge/预计-40m-orange.svg) ![done 17-07-12 18:45](https://img.shields.io/badge/完成-17/07/12 18:45-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![exercise ](https://img.shields.io/badge/-exercise-72c7ff.svg) 
     editor整合到docute  + 思考接下来的事情

#### [x]editor整合到docute
![need 1h](https://img.shields.io/badge/预计-1h-orange.svg) ![started 17-07-12 15:52](https://img.shields.io/badge/开始-17/07/12 15:52-orange.svg) ![done 17-07-12 20:27](https://img.shields.io/badge/完成-17/07/12 20:27-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     让docute直接启动可以看到页面效果即可.
  ```mind
  -editor整合docute
    -查看editor关键代码 3m
    -查看docute关键代码 3m
    -代码整合 30m
      -+editor代码抽离&运用 1:26
      -xdocute可整合的区
      -editor单页面 20m
      -+手机viewport&mind滚动条 5m
    -测试 6m
   ```

##### [x]构思
![need 8m](https://img.shields.io/badge/预计-8m-orange.svg) ![started 17-07-12 15:52](https://img.shields.io/badge/开始-17/07/12 15:52-orange.svg) ![done 17-07-12 15:58](https://img.shields.io/badge/完成-17/07/12 15:58-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
##### [x]查看editor关键代码
![need 3m](https://img.shields.io/badge/预计-3m-orange.svg) ![started 17-07-12 15:58](https://img.shields.io/badge/开始-17/07/12 15:58-orange.svg) ![done 17-07-12 16:07](https://img.shields.io/badge/完成-17/07/12 16:07-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      mind.html 中主要代码 jquery editor.md

##### [x]查看docute关键代码
![need 3m](https://img.shields.io/badge/预计-3m-orange.svg) ![done 17-07-12 16:11](https://img.shields.io/badge/完成-17/07/12 16:11-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      本地无法运行, 查看官网, 加载程序主要是config.js 其他部分已经编译到一起. ![](https://img.shields.io/badge/时间-16:11-ff69b4.svg)

##### [x]代码整合
![need 45m](https://img.shields.io/badge/预计-45m-orange.svg) ![started 17-07-12 16:12](https://img.shields.io/badge/开始-17/07/12 16:12-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/12 18:34-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/12 17:38-orange.svg) ![cancelled 17-07-12 18:44](https://img.shields.io/badge/取消-17/07/12 18:44-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      需要docute的菜单和布局；editor.md的功能.
考虑editor.md单独运行无编辑输入框 ![](https://img.shields.io/badge/时间-16:13-ff69b4.svg)
查看html dom 猜想用editor替换content-wrap内容. ![](https://img.shields.io/badge/时间-16:19-ff69b4.svg)
全窗口预览HTML可进入预览模式.需要默认进入此模式. ![](https://img.shields.io/badge/时间-16:28-ff69b4.svg)
问题：docute菜单和滚动功能与editor.md整合会出问题. 
html中dom查看editormd-preview dom属性在改变却没搜索到相关代码. ![](https://img.shields.io/badge/时间-16:49-ff69b4.svg)
搜索preview-close-btn 有找到相关代码, 考虑外部直接调用预览模式. ![](https://img.shields.io/badge/时间-17:00-ff69b4.svg)
editormd.toolbarHandlers.preview() 报错.
直接找到按钮class, 触发事件$(".fa-desktop").click(); 控制台成功执行. ![](https://img.shields.io/badge/时间-17:06-ff69b4.svg)
Html js 直接写入触发代码失败,应为页面还为初始化成功.
发现 editormd 有实例, testEditor.previewing(); 控制台运行可以. js运行报错,可以判断部分实例还未初始化完成. ![](https://img.shields.io/badge/时间-17:08-ff69b4.svg)
定时执行知道运行成功即可. ![](https://img.shields.io/badge/时间-17:15-ff69b4.svg)
testEditor.getPreviewedHTML() 复制出内容添加到页面尝试失败,因为内容包含canvas. ![](https://img.shields.io/badge/时间-17:30-ff69b4.svg)
在尝试直接把dom 移动到根目录移动成功. ![](https://img.shields.io/badge/时间-17:34-ff69b4.svg)
$(".markdown-body").appendTo("body");
$("#layout").hide();    移动成功. ![](https://img.shields.io/badge/时间-17:36-ff69b4.svg)
查看docute 和editor dom结构, 有整合的可能性. 似乎没整合的必要. ![](https://img.shields.io/badge/时间-18:35-ff69b4.svg)
整合的好处是：有菜单和滚动定位. 因为这不是想要的版本,所以整合没意义. ![](https://img.shields.io/badge/时间-18:37-ff69b4.svg)

##### [x]editor单页面
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![started 17-07-12 20:09](https://img.shields.io/badge/开始-17/07/12 20:09-orange.svg) ![done 17-07-12 20:27](https://img.shields.io/badge/完成-17/07/12 20:27-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      包含所有功能的页面. 应该是配置参数的区别.

#### [x]解析7-11.tlog文件并查看
![done 17-07-12 09:18](https://img.shields.io/badge/完成-17/07/12 09:18-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     感觉很漂亮,够用3年. 还需要整个页面展示

#### [x]tlog文件一键插入模板
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 29m](https://img.shields.io/badge/耗时-29m-orange.svg) ![started 17-07-12 15:22](https://img.shields.io/badge/开始-17/07/12 15:22-orange.svg) ![done 17-07-12 15:51](https://img.shields.io/badge/完成-17/07/12 15:51-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     找到快捷键,
Sublime中的Snippet  http://www.jianshu.com/p/356bd7b2ea8e
Sublime Text 3\Packages\tlog\tmp_diy1.sublime-snippet tmp触发模板成功,但是出现多余缩进,<scope>source.tlog</scope> tlog中无法运行,注释所有文件可触发. ![](https://img.shields.io/badge/时间-15:39-ff69b4.svg)
sublime-snippet 文件中的 ${1:}前删除\tab 隐藏符号即可.

#### [x]任务详情不显示time
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 07m](https://img.shields.io/badge/耗时-07m-orange.svg) ![started 17-07-11 21:58](https://img.shields.io/badge/开始-17/07/11 21:58-orange.svg) ![done 17-07-11 22:05](https://img.shields.io/badge/完成-17/07/11 22:05-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
     filtesFlags 中添加 "life" 问题解决.
![](http://cdn.d7game.com/markdown/2017-07-11_WT{)2I.png)

#### [ ]标签配色自定义
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]自动获取任务开始时间
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     部分任务会忘记添加开始时间,则取值最近任务的完成时间.

#### [x]life summary view 等标签不显示done
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 00m](https://img.shields.io/badge/耗时-00m-orange.svg) ![started 17-07-11 16:25](https://img.shields.io/badge/开始-17/07/11 16:25-orange.svg) ![done 17-07-11 16:26](https://img.shields.io/badge/完成-17/07/11 16:26-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     上面任务中已经完成.

#### [x]时间日志不显示任务状态信息
![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 13m](https://img.shields.io/badge/耗时-13m-orange.svg) ![started 17-07-11 16:11](https://img.shields.io/badge/开始-17/07/11 16:11-orange.svg) ![done 17-07-11 16:24](https://img.shields.io/badge/完成-17/07/11 16:24-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     不显示 started created toggle done cancle

#### [x]标签按顺序显示
![need 45m](https://img.shields.io/badge/预计-45m-orange.svg) ![used 23m](https://img.shields.io/badge/耗时-23m-orange.svg) ![started 17-07-11 19:19](https://img.shields.io/badge/开始-17/07/11 19:19-orange.svg) ![done 17-07-11 19:42](https://img.shields.io/badge/完成-17/07/11 19:42-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     创建 评估 耗时 开始 结束 暂停 其他

#### [ ]任务时间计算&结果分析
![need 20m+30m](https://img.shields.io/badge/预计-20m+30m-orange.svg) ![started 17-07-11 16:30](https://img.shields.io/badge/开始-17/07/11 16:30-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/11 17:54-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/11 17:05-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/11 17:00-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![better ](https://img.shields.io/badge/-better-1ba4ff.svg) 
     耗时,跨天用更深的颜色表示.     
相关标签: need started done used evaluate
need == evaluate 评估, evaluate是在 (->)中获取的.
done-started, 包含toggle且跨天计算很麻烦. ![](https://img.shields.io/badge/时间-16:43-ff69b4.svg)
toggle可能有遗漏. 为单数时有遗漏. 
把start toggle done 放到一个数组计算. toggle为单就删除最后一个. ![](https://img.shields.io/badge/时间-16:50-ff69b4.svg)
难点：含子任务的计算. ![](https://img.shields.io/badge/时间-16:59-ff69b4.svg)
时间相减计算 moment('2017-07-11 ![](https://img.shields.io/badge/时间-17:00-ff69b4.svg)') - moment('2017-07-11 ![](https://img.shields.io/badge/时间-16:00-ff69b4.svg)')  //3600000

##### [ ]不含暂停的单任务计时.
![done 17-07-11 17:50](https://img.shields.io/badge/完成-17/07/11 17:50-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
##### [ ]暂停型任务计算
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
##### [ ]含子任务计算
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
##### [ ]跨天一级任务计算,部分子任务已完成.不在列表中
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
##### [ ]need(20m+30m) 时间计算.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      可以只做显示. 后期需要用作计算.

##### [ ]注意2次想点外卖后及时停止
![done 17-07-11 17:53](https://img.shields.io/badge/完成-17/07/11 17:53-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]标签图片标签显示中文
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 01:06](https://img.shields.io/badge/耗时-01:06-orange.svg) ![started 17-07-11 14:47](https://img.shields.io/badge/开始-17/07/11 14:47-orange.svg) ![done 17-07-11 15:53](https://img.shields.io/badge/完成-17/07/11 15:53-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     z1flag中的英文整理成json数据.先完成代码逻辑,在考虑标签文件导入.  
问题：z1flag和js如何两边管理更新.
思考的时候走神.  ![](https://img.shields.io/badge/时间-14:59-ff69b4.svg)
替换逻辑： ![](https://img.shields.io/badge/时间-15:04-ff69b4.svg)
思考vux项目如何处理的？多语言采用yml格式.
github 搜索yml文件解析. 找到js-yaml ![](https://img.shields.io/badge/时间-15:15-ff69b4.svg)
jsyaml.load('d7game: jiang\nname: thinkido'); 测试成功. ![](https://img.shields.io/badge/时间-15:21-ff69b4.svg)
yml格式  http://blog.csdn.net/u011250882/article/details/48770237
yml的注释和md的标题一样使用#符号.
重新编辑调整z1flag.md 内容. ![](https://img.shields.io/badge/时间-15:44-ff69b4.svg)
在内容里面添加#,依然被识别为内容. 期望被当做注释不识别的想法落空. ![](https://img.shields.io/badge/时间-15:50-ff69b4.svg)
删除内容中#开始的字符. ![](https://img.shields.io/badge/时间-15:52-ff69b4.svg)

#### [x]描述中的时间替换成标签图片
![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 15m](https://img.shields.io/badge/耗时-15m-orange.svg) ![started 17-07-11 14:19](https://img.shields.io/badge/开始-17/07/11 14:19-orange.svg) ![done 17-07-11 14:34](https://img.shields.io/badge/完成-17/07/11 14:34-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     '时间日志测试 ![](https://img.shields.io/badge/时间-13:33-ff69b4.svg) '.match(/\d\d:\d\d/g)  
'时间日志测试 ![](https://img.shields.io/badge/时间-13:33-ff69b4.svg) '.replace(/(\d\d:\d\d)/g,"新时间$1"); 测试成功 ![](https://img.shields.io/badge/时间-14:24-ff69b4.svg)
'时间日志测试 ![](https://img.shields.io/badge/时间-13:33-ff69b4.svg) ![](https://img.shields.io/badge/时间-22:33-ff69b4.svg) '.replace(/(\d\d:\d\d)/g,"d7game$1"); 替换多个字符成功.
"https://img.shields.io/badge/时间-$1-ff69b4.svg"  模板
'时间日志测试 ![](https://img.shields.io/badge/时间-13:33-ff69b4.svg) ![](https://img.shields.io/badge/时间-22:33-ff69b4.svg) '.replace(/(\d\d:\d\d)/g,"![](https://img.shields.io/badge/时间-$1-ff69b4.svg)");   测试成功
js中添加代码.

#### [x]如果是当天就不显示日期
![need 40m](https://img.shields.io/badge/预计-40m-orange.svg) ![started 17-07-11 20:38](https://img.shields.io/badge/开始-17/07/11 20:38-orange.svg) ![done 17-07-11 21:57](https://img.shields.io/badge/完成-17/07/11 21:57-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/11 21:30-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/11 20:50-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     不显示当天;不显示年月时间(跨月则显示);
1.在文件内容中无法判断当日日期. 现在是通过文件名判断. 
2.其次是通过当日任务大部分日期,肯能出错;
3.通过生活类时间判断当日日期. 问题：睡觉时间可能会到第二天.  ![](https://img.shields.io/badge/时间-14:17-ff69b4.svg)
计算当天日期成功. ![](https://img.shields.io/badge/时间-21:43-ff69b4.svg)
任务详情中的任务替换失败. ![](https://img.shields.io/badge/时间-21:53-ff69b4.svg)

#### [x]md标签图片分类颜色
![need 40m](https://img.shields.io/badge/预计-40m-orange.svg) ![used 36m](https://img.shields.io/badge/耗时-36m-orange.svg) ![started 17-07-11 19:52](https://img.shields.io/badge/开始-17/07/11 19:52-orange.svg) ![done 17-07-11 20:28](https://img.shields.io/badge/完成-17/07/11 20:28-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     任务 系统 自定义;[好的,坏的]
自定义浅蓝, 系统橙色，任务绿色、浅绿  好的天蓝，坏的红色.
浅蓝#72c7ff 天蓝#1ba4ff green yellowgreen orange red  ![](https://img.shields.io/badge/时间-19:59-ff69b4.svg)

#### [x]md中标签替换成图片
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 37m](https://img.shields.io/badge/耗时-37m-orange.svg) ![started 17-07-11 13:33](https://img.shields.io/badge/开始-17/07/11 13:33-orange.svg) ![done 17-07-11 14:10](https://img.shields.io/badge/完成-17/07/11 14:10-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     替换模板 https://img.shields.io/badge/<SUBJECT>-<STATUS>-<COLOR>.svg
示例 https://img.shields.io/badge/-done-green.svg
  https://img.shields.io/badge/tag-d7game-green.svg
已替换.出现2个问题. 1、标题和标签显示布局. 2、暂停多个值,需要拆分. ![](https://img.shields.io/badge/时间-13:47-ff69b4.svg)
=标题和标签布局, 错乱.标签图片需要换行即可. 任务后加标签正常. ![](https://img.shields.io/badge/时间-13:51-ff69b4.svg)
 时间日志中换行的子任务,取消标签图片. 没有被解析.  ![](https://img.shields.io/badge/时间-14:00-ff69b4.svg)
=暂停多个值, 匹配时间的正则表达式. '17-07-11 ![](https://img.shields.io/badge/时间-13:33-ff69b4.svg)'.match(/\d\d-\d\d-\d\d \d\d:\d\d/g)

#### [ ]editor.md的显示不一样
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     多行文本换行处理. list,

#### [x]parsetlog.js 解析运行报错unproject
![need 35m](https://img.shields.io/badge/预计-35m-orange.svg) ![used 05m](https://img.shields.io/badge/耗时-05m-orange.svg) ![started 17-07-11 11:42](https://img.shields.io/badge/开始-17/07/11 11:42-orange.svg) ![done 17-07-11 11:47](https://img.shields.io/badge/完成-17/07/11 11:47-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
     断点调试,添加一个null判断即可.![](http://cdn.d7game.com/markdown/2017-07-11_02IKG.png)

#### [x]解析描述中思维导图
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 51m](https://img.shields.io/badge/耗时-51m-orange.svg) ![started 17-07-11 10:48](https://img.shields.io/badge/开始-17/07/11 10:48-orange.svg) ![done 17-07-11 11:39](https://img.shields.io/badge/完成-17/07/11 11:39-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     难点,描述中思维导字包含缩进空格字符,在md中无法被识别.需要删除多余字符.
需要在tlog中解析时去除空格.
实际运行发现, ```mind当中的空格全部在解析的时候都丢失了. ![](https://img.shields.io/badge/时间-10:53-ff69b4.svg)
需要修改 lines[i].replace(/^\s+/, '') ![](https://img.shields.io/badge/时间-11:08-ff69b4.svg),
获取任务第一行缩进内容,然后删除之后描述的每一行换行缩进. 
避免意外缩进导致删除了所有空格, 空格字符必须大于4.  ![](https://img.shields.io/badge/时间-11:17-ff69b4.svg)
'    d7game删除前面缩进空格测试,   空格字符必须大于4.'.replace(/^(\s*)/,''); ![](https://img.shields.io/badge/时间-11:32-ff69b4.svg)
'    d7game删除前面缩进空格测试, 空格字符必须大于4.'.match(/^(\s{4,})/);

#### [x]每日日志的标题标签
![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 21m](https://img.shields.io/badge/耗时-21m-orange.svg) ![started 17-07-13 09:44](https://img.shields.io/badge/开始-17/07/13 09:44-orange.svg) ![done 17-07-13 10:05](https://img.shields.io/badge/完成-17/07/13 10:05-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     日志文件需要一个标题,任务的标签作为当日日志的关键词. @title

#### [x]markdown 标签图片制作方式.
![used 5m](https://img.shields.io/badge/耗时-5m-orange.svg) ![done 17-07-10 15:18](https://img.shields.io/badge/完成-17/07/10 15:18-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     http://shields.io/
https://img.shields.io/badge/%E5%AE%8C%E6%88%90-18-green.svg

#### [x]疑问：amd格式js如何运行使用？
![need 15m](https://img.shields.io/badge/预计-15m-orange.svg) ![used 20m](https://img.shields.io/badge/耗时-20m-orange.svg) ![started 17-07-10 13:52](https://img.shields.io/badge/开始-17/07/10 13:52-orange.svg) ![done 17-07-10 14:12](https://img.shields.io/badge/完成-17/07/10 14:12-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![new ](https://img.shields.io/badge/-new-red.svg) 
     http://www.css88.com/archives/4826
 JavaSript模块规范 http://blog.chinaunix.net/uid-26672038-id-4112229.html
和 sea.js的方法类似.

#### [x]markdown思维导图修改整合
![created 17-07-10 08:18](https://img.shields.io/badge/创建-17/07/10 08:18-orange.svg) ![need 1:55](https://img.shields.io/badge/预计-1:55-orange.svg) ![started 17-07-10 08:20](https://img.shields.io/badge/开始-17/07/10 08:20-orange.svg) ![done 17-07-11 10:28](https://img.shields.io/badge/完成-17/07/11 10:28-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![title ](https://img.shields.io/badge/-title-72c7ff.svg) 
     1、解析思维导字20m; 2、渲染导图40m; 3、导入思维文字25m; 4、markdown mind整合30m.
1:30-->1:55; 

##### [x]任务分解理清思路
![done 17-07-10 08:25](https://img.shields.io/badge/完成-17/07/10 08:25-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
##### [x]解析思维导字
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![started 17-07-10 08:25](https://img.shields.io/badge/开始-17/07/10 08:25-orange.svg) ![done 17-07-10 11:04](https://img.shields.io/badge/完成-17/07/10 11:04-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/10 10:32-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/10 10:10-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      现在markdown对 -列表有解析.在于解析后的数据是怎样的？1、找代码运行；
G:\nodejs\editor.md file:///G:/nodejs/editor.md/examples/simple.html
editormd.regexs = ![](https://img.shields.io/badge/时间-08:41-ff69b4.svg)
通过task 找到 markedRenderer.listitem
在marked.min.js 中解析的,查看或修改代码变得更麻烦.放弃系统原逻辑解析. 开始自己写. ![](https://img.shields.io/badge/时间-08:55-ff69b4.svg)
github搜索```mind无果. 17-07-10 ![](https://img.shields.io/badge/时间-09:00-ff69b4.svg)
大概在editormd.markdownToHTML = 需要配置自定义插件. 
taskList: settings.taskList,
代码中已经实现flowchart,搜索关键词找到 markedRenderer.code =  自定义插件
Renderer.code 添加成功,4个月前添加过tlog已经忘记,直到看见这里的代码. ![](https://img.shields.io/badge/时间-09:10-ff69b4.svg)
修改测试mind文字内容,便于营销. ![](https://img.shields.io/badge/时间-09:13-ff69b4.svg)
自己写逻辑也需要理清逻辑, 感觉需要花1小时,又开始找marked.js的解析逻辑. ![](https://img.shields.io/badge/时间-09:17-ff69b4.svg)
下载marked 搜索还没找到解析的地方. 解析涉及(缩进)容错;  ![](https://img.shields.io/badge/时间-09:34-ff69b4.svg)
markend.js Renderer.prototype.list 解析list; 程序内存中没有list数组,直接转换成了list. 
case "list_start" & case "list_item_start"   ![](https://img.shields.io/badge/时间-09:47-ff69b4.svg)
markdownParser.parse && jsMindScreenShot\lib\tmpl.js 的解析和模板3分钟没懂,之前没遇见过这种方式. ![](https://img.shields.io/badge/时间-09:55-ff69b4.svg)
我希望解析成怎样的数据? [{title:"d7game",childs:[]},{title:"next li"}] 
现有数据是dom结构的ul&li.  
我需要转换成怎样的数据？ 不知道,需要确定使用的mind插件而定. ![](https://img.shields.io/badge/时间-10:01-ff69b4.svg)
wc的时候想到 mindmapit 已经解析并渲染,主要是vue的. ![](https://img.shields.io/badge/时间-10:33-ff69b4.svg)
mindmapit\src\js\Parser.js 文件对list进行解析,js 是commonjs格式.
疑问：commonjs 文件如何直接调用?           
支持commonjs规范  https://segmentfault.com/q/1010000005881547 
查看 editormd.js中的模块代码并搜索关键代码.if(typeof module !== 'undefined' && module.exports) {
javascript的module 模块化  http://blog.csdn.net/gdp12315_gu/article/details/51547868 ![](https://img.shields.io/badge/时间-10:48-ff69b4.svg)
疑问 parser如何直接使用？parser js直调模块打包?
现在先考虑复制逻辑代码直接测试. ![](https://img.shields.io/badge/时间-10:56-ff69b4.svg)
控制台打印解析后的数据,和我预期的结构类似. ![](https://img.shields.io/badge/时间-11:05-ff69b4.svg)

##### [x]导入思维文字
![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![done 17-07-10 10:34](https://img.shields.io/badge/完成-17/07/10 10:34-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      直接在上一步中完成.

##### [x]自定义mind插件,自己没有动脑想过
![done 17-07-10 20:30](https://img.shields.io/badge/完成-17/07/10 20:30-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![summary ](https://img.shields.io/badge/-总结-1ba4ff.svg) ![error ](https://img.shields.io/badge/-error-red.svg) 
##### [x]渲染导图
![need 40m](https://img.shields.io/badge/预计-40m-orange.svg) ![started 17-07-10 11:11](https://img.shields.io/badge/开始-17/07/10 11:11-orange.svg) ![done 17-07-11 09:58](https://img.shields.io/badge/完成-17/07/11 09:58-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/11 08:26-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/10 20:50-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/10 18:43-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/10 17:45-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/10 17:25-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/10 17:11-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/10 14:34-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/10 13:52-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/10 13:31-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/10 12:05-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      同样采用mindmapit的canvas绘制逻辑.
查看源代码了解逻辑. ![](https://img.shields.io/badge/时间-11:24-ff69b4.svg)
功能需要先生成dom标签, 这样会先限制思维导图的数量. 后期肯定有多个导图需要的情况. 
疑问：多个导图如何处理？ ![](https://img.shields.io/badge/时间-11:40-ff69b4.svg)
问题：js如何调用commonjs文件？ 如果都是commonjs直接调用即可. ![](https://img.shields.io/badge/时间-11:49-ff69b4.svg)
模块化JavaScripts https://segmentfault.com/a/1190000006138597
vue会自动根据import加载编译amd规范文件,而js直接调用时,import如何处理？ ![](https://img.shields.io/badge/时间-12:00-ff69b4.svg)
vue.js 中typeof define === 'function' && define.amd 可以看出amd。 ![](https://img.shields.io/badge/时间-12:04-ff69b4.svg)
vue引入的js是amd,cmd还是commonjs格式?  vue是amd,猜测引入cmd不行. 查资料commonjs可以. ![](https://img.shields.io/badge/时间-13:17-ff69b4.svg)
![](http://cdn.d7game.com/markdown/2017-07-10_SRM.png)
![](http://cdn.d7game.com/markdown/2017-07-10_T7B4.png)
![](http://cdn.d7game.com/markdown/2017-07-10_2(E4.png)
![](http://cdn.d7game.com/markdown/2017-07-10_T7B4.png)
![](http://cdn.d7game.com/markdown/2017-07-10_LSM.png)
![](http://cdn.d7game.com/markdown/2017-07-10_MGY4]_3.png)
![](http://cdn.d7game.com/markdown/2017-07-10_0(SEG4S.png)
回想之前自己的js是commonjs打包方式. ![](https://img.shields.io/badge/时间-13:44-ff69b4.svg)
查看jsmind的数据格式,现在可以考虑用jsmind绘制界面.数据格式是单层结构-对象数组. ![](https://img.shields.io/badge/时间-13:51-ff69b4.svg)
jsmind可以展示,他还需要计算内容宽高. ![](https://img.shields.io/badge/时间-14:35-ff69b4.svg)
G:\nodejs\editor.md\plugins\plugin-template.js 有插件模板可以参考使用. 其中思维导图可以像其他插件一样使用. ![](https://img.shields.io/badge/时间-14:39-ff69b4.svg)
组件需要自动生成canvas_id,并作为参数传入绘制导图api中.  ![](https://img.shields.io/badge/时间-14:42-ff69b4.svg)
mind.html 作为开发测试文件. 
如果直接尝试插件开发,原来的commonjs格式能直接使用, 难点：同时包含几个知识点,不熟悉知识点出问题难以掌控.  ![](https://img.shields.io/badge/时间-15:06-ff69b4.svg)
index.html 中 editormd(??,options) options中传参flowChart : true 后,开启并加载了对应js ![](https://img.shields.io/badge/时间-15:29-ff69b4.svg) 
flowchart 已被混淆,无法查看和参考.
var loadFlowChartOrSequenceDiagram
flowchart是外部js文件直接在lib中引用即可. 不是自定义插件开发,参考emoji发现 this.executePlugin .![](https://img.shields.io/badge/时间-16:01-ff69b4.svg)
executePlugin: function(name, path) //  name 是加载后回调运行的function. ![](https://img.shields.io/badge/时间-16:21-ff69b4.svg)
markedRenderer.emoji = 直接渲染显示emoji表情,无需加载显示代码.   ![](https://img.shields.io/badge/时间-16:33-ff69b4.svg)
流程：1. markedRenderer.code 生成html代码. 2. katexRender 找到所有className, 调用渲染.
查看代码,有2中代码加载方式.其中之一 loadQueues: function() ![](https://img.shields.io/badge/时间-17:37-ff69b4.svg)
xx客户电话咨询打断.
mind 插件化构思：1、mind-plug代码. 2、drawMind  3、加载方式   ![](https://img.shields.io/badge/时间-18:50-ff69b4.svg)
 1.mind-plug代码. 查看plugins下面的代码,发现插件都是弹出框类型.思考思维导图不用plugin的方式做, 查看flowchart的代码. ![](https://img.shields.io/badge/时间-18:57-ff69b4.svg)
 flowchart代码太多,编译前后的花10分钟没能理解 ![](https://img.shields.io/badge/时间-19:07-ff69b4.svg)
 是多个commonjs 格式文件通过webpack方式打包. ![](https://img.shields.io/badge/时间-19:09-ff69b4.svg)
xx剪指甲
修改文件加载代码,lib/mindMap.js 
报错require is not defined , 查看emoji.dialog.js中的代码require是在其他地方.  ![](https://img.shields.io/badge/时间-19:41-ff69b4.svg)
疑问: 查看代码中只有一个参数,只引入了一个文件.
基本代码运行错误, 'fn' of undefined ![](https://img.shields.io/badge/时间-20:12-ff69b4.svg)
进入模块化的代码是 浏览器,不是amd和cmd. 脑图绘制受阻.   ![](https://img.shields.io/badge/时间-20:28-ff69b4.svg)         
 ```mind
 - d7game.com
     - d7game (Based on KaTeX);
         - thinkido;
         - Task lists;
     - tlog;
         -timelog
     - 时间管理;
         - Task l ists;
         - time manager;
         - time pay;
 ```
 问题：1、test-plugin.js 不是按代码预期的模块方式运行.  2、mindMap.js模块化开发感觉停滞不前.
把所有需要引用的类低吗直接复制到mindMap.js中,解决js import问题. ![](https://img.shields.io/badge/时间-08:41-ff69b4.svg)
export default class TreeNode  才用了类定义,纯js如何修改使用？
Javascript定义类class的三种方法 http://blog.csdn.net/feizhixuan46789/article/details/51471270
修改TreeNode类后运行 Uncaught SyntaxError: Unexpected token ;  报错 ![](https://img.shields.io/badge/时间-08:56-ff69b4.svg)
单独抽离代码到temp.js中测试,同样的报错信息. 上面技术文章中的方法有错. ![](https://img.shields.io/badge/时间-09:17-ff69b4.svg)
JS定义类的六种方式   http://www.jb51.net/article/84089.htm
改成构造函数方式,运行正常. ![](https://img.shields.io/badge/时间-09:30-ff69b4.svg)
修复运行时错误,代码正常运行.但效果不正确. canvas大小不一样. 
修改遗漏的 getter 和 utils后,效果正常了一半. 发现是自己的思维导字缺少根目录,添加后正常.
![](http://cdn.d7game.com/markdown/2017-07-11_CIS.png)

##### [x]多思维导图扩展
![need 35m](https://img.shields.io/badge/预计-35m-orange.svg) ![started 17-07-11 10:07](https://img.shields.io/badge/开始-17/07/11 10:07-orange.svg) ![done 17-07-11 10:21](https://img.shields.io/badge/完成-17/07/11 10:21-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      ![](http://cdn.d7game.com/markdown/2017-07-11_9O0_68.png)

#### [x]markdown 的思维导图构思
![done 17-07-09 21:14](https://img.shields.io/badge/完成-17/07/09 21:14-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     http://www.markdown.cn/
markdown静态博客框架 https://hexo.io/themes/
Markdown文本一键转化思维导图  https://www.waerfa.com/convert-markdown-to-mind-map-by-workflow-service-or-pop-clip-extension
markdown图表 http://knsv.github.io/mermaid/#mermaid
查看源代码 jsMindScreenShot,理解逻辑后判断把代码抽离出,整合到页面即可.
插件是基于node服务器并生成图片.这里只需要解析成 map需要的数据即可. ![](https://img.shields.io/badge/时间-20:15-ff69b4.svg)
百度脑图  https://github.com/fex-team/kityminder-core
markmap文章生成图脑  https://github.com/dundalek/markmap
vue实现的draw,需要抽离.  https://github.com/JoseTomasTocino/mindmapit
可修改项目 mindmapit  jsMindScreenShot

#### [ ]任务中的标签用md标签显示.
![need 15m](https://img.shields.io/badge/预计-15m-orange.svg) ![cancelled 17-07-09 18:57](https://img.shields.io/badge/取消-17/07/09 18:57-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     md没有标签, editor.md中是纯图片.

#### [ ]md显示pc版完善构思
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     首页，
相关素材：资料、整理、自写、观点、视频、评论、
当文章删除过后,百度引擎收录也会消失. 

#### [ ]md添加多功能,如editor.md
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]md文件如何显示和关联
![need 45m](https://img.shields.io/badge/预计-45m-orange.svg) ![started 17-07-08 19:24](https://img.shields.io/badge/开始-17/07/08 19:24-orange.svg) ![done 17-07-09 15:08](https://img.shields.io/badge/完成-17/07/09 15:08-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/08 21:12-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![better ](https://img.shields.io/badge/-better-1ba4ff.svg) 
     一般cms系统会做管理. cms系统是列出所有分类,分类中按时间为顺序分页显示的方式展示.
首页会根据热度，评论，经典,排名，人物等展示信息. 单个帖子会有人物，上一贴和下一贴的展示.
现在我的md是个人日记类. 先查看md在线浏览系统docute https://docute.js.org F:\Example\docute-master
docute 本地运行html并查看文件结构. ![](https://img.shields.io/badge/时间-19:51-ff69b4.svg) 
页面主要是config.js控制的. 1md/页.
可以生成文件和菜单js. html引用后config.js读取数据展示内容.这是完全自己实现的方式.![](https://img.shields.io/badge/时间-19:57-ff69b4.svg) 
有什么其他md系统可用作管理？需要百度查看.
有项目源代码vue src,可以自己修改编译后使用. ![](https://img.shields.io/badge/时间-20:02-ff69b4.svg)
搜索"支持md的cms 系统"只看到一条,http://dynalon.github.io/mdwiki 和docute是一样的.
查看 http://www.misitang.com 也是非常简单.
思考：1、docute静态文件 2、自开发系统; ![](https://img.shields.io/badge/时间-20:15-ff69b4.svg)
1、docute静态文件 ,接着上面的继续思考, 缺少分类. 问题：(1).30天文件用dropdown按钮切换很麻烦.
还会设计年,月. 现有docute框架不适合； (2).需要根据有哪些页面要下回来设计,在docute基础上思考,网站类似官网或者项目网站；其他页面现在都没有,有的主要是每天日志tlog.md .
2、自开发cms系统,和之前手稿图一样. 难点：如此多是日志如何展示,现在是日期为标题. 一个blog都是标题,主题和标签. 而日志完全不同,缺少主题和标签. 
那日志的优势是什么呢？个人的计划、为什么超时或按时完成,如何安排的一天. 那么文章就需要突出这些要点.  如何突出?查看没有带颜色字体. ![](https://img.shields.io/badge/时间-21:10-ff69b4.svg)
附加生成主题、5个标签.

##### [x]家里床上查看其他网页想到灵感,得出答案.
![done 17-07-09 15:09](https://img.shields.io/badge/完成-17/07/09 15:09-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![summary ](https://img.shields.io/badge/-总结-1ba4ff.svg) 
#### [x]序列图学习使用
![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 25m](https://img.shields.io/badge/耗时-25m-orange.svg) ![started 17-07-08 18:57](https://img.shields.io/badge/开始-17/07/08 18:57-orange.svg) ![done 17-07-08 19:22](https://img.shields.io/badge/完成-17/07/08 19:22-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![study ](https://img.shields.io/badge/-study-72c7ff.svg) 
     和流程图一样.
示例代码很简单,已理解. ![](https://img.shields.io/badge/时间-18:59-ff69b4.svg)
在查看官网说明  ![](https://img.shields.io/badge/时间-19:01-ff69b4.svg)
![](http://cdn.d7game.com/markdown/2017-07-08_{_U1PIR.png)
![](http://cdn.d7game.com/markdown/2017-07-08_Q02ZI.png)
```seq
   A->B:hello d7game\n实线闭括号
   B-->C:hello thinkido\n虚线闭括号
   C->>D:hello jiang\n实线开括号
   D-->>A:hello tlog\n虚线开括号
```
UML入门之交互图-时序图详解 http://blog.csdn.net/doniexun/article/details/38317537
实线：请求，虚线：返回.
箭头分同步和异步. 还不懂如何对应. ![](https://img.shields.io/badge/时间-19:22-ff69b4.svg)

#### [x]任务描述在解析后换行丢失
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 02m](https://img.shields.io/badge/耗时-02m-orange.svg) ![started 17-07-08 17:21](https://img.shields.io/badge/开始-17/07/08 17:21-orange.svg) ![done 17-07-08 17:23](https://img.shields.io/badge/完成-17/07/08 17:23-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
     搜索.desc找到添加desc的地方,在换行时添加\\r\\n换行字符.

#### [x]构思系统如何生成到服务器上的文件
![need 45m](https://img.shields.io/badge/预计-45m-orange.svg) ![used 33m](https://img.shields.io/badge/耗时-33m-orange.svg) ![started 17-07-08 16:07](https://img.shields.io/badge/开始-17/07/08 16:07-orange.svg) ![done 17-07-08 16:40](https://img.shields.io/badge/完成-17/07/08 16:40-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     最笨&不可取：本地手动操作,输出字符复制到新建文件并上传,然后添加这文件的新链接;
导入功能复制文本或拖动文件上传,最好是拖动文件.可以多文件一起导入. 拖动好处附带文件名,可以获取日期. 通过文本中done的日期判断可能出错.
拖动上传：前端解析内容,任务导入系统,md内容直接传递后台接口,后台创建文件.
难点：导入时文件已经存在如何处理？ 覆盖、修改。
 不存在：直接添加,完全没问题. 如昨日没有数据,今天导入昨天的.
 覆盖会导致文件丢失,基本不做考虑.
 修改：比对导入任务和系统中当日任务冲突情况, 比对标题名相似度>60覆盖,否则新建. 导入时前端结算后并传递给后端. 
近似字符串匹配问题 http://www.frontfans.com/archives/257
 console.log(strSimilarity2Percent(t1, t2));

#### [x]md文件查看及微调
![done 17-07-08 16:06](https://img.shields.io/badge/完成-17/07/08 16:06-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     本地md文件浏览查看效果,其中###标题前需要换行. 否则影响排版.

#### [x]flowchart学习使用
![created 17-07-08 15:30](https://img.shields.io/badge/创建-17/07/08 15:30-orange.svg) ![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 27m](https://img.shields.io/badge/耗时-27m-orange.svg) ![started 17-07-08 16:43](https://img.shields.io/badge/开始-17/07/08 16:43-orange.svg) ![done 17-07-08 17:10](https://img.shields.io/badge/完成-17/07/08 17:10-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![study ](https://img.shields.io/badge/-study-72c7ff.svg) 
     在线学习练手 http://pandao.github.io/editor.md/ 
需要一个参考练习题,仿造网站中的例子视图,制作一样的. ![](https://img.shields.io/badge/时间-16:44-ff69b4.svg)
官网 http://flowchart.js.org/
看明白了代码中的结构和意思. ![](https://img.shields.io/badge/时间-16:54-ff69b4.svg)
![](http://cdn.d7game.com/markdown/2017-07-08_Y$IJ.png)
```flow
 st=>start: 用户登陆
 op=>operation: 登陆操作
 cond=>condition: 登陆成功 Yes or No?
 e=>end: 进入后台
 st->op->cond
 cond(yes)->e
 cond(no)->op
```
测试成功. ![](https://img.shields.io/badge/时间-17:04-ff69b4.svg)
疑问：有api类似说明哪些元素？没有看到.
查看彩色图示例代码 ![](https://img.shields.io/badge/时间-17:10-ff69b4.svg)

#### [ ]项目报表每天更新直到完结.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     从一开始就生成显示,经常更新也便于百度收录.
每天凌晨4点定时更新项目文件. ![](https://img.shields.io/badge/时间-17:29-ff69b4.svg)
从数据库中找出所有任务, 按时间或分类显示.     

#### [x]统计报表思路
![created 17-07-08 15:12](https://img.shields.io/badge/创建-17/07/08 15:12-orange.svg) ![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 43m](https://img.shields.io/badge/耗时-43m-orange.svg) ![started 17-07-08 15:12](https://img.shields.io/badge/开始-17/07/08 15:12-orange.svg) ![done 17-07-08 15:55](https://img.shields.io/badge/完成-17/07/08 15:55-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     统计报表: 进步 退步 待提升 生活规律
  完成/总比 突然 新建 计划 健康 
  完成时间:按时完成比 时间误差比(预计时间,误差时间)如何计算 未计划数 
  工作 生活 
  习惯: 固定时间段,
  早起、起床、赖床的计算方式; ![](https://img.shields.io/badge/时间-15:12-ff69b4.svg)
  日、周、月 计划报表 , 累积数据,昨日比对;
  生活：吃饭 路上 睡眠 发呆 电话
  家庭：电话(父母 情感)
  交际：电话 聚会 讨论 合作
  任务：计划 [完成比] 总结 解决方案 准备 求助
  时间段: 早中晚
  专注：最长 打断 1小时内无中断数
  起床：最早晚 , 周平均 周(起床表)
  睡觉：早晚,周平均, 周(睡觉表)
  锻炼
  日表&周总表;
  碎片时间.
  近30天表, 这是所有数据可能展现的部分内容,还需要当前数据的展示内容. ![](https://img.shields.io/badge/时间-15:44-ff69b4.svg)
 1、现有标签中取出需要的部分.按多排平均显示;
 需要的有什么？完成/总比 突然 新建 计划 健康 吃饭 路上 睡眠, 工作(流水、毛)时间,(统筹)节约时间, 娱乐.

#### [ ]根据地点显示时间百分比,活动半径.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]服务器定时执行tlog2md转换与压力优化
![created 17-07-07 14:16](https://img.shields.io/badge/创建-17/07/07 14:16-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]任务管理进一步构思
![need 45m](https://img.shields.io/badge/预计-45m-orange.svg) ![started 17-07-07 14:05](https://img.shields.io/badge/开始-17/07/07 14:05-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]陈赫搞笑视频-说纠结的主题似乎和时间也有关系
![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 25m](https://img.shields.io/badge/耗时-25m-orange.svg) ![started 17-07-07 21:44](https://img.shields.io/badge/开始-17/07/07 21:44-orange.svg) ![done 17-07-07 22:09](https://img.shields.io/badge/完成-17/07/07 22:09-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     我为什么长胖了 https://v.qq.com/x/page/v03797dtwp4.html

#### [x]超时或明显不够时,重新预估时间.构思
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 04m](https://img.shields.io/badge/耗时-04m-orange.svg) ![started 17-07-07 22:11](https://img.shields.io/badge/开始-17/07/07 22:11-orange.svg) ![done 17-07-07 22:15](https://img.shields.io/badge/完成-17/07/07 22:15-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     修改时间的时刻和次数;
添加标签@add ,可以多个. 时刻和时长作为key&value 不行.
现在不急着做那么复杂, 只添加新增加的时间即可. 也可以是 need 和 add两个配合使用. 前期直接need(20m+10m)即可.

#### [ ]app中可以直接购买书籍
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]"1h+1:30"字符与时间的转换
![need 50m](https://img.shields.io/badge/预计-50m-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]学习类任务
![created 17-07-07 12:03](https://img.shields.io/badge/创建-17/07/07 12:03-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     可以用斜体表示. 分工作学习“粗体斜体”,平时学习“普通斜体”; ![](https://img.shields.io/badge/时间-12:07-ff69b4.svg)

#### [x]tlog2md 基本功能
![need 1h+1:30](https://img.shields.io/badge/预计-1h+1:30-orange.svg) ![started 17-07-07 10:45](https://img.shields.io/badge/开始-17/07/07 10:45-orange.svg) ![done 17-07-08 15:11](https://img.shields.io/badge/完成-17/07/08 15:11-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/08 13:47-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/08 12:17-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/08 10:12-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/08 10:02-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/08 08:45-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/07 21:44-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/07 19:29-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/07 19:09-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/07 15:37-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/07 15:06-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/07 12:50-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/07 12:08-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     表格、时间日志[完成]、未完成、Md内容;二级任务from~~空格~~; 生活和观点任务分别罗列;
基本代码. ![](https://img.shields.io/badge/时间-11:24-ff69b4.svg)
现在获取已完成任务已有,还需要未完成.
xx合租群,他人空调坏了消息回复查看. 8m
一二次的完成与未完成显示方式. ![](https://img.shields.io/badge/时间-11:33-ff69b4.svg)
^未完成的一级任务前添加[],便于识别区分;  ![](https://img.shields.io/badge/时间-11:43-ff69b4.svg)
修改现有逻辑,     
完成任务.生活类任务,不计入工作日报.观点总结类任务,消耗时间太少,不计算.未完成,不包含view think talk summary类任务 ; 
疑惑：时间日志包含所有完成任务. 生活类任务,不计入工作日报吗？是的不包含,时间日志已经是属于生活。 已完成列表就显示工作; ![](https://img.shields.io/badge/时间-11:52-ff69b4.svg)
^think 类未完成是疑惑问题,需要单独罗列并求助; 
难点：各种任务相互交际、包含. 怎样合理显示？ ![](https://img.shields.io/badge/时间-11:53-ff69b4.svg)
生活和工作可以用不同字体区分,同样显示在时间日志中. 工作任务显示粗体.      
^总结和解决方案统计 ![](https://img.shields.io/badge/时间-12:08-ff69b4.svg)
二级任务在不同的2个时间点完成.如何显示？分开显示. 一级任务当后缀. 显示二级任务来源. from
所有二级任务完成时,一级任务的显示方式？ 独立只显示一级任务. ![](https://img.shields.io/badge/时间-12:35-ff69b4.svg)
生活、观点、总结、解决方案、工作、学习、change、better、health 需要显示主要标签内容;
健康显示周次数、月、年时间；change、better、health 有则显示无则隐藏.
^其他暂不考虑, 不是这个任务考虑. 疑问？什么时候考虑,之前框架中无此考虑和安排. ![](https://img.shields.io/badge/时间-13:10-ff69b4.svg)
=表格：统计关键标签任务数量.  遍历所有任务,把任务中的所有标签技术统计.
=时间日志 已时间为key,存储任务数据. 时间字符串能直接比较大小？
 "17-07-07 ![](https://img.shields.io/badge/时间-10:21-ff69b4.svg)" > "17-07-07 ![](https://img.shields.io/badge/时间-10:20-ff69b4.svg)"  true
 "17-07-07 ![](https://img.shields.io/badge/时间-10:21-ff69b4.svg)" > "17-07-07 ![](https://img.shields.io/badge/时间-10:22-ff69b4.svg)" false
 经过测试,可以直接比较. ![](https://img.shields.io/badge/时间-13:23-ff69b4.svg)
 完成任务计算2级任务. ![](https://img.shields.io/badge/时间-14:04-ff69b4.svg)
=未完成, 不包含这些标签的任务. view think summary solution life talk ,
 只计算一级任务~~是不行的.summary属于二级任务~~.
=观点 md的list方式显示.
已完成任务的计数方式成问题. 分开显示.1级任务数和2级任务数. 如果任务跨天,显示的信息就不准确.
先简化工作1/2级任务一起计数. 当包含二级任务时,不计算1级任务;
思路理清 ![](https://img.shields.io/badge/时间-13:43-ff69b4.svg)
xxta44报时消息,引发的思绪. 10m ![](https://img.shields.io/badge/时间-13:58-ff69b4.svg)
=表格
 使用underscore来计算获取数据很方便. ![](https://img.shields.io/badge/时间-14:13-ff69b4.svg)
 一级标签计数, 如果快速完成可以在getdonetask中实现, 后面会多次遍历数组,还需要再次优化. 先考虑完成基本功能,
 还没有实际产出, ![](https://img.shields.io/badge/时间-14:59-ff69b4.svg)
tags应该是obejct对象,其中toggle 的值应该是数组或者字符串; 现在array中是的tag是属性名不相同的. ![](https://img.shields.io/badge/时间-15:39-ff69b4.svg)
_.object 转换数组成对象失败,参数类型细微不同. ![](https://img.shields.io/badge/时间-15:58-ff69b4.svg)
tags数组转换成object,空数组还是array, ![](https://img.shields.io/badge/时间-16:09-ff69b4.svg)
tags转换Object bug修复完毕. ![](https://img.shields.io/badge/时间-16:24-ff69b4.svg)
发现parseTlog 最上方只有一个项目时,报错unproject.tasks,找不到unproject. ![](https://img.shields.io/badge/时间-16:26-ff69b4.svg)
逻辑bug,可能对方没理解我需求,最终还是缺少测试. 现在问题：区域分隔符后的任务重新计算.
bug1:区域分隔符后的任务属于 unproject
bug2:最上方只有一个项目时,报错 unproject;  ![](https://img.shields.io/badge/时间-16:32-ff69b4.svg)
fixed bug1  areaBegin = true; 时,切换project为unproject ; 报新错误,可能修改到其他逻辑,如：最下面的引用链接归类. ![](https://img.shields.io/badge/时间-16:51-ff69b4.svg)
递归方法写的另类. ![](https://img.shields.io/badge/时间-17:44-ff69b4.svg)
修改代码失败,可以认为的通过修改文件内容顺序,规避Bug. ![](https://img.shields.io/badge/时间-18:21-ff69b4.svg)
xx沮丧,吃水果,订餐. ![](https://img.shields.io/badge/时间-18:54-ff69b4.svg)
也可以通过插入空白项目让现在的文件解析正常,以前的文件都需要修改;方式不可取. ![](https://img.shields.io/badge/时间-18:57-ff69b4.svg)
fixed bug 1&2 修复成功.
getDoneTasks 获取不到数据了,奇怪. 应该tags 的结构变了. ![](https://img.shields.io/badge/时间-20:24-ff69b4.svg)
一级已完成任务列表和计数. ![](https://img.shields.io/badge/时间-20:36-ff69b4.svg)
donelist 含完成子任务的1级任务不正确. ![](https://img.shields.io/badge/时间-20:44-ff69b4.svg)
因为 donelist 修改逻辑,删除了for,没有删除对应的break导致影响了外层for循环.修改后donelist正常. ![](https://img.shields.io/badge/时间-21:11-ff69b4.svg)
还需要二级任务done计数.   ![](https://img.shields.io/badge/时间-21:35-ff69b4.svg)

##### [x]任务需要划分为思考和执行2中阶段.
![done 17-07-07 12:36](https://img.shields.io/badge/完成-17/07/07 12:36-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![solution ](https://img.shields.io/badge/-solution-1ba4ff.svg) 
#### [x]时间管理视频博士查看3/2
![need 10m](https://img.shields.io/badge/预计-10m-orange.svg) ![used 21m](https://img.shields.io/badge/耗时-21m-orange.svg) ![started 17-07-07 10:22](https://img.shields.io/badge/开始-17/07/07 10:22-orange.svg) ![done 17-07-07 10:43](https://img.shields.io/badge/完成-17/07/07 10:43-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     记录时间的流向;确定目标;
常见问题：刚才几个小时不见了,感觉自己很忙,但想不起做了什么.
本杰明富兰克林-十三种德行 https://www.douban.com/note/527763637/
smart原则,

#### [x]时间管理视频博士查看2/2
![need 10m](https://img.shields.io/badge/预计-10m-orange.svg) ![used 16m](https://img.shields.io/badge/耗时-16m-orange.svg) ![started 17-07-07 10:05](https://img.shields.io/badge/开始-17/07/07 10:05-orange.svg) ![done 17-07-07 10:21](https://img.shields.io/badge/完成-17/07/07 10:21-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     最好的时间干最好的事;
缩短别人干扰的时间: 勿打扰,学会拒绝;站着谈话或整理问题件(坐着会谈很久);用电话解决问题,不是随意走动;门开着不要坐在别人看得到的地方被陌生人打扰;
利用碎片时间；适当的休息；定期总结；
时间清单；优先顺序；每天坚持；

#### [x]时间管理视频博士查看1/2
![need 5m](https://img.shields.io/badge/预计-5m-orange.svg) ![used 04m](https://img.shields.io/badge/耗时-04m-orange.svg) ![started 17-07-07 10:00](https://img.shields.io/badge/开始-17/07/07 10:00-orange.svg) ![done 17-07-07 10:04](https://img.shields.io/badge/完成-17/07/07 10:04-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     一生只有3天;彼得德鲁克;老张糟糕的星期天,

#### [x]到公司和上班是两种情况
![done 17-07-07 09:09](https://img.shields.io/badge/完成-17/07/07 09:09-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
     现在先处理为一种,到公司就工作;

#### [x]时间六大杀手
![done 17-07-07 09:25](https://img.shields.io/badge/完成-17/07/07 09:25-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     无计划;朋友拜托,不懂拒绝;帕金森定律(时间充裕拖延);不速之客(同事、上司、客户);无效会议;事必躬亲(不授权);

#### [x]拒绝的艺术
![done 17-07-07 09:16](https://img.shields.io/badge/完成-17/07/07 09:16-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     面对请求,回顾事务安排5分钟后回答. 拒绝事说明具体理由;

#### [x]干扰排除方法
![done 17-07-07 09:14](https://img.shields.io/badge/完成-17/07/07 09:14-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     下属
 集中汇报,不要遇见一个问题汇报一个; 
 举行例会,定期解决.
 设立窗口时间(每天固定处理时间); 
 便条、邮件;
同事
 预约时间，表明太对，双发达成共识。
 不要随意打扰对方
 不要做好好先生
上司召见
 让上司知道自己工作日程
 即时阶段性汇报
 一边等待一边处理事情

#### [x]生活类重复任务导入管理问题思考
![need 15m](https://img.shields.io/badge/预计-15m-orange.svg) ![used 09m](https://img.shields.io/badge/耗时-09m-orange.svg) ![started 17-07-07 09:26](https://img.shields.io/badge/开始-17/07/07 09:26-orange.svg) ![done 17-07-07 09:35](https://img.shields.io/badge/完成-17/07/07 09:35-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     生活是重复类事件,可能新增.也可能删除; 
导入的tlog,只处理已完成工作;
难点：任务改名会与旧数据不匹配, 这就需要修改数据库内所有字段. 我有技术和权限可以修改,普通用户无法修改. 解决：1、系统界面提供修改标签名 
为减少错误,需要模板导入导出. 
新增标签如何处理？ 1、弹窗确定后进入系统; 2、系统中新增然后导出新模板使用.

#### [x]高效时间管理——职场14/14
![need 8m](https://img.shields.io/badge/预计-8m-orange.svg) ![used 05m](https://img.shields.io/badge/耗时-05m-orange.svg) ![started 17-07-06 21:35](https://img.shields.io/badge/开始-17/07/06 21:35-orange.svg) ![done 17-07-06 21:40](https://img.shields.io/badge/完成-17/07/06 21:40-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     GTD的时间管理;
收集、整理、组织、回顾、行动、
收集：清空头脑的事情到软件系统；
整理：2分钟原则；
组织：任务分类、先后；
行动：集中精力执行，不要犹豫；
顺口溜，

#### [x]高效时间管理——职场13/14
![need 6m](https://img.shields.io/badge/预计-6m-orange.svg) ![used 09m](https://img.shields.io/badge/耗时-09m-orange.svg) ![started 17-07-06 21:26](https://img.shields.io/badge/开始-17/07/06 21:26-orange.svg) ![done 17-07-06 21:35](https://img.shields.io/badge/完成-17/07/06 21:35-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     甩掉偷时间的猴子;
甩猴法：时间管理理论; 
主管忙的时候发现下属有限的打高尔夫,意识到不会授权。
案例分析：
案例启示；必须交人如何捕鱼而不是一条鱼；
甩猴子联系：
  老板问：关于水利系统软件的那个项目，想和你谈一谈;
  部署问：我们解决项目预算超支的问题？
  同事间：我什么能拿到这软件的操作说明书？
  女朋友：什么时候可以陪我一起逛街？
  朋友问：这周六咋们一起去打羽毛球吧？
  父母问：家里空调坏了，换一个新的吧？
参考答案：红色看不见.

#### [x]高效时间管理——职场12/14
![need 5m](https://img.shields.io/badge/预计-5m-orange.svg) ![used 03m](https://img.shields.io/badge/耗时-03m-orange.svg) ![started 17-07-06 21:23](https://img.shields.io/badge/开始-17/07/06 21:23-orange.svg) ![done 17-07-06 21:26](https://img.shields.io/badge/完成-17/07/06 21:26-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     授权与协作；
授权的5W2H法则； how & how much;

#### [x]高效时间管理——职场11/14
![need 10m](https://img.shields.io/badge/预计-10m-orange.svg) ![used 05m](https://img.shields.io/badge/耗时-05m-orange.svg) ![started 17-07-06 21:17](https://img.shields.io/badge/开始-17/07/06 21:17-orange.svg) ![done 17-07-06 21:22](https://img.shields.io/badge/完成-17/07/06 21:22-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     充分利用时间;
学习和阅读；业余时间不会管理，工作时间也不会管理.
独处和思考. 思考未来可能发生的状况,自我暗示、身心放松。
补充精力，
适度社交与娱乐；注重家庭生活；发展第二职业；

#### [x]高效时间管理——职场10/14
![need 10m](https://img.shields.io/badge/预计-10m-orange.svg) ![used 05m](https://img.shields.io/badge/耗时-05m-orange.svg) ![started 17-07-06 21:12](https://img.shields.io/badge/开始-17/07/06 21:12-orange.svg) ![done 17-07-06 21:17](https://img.shields.io/badge/完成-17/07/06 21:17-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     善用碎片时间.
路上、家里、排队、电脑死机重启、回忆等待、餐馆等菜、等车、飞机、
学习、阅读、休息、

#### [x]高效时间管理——职场9/14
![need 10m](https://img.shields.io/badge/预计-10m-orange.svg) ![used 05m](https://img.shields.io/badge/耗时-05m-orange.svg) ![started 17-07-06 21:06](https://img.shields.io/badge/开始-17/07/06 21:06-orange.svg) ![done 17-07-06 21:11](https://img.shields.io/badge/完成-17/07/06 21:11-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     提高时间利用率：在一定时间内完成更多事情;
孙子兵法做典故，
统筹安排，平时作业；优化流程、简化操作；整理整顿，快速定位；效率更高的工具；
第一次就把事情做好.

#### [x]高效时间管理——职场8/14
![need 10m](https://img.shields.io/badge/预计-10m-orange.svg) ![used 07m](https://img.shields.io/badge/耗时-07m-orange.svg) ![started 17-07-06 20:58](https://img.shields.io/badge/开始-17/07/06 20:58-orange.svg) ![done 17-07-06 21:05](https://img.shields.io/badge/完成-17/07/06 21:05-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     克服懒惰，习惯勤奋；提高时间利用率；善用碎片时间；充分利用业余时间；授权协作,让别人做自己的事;
看1个人看他的8小时的业余时间在做什么？
甩掉头偷时间的猴子;GTD时间管理;
懒惰的解释与种类，表现。
什么是勤奋，使命感，危机感，
 古人名言，

#### [x]高效时间管理——职场7/14
![need 10m](https://img.shields.io/badge/预计-10m-orange.svg) ![used 16m](https://img.shields.io/badge/耗时-16m-orange.svg) ![started 17-07-06 20:41](https://img.shields.io/badge/开始-17/07/06 20:41-orange.svg) ![done 17-07-06 20:57](https://img.shields.io/badge/完成-17/07/06 20:57-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     养成习惯，行为形成习惯、习惯形成习惯、习惯养成性格、性格决定命运.
我们不被习惯成就，就是被习惯毁掉！
时间管理=好习惯=效率
如何养成良好的时间管理习惯
我们有这些好习惯吗？
 明确的学习、工作和生活目标；
 事先安排，每周将工作排除有限次数
 在搞笑事件完成重要工作
 授权
 有效对付干扰，不怕说“不”
 利用碎片时间
 保证不受干扰的时间，
 要是当机立断，不拖延
 不可求尽善尽美
 文件柜、办公桌整洁、条理清晰、避免杂乱无章
 有的李助理或文员、充分利用电脑等现代办公设备
 在固定时间处理往来邮件或信息
 丢掉无用文件
 每件事善始善终，避免多头处理
 万不得已才召开会议
 常年使用工作效率手册
 学会休息，娱乐与业余爱好

#### [x]高效时间管理——职场6/14
![need 18m](https://img.shields.io/badge/预计-18m-orange.svg) ![used 09m](https://img.shields.io/badge/耗时-09m-orange.svg) ![started 17-07-06 20:31](https://img.shields.io/badge/开始-17/07/06 20:31-orange.svg) ![done 17-07-06 20:40](https://img.shields.io/badge/完成-17/07/06 20:40-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     统筹安排
统筹介绍-华罗庚.
统筹具体操作和实施要求.
将工作分解成许多：连续作业、离散时间、管理事项
课本中烧开水具体讲解.
并行工作模式,什么情况能并行模式？
穿行工作模式，
实际测试题、练习题

#### [x]高效时间管理——职场5/14
![need 10m](https://img.shields.io/badge/预计-10m-orange.svg) ![used 13m](https://img.shields.io/badge/耗时-13m-orange.svg) ![started 17-07-06 20:18](https://img.shields.io/badge/开始-17/07/06 20:18-orange.svg) ![done 17-07-06 20:31](https://img.shields.io/badge/完成-17/07/06 20:31-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     制定计划：凡事预则立不预则废！人无远虑必有近忧！
什么是计划？为什么要做计划？
制定计划的要点：
 目标分析、约束条件分析、可行性分析、拟订具体方案、涉及行动步骤、整合并分配资源、过程监控和反馈、结果评估与计划修正。     
 目标分析：具体目标、目标分解、影响目标实现的决定性因素；
 约束条件分析：环境、资源、技术、人员、时间、金钱等约束性条件分析
 可行性分析：有事、劣势、机会、风险、可能性、可行性等
 拟订具体方案：可行性措施拟订具体实施步骤、方式方法、相应评估和考核方法；
 设计行动步骤：行动有效而合理的排序，那写可以同时进行和按顺序执行、
 整合并分配资源：
 过程监控和反馈：各环节需要的时间并推算出总体时间。
 结果评估与计划修正：结果追踪计划、评估、修正的方法
制定的常见误区
 目标笼统不细化
 约束条件股记不足，过于乐观、理想化；
 可能性与可行性估计不足，
 实施方案不具体，操作性不强:闭门造车，没有调查。
 计划没有弹性、整合不力或分配不当
 计划实施前缺乏必要的沟通确认
 实施中缺乏必要的监控与反馈：一个地方出现问题导致后面所有工作浪费.
 实施后缺乏评估与修正
自测练习

#### [x]高效时间管理——职场4/14
![need 10m](https://img.shields.io/badge/预计-10m-orange.svg) ![used 12m](https://img.shields.io/badge/耗时-12m-orange.svg) ![started 17-07-06 20:06](https://img.shields.io/badge/开始-17/07/06 20:06-orange.svg) ![done 17-07-06 20:18](https://img.shields.io/badge/完成-17/07/06 20:18-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     明确目标；
时间管理常用策略：明确目标、定制计划、头筹安排、养成习惯；
视频主要是对概念的解释说明；
缺乏目标常见时间管理误区：
 事前缺乏目标，忙碌而盲目；
 事中缺乏计划，计划赶不上变化。
 事后缺乏检查，以大概差不多安慰自己.
如何坚持时间管理的目标导向：
 有目的就要有目标
 无目标就没计划
 无计划就不花时间
自测练习

#### [x]高效时间管理——职场3/14
![need 10m](https://img.shields.io/badge/预计-10m-orange.svg) ![used 09m](https://img.shields.io/badge/耗时-09m-orange.svg) ![started 17-07-06 19:56](https://img.shields.io/badge/开始-17/07/06 19:56-orange.svg) ![done 17-07-06 20:05](https://img.shields.io/badge/完成-17/07/06 20:05-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     轻重缓急；四象限原则
重要紧急的立刻做,重要不紧急有计划的做,不重要但紧急的选择去做,不重要不紧急的尽量少做.
违背轻重缓急原则的常见误区：
 疲于奔命：到处救火、身心交瘁
 琐事缠身：繁文缛节、细枝末叶、盲目且盲目；
 计划赶不上变化：被电话、会议、到访、借贷、报告、请示、指示、求帮忙事情干扰.
如何坚持轻重缓急原则.
自测练习：

#### [x]高效时间管理——职场2/14
![need 10m](https://img.shields.io/badge/预计-10m-orange.svg) ![used 10m](https://img.shields.io/badge/耗时-10m-orange.svg) ![started 17-07-06 19:46](https://img.shields.io/badge/开始-17/07/06 19:46-orange.svg) ![done 17-07-06 19:56](https://img.shields.io/badge/完成-17/07/06 19:56-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     重要优先原则(ABC法则)
常见误区：做事没重点、没主次、拖延；

#### [x]高效时间管理——职场1/14
![created 17-07-06 19:39](https://img.shields.io/badge/创建-17/07/06 19:39-orange.svg) ![used 07m](https://img.shields.io/badge/耗时-07m-orange.svg) ![started 17-07-06 19:39](https://img.shields.io/badge/开始-17/07/06 19:39-orange.svg) ![done 17-07-06 19:46](https://img.shields.io/badge/完成-17/07/06 19:46-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     2/8原则,价值优先、重要优先、轻重缓急.
时间浪费：80岁寿命，20岁工作,65岁退休,45年工作,可控时间1/3不到,睡觉衣食住行, 新闻、
放到有价值的事情上4年.

#### [x]高效能工作密码10/10
![created 17-07-06 17:47](https://img.shields.io/badge/创建-17/07/06 17:47-orange.svg) ![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 06m](https://img.shields.io/badge/耗时-06m-orange.svg) ![started 17-07-06 17:47](https://img.shields.io/badge/开始-17/07/06 17:47-orange.svg) ![done 17-07-06 17:53](https://img.shields.io/badge/完成-17/07/06 17:53-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     高效工作的艺术；
原则：收集 明确 计划 检视 行动
打造自己的高效办公区：收集区，番茄区，项目资料区，工具区，
打造自己的资料库：信息收集、资料加工、数据同步；
个人成长与群体学习：人脉管理 群体学习 实践社群
人脉管理&工具；
工作于生活的习惯：早起、清单、工具
练习：整理办工作；选择合适的效率工具；加入实践社区;

#### [x]高效能工作密码9/10
![created 17-07-06 17:27](https://img.shields.io/badge/创建-17/07/06 17:27-orange.svg) ![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 19m](https://img.shields.io/badge/耗时-19m-orange.svg) ![started 17-07-06 17:28](https://img.shields.io/badge/开始-17/07/06 17:28-orange.svg) ![done 17-07-06 17:47](https://img.shields.io/badge/完成-17/07/06 17:47-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     总结和计划;
时间日志：以天为单位、精确到小时。好处在于更靠谱一些;
美好是因为无视美好的逝去。 晨间日志，九宫格、每天5-10分钟;  <<晨间日记的奇迹>>
曼陀罗思考法
九宫格晨间日记：有理想才不会变咸鱼；坚持比昨天更重要；成功日记；人脉链接；财务状况；先吃掉青蛙；健康与饮食；工作学习创意；
使用软件：印象笔记；格式丰富：图片和链接；日记最下方写下年度目标，加深印象;
图文并茂的晨间日记，作者自己的现身说法。
周回顾：零态清爽、建始禁毒、回顾清单、提高层面[周月年计划]；
xx手机无法播放,换电脑查看视频, 电脑依然不能播放;
九宫格周回顾;
项目清单的回顾;
练习：评价时间日志；定制日记模板；周回顾模板；定目标；每周为长期目标做出一个行动;

#### [x]高效能工作密码8/10
![created 17-07-06 17:06](https://img.shields.io/badge/创建-17/07/06 17:06-orange.svg) ![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 21m](https://img.shields.io/badge/耗时-21m-orange.svg) ![started 17-07-06 17:06](https://img.shields.io/badge/开始-17/07/06 17:06-orange.svg) ![done 17-07-06 17:27](https://img.shields.io/badge/完成-17/07/06 17:27-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     提升专注度,
一次做一件事：有其他突发事情就收集起来.
不能专注10分钟-->番茄工作法:面对复杂任务，无从开始; 各种琐事，一地鸡毛； 拖延症，越拖越久;
各种时间段的番茄工作法；案例：阅读、锻炼、上网、午休；
每天先吃掉那只青蛙,
番茄事件+时间日志,
间歇时间可以做什么？微运动(俯卧撑)、补充水分、其他
练习：今日待办清单；重要事件分配高效时段；番茄工作法节奏工作；记入时间日志；评估效果；
不可控的时间越来越少.

#### [ ]尝试每天提前一点起床时间
![created 17-07-06 16:46](https://img.shields.io/badge/创建-17/07/06 16:46-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![try ](https://img.shields.io/badge/-try-72c7ff.svg) ![change ](https://img.shields.io/badge/-change-72c7ff.svg) 
     还未起床前,已经是一次努力; 最早提前到4点,每次提前10分钟. 每持续做到一阶段感觉良好,进入下一次尝试;

#### [x]高效能工作密码7/10
![created 17-07-06 16:42](https://img.shields.io/badge/创建-17/07/06 16:42-orange.svg) ![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 24m](https://img.shields.io/badge/耗时-24m-orange.svg) ![started 17-07-06 16:42](https://img.shields.io/badge/开始-17/07/06 16:42-orange.svg) ![done 17-07-06 17:06](https://img.shields.io/badge/完成-17/07/06 17:06-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     组织整理,
分析自己的时间日志？1、时间浪费在哪儿？2、高效时段在哪儿？
一天24小时; 
两类事情：思考类、事务类；
高效时段：任务相对复杂,相对整块时间，不被打扰;
其他时段：流程相对熟悉、需要和他人沟通、对整块时间要求不高;
要事第一, 高效的时间做重要的事情和创造的事情. 什么是“重要的事情” ；
案例：烦躁的上午； 某日：早晨8:30分，正准备备课突然听到京光高速运营图已经公布，心中顿时不淡定了，又想起来近3天来好像有很多事情等待完成，烦躁油然而生.
碎片时间利用：等待 途中 排队 间歇 
为碎片时间准备了什么： 清单、资料。 做最简单的事情.
每日计划:  日程表 今日待办 等待清单 项目清单 
计划安排原则： 寻找高效时间[了解大家日程表,尽可能不被打扰的时间]，专注工作[关闭QQ,不查收email,一次一事]、其他时间[按情景、按批次处理事情(电话、邮件)]   
另外8小时：工作、睡觉、另外8小时.
案例：我的另外8小时;
找到志同道合的伙伴;
练习：分析自己的24小时, 自己的时段在什么时候？哪些碎片时间可以利用？为碎片时间可以做哪些准备？实践一周及准备？
练习2：规划自己另外8小时？  思考另外8小时的安排与使用？ 开始行动.

#### [x]高效能工作密码6/10
![created 17-07-06 16:19](https://img.shields.io/badge/创建-17/07/06 16:19-orange.svg) ![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 23m](https://img.shields.io/badge/耗时-23m-orange.svg) ![started 17-07-06 16:19](https://img.shields.io/badge/开始-17/07/06 16:19-orange.svg) ![done 17-07-06 16:42](https://img.shields.io/badge/完成-17/07/06 16:42-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     个人知识管理,
认识误区：信息 收藏 阅读 存储 ，知识 学习 思考 掌握
知识工作者的工作效率：快速找到信息,找到专家团体，应用与传承，持续学习.
信息收集与筛选, 
信息分类:以项目为核心；以知识体系为核心.
项目用风琴夹格子为单位管理项目.
项目总结,
从量变到质变：知识点应用沉淀.
以知识体系为核心:关注点[明确关注点 兴趣爱好 1w小时]；知识库[建立分类 利用标签 方便查询 资料电子化]；工具[印象笔记 风琴夹]
实践：知识体系信息管理 
练习：手机项目资料；建立知识体系;选择适合的知识管理工具.

#### [x]高效能工作密码5/10
![created 17-07-06 15:56](https://img.shields.io/badge/创建-17/07/06 15:56-orange.svg) ![need 51m](https://img.shields.io/badge/预计-51m-orange.svg) ![used 22m](https://img.shields.io/badge/耗时-22m-orange.svg) ![started 17-07-06 15:56](https://img.shields.io/badge/开始-17/07/06 15:56-orange.svg) ![done 17-07-06 16:18](https://img.shields.io/badge/完成-17/07/06 16:18-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     声音不清晰; 清单回顾,
项目定义与处理方法,分解,smart原则,
目标设定举例,
马拉松世界冠军三田本一用智慧战胜对手, 赛前先到赛场观察一篇,分解为多个小目标.  好处：尽在掌控,减少挫败.
李恕全 音乐梦想的案例. 
关键人物
项目与清单的关系；第一步行动；项目常规化:总结规律，手机资料. 制作清单.回顾分享.
练习：近期目标分解出第一个行动； 毫不犹豫；已完成项目继续标准化,相关资料收集完成.
拖动视频查看关键点减少时间.

#### [x]高效能工作密码4/10
![created 17-07-06 15:17](https://img.shields.io/badge/创建-17/07/06 15:17-orange.svg) ![need 46m](https://img.shields.io/badge/预计-46m-orange.svg) ![used 25m](https://img.shields.io/badge/耗时-25m-orange.svg) ![started 17-07-06 15:17](https://img.shields.io/badge/开始-17/07/06 15:17-orange.svg) ![done 17-07-06 15:42](https://img.shields.io/badge/完成-17/07/06 15:42-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     清空原则回顾, 不行动无意义, 猴年马月; 
不能启动的原因？定义太模糊，找不到第一步行动;  可立刻执行的简单第一步行动;
行动加工公式=动词+场景+任务=行动
切换任务带来的额外开销;
情景清单：电话清单、邮件清单、购物清单、外出清单、上网清单;
购物时：想卖的没买,不想买的(促销便宜)就买了.
被网络吞噬的时间？不知道上网干什么,就网上闲逛. 新闻、购物、微信... ![](https://img.shields.io/badge/时间-15:31-ff69b4.svg)
拔网线, 上网清单、时间计划：今日、明天、本周、本月、季度、今年、someday、maybe
外国人与“马”有关,马上就到,有人快有人慢每人不同.
风琴夹工作法：赶出大脑后记录下来.  标签做区分可以梳理事物.
案例分析; 练习：事件转换为可执行行动；制定适合自己的情景；批次处理行动；按时间规划自己的行动;明确“今日待办”

#### [ ]在不同环境运用时间管理
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     如办公室,老板、同事、项目

#### [x]高效能工作密码3/10
![created 17-07-06 14:36](https://img.shields.io/badge/创建-17/07/06 14:36-orange.svg) ![used 47m](https://img.shields.io/badge/耗时-47m-orange.svg) ![started 17-07-06 14:30](https://img.shields.io/badge/开始-17/07/06 14:30-orange.svg) ![done 17-07-06 15:17](https://img.shields.io/badge/完成-17/07/06 15:17-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     怦然心动的整理方法,断舍离,各归各位; 丢弃;2分钟法则;拖延症.
声音很不清晰, 他的视频大纲不知道讲了什么,哪里该跳过.
新建文件夹积累,等待清单,
两分钟法则，2分钟之内完成，就中断计划去完成它，或把它推迟到计划执行完毕之后。
我个人会放到当前任务后的碎片休息时间. ![](https://img.shields.io/badge/时间-14:48-ff69b4.svg)
合理规划,团队分享(让别人知道自己的档期);
xx吃水果,
工具介绍：google日历 365日历同步 email  + doit.im
sm=someday maybe ,项目project, 行动清单，清空收集箱，清空原则图. 练习：逐个清空收件箱；事件加工成清单;
清空桌面,邮件; 

#### [x]高效能工作密码2/10
![created 17-07-06 13:41](https://img.shields.io/badge/创建-17/07/06 13:41-orange.svg) ![need 51m](https://img.shields.io/badge/预计-51m-orange.svg) ![started 17-07-06 13:41](https://img.shields.io/badge/开始-17/07/06 13:41-orange.svg) ![done 17-07-06 15:52](https://img.shields.io/badge/完成-17/07/06 15:52-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/06 15:44-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/06 14:25-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     电话、会议、微信、微博、豆瓣、聊天
杂事占据时间与大脑,
不靠谱标签,
据统计人生10%的时间用于找东西，
有很多突发消息影响工作：公交卡充值，话费欠费，明日开会.
写下来才可能行动,否则都忘记了. 有句话三月不减肥徒伤悲悲悲...
灵感记录, 高效和专注. 清空大脑,克服干扰,
xx播放中断，网络或音频问题无法播放,导致看2个新闻,乐视总被欠款被16家公司堵门. ![](https://img.shields.io/badge/时间-14:23-ff69b4.svg)
收集箱,
xx手机电脑无法播放。 
遇见下面情况,你会怎么办？ 晚上就请，出差忘记带名片。 公交车上发现卡里剩5元； 写报告中想起花费没了; 写报告中qq消息提示明天15点会议;
练习：头脑大扫除, 记录到收纳箱；
寻找自己的收集箱；
工具推荐：变迁、印象笔记、onenote、收纳箱；双层文件夹；
收集箱范例、
无法播放视频就直接拖动视频,查看视频用ppt的内容。 ![](https://img.shields.io/badge/时间-15:52-ff69b4.svg)

#### [ ]每个任务的解决方法
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     和总结相同的解决方法; 也可以分享,他人借鉴学习解决方法;
借鉴keep 主动推送我要上经典, 管理员审核.

#### [x]误区：起床一定要设置闹钟,不要靠梦想和意志力
![done 17-07-06 13:25](https://img.shields.io/badge/完成-17/07/06 13:25-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
     设置闹钟的好处在于,可以安心的睡觉达到高质量睡眠。减少想要靠意志力起床失败导致的挫败感和休息不好.

#### [x]康熙起居注
![created 17-07-06 13:05](https://img.shields.io/badge/创建-17/07/06 13:05-orange.svg) ![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 15m](https://img.shields.io/badge/耗时-15m-orange.svg) ![started 17-07-06 13:00](https://img.shields.io/badge/开始-17/07/06 13:00-orange.svg) ![done 17-07-06 13:15](https://img.shields.io/badge/完成-17/07/06 13:15-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     百度,pdf下载, 相关搜索：古代皇帝都有起居注吗？
记录康熙皇帝日常起居的书有《清圣祖实录》《康熙起居注》
帝王起居注 https://www.zhihu.com/question/20451678
大部分皇帝都有起居注,从一开始的起居逐渐演变到一言一行.
制约皇帝言行的《起居注》 http://blog.sina.com.cn/s/blog_487551be0100uzle.html

#### [x]高效能工作密码1/10
![used 01:08](https://img.shields.io/badge/耗时-01:08-orange.svg) ![started 17-07-06 12:24](https://img.shields.io/badge/开始-17/07/06 12:24-orange.svg) ![done 17-07-06 13:32](https://img.shields.io/badge/完成-17/07/06 13:32-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     康熙起居注 奇特的一生 怎么过一天就是怎么过一生 
记录自己的时间消耗在哪里,像医生问诊一样需要知道我们的时间状况.通过如何过一天来诊断.
如果不想过这样的一生就需要做改变.
发呆时是想冥想还是高效工作? 知己知彼,

#### [ ]每天或每个任务的打分
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     哪里感到遗憾,还有可以提升努力的空间; 如腹肌撕裂者锻炼的时候,很明显自己在做登山跑没有用全力.
只是感到疲累的时候,努力的带动大腿.

#### [x]系统功能版本划分方式理清
![created 17-07-06 11:37](https://img.shields.io/badge/创建-17/07/06 11:37-orange.svg) ![need 15m](https://img.shields.io/badge/预计-15m-orange.svg) ![used 20m](https://img.shields.io/badge/耗时-20m-orange.svg) ![started 17-07-06 11:37](https://img.shields.io/badge/开始-17/07/06 11:37-orange.svg) ![done 17-07-06 11:57](https://img.shields.io/badge/完成-17/07/06 11:57-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     用标签划分,如何显示和编辑.
编辑可以和日程任务管理方式一样,其中.
难点：从项目列表、划分到不同版本. 版本间的切换. ![](https://img.shields.io/badge/时间-11:47-ff69b4.svg)
简单的方式可以在项目标签中添加子标签, 可以多选. 人为的取消多版本标签.
选择下面标签后就显示当前项目中的所有任务. ![](https://img.shields.io/badge/时间-11:53-ff69b4.svg)
v2 项目专栏,左边显示项目所有任务,右边就像周计划一样的列表显示各版本任务.
项目可以按部门、人员、版本显示.
疑问：各显示方式的任务如何操作. 任务做简单操作,不用做编辑,直接在各列表中拖动即可. 需要额外的项目任务页面. ![](https://img.shields.io/badge/时间-11:56-ff69b4.svg)

#### [ ]任务评价开放等级
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     自己、项目成员、朋友、陌生人.

#### [ ]问题与解决方法列出
![created 17-07-06 11:20](https://img.shields.io/badge/创建-17/07/06 11:20-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     他人可以查看了解,并提出自己的解决方法.
现在的任务添加评论功能即可.
相关问题存在,同行竞争会了解我的所有情况.

#### [x]任务时间计算方式理清
![created 17-07-06 11:11](https://img.shields.io/badge/创建-17/07/06 11:11-orange.svg) ![need 15m](https://img.shields.io/badge/预计-15m-orange.svg) ![used 07m](https://img.shields.io/badge/耗时-07m-orange.svg) ![started 17-07-06 11:11](https://img.shields.io/badge/开始-17/07/06 11:11-orange.svg) ![done 17-07-06 11:18](https://img.shields.io/badge/完成-17/07/06 11:18-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
     任务中间加入了其他任务, 通常计算结束-开始的时间差.
但是这样一天的时间总和超过24小时.  可以自动把任务归纳为暂停的方式处理,这样又很难看出此任务被打断情况.
那就显示3段时间. 预期、总耗时、毛时间. 暂停次数.
相关情况还有：多任务重叠,特别是生活类. 如 吃饭+音视频 如何处理? 显示主任务,另外的任务列入附加完成.
计算为节约时间.

#### [ ]到时间但暂未完成的任务
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
     学习是无止境,当下阶段完成,担感觉又未学习完到安心的程度.

#### [ ]查看他人刻意练习的过程
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]刻意练习类任务
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     划分为每一天练习打卡,直到完全做到的那一天. ![](https://img.shields.io/badge/时间-09:55-ff69b4.svg)
记录每天练习过程描述, 难点是其他相关任务? 任务后面添加一个标签即可.
完成：当日完成,最终完成. 点击完成后,安装状态改变为最终完成. 最终完成会结束重复出现.

#### [ ]评估总结行为,在评估回顾一天
![created 17-07-06 09:46](https://img.shields.io/badge/创建-17/07/06 09:46-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     昨天是无计划的,明天如何做好？
发现1个问题,然后标记 @question=100 ,每次记录改善该行为尝试的方式.

#### [ ]显示特别标签数或内容
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     如better,今天尝试改变的行为结果.

#### [ ]可选择日期和主题生成html
![created 17-07-06 09:23](https://img.shields.io/badge/创建-17/07/06 09:23-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     通过日tlog文件,通过任务标签生成主题html文章. 可以是感受,技术,成绩.

#### [x]系统日报格式改良
![created 17-07-06 09:11](https://img.shields.io/badge/创建-17/07/06 09:11-orange.svg) ![done 17-07-06 09:20](https://img.shields.io/badge/完成-17/07/06 09:20-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     分几部分：统计报表、时间日志、完成工作、未完工作、总结、遇见什么问题、尝试哪些
查看日事清并参考.
结果报告 分析 甘特图 时间日志 源文件
结果：进步 退步 待提升 生活规律
md格式展现, 标题替代[]任务.
时间日志：只显示标题和标签. 难点: 一二级任务的显示. 二级任务前面添加一个符号做区分. 如空格.
统计 时间日志 未完成. 总结 问题 尝试 详情md. 
[浏览 打分 赞 踩 收藏 评论 关注]
网页图片 + 二维码 
添加观点与总结.不属于执行任务,但占据少量时间.  ![](https://img.shields.io/badge/时间-13:36-ff69b4.svg)

#### [ ]积累性工作如何处理
![created 17-07-06 09:09](https://img.shields.io/badge/创建-17/07/06 09:09-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     对已完成工作的改进优化,问题：很难定位找到曾经的工作记录.

#### [x]时间任务生态圈
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![started 17-07-05 16:36](https://img.shields.io/badge/开始-17/07/05 16:36-orange.svg) ![done 17-07-05 18:25](https://img.shields.io/badge/完成-17/07/05 18:25-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/05 18:25-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     名言 历史 学习
相关所有行为：名言 故事 学习 记录 管理 时间 公告 兼职 时薪 培训 任务管理 作者 规划 
 观点
相关：电影 电视剧 小说 科幻 未来 音乐 游戏 软件 
用户：老板 急人 懒人 老师 
相关行业：房租 单车 滴滴 外卖 跑腿 
时间提供的服务：期货 新酒老酒 教练 按摩
如何思考考虑一个事物的生态圈, 行业中的所有环节和人员.
xx 犯困一会儿.
时间经济：
生态圈 BAT    http://datanews.caixin.com/2015-12-16/100888651.html
查看饿了么无内容可以参考, 意外关闭音频, 查看微信ta44上半年总结.
时间经济学
时间规划局电影下载重新查看寻找灵感,
经济学中的时间 https://www.zybang.com/question/50560b9b461dd973cfebf2a27cbb7a1f.html

#### [x]统计分析显示内容构思
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 22m](https://img.shields.io/badge/耗时-22m-orange.svg) ![started 17-07-05 16:08](https://img.shields.io/badge/开始-17/07/05 16:08-orange.svg) ![done 17-07-05 16:30](https://img.shields.io/badge/完成-17/07/05 16:30-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     专注度 突发任务 暂停次数
时间块：睡觉 洗漱 wc10 路上 上班 吃饭[早中晚] 学习 其他
数据统计内容：完成 未完成 总共, 突发 延迟 提前 
步行 移动速度 
任务打断次数 按时完成 延迟完成 起床 睡觉
N次评估 N次总结
连续 坚持 赞 粉丝 浏览 任务数 ![](https://img.shields.io/badge/时间-16:09-ff69b4.svg)
工作：完成百分比 突发 暂停 延迟 专注度 
健康：早起 早睡 早中晚饭 锻炼
习惯：定时习惯 超过21次习惯.
生活：... 路上时长 吃饭时长 移动速度 ![](https://img.shields.io/badge/时间-16:16-ff69b4.svg)
专注度-超过25分钟无干扰
累积任务 天 管理时间 工作时长 有效时长
先直接以2行的表格显示, 以后在考虑图标显示.

#### [x]下一步、上一版和已完成待发布的显示方式
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 05m](https://img.shields.io/badge/耗时-05m-orange.svg) ![started 17-07-05 15:53](https://img.shields.io/badge/开始-17/07/05 15:53-orange.svg) ![done 17-07-05 15:58](https://img.shields.io/badge/完成-17/07/05 15:58-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     用统一的版本树显示, next,待发布,当前、历史版本.
next和待发布经常都在变,可以动态从数据库获取.

#### [x]新的分类模板tlog
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 40m](https://img.shields.io/badge/耗时-40m-orange.svg) ![started 17-07-05 13:41](https://img.shields.io/badge/开始-17/07/05 13:41-orange.svg) ![done 17-07-05 14:21](https://img.shields.io/badge/完成-17/07/05 14:21-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     工作、生活、健康、观点、学习、练习
生活：日常类起床、吃、上班、下班
观点：总结和思绪类,无需执行.不算入当日完成任务.
工作：事业相关,日常工作相关事情.
学习：现在暂未单独罗列,有人会这么划分,我不需要.
疑问-重要紧急的这种方式似乎不适合我,或者我不会划分.
四象限法则 http://www.jianshu.com/p/2f69b68e85e0
焦点转移到四象限学习. ![](https://img.shields.io/badge/时间-14:11-ff69b4.svg)
work life view health family study
模板1.tlog ,

#### [ ]单个任务添加项目标签
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     后期优化功能,现在暂不需要.

#### [ ]系统自动生成第二天的tlog文件
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     疑问以后在系统中操作还是在sublime中操作,
sublime 主要好处是和编写代码一样, 有快捷键、颜色区分、标签自动补齐.
系统也有快捷键.颜色区分. 缺少自动补齐,系统也不需要,全部是界面可视化选择代替. ![](https://img.shields.io/badge/时间-15:12-ff69b4.svg)

#### [x]系统日报格式
![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 30m](https://img.shields.io/badge/耗时-30m-orange.svg) ![started 17-07-05 11:32](https://img.shields.io/badge/开始-17/07/05 11:32-orange.svg) ![done 17-07-05 12:02](https://img.shields.io/badge/完成-17/07/05 12:02-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     分几部分：统计报表、时间日志、完成工作、未完工作、总结、遇见什么问题、尝试哪些
查看日事清并参考.
结果报告 分析 甘特图 时间日志 源文件
结果：进步 退步 待提升 生活规律

#### [x]两个方向需要做的工作
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![done 17-07-05 11:32](https://img.shields.io/badge/完成-17/07/05 11:32-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     1:系统功能 2:数据分析、整理
1、网站系统,任务导入、显示、rest、
2、数据分析：每天任务的统计、分析、生成报告; 文件展示、页面管理;

#### [ ]智能手表系统
![cancelled 17-07-05 11:15](https://img.shields.io/badge/取消-17/07/05 11:15-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     计步和任务开始、结束、暂停;手势控制操作,
通过淘宝查看智能手表手势,找到不同价格产品发现手表只有默认手势：旋转显屏.
这种用户产品展示也不应该会有开发用手势说明.
很少的开发现相关资料 http://blog.csdn.net/pamchen/article/details/27530969
赞不考虑.

#### [ ]独立与时间系统的找人单页面
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     合作常见问题的解决方案,

#### [ ]超出10分没有开始任务就发起警报
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]合作常见问题
![done 17-07-05 10:33](https://img.shields.io/badge/完成-17/07/05 10:33-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     年龄、经验[工作+兼职]、能力、作品、[简历]、工资、时薪
合作方式、喜欢人与合作方式、失败的合作与缺点.
所需技术经验、
时间、报价、报价方式、预算、
价格不合适,无经验,新增需求,时间赶,没时间,

#### [x]纠结开发功能或找人参与
![done 17-07-05 10:13](https://img.shields.io/badge/完成-17/07/05 10:13-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![noway ](https://img.shields.io/badge/-noway-72c7ff.svg) ![change ](https://img.shields.io/badge/-change-72c7ff.svg) 
     找人参与是必然的,越早越好.当下延迟开发不会有损失.
如果找人参与需要什么？1、直接找人参与多沟通；2、做好系统找人有积累;
情况：之前找了几个人都没合作成功导致沟通时间浪费.
如果是系统找人,那就回到了继续开发功能. 感觉成了死循环. 系统找人和tlog功能的相关性和优先级是怎样的？
外包系统可以作为单任务求助,是比较后期的功能; 可以考虑其他方法实现快速找人, 找人的需求是：1、需求描述介绍 2、基本沟通问题(能力经验)；3、时间、报价、报价方式  

#### [ ]时间管理音频张萌11问
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![record ](https://img.shields.io/badge/-record-72c7ff.svg) 
     哪些工作，年薪、什么房子、车、衣服、别人看我、如何帮人、知识渊博？行走、满足、平衡工作和生活的人？

#### [x]区域标示批量添加
![need 1h](https://img.shields.io/badge/预计-1h-orange.svg) ![used 01:02](https://img.shields.io/badge/耗时-01:02-orange.svg) ![started 17-07-05 08:37](https://img.shields.io/badge/开始-17/07/05 08:37-orange.svg) ![done 17-07-05 09:39](https://img.shields.io/badge/完成-17/07/05 09:39-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![flag ](https://img.shields.io/badge/-flag-72c7ff.svg) 
     看源代码逻辑,代码中 所有非任务、项目、链接的统一处理, ![](https://img.shields.io/badge/时间-08:56-ff69b4.svg)
构思逻辑：发现区域分割符,查找是否有区域标签,如果有则添加到 待添加数组, 疑问：项目、任务(一级二级)都需要添加吗？ 项目也只是一个标签,系统已任务为主,项目不用添加.一级二级任务需要添加.
如果区域标签开始且紧接的一行文字带有标签,否则区域标签为空. 
在发现一个区域标签后,则清空上一个区域标签数据.
测试正则---- > -----------------------/r/n(.*)/r/n 不能使用,因为是line单行执行. ![](https://img.shields.io/badge/时间-09:19-ff69b4.svg)
单行逻辑,寻找标签. ![](https://img.shields.io/badge/时间-09:31-ff69b4.svg)
测试成功.

#### [x]上班前早起的这段时间使用率低
![done 17-07-05 08:36](https://img.shields.io/badge/完成-17/07/05 08:36-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) ![error waste](https://img.shields.io/badge/error-waste-red.svg) 
#### [ ]习惯列表
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     超过21天的,100天的行为习惯.

#### [x]易效能10堂课10/10
![created 17-07-04 19:51](https://img.shields.io/badge/创建-17/07/04 19:51-orange.svg) ![need 27m](https://img.shields.io/badge/预计-27m-orange.svg) ![used 38m](https://img.shields.io/badge/耗时-38m-orange.svg) ![started 17-07-04 19:51](https://img.shields.io/badge/开始-17/07/04 19:51-orange.svg) ![done 17-07-04 20:29](https://img.shields.io/badge/完成-17/07/04 20:29-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     五万米高空，取决于道,寻找人生的过程; 3圆交际,5年愿景. 平衡：家庭、健康、事业；九宫格;一日一世一生;
梦想描述、调整、

#### [x]华罗庚《统筹方法》查看
![need 15m](https://img.shields.io/badge/预计-15m-orange.svg) ![used 06m](https://img.shields.io/badge/耗时-06m-orange.svg) ![started 17-07-04 19:41](https://img.shields.io/badge/开始-17/07/04 19:41-orange.svg) ![done 17-07-04 19:47](https://img.shields.io/badge/完成-17/07/04 19:47-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     https://www.douban.com/note/262853816/
统筹方法局限性 https://wenku.baidu.com/view/700b5f7a7c1cfad6185fa775.html

#### [x]思维导图书籍理论学习
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 30m](https://img.shields.io/badge/耗时-30m-orange.svg) ![started 17-07-04 18:56](https://img.shields.io/badge/开始-17/07/04 18:56-orange.svg) ![done 17-07-04 19:26](https://img.shields.io/badge/完成-17/07/04 19:26-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     选择软件操作,已经使用过很多次;
http://www.ximalaya.com/11456261/album/3306724
查看当当书籍目录, 下载pdf 浏览查看图片. ![](https://img.shields.io/badge/时间-19:10-ff69b4.svg)
查看pdf 手绘图, 和我之前软件项目思维导图是一样的.

#### [x]叶武滨：时间管理干货100讲泛读
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 21m](https://img.shields.io/badge/耗时-21m-orange.svg) ![started 17-07-04 17:26](https://img.shields.io/badge/开始-17/07/04 17:26-orange.svg) ![done 17-07-04 17:47](https://img.shields.io/badge/完成-17/07/04 17:47-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     大纲 http://news.tuxi.com.cn/newstsg/17/0124/00/39679649zjf624524068.html
http://searchnews.tuxi.com.cn/cse/search?q=%CA%B1%BC%E4%B9%DC%C0%ED%B8%C9%BB%F5100%BD%B2&s=476112456177414494

#### [x]易效能10堂课9/10
![need 21m](https://img.shields.io/badge/预计-21m-orange.svg) ![started 17-07-04 17:11](https://img.shields.io/badge/开始-17/07/04 17:11-orange.svg) ![done 17-07-04 19:51](https://img.shields.io/badge/完成-17/07/04 19:51-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/04 19:27-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/04 17:26-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     事半功倍;方法,例子,软件操作. 初学者项目视角3个月, 然后半年,一年.
项目分解法：把事情当做项目进行分解。
情景清单，早晚反思，

#### [x]Omnifocus软件了解
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 20m](https://img.shields.io/badge/耗时-20m-orange.svg) ![started 17-07-04 16:59](https://img.shields.io/badge/开始-17/07/04 16:59-orange.svg) ![done 17-07-04 17:19](https://img.shields.io/badge/完成-17/07/04 17:19-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     http://www.mifengtd.cn/articles/omnifocus-1.html
http://www.mifengtd.cn/articles/iphone-mac-omnifocus-gtd-ztd.html
https://www.omnigroup.com/omnifocus/
https://www.zhihu.com/question/21006398
二分钟法则、删除、指派给别人、归档、或者放在日程表上以后做

#### [x]易效能10堂课8/10
![need 18m](https://img.shields.io/badge/预计-18m-orange.svg) ![used 18m](https://img.shields.io/badge/耗时-18m-orange.svg) ![started 17-07-04 16:48](https://img.shields.io/badge/开始-17/07/04 16:48-orange.svg) ![done 17-07-04 17:06](https://img.shields.io/badge/完成-17/07/04 17:06-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     奇妙清单讲解,  音频比视频差太多.比文字好。

#### [x]易效能10堂课7/10
![created 17-07-04 16:21](https://img.shields.io/badge/创建-17/07/04 16:21-orange.svg) ![need 23m](https://img.shields.io/badge/预计-23m-orange.svg) ![used 24m](https://img.shields.io/badge/耗时-24m-orange.svg) ![started 17-07-04 16:21](https://img.shields.io/badge/开始-17/07/04 16:21-orange.svg) ![done 17-07-04 16:45](https://img.shields.io/badge/完成-17/07/04 16:45-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     电子清单,3流程：收集 排程 执行。 app讲解见面, 日历 截止日期 无截止日期 

#### [x]易效能10堂课6/10
![need m](https://img.shields.io/badge/预计-m-orange.svg) ![used 25m](https://img.shields.io/badge/耗时-25m-orange.svg) ![started 17-07-04 15:56](https://img.shields.io/badge/开始-17/07/04 15:56-orange.svg) ![done 17-07-04 16:21](https://img.shields.io/badge/完成-17/07/04 16:21-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     更仔细分类,少而精, 工具(日历、清单、笔记) 基本分类：工作、家庭、私人。 课程铺排， 访问日历权限的app， 一人添加多人知道.

#### [x]易效能10堂课5/10
![need 22m](https://img.shields.io/badge/预计-22m-orange.svg) ![used 26m](https://img.shields.io/badge/耗时-26m-orange.svg) ![started 17-07-04 15:30](https://img.shields.io/badge/开始-17/07/04 15:30-orange.svg) ![done 17-07-04 15:56](https://img.shields.io/badge/完成-17/07/04 15:56-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     如何做要事, 斯蒂芬科维, 初学者一周代办,一周20只青蛙, 情景清单(电脑、电话、在家、外出),A4纸工作法, 收件箱(50件事情记录), 要不要做、结果、行动是什么？ 50件事删除部分、延迟部分、放到日程，情景变化后任务调整, 委托别人做不重要的. 女孩找出3只青蛙的案例,

#### [ ]专注度统计
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     番茄工作法是一种形式,

#### [x]易效能10堂课4/10
![need 19m](https://img.shields.io/badge/预计-19m-orange.svg) ![used 19m](https://img.shields.io/badge/耗时-19m-orange.svg) ![started 17-07-04 15:10](https://img.shields.io/badge/开始-17/07/04 15:10-orange.svg) ![done 17-07-04 15:29](https://img.shields.io/badge/完成-17/07/04 15:29-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     专注,初学者100个番茄钟(和我的暂停类似),一次一事,休息4分钟(锻炼、回电话、喝水、冥想),番茄钟不能分割. 减少内部外部打断, 如何让番茄钟更有效, 预估任务, 作息时间表, 推迟外部打断, 内部打算6-15秒, 番茄钟的实操.

#### [x]易效能10堂课3/10
![created 17-07-04 14:46](https://img.shields.io/badge/创建-17/07/04 14:46-orange.svg) ![used 24m](https://img.shields.io/badge/耗时-24m-orange.svg) ![started 17-07-04 14:46](https://img.shields.io/badge/开始-17/07/04 14:46-orange.svg) ![done 17-07-04 15:10](https://img.shields.io/badge/完成-17/07/04 15:10-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     多年晨间日记,美好的一天,富兰克林 曾国藩,知道自己,忘记自己,觉悟. 日记、总结、反思. 7年记录积累, 记录事实,不是秘密.透过事实找真相. 模板讲解, 目标是慢慢修改出来的.第一次时间长; 没记录就没发生.
模板 https://list.yinxiang.com/moban/0a3babbe-d781-4278-bc30-2f7e0d42880f.php

#### [x]易效能10堂课2/10
![need 23m](https://img.shields.io/badge/预计-23m-orange.svg) ![used 22m](https://img.shields.io/badge/耗时-22m-orange.svg) ![started 17-07-04 14:24](https://img.shields.io/badge/开始-17/07/04 14:24-orange.svg) ![done 17-07-04 14:46](https://img.shields.io/badge/完成-17/07/04 14:46-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     列出坏习惯, 南怀瑾，计划第一步,纸笔收集, 大脑如CPU非硬盘. 大脑关注的事情, 环球旅行，5分钟亲音乐,列出50个,大脑记不住,活在当下,收集,

#### [x]时间的名言
![used 2m](https://img.shields.io/badge/耗时-2m-orange.svg) ![done 17-07-04 19:52](https://img.shields.io/badge/完成-17/07/04 19:52-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     https://www.baidu.com/s?word=%E6%97%B6%E9%97%B4%E7%9A%84%E5%90%8D%E8%A8%80&ie=utf-8&tn=98012088_5_dg&ch=5
http://www.lz13.cn/mingrenmingyan/8528.html

#### [ ]app快速卡片
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     没有进入app软件,就能通过系统外挂卡片就能进行操作.

#### [x]易效能10堂课1/10
![need 26m](https://img.shields.io/badge/预计-26m-orange.svg) ![used 01:02](https://img.shields.io/badge/耗时-01:02-orange.svg) ![started 17-07-04 13:22](https://img.shields.io/badge/开始-17/07/04 13:22-orange.svg) ![done 17-07-04 14:24](https://img.shields.io/badge/完成-17/07/04 14:24-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     一天=一生,线下线上,习惯不容易改变,罗列坏习惯,突破,好习惯：早起,反思,日记,日程表,记录昨天,日历,周清单,早计划晚总结，淘宝番茄钟,情景(堵车),高精力吃青蛙, 打电话记不清楚说过什么. 记录分析才能安排时间.
xx犯困,桌面趴着大概25分钟,完全无法相信. ![](https://img.shields.io/badge/时间-14:02-ff69b4.svg)
先早起21天,

#### [ ]意愿匹配的行动
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
     小愿望配大行动,通常直接叫苦累就不干了.

#### [ ]软件的使用录制自己操作录像作为教程
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]易效能音频-问答
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 33m](https://img.shields.io/badge/耗时-33m-orange.svg) ![started 17-07-04 12:43](https://img.shields.io/badge/开始-17/07/04 12:43-orange.svg) ![done 17-07-04 13:16](https://img.shields.io/badge/完成-17/07/04 13:16-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![change ](https://img.shields.io/badge/-change-72c7ff.svg) 
     践行问题列表：教练、知识产权、软件使用、群、印象笔记、模板、9宫格梦线版、情景清单、离线保持、最好学习是教会别人、不知青蛙、意愿、拖延症、健康食谱、担心、
和吃饭时间完全重合.

#### [x]议论文百科查看
![need 15m](https://img.shields.io/badge/预计-15m-orange.svg) ![started 17-07-04 12:36](https://img.shields.io/badge/开始-17/07/04 12:36-orange.svg) ![done 17-07-04 16:39](https://img.shields.io/badge/完成-17/07/04 16:39-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/04 12:43-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]方法论的演变过程
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]贡献权限设置
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]没有理清项目整体工作量多少,不知道多少功能.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     不敢展开相关工作.

#### [ ]点滴提升时间价值
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     现在的价值数值,梦想的时间价值. 如何逐步提升.

#### [ ]刻意练习
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]学习、练习、评卷、教授
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]练习题
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]统筹安排的方式说明
![created 17-07-04 11:22](https://img.shields.io/badge/创建-17/07/04 11:22-orange.svg) ![done 17-07-04 11:30](https://img.shields.io/badge/完成-17/07/04 11:30-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     课文统筹安排-1课文   http://3y.uu456.com/ss_rnik1zp27oe1i2xjo2_1.html
统筹方法 课文    http://news.21cnjy.com/A/90/612/V173422.shtml

#### [ ]没有安排好事情,焦虑的心情无法活在当下(无法做事)
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
#### [x]叶武滨:日理万机的秘密
![need 33m](https://img.shields.io/badge/预计-33m-orange.svg) ![used 46m](https://img.shields.io/badge/耗时-46m-orange.svg) ![started 17-07-04 11:10](https://img.shields.io/badge/开始-17/07/04 11:10-orange.svg) ![done 17-07-04 11:56](https://img.shields.io/badge/完成-17/07/04 11:56-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     http://video.tudou.com/v/XMTQ4OTc2MjA0OA==.html?spm=a2h28.8313471.fl.dlink_1_1&f=28135363
固定日程 弹性清单 番茄时钟,小孩从5分钟开始锻炼. 3D 28原则 7个习惯 GTD 无线工具
因为主要意思不懂,就需要讲故事.

#### [ ]专注的方法-我是用任务评估的倒计时
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     当单个任务时间太长时,会出现其他事情干扰.如厕所、喝水、电话、同事配合、

#### [ ]时间管理系统演讲框架
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]说名常见问题
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]用大家都易懂的比喻,等比说明分析时间管理
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]第0集部分框架说明视频框架
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]让他人体验快速改变的方法
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     体力运动

#### [ ]从老师分享的角度考虑
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]每个人基础不一样
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
#### [x]演讲的框架
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 14m](https://img.shields.io/badge/耗时-14m-orange.svg) ![started 17-07-04 20:31](https://img.shields.io/badge/开始-17/07/04 20:31-orange.svg) ![done 17-07-04 20:45](https://img.shields.io/badge/完成-17/07/04 20:45-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     https://wenku.baidu.com/view/6d185ccfb9d528ea81c7799a.html
https://wenku.baidu.com/view/9e0c8e4f767f5acfa1c7cd19.html
http://www.sohu.com/a/115023378_497733

#### [x]叶武滨:时间管理一日看人生视频
![created 17-07-04 10:18](https://img.shields.io/badge/创建-17/07/04 10:18-orange.svg) ![used 47m](https://img.shields.io/badge/耗时-47m-orange.svg) ![started 17-07-04 10:00](https://img.shields.io/badge/开始-17/07/04 10:00-orange.svg) ![done 17-07-04 10:47](https://img.shields.io/badge/完成-17/07/04 10:47-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     http://video.tudou.com/v/XMTQzOTMyMjUwNA==.html?spm=a2hzp.8244740.0.0&f=28135363

#### [x]计划看未来
![done 17-07-04 10:13](https://img.shields.io/badge/完成-17/07/04 10:13-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]番茄工作法适合创造性类无法切割时间的工作.
![done 17-07-04 09:41](https://img.shields.io/badge/完成-17/07/04 09:41-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![exp ](https://img.shields.io/badge/-exp-72c7ff.svg) 
#### [x]纪元-时间管理音频
![need 40m](https://img.shields.io/badge/预计-40m-orange.svg) ![used 44m](https://img.shields.io/badge/耗时-44m-orange.svg) ![started 17-07-04 09:16](https://img.shields.io/badge/开始-17/07/04 09:16-orange.svg) ![done 17-07-04 10:00](https://img.shields.io/badge/完成-17/07/04 10:00-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]下载张萌app的思维导图
![done 17-07-04 09:24](https://img.shields.io/badge/完成-17/07/04 09:24-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]第三方插件开发技术方案
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![started 17-07-03 20:30](https://img.shields.io/badge/开始-17/07/03 20:30-orange.svg) ![done 17-07-03 21:05](https://img.shields.io/badge/完成-17/07/03 21:05-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     插件列表选择插件安装. 本地开发,提供注册用户token可以获取个人数据或者测试数据.完成后上传插件js.
因为设计界面,平台就需要提供不同位置的菜单接口控制;
路由、系统插件、菜单;
路由和菜单由package配置控制; 路由指向用户上传的模块js页面;
疑问：纯功能性插件有吗？ 菜单按钮直接运行js. 主要用户不同平台数据利用; ![](https://img.shields.io/badge/时间-20:39-ff69b4.svg)
对现有页面修改参考 模板引擎. 当前vue是编译后的问题,不能修改系统页面; 提供了数据接口,用户可以自定义界面; 
插件需要统一的包名规范 package . 提供了element ui 用户可以直接本地DIY页面.可以根据自己需要; ![](https://img.shields.io/badge/时间-20:44-ff69b4.svg)

##### [x]走小差-突然想起迅雷下载,迅雷打开后中间的视频又吸引了我的眼球.
![done 17-07-03 21:07](https://img.shields.io/badge/完成-17/07/03 21:07-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![summary ](https://img.shields.io/badge/-总结-1ba4ff.svg) ![error outfocus](https://img.shields.io/badge/error-outfocus-red.svg) 
#### [x]weixin.sougou 搜索时间管理
![done 17-07-03 18:09](https://img.shields.io/badge/完成-17/07/03 18:09-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     可以搜索约23,841条相关结果,其中只有20页文章, 15页公众号.

#### [ ]使用黑色UI皮肤,就像之前ps软件或flex皮肤一样
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]重新构思系统思维文字&手稿图
![need 2h](https://img.shields.io/badge/预计-2h-orange.svg) ![started 17-07-03 11:00](https://img.shields.io/badge/开始-17/07/03 11:00-orange.svg) ![done 17-07-03 18:07](https://img.shields.io/badge/完成-17/07/03 18:07-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/07/03 14:24-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/07/03 12:00-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![sleepy ](https://img.shields.io/badge/-sleepy-72c7ff.svg) 
     系统需要功能,及界面设计.
已完成任务如何显示处理. 所有 已完成 未完成 取消. 按时间排列、热度、
新建按钮显示位置确定:放在左上角第一个任务. ![](https://img.shields.io/badge/时间-15:16-ff69b4.svg)
思维文字：首页、市场、
xx犯困. 
把现有思维文字在写一次:  计划、市场、群体、
整个环节行为：设目标 分解 重组 评估 排期 [归纳|标签] 准备 咨询 求助 执行 记录 分析 总结 归档
思维文字: 日程 计划 市场 [任务] 日报 总结 [功过] 群组 
        排行榜 挑战任务 称号 经典 名人  
         建议 教程 社区 公司 合作 版本 功能 价格
计划[日 周 月] 
分解: 把目标划分为尽量小的一个环节任务,是经验的体现,能力提升的过程.同时便于看到进度,有实时反馈.分配任务;
 通常因为经验不足或创新项目时,任务总是未知的. 容易犯错: 分解的任务不能合成一个项目,在多人合作中会出现2个部门或人都完成了自己的工作,但项目整理还未做完,然后出问题就开始推卸责任.
重组：还未作过,无经验.
评估: 根据自己经验,判断完成任务需要的时间. 就像是高考的成绩评分一样重要. 也像合作中客户和老板问这工作需要多少时间. 一旦评估错误就容易导致其他合作环节的错误.
排期：把任务分配到月周日,合理安排工作。常见错误任务分配太多,任务没有完成导致经常加班,没有间隔时间和突发事件安排.     
准备：罗列出历史相关经验,系统推荐经验.
总结[每个任务的总结]
报告[日 周 月]

#### [ ]显示已管理总时长,天,时.纯分钟
![created 17-07-03 10:37](https://img.shields.io/badge/创建-17/07/03 10:37-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]显示已经坚持的行为习惯次数
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]已发送文件给前端并支付部分订金
![done 17-06-30 21:59](https://img.shields.io/badge/完成-17/06/30 21:59-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]突发任务统计
![created 17-06-30 19:54](https://img.shields.io/badge/创建-17/06/30 19:54-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     1、当天第一个工作任务开始后; 上班时间(如9点,时间自设)后添加的.
2、当天已经分配很多工作,
新增并完成的任务. 自动添加标签@new . 9点以后比较好处理.

#### [x]自己调整水平滚动条失败
![done 17-06-30 19:46](https://img.shields.io/badge/完成-17/06/30 19:46-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]已完成的任务可以添加分类标签
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     在日任务分页中查看历史数据并添加修改标签分类. 人物、时间等不能修改.

#### [x]联系的前端都没时间.
![done 17-06-30 17:37](https://img.shields.io/badge/完成-17/06/30 17:37-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     有一个最快是明天下午开始.

#### [x]模板创建编辑的功能设计构思
![created 17-06-30 15:50](https://img.shields.io/badge/创建-17/06/30 15:50-orange.svg) ![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 09m](https://img.shields.io/badge/耗时-09m-orange.svg) ![started 17-06-30 15:51](https://img.shields.io/badge/开始-17/06/30 15:51-orange.svg) ![done 17-06-30 16:00](https://img.shields.io/badge/完成-17/06/30 16:00-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     创建空白任务和子任务及描述. 点选模板后直接把任务及描述添加到当前任务中.
疑问pc如何删除模板和取消模板任务. 
未编辑的新任务可以取消.编辑后无法取消.
不使用的可以不管,赞不删除. 如要删除,可以勾选后在底部条件显示删除按钮.

#### [ ]任务列表页面,右边小窗口显示任务详情.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]Tlog、MD文件构思
![created 17-06-30 15:16](https://img.shields.io/badge/创建-17/06/30 15:16-orange.svg) ![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 10m](https://img.shields.io/badge/耗时-10m-orange.svg) ![started 17-06-30 15:16](https://img.shields.io/badge/开始-17/06/30 15:16-orange.svg) ![done 17-06-30 15:26](https://img.shields.io/badge/完成-17/06/30 15:26-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     日志替代笔记,包含 Tlog、MD和报告文件.
Tlog纯文本编辑器,主要用于显示或导出功能. 暂无作用,是后期多人交流保存可能使用;
超过当日时间的不能修改. 
MD主要做页面查看显示用. 分3部分;   ![](https://img.shields.io/badge/时间-15:21-ff69b4.svg)
1、分析报告数据. 2、任务标题列表 3、把任务自动替换成标题,显示成html.
任务以时间顺序排列.
html如何作为文章被搜索引擎收录.

#### [x]自己修改布局
![done 17-06-30 14:12](https://img.shields.io/badge/完成-17/06/30 14:12-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![error ](https://img.shields.io/badge/-error-red.svg) 
     整理需求的时候,想着想着就开始尝试修改布局.

#### [x]布局需求视频录制
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 28m](https://img.shields.io/badge/耗时-28m-orange.svg) ![started 17-06-30 13:10](https://img.shields.io/badge/开始-17/06/30 13:10-orange.svg) ![done 17-06-30 13:38](https://img.shields.io/badge/完成-17/06/30 13:38-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     =整理需求和思绪; 视频录制;

#### [x]求助操作及显示方式
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![started 17-06-30 14:16](https://img.shields.io/badge/开始-17/06/30 14:16-orange.svg) ![done 17-06-30 14:41](https://img.shields.io/badge/完成-17/06/30 14:41-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/06/30 14:37-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/06/30 14:25-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![better ](https://img.shields.io/badge/-better-1ba4ff.svg) 
     展示以及有了,如何操作和查看.
锻炼的时候在想.  市场界面按钮组罗列,并显示接单按钮;
在任务详情中显示“求助”“取消” 按钮. 其他操作和普通任务一样.
求助需要输入价格并支付.
二级任务求助需要显示1级任务和联系联合及沟通语音记录. 任务名称显示二级名+1级任务名. 换行描述. ![](https://img.shields.io/badge/时间-14:22-ff69b4.svg)
手机如何显示？手机和电脑相同显示方式. 
编辑模式和显示不同. 电脑好删除取消标签. 手机如何删除取消.
手机和z39phone 相同, 手机中选中的标签可以靠前.

#### [x]日周月任务安排方式
![created 17-06-30 11:05](https://img.shields.io/badge/创建-17/06/30 11:05-orange.svg) ![done 17-06-30 11:31](https://img.shields.io/badge/完成-17/06/30 11:31-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     先定大目标、然后分解目标制定计划.
一、定制目标 1、分配到每月 2、每月收纳箱然后分配到每周 3、每周收纳箱分解到每天. 
二、任务直接分配到每天;
任务数量分为: 总数、已分配、已完成. 如此多的数据如何显示,  显示 已完成/总数. 右上角未分配. ![](https://img.shields.io/badge/时间-11:18-ff69b4.svg)
用日事清现有的 月 日 周的显示方式.
![](http://cdn.d7game.com/markdown/2017-06-30_0N~R.png) 
![](http://cdn.d7game.com/markdown/2017-06-30_5IU.png) 自己的方式太难显示,工作量大. 数字的显示用纯文本.

#### [x]上次想法的目标是周末展示自己的想法
![created 17-06-30 10:30](https://img.shields.io/badge/创建-17/06/30 10:30-orange.svg) ![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 01:23](https://img.shields.io/badge/耗时-01:23-orange.svg) ![started 17-06-30 19:59](https://img.shields.io/badge/开始-17/06/30 19:59-orange.svg) ![done 17-06-30 21:22](https://img.shields.io/badge/完成-17/06/30 21:22-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     可以通过纯静态布局展示. 我错误的习惯性把他做成一个完善的系统.
问题情况,持续到今天的一直遗忘.
明天就周六,程序已经无法完成. 对我个人最快的使用方式是,完成导入功能.并出自动分析报告.生成Html;
需要把分析报告数据导出成表格, 需要字符串拼接. markdown-table ![](https://img.shields.io/badge/时间-20:08-ff69b4.svg)
每天统计的数据记录到数据库中, 后期还能在生成月报,年报. 
想法是拿自己现有的经验积累和程序与开心喜悦交流沟通. 现在已经遗忘展现什么,也还没有交流的问题？没做好准备. ![](https://img.shields.io/badge/时间-20:12-ff69b4.svg)
准备问题和分享话题.  1、我自己的问题.  2、分享：常见问题及解决方法.  ![](https://img.shields.io/badge/时间-20:27-ff69b4.svg)
1、大家每天时间去哪儿了？如这周5天.
2、每天有效工作时间几小时
3、如何确定自己和昨天相比是进步的
4、时间管理都做过什么？
5、在时间方面什么地方消费过？如找阿姨打扫家里,节约自己时间. 购买资料,减少自己搜索查找和整理;
6、如何确保自己是努力并全力以赴的？
7、在时间管理上投入过多少时间或者前？
8、是怎样看待时间和钱？
9、如何利用我领先的技术和经验，你们会怎么做？
分享：通过提问,说出大家常见问题和痛点.让大家看会自己现状.
分享他人和自己的经验及方法. ![](https://img.shields.io/badge/时间-20:51-ff69b4.svg)
痛点问题是什么？ 大家总是说或听到没时间,时间去哪儿了？
 工作没有进展，常常被下属搅得燋头烂额，无法专心工作。觉得自己没有完成当天的工作。总完不成工作计划，负责的项目常常出现延期。
 自己和员工投入了大量时间,如此努力都做了什么？
计划总玩不成？ 
为了省几百块钱房租，导致在通勤时间较长。
为了房价便宜,买偏远的房子上班时间1小时;
经常刷微信朋友圈却又不直接联系朋友,朋友圈里有什么是自己需要的？
今天忙不完的事情,前几天却闲的很?
经常用几分钟在微信里说不到10句话,却没有打电话？
买了很多不用的东西在家占地方,有经常整理,保养维护.
每个月路上的时间有多少,都干什么了？
购物时为了几块钱讨价还价?
为了节省外卖5元钱,多用30分钟去餐厅吃饭和排队？
忙着去好吃的餐厅排队1小时,也不肯花5元叫外卖或其他？
路上花几个小时就为了看对方一眼，并未做任何有助于对方的事情，甚至未做任何事情？
长途旅游5天,20%以上时间用在路上白天睡觉?
看电视剧，玩游戏,刷微博、朋友圈。 ![](https://img.shields.io/badge/时间-21:22-ff69b4.svg)

#### [ ]收纳箱展开与关闭&快捷键
![created 17-06-30 10:28](https://img.shields.io/badge/创建-17/06/30 10:28-orange.svg) ![need 1h](https://img.shields.io/badge/预计-1h-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]标签中放入各种特性,如周末,总结,生活,工作
![created 17-06-30 10:03](https://img.shields.io/badge/创建-17/06/30 10:03-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     添加常用标签,每天活在每次刷新重新排列顺序. 
标签对应一个@flag, 添加到任务中如何显示到界面.如导入的任务中包含位置flag如何处理？
全部显示到"标签"分栏中,并添加到数据库.  ![](https://img.shields.io/badge/时间-15:40-ff69b4.svg)

#### [ ]生活采购类事务周任务收纳箱
![created 17-06-30 09:49](https://img.shields.io/badge/创建-17/06/30 09:49-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     工作太忙,在周末统一时间超时或淘宝购买.  @week, 自动读取今日日期,把任务添加到周六任务中.并且从今日列表中删除. ![](https://img.shields.io/badge/时间-16:02-ff69b4.svg)

#### [ ]统计暂停次数
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]其他人看了任务都能知道我现在和接下来做什么
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     就像助手一样安排工作,其他人看了也可以做好配合工作,并等待.

#### [x]手稿图手机拍照
![done 17-06-28 14:06](https://img.shields.io/badge/完成-17/06/28 14:06-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
##### [x]拍照后就顺手翻朋友圈.
![created 17-06-28 14:07](https://img.shields.io/badge/创建-17/06/28 14:07-orange.svg) ![done 17-06-28 14:07](https://img.shields.io/badge/完成-17/06/28 14:07-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![summary ](https://img.shields.io/badge/-总结-1ba4ff.svg) ![error ](https://img.shields.io/badge/-error-red.svg) 
#### [ ]用自己的时间管理日志为分析案例做讲解
![created 17-06-28 09:48](https://img.shields.io/badge/创建-17/06/28 09:48-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     如果一步步成长和演变.

#### [x]搜索“时间管理”
![created 17-06-28 08:25](https://img.shields.io/badge/创建-17/06/28 08:25-orange.svg) ![started 17-06-28 08:25](https://img.shields.io/badge/开始-17/06/28 08:25-orange.svg) ![done 17-06-28 09:47](https://img.shields.io/badge/完成-17/06/28 09:47-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/06/28 09:17-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/06/28 08:50-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     如何进行时间管理？ https://www.zhihu.com/question/19705539
褪墨・时间管理    http://www.mifengtd.cn/
百度经验    http://jingyan.baidu.com/article/29697b912fc039ab21de3c53.html
GTD时间管理-知道    http://baike.baidu.com/link?url=o7mErHOHrLo8v5hLPF9qee-u1g2sbK38TjspHyyMr2Z4FrpA9uFqS87xf3tNuEFYIpHlyZ1_W59y5ZmNm4ALB9VH4Ut5a6B8QXrMBTlFR7mC6pdnWZgpqTOnA3GWxuM4
哈佛时间管理-当当 http://search.dangdang.com/?key=%B9%FE%B7%F0%CA%B1%BC%E4%B9%DC%C0%ED&act=click
书籍 哈佛时间管理课：畅销4版 哈佛商学院的时间管理课 高效能人士的时间管理艺术 时间管理——高效率人士的成功利器  哈佛时间管理课套装2册（哈佛凌晨五点半+哈佛晚间八点半）

#### [ ]计划分解 任务评估 真实记录 结果比对 分析原因 经验总结
![created 17-06-28 07:57](https://img.shields.io/badge/创建-17/06/28 07:57-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     源于其他人的经验.

#### [ ]首页
![need 23h](https://img.shields.io/badge/预计-23h-orange.svg) ![started 17-06-27 20:21](https://img.shields.io/badge/开始-17/06/27 20:21-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     其中布局不擅长,如何确实部分css会导致时间延长.

##### [ ]评估
![started 17-06-27 20:21](https://img.shields.io/badge/开始-17/06/27 20:21-orange.svg) ![done 17-06-27 20:42](https://img.shields.io/badge/完成-17/06/27 20:42-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
##### [ ]数据库设计
![need 1:30](https://img.shields.io/badge/预计-1:30-orange.svg) ![started 17-06-27 20:42](https://img.shields.io/badge/开始-17/06/27 20:42-orange.svg) ![done 17-06-27 21:50](https://img.shields.io/badge/完成-17/06/27 21:50-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/06/27 20:59-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/06/27 20:48-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      2个表->7个表
任务 成员 标签 时间 快捷键 多层分类 群
task:id uid title desc to created state project due duration assess(评估)  help(求助) ~~tasks flags~~    ![](https://img.shields.io/badge/时间-21:10-ff69b4.svg)
tasks:id tid uid started toggle done cancle
friend: id uid  用系统&z43数据
flag: id title type(分类) job weight(重要性) count ![](https://img.shields.io/badge/时间-21:18-ff69b4.svg)
flags: id fid tid(taskid) uid
time: id duration count
times:id uid timeid 
key: id title uid tid uid_to fid ![](https://img.shields.io/badge/时间-21:24-ff69b4.svg)
group: 和z43相同.
多层分类：flag 表的 type 和job中获取. 这种方式不行, 分类含 全部状态.  是否能通过type完成？现在先百度 ![](https://img.shields.io/badge/时间-21:35-ff69b4.svg)
无限级分类数据库设计    http://www.oschina.net/question/1092_256  ![](https://img.shields.io/badge/时间-21:43-ff69b4.svg)
多层分类不会,看资料后依然无结果.

##### [ ]数据库创建
![need 1h](https://img.shields.io/badge/预计-1h-orange.svg) ![started 17-06-28 10:42](https://img.shields.io/badge/开始-17/06/28 10:42-orange.svg) ![done 17-06-28 12:25](https://img.shields.io/badge/完成-17/06/28 12:25-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/06/28 12:05-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/06/28 11:42-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      mysql local 2 workbench, z43项目遗留问题db同步. ![](https://img.shields.io/badge/时间-10:56-ff69b4.svg)
task 表修改完善
task :id uid tid title desc created state pid(project) duration  help(求助) money ~~to tasks flags~~    
tasks:id uid tid started toggle done cancle due assess(评估)
数据库一对多的关系不熟练. ![](https://img.shields.io/badge/时间-11:29-ff69b4.svg)
task 表创建. ![](https://img.shields.io/badge/时间-11:42-ff69b4.svg)
tasks 创建 ![](https://img.shields.io/badge/时间-11:48-ff69b4.svg)
time times flag flags key ![](https://img.shields.io/badge/时间-12:26-ff69b4.svg)

##### [ ]边创建db边犹豫的修改,时间可以分为：执行力和创新修正.
![done 17-06-28 12:28](https://img.shields.io/badge/完成-17/06/28 12:28-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![summary ](https://img.shields.io/badge/-总结-1ba4ff.svg) ![error ](https://img.shields.io/badge/-error-red.svg) 
##### [ ]在坐不住的之后,先勇敢的start任务.然后状态就好了.
![done 17-06-28 22:19](https://img.shields.io/badge/完成-17/06/28 22:19-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![grow ](https://img.shields.io/badge/-grow-72c7ff.svg) ![summary ](https://img.shields.io/badge/-总结-1ba4ff.svg) 
##### [ ]任务都按时完成,但让自己感到泄气的是感觉和日事清没多大区别,实际有.担心做的又没人用;
![done 17-06-28 22:21](https://img.shields.io/badge/完成-17/06/28 22:21-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![feel bad](https://img.shields.io/badge/feel-bad-72c7ff.svg) ![error warry](https://img.shields.io/badge/error-warry-red.svg) 
##### [ ]前端逻辑
![need 7:20](https://img.shields.io/badge/预计-7:20-orange.svg) ![started 17-06-28 17:00](https://img.shields.io/badge/开始-17/06/28 17:00-orange.svg) ![done 17-06-29 17:38](https://img.shields.io/badge/完成-17/06/29 17:38-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/06/29 14:52-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/06/29 14:36-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/06/29 13:31-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/06/29 12:05-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/06/29 10:11-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/06/28 22:16-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/06/28 19:08-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/06/28 18:37-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/06/28 18:28-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/06/28 18:04-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![case think](https://img.shields.io/badge/case-think-72c7ff.svg) 
      拖动2h 快捷键2h 导入2h 添加30m 完成20m (时长 任务 标签)选中状态30m 
需要时长 标签 人物列表之间禁止拖动; bug当列表无内容时无法拖动 ;         
拖动：禁止拖动不同类型拖动. ![](https://img.shields.io/badge/时间-17:07-ff69b4.svg)
    内容为空依然能推动,ul style设置min-height: 300px;即可. 添加css  ![](https://img.shields.io/badge/时间-17:19-ff69b4.svg)
拖动列表mousedown时,显示可拖动区域.
旋转虚线边框 https://codepen.io/techniq/pen/gzyHI
css3边框线条动画   http://www.cnblogs.com/miharu/p/4997285.html   ![](https://img.shields.io/badge/时间-17:30-ff69b4.svg)
在onStart中添加拖动时的css虚线框. ![](https://img.shields.io/badge/时间-17:47-ff69b4.svg)
=快捷键 jquery.hotkeys , 快捷键逻辑 ![](https://img.shields.io/badge/时间-20:01-ff69b4.svg)
=选中样式, item_select . 最短选中一种状态.  ![](https://img.shields.io/badge/时间-19:35-ff69b4.svg)
=li点击动画 cssshake 添加到元素后无动画执行. ![](https://img.shields.io/badge/时间-20:14-ff69b4.svg)
 使用animate.css 动画正常. ![](https://img.shields.io/badge/时间-20:31-ff69b4.svg) 
=上下箭头选择任务 taskSelect ![](https://img.shields.io/badge/时间-21:20-ff69b4.svg)
=任务的选中状态  ![](https://img.shields.io/badge/时间-21:37-ff69b4.svg).
*任务完成的class=item finishedItem  span=diyCheckbox checked
=任务完成&取消完成 ![](https://img.shields.io/badge/时间-22:13-ff69b4.svg)
成员列表直接用文字实现,自己处理不了的时候放弃图片. 17-06-29 ![](https://img.shields.io/badge/时间-10:11-ff69b4.svg)
=列表顺序可拖动, 查看官网案例和代码, 把父容器作为参数传入即可.  ![](https://img.shields.io/badge/时间-10:18-ff69b4.svg)
=导入-解析tlog,
 parse.js 查看及修改成parseTlog.js, 代码主要为nodejs运行,改变为js直接运行;  ![](https://img.shields.io/badge/时间-10:40-ff69b4.svg)
 运行代码测试可用性, 测试成功. 发现有乱码, parseTlog转换为utf8后乱码解决. ![](https://img.shields.io/badge/时间-10:59-ff69b4.svg)
xx开小差不知道思想跑哪儿去了,最后在和牛奶. ![](https://img.shields.io/badge/时间-11:16-ff69b4.svg) 
=header 添加"移动"鼠标样式. ![](https://img.shields.io/badge/时间-11:26-ff69b4.svg)
=导入-点击提供输入框,解析后如何处理?暂不提供tlog在线编辑.
^按时间显示,不需要把已完成任务置底. ![](https://img.shields.io/badge/时间-11:30-ff69b4.svg)
xx想到其他,错误总结-->失败项目-->招聘-->细节情况沟通.  ![](https://img.shields.io/badge/时间-11:41-ff69b4.svg)
=导入 导入的任务全部放到今天,做不完可以放到收纳箱中.难点:放到收纳箱中界面如何显示(设计到tlog文件)？本地tlog文件已经变化,可以根据created显示在今日中; 添加“原始”分类按钮? 收纳箱中的任务添加标签为"unset" ![](https://img.shields.io/badge/时间-11:52-ff69b4.svg)
^编辑任务后,也相应修改tlog文件内容,tlog内容为官方固定格式; 后期可以用户定义显示格式; ![](https://img.shields.io/badge/时间-11:58-ff69b4.svg)
=小键盘数字快捷键,现在所有数据都能触发. hotkey.js并未区分小键盘.通过查看event,看到有keycode能用作区分.  ![](https://img.shields.io/badge/时间-12:03-ff69b4.svg)
^导入-tlog如何显示? 常规是4象限,我的tlog是按照时间做的排序.那就用时间.放弃4象限; 还需要按照项目显示,项目不限于时间,是从开始到结束; ![](https://img.shields.io/badge/时间-13:39-ff69b4.svg)
^需要区分查看模式和编辑模式, 如何查看姜师和王某两人的任务? 编辑区在右边垂直列表中; 查看在底部水平列表区; ![](https://img.shields.io/badge/时间-13:45-ff69b4.svg)
^导入和导出的格式不同,因为功能不同. 导入未了方便用户,可以自动为分类子任务添加标签. 显示也可以用这方式排序. ![](https://img.shields.io/badge/时间-13:53-ff69b4.svg)
^导入的任务显示在最下面.自动添加标签import,特别是已完成任务. ![](https://img.shields.io/badge/时间-14:01-ff69b4.svg)
^任务切换顺序或改动任务时间后,都重新计算任务开始时间. ![](https://img.shields.io/badge/时间-14:09-ff69b4.svg)
^难点:生活类任务(如午饭)重叠时,如何处理? 1、延长任务时间.(显示上直接重叠很方便,但是自动计算和层级会比较麻烦.) 2、任务划分成小块.(理应这样,和暂停也很类似.如何实现和管理?)根据任务总时长自动切割,如4h分为3h+1h; 添加字段part? 相关问题有 界面显示,子块拖动. 时间计算直接通过tid相加即可,这样就是实际把任务划分成了几个子任务,那么点击过后里面的子任务如何显示与处理?
^生活类部分任务具有固定时间特性,不受自动计算影响.
现在想的不是属于导入功能应该处理的.应该立刻停止,放到其他时间. ![](https://img.shields.io/badge/时间-14:31-ff69b4.svg)
=再次查看解析tlog文件层级, 层级对的.但是子任务的描述错误. ![](https://img.shields.io/badge/时间-14:55-ff69b4.svg)
 查看代码子任务描述相关代码 currentProject.tasks[taskLen - 1].desc ![](https://img.shields.io/badge/时间-15:19-ff69b4.svg)
 修复desc层级Bug.导入算是完成. ![](https://img.shields.io/badge/时间-15:32-ff69b4.svg)
xx查看技术群问题无人回答,联想起付费问答,qq机器人自动回答. ![](https://img.shields.io/badge/时间-15:48-ff69b4.svg)
^+ 任务详情,
^操作流程：任务分解-》添加任务-》任务重组(查漏补缺)-》任务多人协调分配-》任务周期安排-》当天任务添加-》当日顺序调整-》评估-》执行-》完成-》总结(分析原因、积累经验)-》分类归档 
问题：不懂 犯错 停滞 考虑不周 遗忘 人员流动 丢失 [内斗 孤立 不好意思 杂事 生活难事] ![](https://img.shields.io/badge/时间-16:13-ff69b4.svg)
分解和重组通过多人一起思考,
基于流程进行冥想操作, 细分和重组按部门种类垂直列表划分,列表和手机一样,垂直独立滚动请求.水平显示各部门. 
多人工作划分：模板自动添加子任务并分配; 按人显示工作内容列表,进行分配.显示数量和总时长;
检查进度重复类工作如何处理？
分解、重组、评估、计划、执行、总结、[归档]分别一个页面. 用任务列表简单修改实现, ![](https://img.shields.io/badge/时间-17:07-ff69b4.svg)
感觉任务分解有遗漏;这一版本模块够了.  ![](https://img.shields.io/badge/时间-17:20-ff69b4.svg)
^但是缺少vue&模块间整合,还有数据操作,顺序保存,标签、成员、时间 子列表需要单选和多选. ![](https://img.shields.io/badge/时间-17:25-ff69b4.svg)
想着找他人合作,然后思绪又失控了. ![](https://img.shields.io/badge/时间-17:40-ff69b4.svg)

##### [ ]之前任务引起的相关问题,不应该在当前阶段处理.
![done 17-06-29 14:32](https://img.shields.io/badge/完成-17/06/29 14:32-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![summary ](https://img.shields.io/badge/-总结-1ba4ff.svg) ![error ](https://img.shields.io/badge/-error-red.svg) 
##### [ ]任务暂停片段太多,难以看出总耗时.不知该快该慢.
![done 17-06-29 14:34](https://img.shields.io/badge/完成-17/06/29 14:34-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) ![summary ](https://img.shields.io/badge/-总结-1ba4ff.svg) ![error ](https://img.shields.io/badge/-error-red.svg) 
      他会导致连锁反应,如时间延长到什么时候?

##### [ ]成功的体验了失败
![done 17-06-29 17:59](https://img.shields.io/badge/完成-17/06/29 17:59-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![error ](https://img.shields.io/badge/-error-red.svg) ![classic ](https://img.shields.io/badge/-classic-72c7ff.svg) 
      知道任务分解常出现遗漏,现在清晰的体验了一次.难点在于如何避免.

##### [ ]遗漏任务当做新增任务处理,重新评估时间
![done 17-06-29 18:36](https://img.shields.io/badge/完成-17/06/29 18:36-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![summary ](https://img.shields.io/badge/-总结-1ba4ff.svg) ![rule ](https://img.shields.io/badge/-rule-72c7ff.svg) 
##### [ ]难受的感觉掩盖了目标吗
![done 17-06-30 09:07](https://img.shields.io/badge/完成-17/06/30 09:07-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![summary ](https://img.shields.io/badge/-总结-1ba4ff.svg) ![classic ](https://img.shields.io/badge/-classic-72c7ff.svg) ![feel ](https://img.shields.io/badge/-feel-72c7ff.svg) ![error nofocus](https://img.shields.io/badge/error-nofocus-red.svg) 
      心里面有个目标:周末活动前,把pc v1版完成,做好时间管理分享的准备.
目标不明确(不显眼易忘记),一出现问题就难受的感觉就掩盖了目标.

##### [ ]布局
![need 3:30](https://img.shields.io/badge/预计-3:30-orange.svg) ![started 17-06-28 14:12](https://img.shields.io/badge/开始-17/06/28 14:12-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/06/29 20:30-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/06/29 19:30-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/06/28 16:56-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      框架1h+30m + 45m+ 50m + 10m +10m +10m 
待办区列表,查看以前手机的html,css代码.
感觉坐不住的烦躁,去了好几次厕所. ![](https://img.shields.io/badge/时间-14:21-ff69b4.svg)
框架：应该先做框架, ![](https://img.shields.io/badge/时间-14:42-ff69b4.svg)
待办区 今日任务 时长 标签 布局完成.右边日期用直接用. ![](https://img.shields.io/badge/时间-15:25-ff69b4.svg)
缺少任务&人物,自己搞不定. ![](https://img.shields.io/badge/时间-15:30-ff69b4.svg)
需要和vue&element框架整合,
情况,整个页面做到一个vue中可能代码比较多,就需要把单一区域做成vue组件.
那就出现不同子组件之间拖动列表如何处理？ 猜想接收拖动后,对数据源array做操作即可. 
关于页面需要布局框架可以产考elem/admin代码. ![](https://img.shields.io/badge/时间-15:47-ff69b4.svg)
删除frame.html中无用代码.基本拖动功能,Bug当列表无内容时无法拖动. ![](https://img.shields.io/badge/时间-16:13-ff69b4.svg)
仔细看下,首页中的内容不多. 都是循环的ui/li内容. 完全可以使用一个vue文件. ![](https://img.shields.io/badge/时间-16:32-ff69b4.svg)
主页布局已有. 忘记考虑vue工作,需要创建pc版vue项目.可采用elem/admin代码 ![](https://img.shields.io/badge/时间-16:36-ff69b4.svg)
G:\workspace\197redpack\197redpackB\adminnew 
需要创建git项目代码库,现在d7game服务器没有运行. ![](https://img.shields.io/badge/时间-16:49-ff69b4.svg)
还差任务详情.
布局需求：1、任务详情 2、水平滚动 3、底部加一行标签(水平滚动).
需求制作视频,并把文件打包; 17-06-29 ![](https://img.shields.io/badge/时间-20:09-ff69b4.svg)
可以考虑直接用 element布局,就不需要日事清的html了. ![](https://img.shields.io/badge/时间-20:12-ff69b4.svg)

##### [ ]api接口
![need 3h](https://img.shields.io/badge/预计-3h-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      rest*6 = 30m*6    

##### [ ]前端逻辑+1
![created 17-06-29 18:41](https://img.shields.io/badge/创建-17/06/29 18:41-orange.svg) ![need 9h](https://img.shields.io/badge/预计-9h-orange.svg) ![started 17-06-30 10:15](https://img.shields.io/badge/开始-17/06/30 10:15-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      任务详情;vue框架;模块间整合;数据操作;顺序保存;单选和多选.
3h       1.5h      1h        6*20m     45m       45m
单选和多选用element组件代替实现; 任务详情的标签状态;
=顺序如何保存,添加index字段;

##### [ ]后端
![need 12h](https://img.shields.io/badge/预计-12h-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      6 * 2h 

##### [ ]对接
![need 1:30](https://img.shields.io/badge/预计-1:30-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      前端api代码 6*5=30个*3 = 90m

##### [ ]测试
![need 3h](https://img.shields.io/badge/预计-3h-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]罗列出3种等级需要总结的任务
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     1、延迟完成 2、提前完成 3、所有

#### [ ]任务完成状态,变成添加总结输入区
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]构思第一版可用功能Pc
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 24m](https://img.shields.io/badge/耗时-24m-orange.svg) ![started 17-06-27 19:36](https://img.shields.io/badge/开始-17/06/27 19:36-orange.svg) ![done 17-06-27 20:00](https://img.shields.io/badge/完成-17/06/27 20:00-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     1、主页 2、导入 3、任务编辑 4、md编辑 5、日报数据 6、数据统计&比较 7、饼图显示
时间块：睡觉 洗漱 wc10 路上 上班 吃饭[早中晚] 学习 其他
数据统计内容：完成 未完成 总共, 突发 延迟 提前 
步行 移动速度 
任务打断次数 按时完成 延迟完成 起床 睡觉
N次评估 N次总结
连续 坚持 赞 粉丝 浏览 任务数

#### [ ]一个项目有成百上前个功能和问题,如何安排处理
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]tlog如何导入
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 06m](https://img.shields.io/badge/耗时-06m-orange.svg) ![started 17-06-27 18:43](https://img.shields.io/badge/开始-17/06/27 18:43-orange.svg) ![done 17-06-27 18:49](https://img.shields.io/badge/完成-17/06/27 18:49-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     导入的数据如何处理, 12点前导入的归纳为今天处理,12点以后导入的未完成任务归纳到的待办清单收纳箱.
导入后,tlog文件自动格式化.
难点:多次导入,难以比对. 在导入前,先从缓存中遍历任务名. 如果相同就不导入.前期通过手动人为注意;

#### [x]正式取名时间日志,appstore和国内都能使用.
![done 17-06-27 17:20](https://img.shields.io/badge/完成-17/06/27 17:20-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]md显示方式构思
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 47m](https://img.shields.io/badge/耗时-47m-orange.svg) ![started 17-06-27 16:36](https://img.shields.io/badge/开始-17/06/27 16:36-orange.svg) ![done 17-06-27 17:23](https://img.shields.io/badge/完成-17/06/27 17:23-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     按时间段,以日和项目为单位. 
先完成个人文件显示, 后期可以搜索出所有人的.

#### [x]标签有哪些？用户自添加的如何处理
![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 28m](https://img.shields.io/badge/耗时-28m-orange.svg) ![started 17-06-27 14:28](https://img.shields.io/badge/开始-17/06/27 14:28-orange.svg) ![done 17-06-27 14:56](https://img.shields.io/badge/完成-17/06/27 14:56-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     标签自添加一栏,他人添加的放在最右边,因为会有很多,显示不下就用显示固定数量;
难点：屏幕不同大小,响应式显示.
系统固定有哪些？ 不同行业有不同标签, 开发有很多无种类划分.
标签：
    时长、职位[软件、框架、程序语言、售后、客服、客户、] 
    人数、难度、
    生活、工作
    自定义:z1flag中的
    任务：超时 提前 按时 未评估 无记录 
    ...之前做过的多级分类. 3-06.tlog
标签太多,显示什么由用户自选择. 或者后期系统自动根据行业自动分类;
前期可是前段静态写死,写在js中. 后期直接从数据库查询并缓存到前端;
tlog中的标签全部归纳为DIY ![](https://img.shields.io/badge/时间-15:53-ff69b4.svg)

#### [x]首页构思优化
![done 17-06-27 14:25](https://img.shields.io/badge/完成-17/06/27 14:25-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     当任务信息无子任务不多时,用卡片化内容直接显示.就无需点击查看.
标签采用mysql 并集查询. 交集查询使用电视这样难操作;

#### [ ]专业领域标准化思考方式&不同难度表格
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     如做个应用整个生态链有哪些环节,小公司和大公司通常考虑哪些?

#### [x]element组件再次查看
![done 17-06-27 13:52](https://img.shields.io/badge/完成-17/06/27 13:52-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]添堵-黄牛、项目负责人
![done 17-06-27 12:03](https://img.shields.io/badge/完成-17/06/27 12:03-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bad ](https://img.shields.io/badge/-bad-red.svg) 
     通常在发送情况是才会提及这问题,这个时候出现问题的人难以接受这个概念,带着自责的心里会理解为职责.带着坏心情时不愿听对方的说明,不会认为是自己的问题.无法沟通导致问题得不到解决,麻烦的情况是大家都忙. 更让人伤心的是接着问题持续发生.

#### [x]假如罗列的功能都完成,然后呢？
![done 17-06-27 11:54](https://img.shields.io/badge/完成-17/06/27 11:54-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     现有项目优化,找出最优系统扩大.无用系统删除. 根据用户的任务和使用数据最优化, 电话咨询用户做反馈调查,做调整;
然后是相关领域的应用添加、扩散.

#### [ ]这个项目需要多少钱？假如100万做这项目？
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]思维文字-首页
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 05m](https://img.shields.io/badge/耗时-05m-orange.svg) ![started 17-06-27 11:11](https://img.shields.io/badge/开始-17/06/27 11:11-orange.svg) ![done 17-06-27 11:16](https://img.shields.io/badge/完成-17/06/27 11:16-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![add ](https://img.shields.io/badge/-add-72c7ff.svg) 
     主页思维文字: banner[重要推荐、点击最多] 视频教程 常犯错误 认识误区 方法 名人 排名 群 名人分享视频 
社区、总结 建议 地区
内容已经够多,后期的通过app建议或和他人沟通来完善.

#### [x]手机版布局手稿图
![need 1h](https://img.shields.io/badge/预计-1h-orange.svg) ![started 17-06-27 08:12](https://img.shields.io/badge/开始-17/06/27 08:12-orange.svg) ![done 17-06-27 11:07](https://img.shields.io/badge/完成-17/06/27 11:07-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/06/27 09:50-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/06/27 08:48-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     主页思维文字: banner[重要推荐、点击最多] 视频教程 常犯错误 认识误区 方法 名人 排名 群 名人分享视频 

#### [x]大目标需要框架性划分目标,现在完成了目标也看不出当前进度
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 32m](https://img.shields.io/badge/耗时-32m-orange.svg) ![started 17-06-27 11:17](https://img.shields.io/badge/开始-17/06/27 11:17-orange.svg) ![done 17-06-27 11:49](https://img.shields.io/badge/完成-17/06/27 11:49-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question vision](https://img.shields.io/badge/question-vision-72c7ff.svg) 
     按照产品阶段划分为: 市场调查 产品设计 美术设计 程序编码 运行上线 推广 调整修改 产品完善
产品构思 
可以根据现在项目流程各环节思考. 如 分析、思维文字、导图、手稿图、
相关参考 4P、4C、4S、4R、4V、4I营销理论 http://www.tuicool.com/articles/qAfQBr
可以根据已完成任务的百分比判断进度, 问题是总会有新的任务. 这需要做版本规划,已有任务定时完成. 接着问题是新任务否定了旧任务做的改进. 那直接取消旧任务,这样可以提高版本速度; 这次最麻烦的是被取消旧任务是重要的前置任务.可以考虑新任务替代旧任务,小任务就加班完成. 大量改动就考虑重新修订版本. 这需要衡量继续完成的代价和直接修改版本的代价.

#### [ ]相同任务完成时间的比较,如1个页面布局
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]手机版左右翻页
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]网站页面手稿图理清思路
![need 1h](https://img.shields.io/badge/预计-1h-orange.svg) ![started 17-06-26 16:21](https://img.shields.io/badge/开始-17/06/26 16:21-orange.svg) ![done 17-06-26 19:27](https://img.shields.io/badge/完成-17/06/26 19:27-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/06/26 18:20-orange.svg) ![{3}](https://img.shields.io/badge/继续-17/06/26 17:23-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     首页 规划 下载 教程 社区 博客 进入(系统)
建议 合作 定制 价格 加入 招聘 版本时间轴     
从新理清需要的页面. ![](https://img.shields.io/badge/时间-16:32-ff69b4.svg)
画了几张图,感觉没理清,不满意. ![](https://img.shields.io/badge/时间-17:23-ff69b4.svg)
公司介绍 联系我们 文化 团队 动态 发展历程 管理团队  简介 愿景 视频介绍 ![](https://img.shields.io/badge/时间-18:24-ff69b4.svg)
查看每页进行修改优化.  缺少模板页. ![](https://img.shields.io/badge/时间-18:26-ff69b4.svg)
模板页和时间、项目一样的展示方式,添加模板和添加任务流程一样. 
任务执行页面 ![](https://img.shields.io/badge/时间-18:44-ff69b4.svg)
周、月任务的显示参考日事清. 其中月可以修改成瀑布流显示. ![](https://img.shields.io/badge/时间-18:56-ff69b4.svg)
思维文字 计划 市场 群(公司) 笔记 日报 周报 月报 项目报 技术报  的手稿图  
日报 周报 月报 一样. 笔记 项目报 技术报 一样.  ![](https://img.shields.io/badge/时间-19:15-ff69b4.svg)
还缺市场任务 领取和取消是同一个页面入口. 完成和编辑和普通任务相同. ![](https://img.shields.io/badge/时间-19:27-ff69b4.svg)

#### [ ]任务设计多个方案
![created 17-06-26 15:53](https://img.shields.io/badge/创建-17/06/26 15:53-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     一个网站设计多个手稿图, 1个人设计5稿,或者多人阻塞顺序设计不同5稿;

#### [x]网站系统功能构思
![created 17-06-26 15:08](https://img.shields.io/badge/创建-17/06/26 15:08-orange.svg) ![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 27m](https://img.shields.io/badge/耗时-27m-orange.svg) ![started 17-06-26 15:11](https://img.shields.io/badge/开始-17/06/26 15:11-orange.svg) ![done 17-06-26 15:38](https://img.shields.io/badge/完成-17/06/26 15:38-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     功能 价格 下载 解决 方案 博客 课程 帮助 规划 
大事件 求助 建议 需求 合作 待完项目 版本时间轴
任务分类
参考看云系统5分钟无结果.
首页展示数据,
首页 规划 下载 教程 社区 博客 进入(系统)
建议 合作 定制 价格 加入 招聘 版本时间轴    

#### [ ]如何把他人往项目里带
![created 17-06-26 14:43](https://img.shields.io/badge/创建-17/06/26 14:43-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     通过提问,逐步让他参与项目. 认同、思考、行动、参与

#### [ ]编辑模式&查看模式
![created 17-06-26 14:41](https://img.shields.io/badge/创建-17/06/26 14:41-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]产品优化-时间监听(按键、屏幕、时间)
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]工作和生活、学习重要性4象限的划分
![created 17-06-26 14:23](https://img.shields.io/badge/创建-17/06/26 14:23-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     上班就是重要,个人学习就是重要不紧急;

#### [ ]任务执行专注度.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]键盘快捷键动画ctrl&shift+数字.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]批量操作&任务属性带选中状态.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     对任务批量添加属性,如今日添加的任务都是属于项目“z39timelog”和负责任“姜禄建”.
任务属性：选中状态时,添加的任务都带这属性.

#### [ ]选择任务,右边就显示任务详情.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     标题、描述、创建、相关人、分类、结束、子任务;

#### [ ]通过查看用户的任务和描述,主管的猜测判断进行优化.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     如为考虑到的wc任务,在单人或多人经常出现.就需要考虑改进.

#### [ ]素材拖动区,就像FLASH一样.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     任何和常见杂事一样,可以直接拖动添加. 如喝水,可以拖动到时间轴,点击后直接添加到当前时间;

#### [ ]鸡蛋挑骨头-否定日事清来寻求优化
![need 40m](https://img.shields.io/badge/预计-40m-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]实际教学展示视频录制
![created 17-06-26 11:00](https://img.shields.io/badge/创建-17/06/26 11:00-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]教练教学自我视频录制
![created 17-06-26 10:59](https://img.shields.io/badge/创建-17/06/26 10:59-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]教练有效教学价值观点
![created 17-06-26 10:58](https://img.shields.io/badge/创建-17/06/26 10:58-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]相同问题
![created 17-06-26 10:47](https://img.shields.io/badge/创建-17/06/26 10:47-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     关注和赞助问题,解决后可以获得通知;

#### [ ]朋友问题克隆
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     帮助朋友解决问题,已解决后,问题同时关闭.

#### [ ]共享生活
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     闲置物品,可用时间和服务代替; 租借,超时自动购买;

#### [ ]付费问题列表,其他人电话解答.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     他人可以听历史解决方法.

#### [ ]相关应用app,如早起App
![created 17-06-26 10:25](https://img.shields.io/badge/创建-17/06/26 10:25-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]可接受和希望的合作方向,还有暂不考虑列表
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]项目日报,周报
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]如果有效积累,减少泄露
![created 17-06-26 09:32](https://img.shields.io/badge/创建-17/06/26 09:32-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]需要分类罗列出自己的想法,别人才能有效提出意见
![created 17-06-26 09:32](https://img.shields.io/badge/创建-17/06/26 09:32-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]用手机布局的方式思考pc端
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     同样的功能和布局方式,可以减少开发的工作量.

#### [ ]所到之处就分享演讲时间管理,并获取反馈
![created 17-06-26 09:21](https://img.shields.io/badge/创建-17/06/26 09:21-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     提前做好准备.

#### [ ]他人用过什么任务、时间管理工具,感觉怎样？
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]和他人沟通请教时间管理,如何沟通
![created 17-06-26 08:54](https://img.shields.io/badge/创建-17/06/26 08:54-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     常见的问题,

#### [ ]容易忘记就用软件记录,提前一天做准备
![created 17-06-26 08:54](https://img.shields.io/badge/创建-17/06/26 08:54-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]录制自己的视频语音,在电梯里面听
![created 17-06-26 08:52](https://img.shields.io/badge/创建-17/06/26 08:52-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]选择任务时,出现同类历史情况和总结
![created 17-06-26 08:51](https://img.shields.io/badge/创建-17/06/26 08:51-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]新版界面构思
![created 17-06-26 08:50](https://img.shields.io/badge/创建-17/06/26 08:50-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]他人的日报周报格式
![created 17-06-26 08:50](https://img.shields.io/badge/创建-17/06/26 08:50-orange.svg) ![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 31m](https://img.shields.io/badge/耗时-31m-orange.svg) ![started 17-06-26 09:42](https://img.shields.io/badge/开始-17/06/26 09:42-orange.svg) ![done 17-06-26 10:13](https://img.shields.io/badge/完成-17/06/26 10:13-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     张萌推荐赢我的笔记总结读书旅行演讲灵感随手记事本子 中找到日志
百度无结果,并记录了其他想法.

#### [ ]起床闹铃DIY-学习音频
![created 17-06-26 08:49](https://img.shields.io/badge/创建-17/06/26 08:49-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]从游戏角度看时间管理
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]建立新手区
![created 17-06-26 08:48](https://img.shields.io/badge/创建-17/06/26 08:48-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     初级用户看见其他人已经远超自己,失去激情和动力.-社区常见问题
如初学者和高手对战被完虐,每个人都想第一和爱比较.一旦有了比较心,就容易挫败;又无法短时间超越高手,从而导致放弃; 起初和自己比较,看见自己的一次次进步; 需要通过建立新区,新手保护. 

#### [ ]经常未完成任务
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]今日打卡排名&总共排名
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]任务子任务模块化如何使用
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]周计划、年计划的任务单元
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]按照垂直时间轴方式显示.
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![started 17-06-26 13:17](https://img.shields.io/badge/开始-17/06/26 13:17-orange.svg) ![done 17-06-26 14:19](https://img.shields.io/badge/完成-17/06/26 14:19-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
##### [x]焦点迷失在任务的海洋中.一样望去全是任务,找不到正在做哪个.也就忘记当下任务,做其他的了.
![done 17-06-26 14:20](https://img.shields.io/badge/完成-17/06/26 14:20-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![summary ](https://img.shields.io/badge/-总结-1ba4ff.svg) 
#### [x]搜索内容重复:任务延迟到下一天会重复出现到多个文本中.
![done 17-06-24 22:04](https://img.shields.io/badge/完成-17/06/24 22:04-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
#### [ ]开始模式-系统全屏.便于专注当前任务
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]罗列出常见干扰行为
![need 10m](https://img.shields.io/badge/预计-10m-orange.svg) ![used 06m](https://img.shields.io/badge/耗时-06m-orange.svg) ![started 17-06-24 21:30](https://img.shields.io/badge/开始-17/06/24 21:30-orange.svg) ![done 17-06-24 21:36](https://img.shields.io/badge/完成-17/06/24 21:36-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     开机 洗杯 泡茶 喝水 wc wc10 电话(客户 广告) 短信 开会 沟通 微信 广告 电脑问题 购物 QQ 家庭信息 日计划 日总结 早餐 拉肚 吃零食 聊天  拜访 电影 游戏 动画 学习 其他突发问题

#### [x]收费模式
![done 17-06-24 21:26](https://img.shields.io/badge/完成-17/06/24 21:26-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     投资模式:区省市代理、
个人用户、公司、
投资公司 

#### [x]系统行为罗列
![done 17-06-24 21:17](https://img.shields.io/badge/完成-17/06/24 21:17-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     记录、导入、创建、查询、修改、删除、    
分工(通知)、分类(标签)、(日周月年)排期、评估、分析、总结、
(单任务)开始、暂停、取消、完成、添加描述、
关注、分享、求助、领取、赞、支付、评价、沟通、  

#### [ ]人物标签技术分类
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     在继续特定技术的时候,可以找到相关的人提供技术支持.

#### [ ]模块市场
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]模块开发,界面拖动DIY
![created 17-06-24 20:52](https://img.shields.io/badge/创建-17/06/24 20:52-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]键盘操作
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]快捷键显示与测试区域
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]时间是每人拥有的高频发生,如果收费模式适当可以成为全球第一大公司
![done 17-06-24 20:39](https://img.shields.io/badge/完成-17/06/24 20:39-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![vision ](https://img.shields.io/badge/-vision-72c7ff.svg) 
#### [ ]任务序号
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]ui与纯文字版的任务编译页面相互切换
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]了解客户需求,如何提问
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]兼职人员,电话了解未完成任务用户需求
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     需要提供相关问题

#### [ ]网站、app、h5、小程序
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]思考方式:功能性任务、公司性任务
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]网站系统把各类待完任务分类.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]一开始确认意愿,支付10元可获得他人关心和建议
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]从现象中找解决方法
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]关注他人未完成任务
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]任务
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     21天,和徽章对应

#### [ ]信息收集、点赞排序、整理、官方收纳、制作视频、
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]好处收集
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]一万小时定律
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]番茄始终&25m时长修改
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     统计用户暂停时间,自动推荐;

#### [ ]私有数据库
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]整理我现有资料成视频,并寻求他人意见
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]新增资料如何处理
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]现有资料整理
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     方式、方法、问题、视频、书籍、文章、音频、讲师、培训、老师、名人

#### [ ]域名选择
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     D:\Program Files (x86)\新兴万能域名批量查询软件
[数字][数字][time] [think,i,do,in][time] [字母][字母][time]

#### [ ]根据重复次数筛选行为习惯
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     属于筛选条件.

#### [ ]开发者token,可以用于开发者获取个人或他人信息
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![admire ](https://img.shields.io/badge/-admire-72c7ff.svg) 
     微信就是这么做,可以参考微信和支付宝之类的大平台.
个人认证,公司认证;

#### [x]不知道如何思考新的知识体系,如时间管理创业
![done 17-06-24 15:27](https://img.shields.io/badge/完成-17/06/24 15:27-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![unknown wait](https://img.shields.io/badge/unknown-wait-72c7ff.svg) 
     做好现在已罗列工作,其他长远工作是以后需要完成的; 可以从其他行业和大公司已有框架思考.基本工作被人承认后,可以请教朋友或有人主动提供建议;

#### [ ]展示所有工作,他人可以查看或参与完成
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]分享
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]打印
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]数据导入导出,
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]企业用户:使用企业短信或者公众号发送通知信息;
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]查看等级:付费查看,作者同意查看,按次查看,通讯录高付费查看.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]等待任务,分工作和生活.自己的计划他人可以查看,如通讯录;
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]如果有什么东西是我的使命,那就是时间管理
![done 17-06-24 15:09](https://img.shields.io/badge/完成-17/06/24 15:09-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![mission life](https://img.shields.io/badge/mission-life-72c7ff.svg) 
     时间日志就那么突然的从一个念头就开始了8年,工作的时候不知道自己昨天做了什么.

#### [ ]按分类显示个人习惯,如所有早起的备注描述.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     用于自我了解和他人学习,自己查看可以看出区别.

#### [ ]工作中勿打扰-通知
![created 17-06-20 15:01](https://img.shields.io/badge/创建-17/06/20 15:01-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![tool think](https://img.shields.io/badge/tool-think-72c7ff.svg) 
     源于护士小马褂；
桌面放一个小提示版和二维码,可以通过二维码进入系统并保存,在页面中添加备忘录;
提示板告诉工作专注中,勿打扰; 软件微信、QQ可以离线自动回复. 高级外挂软件自动回复. 电话等待铃声;

#### [ ]私人秘书-旅行规划、会议安排
![created 17-06-24 14:54](https://img.shields.io/badge/创建-17/06/24 14:54-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]语音记录-需要标准普通话.
![created 17-06-24 14:54](https://img.shields.io/badge/创建-17/06/24 14:54-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]共享时间
![created 17-06-24 13:41](https://img.shields.io/badge/创建-17/06/24 13:41-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]奇特的一生读后感文章查看
![started 17-06-24 13:00](https://img.shields.io/badge/开始-17/06/24 13:00-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]闭环体系形成相互流动
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]定时收费方式,如2020年之后免费,现在按使用次数收费.每次1毛;
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]sublime和npm是如何做的
![need 45m](https://img.shields.io/badge/预计-45m-orange.svg) ![used 21m](https://img.shields.io/badge/耗时-21m-orange.svg) ![started 17-07-03 20:02](https://img.shields.io/badge/开始-17/07/03 20:02-orange.svg) ![done 17-07-03 20:23](https://img.shields.io/badge/完成-17/07/03 20:23-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]一个人不够就集合大家的力量
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]按时间分组排名、进阶、
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]分解、重组、四象限、统筹习题
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]目标重组
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]目标分解
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]日周月年计划
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]四象限任务分时间段-上班工作下班生活
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]事情安排总时间
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]粉丝
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]姜禄建我的一生
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]md文件附加思维导图
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]构思项目开发各环节配合常见问题
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     可以脱离管理人员

#### [ ]他人模块开发配合
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]长期可输入的文章
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]查看时间列表,统筹安排
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]计划、评估、执行、总结
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]语音临时记录
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]需求区
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]误区、有效经验、错误经验、方法、
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]精品区
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]问答区
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]线下活动
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]徽章
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]任务-奇特任务(随机他人任务)
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]多种投资、代理方式
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]给他人参与的接口
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]平台思考
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]学习资料、打卡、定目标、改习惯、给建议、分享、教学、
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]时间管理工作分类规划
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]时间管理网站、QQ群、贴吧、微信群、论坛
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]项目推送到多职位的人
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]任务分类展示
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]经验分享图文+分类数据库设置
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]重复坚持的习惯次数
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]需要帮忙的任务
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]想完成的任务
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]录制视频
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]每天和2个人聊时间管理
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]玩不成的任务就成了需求
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]互助服务,付费帮忙划分目标,经验传授
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]成长等级
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]社区共修的人,如keep
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]sublime自动上传当日任务
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]任务模板
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]自动导入工作日志
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]系统生成日报
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]每天重复做有改变的事,缩短改变周期.
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]查看知乎GTD文章
![done 17-06-23 15:17](https://img.shields.io/badge/完成-17/06/23 15:17-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]如何节省别人的时间 https://www.zhihu.com/question/34611072
![done 17-06-23 15:05](https://img.shields.io/badge/完成-17/06/23 15:05-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![save url](https://img.shields.io/badge/save-url-72c7ff.svg) 
#### [x]时间管理记事本销量查看
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 22m](https://img.shields.io/badge/耗时-22m-orange.svg) ![started 17-06-23 13:49](https://img.shields.io/badge/开始-17/06/23 13:49-orange.svg) ![done 17-06-23 14:11](https://img.shields.io/badge/完成-17/06/23 14:11-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     1688 淘宝 京东
通过淘宝搜索“效率记事本” 最多销量的名字中找关键词,并找点击相似按钮.
https://s.taobao.com/search?spm=a230r.1.14.7.d25quM&type=similar&app=i2i&rec_type=1&uniqpid=&nid=44419595841
百度上很少购买数据.
百度指数：效率手册 150/天  日程计划本 无数据
知乎 GTD 话题关于用户 24839 个关注. 里面主要话题是 云笔记本,如onenote

#### [x]时间管理记事本查看
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 33m](https://img.shields.io/badge/耗时-33m-orange.svg) ![started 17-06-23 13:12](https://img.shields.io/badge/开始-17/06/23 13:12-orange.svg) ![done 17-06-23 13:45](https://img.shields.io/badge/完成-17/06/23 13:45-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]思考方式
![done 17-05-04 22:48](https://img.shields.io/badge/完成-17/05/04 22:48-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     单机-网络版
个人-多人-互助-组队 
个人-[时长 频率] 
    时长：如果我有用了1万次后的数据.  关键成长记录, 专业,让相同的一件事做出不同的体会和经历.就像是视频网站所有人的视频一样.我关心的是什么？ 不同的情况我是如何反应. 每天进步改进,用同一件事,不同方式体位人生.
    频率：短时间我需要操作1k次. 我希望更短时间完成. 重复的事情简化.
多人-1万人使用. 低中高等级,不同阶段的界面和功能.每个版本都能正常使用.
模板选择,适合不同用户.
加入,可以置换的方式达到免费.
对比.分析.总结.改进. 
认清自我,确定目标,修订目标. 
目的：知道自己每一天时间用在什么地方,肯定、积累、分析、改进. 
资料：名言 书籍 方法 经验 电影 视频 软件 培训
年度-冠军,期 组 队 区 市 省 国

#### [x]思维文字
![done 17-05-04 21:31](https://img.shields.io/badge/完成-17/05/04 21:31-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     主页 赚钱 工作 生活 我的
主页[资源 文章 建议 方法 视频 社区 人才 学习 任务 排行榜 咨询]
赚钱[方式 时长 金额 信誉 难度 紧迫 奖金 详细度 经验 认证]
工作[所有 未完 报告]
   所有:每日任务, 完成的放下面.
   未完:月 为单位显示内容. 提供全年.
   报告:月 为单位显示系统分析报告. 分析 行为进步及落后. 计划评估,推荐方案.
我的[赚钱 求助 任务 经验] 

#### [x]logo优化淘宝找人沟通,消耗1小时无果
![used 1h](https://img.shields.io/badge/耗时-1h-orange.svg) ![done 17-04-21 11:02](https://img.shields.io/badge/完成-17/04/21 11:02-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     错误以为自己也能1小时制作.只有基本功的我.做了2分钟就发现不熟练的我修改会浪费时间.
logo优化需求视频语音描述.  语音考虑现有软件完成. 可以导出或提供url访问地址即可.17-04-21 ![](https://img.shields.io/badge/时间-11:33-ff69b4.svg)

#### [x]logo需求美化md格式
![done 17-04-20 12:31](https://img.shields.io/badge/完成-17/04/20 12:31-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]logo需求整理成markdown,初级美术说做不了.重新找人 4th
![done 17-04-20 11:47](https://img.shields.io/badge/完成-17/04/20 11:47-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]logo需求沟通,提供产考效果 3th
![used 2m](https://img.shields.io/badge/耗时-2m-orange.svg) ![done 17-04-20 11:13](https://img.shields.io/badge/完成-17/04/20 11:13-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![first ](https://img.shields.io/badge/-first-72c7ff.svg) 
     1、分前景和背景, 前景“T”. 背景是logo
现在效果 ![](http://cdn.d7game.com/markdown/2017-04-20_9b3916c5.jpg)
前景参考图![](http://cdn.d7game.com/markdown/2017-04-20_9ee9ff5fd8e35b.png)
背景参考图中的点，需要类似效果 ![](http://cdn.d7game.com/markdown/2017-04-20_eef5f535d2ef7.jpg)
1个小时能解决的，背景透明+图层填充
前景：球形渐变+蒙版 + 浮雕  17-04-20 ![](https://img.shields.io/badge/时间-11:21-ff69b4.svg)

#### [x]聊天发现相关时间记录“乐动”查看
![done 17-04-19 19:18](https://img.shields.io/badge/完成-17/04/19 19:18-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]logo外包沟通付款2th
![done 17-04-19 13:14](https://img.shields.io/badge/完成-17/04/19 13:14-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]相关词语
![done 17-04-17 21:19](https://img.shields.io/badge/完成-17/04/17 21:19-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     时间交易 时间管理 24PI   视频录像 1997时间相关搜索

#### [x]现在怀疑app后台,逻辑不能正常运行.
![done 17-04-17 16:02](https://img.shields.io/badge/完成-17/04/17 16:02-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     情况-静止状态持续了2-3小时.这段时间去吃饭有大量运行. 连线测试正常.
拔掉线关闭app重启再次测试-初次测试运动动作时锁屏后正常进入静止状态.
再次测试-静止状态摇动手机,仍然进入运动状态.

#### [x]经测试app后台摇一摇不一样
![created 17-04-17 15:03](https://img.shields.io/badge/创建-17/04/17 15:03-orange.svg) ![done 17-04-17 21:20](https://img.shields.io/badge/完成-17/04/17 21:20-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
#### [x]用户修改标题后的颜色.
![need 15m](https://img.shields.io/badge/预计-15m-orange.svg) ![used 02m](https://img.shields.io/badge/耗时-02m-orange.svg) ![started 17-04-16 22:54](https://img.shields.io/badge/开始-17/04/16 22:54-orange.svg) ![done 17-04-16 22:56](https://img.shields.io/badge/完成-17/04/16 22:56-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]自定义标题
![done 17-04-16 23:03](https://img.shields.io/badge/完成-17/04/16 23:03-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]地址显示优化
![need 10m](https://img.shields.io/badge/预计-10m-orange.svg) ![used 07m](https://img.shields.io/badge/耗时-07m-orange.svg) ![started 17-04-16 20:06](https://img.shields.io/badge/开始-17/04/16 20:06-orange.svg) ![done 17-04-16 20:13](https://img.shields.io/badge/完成-17/04/16 20:13-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     新动作地址为无. 当速度为0时,继续采用上一个地址.

#### [x]修改进行中的标签颜色
![need 10m](https://img.shields.io/badge/预计-10m-orange.svg) ![done 17-04-16 20:04](https://img.shields.io/badge/完成-17/04/16 20:04-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]步数显示
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 10m](https://img.shields.io/badge/耗时-10m-orange.svg) ![started 17-04-16 19:47](https://img.shields.io/badge/开始-17/04/16 19:47-orange.svg) ![done 17-04-16 19:57](https://img.shields.io/badge/完成-17/04/16 19:57-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     <百步, 大于10,小于100.
~~2百步. 向上取整.~~
1.5k步.1500.

#### [x]摇一摇“新建”标签
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 28m](https://img.shields.io/badge/耗时-28m-orange.svg) ![started 17-04-16 18:48](https://img.shields.io/badge/开始-17/04/16 18:48-orange.svg) ![done 17-04-16 19:16](https://img.shields.io/badge/完成-17/04/16 19:16-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     数据标签单独处理. 

#### [x]通过加速度值简单实现计步.
![done 17-04-16 18:20](https://img.shields.io/badge/完成-17/04/16 18:20-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     几步不准,但会有一个相对数字给以参考.

#### [x]摇一摇逻辑判断优化
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 01:49](https://img.shields.io/badge/耗时-01:49-orange.svg) ![started 17-04-16 16:07](https://img.shields.io/badge/开始-17/04/16 16:07-orange.svg) ![done 17-04-16 17:56](https://img.shields.io/badge/完成-17/04/16 17:56-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     情况-走路时不停的抖动或持续触发摇一摇.
1.添加计数器.5次激活;  2.添加方向计算.通过值相减判断无效.![](https://img.shields.io/badge/时间-16:35-ff69b4.svg) 需要添加三维空间几何向量计算方向.百度10分钟无结果. 
vector-op.js 尝试自己计算,当x y z 2个方向的值改变时,判断为一次动作的变向.
![](http://cdn.d7game.com/markdown/2017-04-16_])TO2]E.png)
![](http://cdn.d7game.com/markdown/2017-04-16_3ZF]GF73G.png)     
vector-op.js 按官网示例运行,结果不一致. 17-04-16 ![](https://img.shields.io/badge/时间-17:26-ff69b4.svg)
~~连续1分钟内3次摇一摇,则取消第一次的新建动作.逻辑优化后不存在这问题.~~
关键词-~几何向量 立体几何向量 ~

#### [ ]操作区域按钮组(新增 停止 合并 删除 取消)
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]新动作的语音提示
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     锁屏后不能直接播放声音,需要其他解决方法.

#### [x]摇一摇触发新动作
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![done 17-04-16 15:54](https://img.shields.io/badge/完成-17/04/16 15:54-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
##### [x]摇一摇功能代码&测试
![done 17-04-16 14:05](https://img.shields.io/badge/完成-17/04/16 14:05-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      accelerometer_shake.html 经过测试,摇一摇的逻辑判断有问题. 现在手机幅度过大就会触发.而不是摇一摇往复摇动. 满足一般用户基本需求.
[LOG] : 6.465118388250999e-12
已经有了基本功能.灵敏度及更高级.

##### [x]摇一摇触发新动作
![done 17-04-16 15:54](https://img.shields.io/badge/完成-17/04/16 15:54-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      1.修改变量解决变量名冲突. 2.去除ui部分功能. 3.剔除多余代码![](https://img.shields.io/badge/时间-14:19-ff69b4.svg) 4.结束动作![](https://img.shields.io/badge/时间-14:39-ff69b4.svg) 5.新建动作![](https://img.shields.io/badge/时间-14:39-ff69b4.svg) 6.代码重构.![](https://img.shields.io/badge/时间-15:10-ff69b4.svg) 7.重构bug ![](https://img.shields.io/badge/时间-15:31-ff69b4.svg) 8.bug 一开始出现1秒的动作. firstIn修改导致.![](https://img.shields.io/badge/时间-15:49-ff69b4.svg) 9.摇一摇代码整合    

#### [ ]自动添加名称~~添加标签~~
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     根据时间段自动设置民称. 如果人为完成上一个动作.则自动记录为下个动作.
“家”标签:家范围内的常用地址.
“公司”标签:家范围内的常用地址.
“餐馆”:陌生地方,他人标注吃饭的地方.
早饭 午饭 晚饭
“移动”: 步行 骑车 汽车

#### [ ]显示每个人的位置和方向
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     澳洲游使用. 头像微信头像圆形.放大时,显示一个文字的缩写.

#### [x]上班这个过程包含很多小动作如何处理?
![done 17-04-14 15:00](https://img.shields.io/badge/完成-17/04/14 15:00-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![question ](https://img.shields.io/badge/-question-72c7ff.svg) 
     离开公司-到公司的时间为上班时间.
疑问：通宵如何记录处理? 记录睡觉前的最后一次离开时间。 

#### [x]公司附近的经纬度距离计算
![created 17-04-14 14:56](https://img.shields.io/badge/创建-17/04/14 14:56-orange.svg) ![used 01:48](https://img.shields.io/badge/耗时-01:48-orange.svg) ![started 17-04-16 20:13](https://img.shields.io/badge/开始-17/04/16 20:13-orange.svg) ![done 17-04-16 22:01](https://img.shields.io/badge/完成-17/04/16 22:01-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![first ](https://img.shields.io/badge/-first-72c7ff.svg) 
     记录数据之后,如何显示处理.
[js经纬度计算两点距离]
如果有目的地就显示与目标距离.
如果没目的地就显示离开距离.
需要给动作设置一个对应的目的地.如出发、回家、看电影
如果为距离约定于0(小于100),触发上班事件. 如果记录大于100则触发下班.以最后离开时间为准.

#### [ ]app自动采集的参数设置界面
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     几分钟起算. 添加删除标题. 个人使用不需要.做产品时需要给诶他人使用.
设置常用场所的位置。如家和公司,电影院.
参数 actionDownTime 动作结束静止时间.

#### [x]打坐图标&隔日标签
![done 17-04-13 20:42](https://img.shields.io/badge/完成-17/04/13 20:42-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]bug gps定位经常会出现人5秒钟出现几个不同地址.
![done 17-04-13 16:34](https://img.shields.io/badge/完成-17/04/13 16:34-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
     解决方法：如果speed速度没变过.责地址不变. 潜在问题：初次定位位置信息不准确.
第一次 或者 速度>0时可以设置地址

#### [x]bug 触发动作时,没有打印静止动作.
![need 25m](https://img.shields.io/badge/预计-25m-orange.svg) ![used 01:14](https://img.shields.io/badge/耗时-01:14-orange.svg) ![started 17-04-13 16:42](https://img.shields.io/badge/开始-17/04/13 16:42-orange.svg) ![done 17-04-13 17:56](https://img.shields.io/badge/完成-17/04/13 17:56-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
     以后都是显示pause.png图标和静止状态的动作;
jQuery.extend( actionDefault ,item  )
var d1 = {name:"jiang"} , d2 ={name:"d7game.com"} ;
jQuery.extend( d1 , d2, {name:"jiang123"} ) ;  d1的数据被覆盖. d1{name: "jiang123"} d2{name: "d7game.com"}
![](http://cdn.d7game.com/markdown/2017-04-13_R{U}1BG.png)
moment().format("x") 和 .format("X") 的大小写参数没注意.
addActionStatic 中的参数用错,错误的使用了 lastChangeTime , 应该是lastActionTime

#### [x]speed 速度标签显示
![done 17-04-13 16:59](https://img.shields.io/badge/完成-17/04/13 16:59-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     speed == 0 显示原地, >0 显示速度. 平均-最大值.如1~5km/h
bug 原地标签显示了2个.
line:171 , 超过24小时,默认添加了一个标签.
item = jQuery.extend( {}, actionDefault ,item  ) ; ~~手机中运行的结果和浏览器不一样. actionDefault的值变了. 浏览器没有. ~~
var d1 = {name:"jiang"} , d2 ={name:"d7game.com"} ;
jQuery.extend( d1 , d2, {name:"jiang123"} ) ;
console.log( JSON.stringify( d1 ) );   // {"name":"jiang123"}
console.log( JSON.stringify( d2 ) );   //{"name":"d7game.com"}  经过测试,并没有变化.
是因为赋值的时候,返回对应应用问题.
jQuery.extend( {}, item ,item  ) ; (17-04-13 ![](https://img.shields.io/badge/时间-18:27-ff69b4.svg))
logAction 和 showAction 的调用混乱 导致. (17-04-13 ![](https://img.shields.io/badge/时间-19:23-ff69b4.svg))

#### [x]获取到的地址显示到动作列表中;
![used 02m](https://img.shields.io/badge/耗时-02m-orange.svg) ![started 17-04-13 16:07](https://img.shields.io/badge/开始-17/04/13 16:07-orange.svg) ![done 17-04-13 16:09](https://img.shields.io/badge/完成-17/04/13 16:09-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     jQuery.extend( item , actionDefault ) ; 因为顺序错误导致原有的数据被覆盖.

#### [x]bug 多显示了2个0秒的动作
![done 17-04-13 16:06](https://img.shields.io/badge/完成-17/04/13 16:06-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     查看代码提交记录, 发现doWithNewDay 可能导致错误.  注释后运行正常,但感觉问题不在这里.
经过才在测试问题确实不在.
actionItem = null ;
lastChangeTime 为 0 ,导致 actionItem.end 
应该是第一次打开app出发动作开始导致的bug.

#### [x]白天黑夜图标下载抠图
![done 17-04-13 14:28](https://img.shields.io/badge/完成-17/04/13 14:28-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]甘特图构思
![created 17-04-13 11:03](https://img.shields.io/badge/创建-17/04/13 11:03-orange.svg) ![cancelled 17-04-13 11:14](https://img.shields.io/badge/取消-17/04/13 11:14-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     甘特图查看.展示生活时间不适合.

#### [x]所有动作栏罗列
![used 5m](https://img.shields.io/badge/耗时-5m-orange.svg) ![done 17-04-13 14:38](https://img.shields.io/badge/完成-17/04/13 14:38-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     全部 静止 动作 推荐 整合 分析

#### [x]根据时间段自动显示图标
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 33m](https://img.shields.io/badge/耗时-33m-orange.svg) ![started 17-04-13 14:48](https://img.shields.io/badge/开始-17/04/13 14:48-orange.svg) ![done 17-04-13 15:21](https://img.shields.io/badge/完成-17/04/13 15:21-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     凌晨:3:00--6:00  light2.png
早晨:6:00---8:00  sun.png
上午:8:00--![](https://img.shields.io/badge/时间-11:00-ff69b4.svg)  sun.png
中午:![](https://img.shields.io/badge/时间-11:00-ff69b4.svg)--![](https://img.shields.io/badge/时间-13:00-ff69b4.svg) tea.png
下午:![](https://img.shields.io/badge/时间-13:00-ff69b4.svg)--![](https://img.shields.io/badge/时间-17:00-ff69b4.svg)  sun.png
傍晚:![](https://img.shields.io/badge/时间-17:00-ff69b4.svg)--![](https://img.shields.io/badge/时间-19:00-ff69b4.svg) light.png
晚上:![](https://img.shields.io/badge/时间-19:00-ff69b4.svg)--![](https://img.shields.io/badge/时间-23:00-ff69b4.svg)  light.png
深夜:![](https://img.shields.io/badge/时间-23:00-ff69b4.svg)--3:00 light2.png

#### [x]静态动作单独一栏
![created 17-04-13 11:03](https://img.shields.io/badge/创建-17/04/13 11:03-orange.svg) ![done 17-04-13 19:53](https://img.shields.io/badge/完成-17/04/13 19:53-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]零碎的时间如何处理
![created 17-04-12 14:13](https://img.shields.io/badge/创建-17/04/12 14:13-orange.svg) ![done 17-04-14 14:44](https://img.shields.io/badge/完成-17/04/14 14:44-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     每个动作都是零碎时间片段.

#### [x]经纬度的小数点精确度
![need 10m](https://img.shields.io/badge/预计-10m-orange.svg) ![done 17-04-13 00:30](https://img.shields.io/badge/完成-17/04/13 00:30-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     经度0.00001度约为1.1米
https://zhidao.baidu.com/question/551550340.html

#### [x]只记录关键(变化)数据
![need 15m](https://img.shields.io/badge/预计-15m-orange.svg) ![used 25m](https://img.shields.io/badge/耗时-25m-orange.svg) ![started 17-04-13 00:02](https://img.shields.io/badge/开始-17/04/13 00:02-orange.svg) ![done 17-04-13 00:27](https://img.shields.io/badge/完成-17/04/13 00:27-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     难点：经纬度数据只有在室外移动的时候才能变化.室内开发测试不出.开发的反馈非常高。测试非常耗时.
还需要考虑超过一天.数据的处理
jQuery.extend( {name:"jiang"} , {name:"d7game.com",key:123} );  //{name: "d7game.com", key: 123}

#### [x]手机数据的保存js
![need 15m](https://img.shields.io/badge/预计-15m-orange.svg) ![used 20m](https://img.shields.io/badge/耗时-20m-orange.svg) ![started 17-04-12 23:42](https://img.shields.io/badge/开始-17/04/12 23:42-orange.svg) ![done 17-04-13 00:02](https://img.shields.io/badge/完成-17/04/13 00:02-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     moveDataArr actionArr
moveDataItem = time px py pz addr speed ox oy oz direction moveDiff

#### [ ]移动轨迹和不同时段路线颜色.
![created 17-04-12 14:52](https://img.shields.io/badge/创建-17/04/12 14:52-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     查看模式显示地图移动数据~~编辑模式不需要显示地图.~~
路线透明度50%.

#### [x]操作界面构思
![done 17-04-12 14:48](https://img.shields.io/badge/完成-17/04/12 14:48-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]所有动作罗列
![done 17-04-12 14:33](https://img.shields.io/badge/完成-17/04/12 14:33-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     开始(新建)、~~停止(结束)~~ 开始和停止有重叠,只需要一个即可.
删除、合并、剪切  快速操作,最后点击完成或者取消.
重命名、标签、添加标签、   属性内容修改.

#### [ ]手动添加标签
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]系统推荐标签
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]自动添加标签
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]动作重命名
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![started 17-04-13 22:41](https://img.shields.io/badge/开始-17/04/13 22:41-orange.svg) ![done 17-04-13 23:22](https://img.shields.io/badge/完成-17/04/13 23:22-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
##### [x]编辑好弹框内容. 名字&值的键值对.
![done 17-04-13 23:06](https://img.shields.io/badge/完成-17/04/13 23:06-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
##### [x]需要把修改后的数据显示到界面
![done 17-04-13 23:22](https://img.shields.io/badge/完成-17/04/13 23:22-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      js对象和当前界面. li 中保存tiemkey 用于修改js数据; 界面定位title元素. 
手机无法正常运行,应为li是动态生成.不具有绑定事件. 需要需该为 
jQuery("#content").on('tap',".output2 li a", function(event) {

#### [ ]系统推荐分类
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]合并操作
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     多选,把多个动作合并成一个.名字采用(非移动)第一个动作名.
必须合并连续的动作. 点击后toast提示. 如果操作异常alert弹框提示.

##### [ ]界面布局
![done 17-04-16 22:31](https://img.shields.io/badge/完成-17/04/16 22:31-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      tlog_checkbox

##### [ ]阻止选择框事件冒泡.
![done 17-04-16 22:44](https://img.shields.io/badge/完成-17/04/16 22:44-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      jQuery(".content").on("tap",".tlog_checkbox input",function(e){console.log("d7game");event.stopPropagation();})  ;//因为上层的事件也是这样的.失败.
jQuery(".tlog_checkbox input").on("tap",function(e){console.log("d7game");event.stopPropagation();})    //阻止成功.但元素是动态生成. 所以不可取.
继续尝试上面方法,监听点击的事件对象.
jQuery("#content").on('tap',".output2 li a", function(event) {
     if( event.target.type == "checkbox" ){ console.log("d7game") ; return; }
写在同一个事件监听中.

#### [x]item.start.format is not a function
![created 17-04-12 21:21](https://img.shields.io/badge/创建-17/04/12 21:21-orange.svg) ![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![done 17-04-12 21:50](https://img.shields.io/badge/完成-17/04/12 21:50-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
     [LOG] : action done   ||  [LOG] : addActionStatic
[LOG] : 动作结束![](https://img.shields.io/badge/时间-21:20-ff69b4.svg):15
[ERROR] : TypeError:
应为是手机,所以无法断点调试.
item.start 因为新添加的代码直接赋值 0 . 应该是moment() 的值.

#### [x]刷新当前动作的时间
![need 10m](https://img.shields.io/badge/预计-10m-orange.svg) ![used 06m](https://img.shields.io/badge/耗时-06m-orange.svg) ![started 17-04-12 19:42](https://img.shields.io/badge/开始-17/04/12 19:42-orange.svg) ![done 17-04-12 19:48](https://img.shields.io/badge/完成-17/04/12 19:48-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]数据标签的显示
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 11m](https://img.shields.io/badge/耗时-11m-orange.svg) ![started 17-04-12 19:31](https://img.shields.io/badge/开始-17/04/12 19:31-orange.svg) ![done 17-04-12 19:42](https://img.shields.io/badge/完成-17/04/12 19:42-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     <span class="tlog_flag">5km/h</span>
<span class="tlog_flag">{0}</span>

#### [x]图标处理成png并添加圆角
![done 17-04-12 21:04](https://img.shields.io/badge/完成-17/04/12 21:04-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]图标临时素材查找下载
![created 17-04-12 20:02](https://img.shields.io/badge/创建-17/04/12 20:02-orange.svg) ![done 17-04-12 20:24](https://img.shields.io/badge/完成-17/04/12 20:24-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     睡觉 起床 洗漱 出发 上班 午饭 下班 wc 

#### [x]首个li的大小及顶部显示不正常.
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 07m](https://img.shields.io/badge/耗时-07m-orange.svg) ![started 17-04-12 19:21](https://img.shields.io/badge/开始-17/04/12 19:21-orange.svg) ![done 17-04-12 19:28](https://img.shields.io/badge/完成-17/04/12 19:28-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
     showAction( {start: moment()} ) 控制台输入命令,可以直接重现.
因为以前的显示内容格式没有删除.

#### [x]静止时首个li正在进行的动作状态变化.
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 04m](https://img.shields.io/badge/耗时-04m-orange.svg) ![started 17-04-12 19:16](https://img.shields.io/badge/开始-17/04/12 19:16-orange.svg) ![done 17-04-12 19:20](https://img.shields.io/badge/完成-17/04/12 19:20-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) ![bug ](https://img.shields.io/badge/-bug-red.svg) 
     最简单的显示文字“静止状态”

#### [x]当前进行中的动作置顶
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![started 17-04-12 17:16](https://img.shields.io/badge/开始-17/04/12 17:16-orange.svg) ![done 17-04-12 18:56](https://img.shields.io/badge/完成-17/04/12 18:56-orange.svg) ![{3}](https://img.shields.io/badge/暂停-17/04/12 17:51-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     bug 不按正常的运行,想不到为什么出现这结果.
因为js变量重名,无报错.导致打印出无效信息.且运行不正常.

#### [x]list布局构思
![done 17-04-12 14:10](https://img.shields.io/badge/完成-17/04/12 14:10-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     效果图 ![](http://cdn.d7game.com/markdown/2017-04-12_showlist.jpg)
<li class="mui-table-view-cell mui-media">
        <img class="mui-media-object mui-pull-left" src="img/logo2.png">
        <div class="mui-media-body">
            <span>5km/h</span><span>吃饭</span>
            <p class="mui-ellipsis">能和心爱的人一起</p>
        </div>
</li>
.tlog_flag{
  padding: 0 3px;
  border-radius: 3px;
  margin-left: 5px;
  color: #fff;
  border: 1px solid #0894ec;
  background-color: #0894ec;
}

#### [x]list展示方式,图文
![need 1h](https://img.shields.io/badge/预计-1h-orange.svg) ![started 17-04-12 15:14](https://img.shields.io/badge/开始-17/04/12 15:14-orange.svg) ![done 17-04-12 17:13](https://img.shields.io/badge/完成-17/04/12 17:13-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     动作图标[移动 静止 吃饭 睡觉 白天 晚上 wc 其他] (17-04-12 ![](https://img.shields.io/badge/时间-11:28-ff69b4.svg))
图片 时长 速度 地点 时间 标签 名称

##### [x]列表布局
![done 17-04-12 16:00](https://img.shields.io/badge/完成-17/04/12 16:00-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
##### [x]数据整合显示
![done 17-04-12 17:13](https://img.shields.io/badge/完成-17/04/12 17:13-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      刚才查看vue资料,最快的方式直接字符串拼接实现.
<li class="mui-table-view-cell mui-media">    
    <li class="mui-table-view-cell mui-media">
    <a href="#userAction" data-number="{0}" data-vote="{6}">
        <img class="mui-media-object mui-pull-left" src="{1}">
        <div class="mui-media-body">
            <span class="tlog_title">{2}</span>{7}
            <p class="mui-ellipsis">{3}-{4} {5}</p>
        </div>
    </a>
</li>
<li class="mui-table-view-cell mui-media">
    <a href="#userAction" data-number="1" data-vote="32m">
        <img class="mui-media-object mui-pull-left" src="img/logo2.png">
        <div class="mui-media-body">
            <span class="tlog_title">动作1</span><span class="tlog_flag">5km/h</span><span class="tlog_flag">吃饭</span>
            <p class="mui-ellipsis">![](https://img.shields.io/badge/时间-11:33-ff69b4.svg)-![](https://img.shields.io/badge/时间-12:33-ff69b4.svg) [公司]互联网金融大厦</p>
        </div>
    </a>
</li>

#### [ ]动作激活的灵敏度
![cancelled 17-04-13 14:35](https://img.shields.io/badge/取消-17/04/13 14:35-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     把moveCount的状态显示出来即可,记录到草稿.~~当moveDiff大于固定值,激活.如果没有持续运动,动作取消~~ 

#### [x]app启动即动作开始
![done 17-04-11 22:58](https://img.shields.io/badge/完成-17/04/11 22:58-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]bug 24点交替
![done 17-04-11 22:34](https://img.shields.io/badge/完成-17/04/11 22:34-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     还待测试.

#### [x]时间转化为24小时制
![need 10m](https://img.shields.io/badge/预计-10m-orange.svg) ![used 03m](https://img.shields.io/badge/耗时-03m-orange.svg) ![started 17-04-11 22:18](https://img.shields.io/badge/开始-17/04/11 22:18-orange.svg) ![done 17-04-11 22:21](https://img.shields.io/badge/完成-17/04/11 22:21-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     new moment().format('h:mm:ss') 改成大小 new moment().format('HH:mm:ss')

#### [ ]短信提醒用户app开机
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     根据用户平均起床时间,自动提醒;

#### [ ]过滤时间小于5分钟的动作
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]10秒内清空抖动数据
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 03m](https://img.shields.io/badge/耗时-03m-orange.svg) ![started 17-04-11 20:27](https://img.shields.io/badge/开始-17/04/11 20:27-orange.svg) ![done 17-04-11 20:30](https://img.shields.io/badge/完成-17/04/11 20:30-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]bug 启动app后第一个1秒的静止动作
![need 10m](https://img.shields.io/badge/预计-10m-orange.svg) ![used 05m](https://img.shields.io/badge/耗时-05m-orange.svg) ![started 17-04-11 20:51](https://img.shields.io/badge/开始-17/04/11 20:51-orange.svg) ![done 17-04-11 20:56](https://img.shields.io/badge/完成-17/04/11 20:56-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]手机静止动作显示
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![used 16m](https://img.shields.io/badge/耗时-16m-orange.svg) ![started 17-04-11 20:34](https://img.shields.io/badge/开始-17/04/11 20:34-orange.svg) ![done 17-04-11 20:50](https://img.shields.io/badge/完成-17/04/11 20:50-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]24点结束今日动作
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![done 17-04-11 22:15](https://img.shields.io/badge/完成-17/04/11 22:15-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     console.log( moment().format('X') ); 
console.log( Math.floor( new Date().getTime()/1000 ) );
new moment(1491868800000).format('YYYY-MM-DD HH:mm:ss');   
new moment("2017-04-11").format('X') ;  "1491840000"

##### [x]静止-直接结束.
![done 17-04-11 22:15](https://img.shields.io/badge/完成-17/04/11 22:15-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
##### [x]移动-正在用手机. 结束今日动作,动作添加字段表明正在进行.
![done 17-04-11 22:15](https://img.shields.io/badge/完成-17/04/11 22:15-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]记录陀螺仪抖动数值变化大小.
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![done 17-04-11 21:11](https://img.shields.io/badge/完成-17/04/11 21:11-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     变化大小需要单独记录到时刻数据中.后期还需要对数据做更多分析 moveDiff

#### [x]手机抖动容错率
![done 17-04-11 20:05](https://img.shields.io/badge/完成-17/04/11 20:05-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     手机抖动次数 moveCount 默认为5.

#### [x]敏感度参数
![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 13m](https://img.shields.io/badge/耗时-13m-orange.svg) ![started 17-04-11 20:14](https://img.shields.io/badge/开始-17/04/11 20:14-orange.svg) ![done 17-04-11 20:27](https://img.shields.io/badge/完成-17/04/11 20:27-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     手机抖动范围值,绝对值小于该值.不算移动.

#### [ ]分析后数据
![need 45m](https://img.shields.io/badge/预计-45m-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     数据特征记录&分类. 时间段、时长、速度、地点、距离

#### [x]关键点数据
![need 45m](https://img.shields.io/badge/预计-45m-orange.svg) ![used 01:06](https://img.shields.io/badge/耗时-01:06-orange.svg) ![started 17-04-11 18:39](https://img.shields.io/badge/开始-17/04/11 18:39-orange.svg) ![done 17-04-11 19:45](https://img.shields.io/badge/完成-17/04/11 19:45-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     数据的开始、结束。数据存储为动作. 
动作开始：手机抖动(移动)持续5次.
动作结束：手机静止2分钟.
动作结束实际时间为静止那一刻.
action startTime endTime . 动作只存储开始时间和结束时间.
time position[经 纬 addr speed] orientation[x y z direction]
{"coordsType":"wgs84","address":{"district":"西湖区","city":"杭州市","country":"中国","postalCode":null,"street":"华星路96","poiName":null,"cityCode":null,"province":"浙江省"},"addresses":"互联网金融大厦","coords":{"latitude":30.28233073809898,"longitude":120.1163613679774,"accuracy":65,"altitude":23.6620407104492,"heading":null,"speed":null,"altitudeAccuracy":10},"timestamp":1491906900240.864}

#### [x]bug position数据为打印出来.进入了 watchPosition out
![created 17-04-11 16:54](https://img.shields.io/badge/创建-17/04/11 16:54-orange.svg) ![done 17-04-11 18:25](https://img.shields.io/badge/完成-17/04/11 18:25-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     应该每秒打印 位置和方向数据.
修改频率管理方式; 每500毫秒获取一次数据, 如果没有到达频率则跳出逻辑.
调试时app锁屏后获取位置信息失败,陀螺仪正常. 初步判断是真机调试有关.

#### [x]变化数据
![need 45m](https://img.shields.io/badge/预计-45m-orange.svg) ![done 17-04-11 16:50](https://img.shields.io/badge/完成-17/04/11 16:50-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     持续2分钟无变化时1分钟监听一次.变化时1秒监听一次;
不同动态修改参数,需要停止监听并重新监听. plus.geolocation.watchPosition( , , option)
提取出代码到function中.
bug 方向每秒打印.位置很8秒打印.
值相同但对象不相等 if( lastOrientation == o ){
h5+中的对象和js对象不同.

#### [x]界面显示数据
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![done 17-04-11 14:33](https://img.shields.io/badge/完成-17/04/11 14:33-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     2个分页显示不同敏感度数据.

#### [ ]数据记录到内存(js对象),定时发送服务器
![need 20m](https://img.shields.io/badge/预计-20m-orange.svg) ![started 17-04-11 14:19](https://img.shields.io/badge/开始-17/04/11 14:19-orange.svg) ![cancelled 17-04-11 14:21](https://img.shields.io/badge/取消-17/04/11 14:21-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     定时发送的作用是什么？为什么不直接发. 如果使用的人多,可以减少并发量. 如果数据内容一样,直接发送工作量更小.

#### [x]创建git项目,转移项目目录后提交代码
![done 17-04-11 14:19](https://img.shields.io/badge/完成-17/04/11 14:19-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]后台定时采集数据
![created 17-04-11 13:35](https://img.shields.io/badge/创建-17/04/11 13:35-orange.svg) ![need 30m](https://img.shields.io/badge/预计-30m-orange.svg) ![used 31m](https://img.shields.io/badge/耗时-31m-orange.svg) ![started 17-04-11 13:41](https://img.shields.io/badge/开始-17/04/11 13:41-orange.svg) ![done 17-04-11 14:12](https://img.shields.io/badge/完成-17/04/11 14:12-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     锁屏后,app在后台定时发送数据到服务器, 5秒一次; 
用列表打印显示数据. jQuery(".output").html('<li class="mui-table-view-cell">Item 2</li>');
插入到开始jQuery(".output").prepend('<li class="mui-table-view-cell">Item 3</li>');

#### [x]程序构思
![done 17-04-11 13:24](https://img.shields.io/badge/完成-17/04/11 13:24-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     手机静止时间参数.
数据导出
采集数据上传服务器接口
运动激活参数：经纬度|陀螺仪
后台定时发送数据到服务器, 5秒一次;
变化的数据. 无变化时1分钟监听一次.变化时1秒监听一次;
关键点数据：数据的开始、结束
分析后数据：数据特征

#### [x]手机设备测试-陀螺仪、加速度
![done 17-04-11 13:00](https://img.shields.io/badge/完成-17/04/11 13:00-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     orientation geolocation [accelerometer]
可以通过手机角度和变化判断行为. 如平放静止,睡觉或工作.
如倾斜静止是手机看电影. 如垂直运动是在衣裤包中,上班途中.

#### [ ]bug_录音mp3没录音但一直都在新增(重复)的文件
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     /z43phone/mp3/2017-04-09 ![](https://img.shields.io/badge/时间-14:57-ff69b4.svg):54.mp3

#### [x]动作采集构思
![done 17-04-10 12:59](https://img.shields.io/badge/完成-17/04/10 12:59-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     动作开始：手机移动或长时间抖动;
动作结束：~~手机停止移动2分钟.~~下一个动作开始;
位置 速度 抖动 步数 [时刻 时长]

#### [x]时间日志app行为采集沟通&视频查看
![done 17-04-08 17:03](https://img.shields.io/badge/完成-17/04/08 17:03-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]时间日志md网页设计
![created 17-04-02 22:00](https://img.shields.io/badge/创建-17/04/02 22:00-orange.svg) ![cancelled 17-04-02 23:01](https://img.shields.io/badge/取消-17/04/02 23:01-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     个人项目无盈利.减少工作量,将已有工作进行下去。  

#### [x]时间日志md展现内容形式
![done 17-04-02 23:00](https://img.shields.io/badge/完成-17/04/02 23:00-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     结果报告 分析 甘特图 时间日志 源文件
结果：进步 退步 待提升 生活规律
分析：任务总数，重要紧急4种任务,突发,生活,吃饭,wc, 预计,延迟, 电影,
  平均，最早，最晚，工作时长

#### [x]任务界面 rishiqing/index.html 整理
![done 17-04-02 13:36](https://img.shields.io/badge/完成-17/04/02 13:36-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [x]位置文件接收查看&需求沟通
![done 17-04-02 20:55](https://img.shields.io/badge/完成-17/04/02 20:55-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     电话沟通20分钟. 文件查看验收15m.

#### [x]位置,陀螺仪数据采集外包沟通
![done 17-04-02 17:56](https://img.shields.io/badge/完成-17/04/02 17:56-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
#### [ ]后台运行app获取位置测试
![cancelled 17-04-02 17:32](https://img.shields.io/badge/取消-17/04/02 17:32-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     plus.geolocation.watchPosition
iOS切换到后台支持获取位置信息功能
控制台打印监听数据变化。控制台只打印不到8次就不能在打印.去厕所有运行就不正常了.

#### [x]项目创建&基本代码
![done 17-04-02 15:42](https://img.shields.io/badge/完成-17/04/02 15:42-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     疑问：没有苹果证书,越狱版app能正常运行高德地图吗？
答：通过系统自带的demo app可以看到获取手机地图位置正常. 可能是使用手机默认的地图

#### [ ]app logo设计
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
     psd设计大小720x720.前景用字母T(字幕可以用横排time 竖排log组成),背景用公司logo黑白色(点图或者字符填充); 注意:设计144x144px为主要参考效果. 我没说的都你决定 。我没说的都绝决定。it行业 app,主题  时间日志. 用处是个人使用,生活工具；
 我原来的logo 是有眼睛牙齿和耳朵， 需要你用点的方式吧轮廓做出来，点可以通过颜色深浅表示， 你可以尝试用 "通道"

##### [ ]logo带草稿的详细需求
![done 17-04-02 14:23](https://img.shields.io/badge/完成-17/04/02 14:23-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      效果图参考 ![](http://cdn.d7game.com/markdown/2017-04-02_logo.png)

##### [ ]设计 找人&沟通
![done 17-04-02 17:02](https://img.shields.io/badge/完成-17/04/02 17:02-orange.svg) ![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      qq:2336047565 50

##### [ ]美术设计&沟通
![critical ](https://img.shields.io/badge/-紧重-yellowgreen.svg) 
      初版完全不能用. 沟通两次后还是不行.

