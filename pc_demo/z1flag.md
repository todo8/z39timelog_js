
# 标签说明
#### (^\w{1,15})\s --> "$1: "正则替换md内容,修改成为 yml格式
#### (\w{2,4}), --> $1#

##### 系统 
critical: 紧重# 重要紧急
high: 重要# 重要不紧急
low: 紧急# 紧急不重要
minor: 杂事# 不重要不紧急
today: 今天
title: 标题

new: 突发# 突发事件,不在今日计划中的

#### 任务类型
task: 任务  
p: 项目# project的缩写.
note: 描述# 没有使用,后期弃用.
desc: 描述# 系统中的字段名,添加描述无需额外标签,写在任务下面即可.
reference: 参考 #参考资料,在书籍和论文中常见. 说明信息来源.

#### 多人协同
to: 负责人# 任务分配归属人
by: 作者# 任务创建人,或tlog作者.

#### 任务状态
created: 创建
due: 排期# 如2017-07-12 设置日期后,任务显示在这一天. 疑问:时间段时如何处理? 多日用逗号隔开. 如2017-07-12,2017-07-13. 时间段用空格或"_" . 如2017-07-12 2017-07-23
started: 开始
toggle: 暂停
cancelled: 取消
done: 完成
used: 耗时
need: 预计# 需要多少时间(30m)
add: 添加# 其他任务考虑遗漏的衍生任务.

##### 自定义
summary: 总结#   =none 没有条件;  see 现象 unknow 出现答案也不认识; grow 成长
life: 生活
think: 思考# 疑问思考,待解决.
active: 活动# 促销活动相关想法
save: 收藏#   website 网站 down 下载链接
first: 第一次#   
tech: 技术
talk: 话题# 自我录制视频内容主题;
feel: 感受#   perplex 困扰
doubt: 疑惑# -不清楚,且观点做法不认同不一致.
keyword: 关键词
book:  书#  none 没有的书,想法或想写.当下是独特的思路观点;
discuss: 讨论# 请教；
change: 改变# 突破
wanted: 想要# 的,待购事项
habit: 习惯# 
tool: 工具#   =think想法待完成
repet: 重复
none: 没有# 不具有的东西;
rule: 规则# 定律
ask: 问题#   me：自问
mission: 使命#  17-06-24 15:35
way: 方法#  17-06-24 15:35
admire: 钦佩# 牛逼
vision: 愿景
solution: 方案# 解决方法
why: 为什么# (做一件事情的原因)
better: 更好#  重复行为的改进方法.
bad: 反面案例# 需要避免的.
idea: 想法# 主意、想法、创意
case: 案例#  think思维
classic: 经典# 名句
waste: 浪费
sleepy: 犯困
view: 观点# 看法、视野、
outfocus: 走神# 出小差
exp: 经验
family: 家庭#  现在还没有.
confident: 自信
exercise: 练习
happy: 快乐
selfDiscipline: 自律
thanks: 谢谢
relax: 放松

mind: 思维文字# 
mindmap: 思维导图# 

dev: 开发
design: 产品构思# 策划,产品经理
art: 美术
test: 测试
service: 客服
operate: 运营
business: 商务
product: 产品
bug: 缺陷bug# IT项目的开发中的缺陷
other: 其他# 所有项目公用标签. 可作为无分类默认标签.

question: 问题# (突发遇见的)
future: 未来# 对未来的看法和展望
remind: 提醒# 他人或客户的时间 (17-04-07 16:16) 
dont: 不希望# 做的事情.感到高兴.
unset: 收纳箱# 未分配任务.

merit: 功德
error: 过错# 犯错 again重复 ~~mistake~~ 
 
record: 记录# 音频笔记.

#### 生活
wakeup: 醒
getup: 起床
wash1 : 洗漱1
bath: 洗澡
wc: 厕所 #lv1:固定10点, lv2:固定中午 lv3:4张纸逐渐1张 lv4:洗漱+厕所同时
breakfast: 早饭
tape: 录音
study: 学习 #听音频
gowork: 去上班
workStart: 上班 #(工作开始)
workReady: 工作准备 #启动开机,打开必用软件,泡茶,关闭广告弹窗.
health: 腹肌撕裂者T4
lunch: 午饭
cartoon: 卡通
dinner: 晚饭
workEnd: 下班 #(工作结束)
gohome: 回家
wash2: 洗漱2
sleep: 睡觉

#### 今日列表
member: 成员
flag: 标签
repeat: 重复
project: 项目

#### 周列表
monday: 周一
tuesday: 周二
wednesday: 周三
thursday: 周四
friday: 周五
saturday: 周六
sunday: 周日

#### 月列表 第1 2 3 4周 , 
weekTh1: 第一周
weekTh2: 第二周
weekTh3: 第三周
weekTh4: 第四周
weekTh5: 第五周

#### 年列表
monthTh1: 一月
monthTh2: 二月
monthTh3: 三月
monthTh4: 四月
monthTh5: 五月
monthTh6: 六月
monthTh7: 七月
monthTh8: 八月
monthTh9: 九月
monthTh10: 十月
monthTh11: 11月
monthTh12: 12月

#### 年列表

##### 注意: @question 不添加@符号才能被软件识别为tab自动不全提示.


